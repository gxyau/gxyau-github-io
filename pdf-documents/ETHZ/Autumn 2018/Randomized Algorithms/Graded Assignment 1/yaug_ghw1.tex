\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Randomized Algorithms - Graded Homework 1}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Randomized Algorithms - Graded Homework 1}
\author{Guo Xian Yau}
\date{\today}

\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{prop}{Proposition}

\begin{document}

\maketitle

\begin{center}
	\textit{\centering Collaborator(s): Konstantinos Andrikopoukos}
\end{center}

\paragraph{Problem 1} Let $R_1,\cdots,R_m$ be $m$ independent rolls of a fair die, and let $X$ counts the number of consecutive 6's in the $m$ rolls, i.e.
\[
	X = |\set{i \in [m-1]: r_i = r_{i+1} = 6}|
\]
Let $r_i$ be a realisation of the $i^{th}$ roll, $R_i$. We only care about two events, namely $r_i = 6$, and $r_i \ne 6$; thus we can view $R_1,\cdots,R_m \overset{i.i.d.}{\sim} Ber(\frac{1}{6})$. Then $\bE[R_i] = \frac{1}{6}$ and $\var[R_i] = \frac{5}{36}$.\\

Now for $i \in [m-1]$, let $X_i$ be the indicator function of the event $r_i = r_{i+1} = 6$. By linearity of expectation, $\bE[X] = \sum_{i=1}^{m-1} \bE[X_i]$. Fix an arbitrary $i \in [m-1]$, then we have
\[
	\bE[X_i] = \bP(X_i = 1) = \bP(r_i = r_{i+1} = 6) = \bP(r_i = 6) \cdot \bP(r_{i+1} = 6) = \frac{1}{36}
\]
since each roll is independent. Therefore $\mu := \bE[X] = \frac{m-1}{36}$.\\

Now consider $\bP(X_i = X_j = 1| j \ne i)$, there are two cases:
\begin{itemize}
	\item $j = i+1$, then $\bP(X_i = X_j = 1|j = i+1) = \bP(r_i = r_{i+1} = r_{i+2} = 6) = \frac{1}{6^3}$
	
	\item $|j - i| \ge 2$, then $\bP(X_i = X_j = 1 \big| |j - i| \ge 2) = \frac{1}{6^4} = \bP(X_i = 1) \bP(X_j = 1)$
\end{itemize}
Thus $X_i,X_j$ are independent if $|i - j| \ge 2$. Thus we may conclude that $\bE[X_i X_j] = \bE[X_i] \bE[X_j]$, and that $\cov(X_i,X_j) = 0$ whenever $|i - j| \ge 2$. If $j = i+1$, then 
\[
	\bE[X_i X_j] = \bP(j = i+1) \cdot \bP(X_i = X_j = 1| j = i + 1) = \frac{m-2}{{m-1 \choose 2}} \cdot \frac{1}{6^3} = \frac{2}{6^3 (m-1)}
\]
If $j \ne i$, we have the following covariance formula
\begin{align*}
	\cov(X_i,X_j) = & \bE[X_i X_j] - \bE[X_i] \bE[X_j]\\
	= & \begin{cases}
		\frac{2}{6^3 (m-1)} & j = i+1\\
		0 & |i-j| \ge 2
	\end{cases}
\end{align*}
Now we can compute $\var[X]$. We have
\begin{align*}
	\var[X] = & \sum_{i=1}^{m-1} \var[X_i] + \sum_{i \ne j} \cov(X_i,X_j)\\
	= & \sum_{i=1}^{m-1} \var[X_i] + \sum_{i=1}^{m-1} \cov(X_i,X_{i+1})\\
	= & {m - 1 \choose 1} \left[ \frac{35}{6^4} + \frac{2}{6^3 (m-1)} \right]\\
	= & 2 \cdot \frac{m-1}{36} \cdot \left[ \frac{35}{2 \cdot 36} + \frac{1}{6(m-1)}\right]\\
	= & 2 \mu \cdot \underbrace{\frac{35m - 23}{72(m-1)}}_{\le 1} \le 2 \mu
\end{align*}
Therefore $\var(X) \le 2 \mu$ as intended.\\

Using Chebychev's inequality, we have
\[
	\bP [ |X - \mu| \ge k] \le \frac{\var(X)}{k^2}
\]
Let $k = \frac{\mu}{2}$, then $\frac{\var(X)}{k^2} = \frac{4}{\mu^2} \cdot \var(X) \le \frac{4}{\mu^2} \cdot 2 \mu = \frac{8}{\mu}$ as required. \qed

\paragraph{Problem 2} Let $Q_n = \set{0,1}^n$ denote the $n$-dimensional hypercube, $n \in \bN$. Let $\ee > 0$ such that $\ee \in (0,1)$, and $m = \exp(\frac{\ee^2 n}{6})$. Let $u,v \in V(Q_n)$ be arbitrary, we define $X^{uv}$ to be the Hemming distance between $u$ and $v$, and
\[
	X_i^{uv} = |u(i) - v(i)| ~ ~ ~ ~ ~ i \in [n]
\]
where $u(i)$ and $v(i)$ denotes the $i^{th}$ coordinate of $u$ and of $v$ respectively. It is easy to see that $X = \sum_{i=1}^{n} X_i^{uv}$. Moreover, $X_i^{uv} \in \set{0,1}$ for all $i \in [n]$; it is equal to 1 if and only if the $i^{th}$ coordinate of $u$ and of $v$ are different, and it is 0 otherwise.\\

Now since we are choosing $u,v \in V(Q_n)$ at random, hence the indicator variable
\[
	X_i^{uv} \overset{i.i.d.}{\sim} Ber(\frac{1}{2}), ~ ~ ~ ~ ~ i \in [n]
\]
for any pair $u,v \in V(Q_n)$. Thus $\bE[X_i^{uv}] = \frac{1}{2}$, and $\var[X_i^{uv}] = \frac{1}{4}$. By linearity of expectation and that $X_i$'s are i.i.d., we get that $\bE[X^{uv}] = \frac{n}{2}$ and $\var[X^{uv}] = \frac{n}{4}$.\\

Now the conditions for Chernoff bound is satisfied, we get
\[
	\bP[X^{uv} < (1-\ee) \mu] \le \bP[X^{uv} \le (1 - \ee) \mu] = \bP[X^{uv} \le (1-\ee) \frac{n}{2}] \le \exp(-\frac{n \ee^2}{4}) \overset{(*)}{\le} \exp(-\frac{n \ee^2}{6})
\]
for any pair $u,v \in V(Q_n)$. In fact, we know that $\frac{1}{4} \ge \frac{1}{6}$, so multiplying both sides by $- n \ee^2$, we get $- \frac{n \ee^2}{4} \le - \frac{n\ee^2}{6}$. It follows the inequality $(*)$ holds since $\exp(\cdot)$ is strictly increasing function.

\begin{lemma}
	For any $l < m$, such that whenever there is a set $W \subseteq V(Q_n)$ with $|W| = l$ and the pairwise distance of vertices in $W$ are at least $(1-\ee)\frac{n}{2}$, there exists a vertex $v_0 \in V(Q_n)$ such that $\min_{w \in W} d(v_0,w) > (1-\ee)\frac{n}{2}$.
\end{lemma}
\begin{proof}
	Let $W \subseteq V(Q_n)$ be $|W| = l$, $l \in \bN$ and pairwise distance of any vertices in $W$ are at least $(1-\ee)\frac{n}{2}$. Then by using argument before the lemma, we have
	\[
		\bP(X^{v_0w} \ge (1-\ee)\frac{n}{2}) = 1 - \bP(X^{v_0w} < (1-\ee) \frac{n}{2}) \ge 1 - \exp(- \frac{n \ee^2}{6}) > 0
	\]
	For any vertex $w \in W$ and some $v_0 \in V(Q_n) \sm W$. In particular, let $W = \set{w_i}_{i=1}^{l}$, using union bound at $(UB)$, let $v_0 \in V(Q_n) \sm W$, we get
	\begin{gather*}
		\bP(X^{v_0w} \ge (1-\ee)\frac{n}{2}, ~ \forall w \in W)\\
		= 1 - \bP(\exists w \in W : X^{v_0 w} < (1-\ee) \frac{n}{2})\\
		\overset{(UB)}{\ge} 1 - \sum_{w \in W} \bP(X^{v_0w} < (1-\ee) \frac{n}{2})\\
		\ge 1 - \frac{l}{m} > 0 ~ ~ ~ (\because l < m, \therefore \frac{l}{m} < 1)
	\end{gather*}
	Now since the probability is positive that such vertex $v_0$ exists, therefore we are done.
\end{proof}

We will now construct a set $W \subseteq V(Q_n)$ with $|W| = m$ such that vertices in $W$ has pairwise distance at least $(1-\ee)\frac{n}{2}$. Let $W' \subseteq V(Q_n)$ be such that $|W'| = l < m$. We will prove this by induction on $l$ up to $m-1$. Clearly $l \ge 2$ for otherwise we only have one vertex and and there is nothing to compare. For $l = 2$, since $2 < m$, since expected distance of any two vertex is $(1-\ee)\frac{n}{2}$ by Chernoff bound, therefore there exists such pair, and we will take this as initial set. Now for $2 < l < m-1$, we apply the lemma iteratively, and adding $v_0$ that we obtained from the lemma into $W'$. For $l = m-1$, let $v_0$ be the vertex obtained from the lemma. Then setting $W := W' \cup \set{v_0}$, we have what we want; by the lemma each pair of vertices in $W$ has distance at least $(1-\ee)\frac{n}{2}$; it has size $m$ because when we add a vertex to $W'$ when $l = m-2$, we obtain a set $W'$ of size $m-1$, and together with $v_0$, we have $W = |W'| + 1 = m - 1 + 1 = m$ as we desired. \qed

\paragraph{Problem 3} Let $T_r = (V,E,r)$ be a rooted tree (at $r \in V$) with $|V| = n$. Let $d_v$ denote the depth of $v$ in $T_r$. The process in the question dictates that we choose $v \in V$ uniformly at random, and delete $v$ (together with its entire branch) until $T$ is empty.

\begin{lemma}
	Given $T_r$, the process described in the question is equivalent to choosing a random ordering $v_1,\cdots,v_n$ of the vertex set $V$ and deleting vertex (when possible) together with its entire branch in the ordering $v_1,\cdots,v_n$.
\end{lemma}
\begin{proof}
	Given the process in the question, let $w_1,\cdots,w_l$ be the vertices that are chosen in the process in that order, with the last vertex being $w_l = r$.  Then we can produce an ordering of $V$ by taking $v_1 = w_1$, and adding the vertices in the next depth level from left to right, and continue this process until every vertex in this branch is added to the ordering. Then we put $w_2$ as the next vertex in the ordering, and continue until we have $w_l = r$ added. Note that this process terminates because the vertex set $|V| = n$ is finite.\\
	
	Now given a random ordering, we can delete the vertices (together with their branch) in order. If a vertex $u$ is in a branch that was already deleted, then we just ignore and move on to the next vertex. This will create an empty tree when we have processed every vertex in the given ordering. Let $w_1,\cdots,w_l$ be the labelled vertices, such that they are deleted in this order. It remains to show that $r$ will be the last vertex deleted, i.e. $w_l = r$.\\
	
	If $w_n = r$, then there is no vertices later being deleted after $r$, which proves that $r$ is the last vertex deleted. If $r = w_k$ for some $k < l$, then there is a vertex $v := w_{k+1}$ such that it is deleted after $r$ is deleted. But this is a contradiction since we delete the entire branch of $r$, and $v$ is in the branch of $r$. Thus $r = w_l$, and deleting $r$ yields an empty tree.
\end{proof}

Using the ordering, we will show that the probability of deleting $v \in V$ is indeed $\frac{1}{1+d_v}$. First, we will prove a lemma.

\begin{lemma}
	Given an arbitrary ordering of the vertices, $v \in V$ will be deleted (together with its branch) if and only if all ancestors of $v$ has an order larger than order of $v$.
\end{lemma}
\begin{proof}
	Suppose in the ordering $v_1,\cdots,v_n$, $v = v_k$ gets deleted. Then all its ancestors would be present at the time we delete $v$. This proves that ancestors of $v$ has larger ordering than $v$. Conversely if all ancestors of $v$ has a larger ordering than $v$, then we will not encounter any of $v$'s ancestors before $v$ when we delete the branches. This proves that $v$ exists by the time we reach $k^{th}$ step in the process, and hence $v = v_k$ gets deleted.
\end{proof}

Now let $v \in V$ be arbitrary. Then
\begin{align*}
	\bP(\text{$v$ gets deleted in the process}) = & \bP(\text{$v$ is chosen before its ancestors})\\
	= &\bP(\text{1 + Number of ancestors of $v$}) \\
	= & \frac{1}{1 + \text{Depth of $v$}} = \frac{1}{1 + d_v}
\end{align*}
Finally, let us define $X$ to be the random variable that counts the number of steps until $T$ is empty. We further define for each $v \in V$
\[
	X_v = \i1(\text{$v$ gets chosen in the process})
\]
Therefore $X = \sum_{v \in V} X_v$. Hence 
\[
	\bE[X] = \sum_{v \in V} \bE[X_v] = \sum_{v \in V} \bP(\text{$v$ gets chosen in the process}) = \sum_{v \in V} \frac{1}{d_v}
\]
as we desired. \qed\\

\textbf{Complain: It is confusing to use the notation $d_v$ as the depth. It usually mean the degree of $v$. I find myself starting to count the degrees of each vertex several times while trying to solve the question even though I read the question thoroughly.}

\paragraph{Problem 4} Let $n \in \bN$ be arbitrary, and $p \in (0,1)$. Before attempting to find the threshold $p_0$, let us prove a lemma from number theory.

\begin{lemma}
	Given $b,c \in \bZ/n\bZ$, set of integers modulo $n$, there is a solution $a$ to the equation
	\[
		a + b + c \equiv 0 \mod n
	\]
	Moreover, $a \in \bZ/n\bZ$ is the unique solution to the above equation.
\end{lemma}
\begin{proof}
	Without loss of generality, we may assume $0 \le b,c \le n-1$. Clearly there is a solution to the above equation, since we can take $a = n - b - c$ if $b+c \le n$, and $a = b + c - n$ if $b + c > n$. Thus $a = |n - b - c|$ is a solution. To prove uniqueness, suppose $0 \le a_1,a_2 \le n-1$ are two solutions to the above mentioned equation. Then we have
	\begin{itemize}
		\item $a_1 + b + c \equiv 0 \mod n$
		
		\item $a_2 + b + c \equiv 0 \mod n$.
	\end{itemize}
	Subtracting the bottom equation from the top, we get that $(a_1 - a_2) + b + c \equiv 0 \mod n$, i.e. $a_1 - a_2$ is again a solution to the equation. Therefore $a_1 - a_2 = kn$ for some $k \in \bZ$. Therefore $a_1 = a_2 + kn$; this is a contradiction since we require $0 \le a_1,a_2 \le n-1$, unless $k = 0$, in which case $a_1 = a_2$, thus proving the solution to the equation is unique.
\end{proof}

Now let $a,b,c \in [n]$ be arbitrarily chosen. We now claim that it suffices to prove for $n$ being prime number. If we have proven for $n$ being prime, then for the composite case, let $n = kl$, and without loss of generality, let's just assume $k,l$ are also primes, and that $k \le l$.  Let $p_k,p_l$ be the weak threshold for the case $n - k$ and $n - l$ respectively. Let $p' := \max \set{p_k,p_l}$, then
\[
	\lim_{n \to \infty} \bP(\text{$[k]_p$ contains a zero sum set or $[l]_p$ contains a zero sum set}) = \begin{cases}
		1 & p \gg p'\\
		0 & p \ll p'
	\end{cases}
\]
In fact, since we have $k$ copies of subset of size $l$, i.e. 
\[
	[1,\cdots,l],[l+1,\cdots,2l],\cdots,[(k-1)l + 1, \cdots, kl]
\]
or conversely $l$ copies of subset of size $k$, we can do this on each subset, and the result holds.\\

It remains to show the reduced case when $n$ is prime. Let $\cS := \set{S \subseteq [n]: |S| = 3}$. Let $X(\cS)$ be the random variable that counts the number of zero sum triplets in $[n]_p$. For $S \in \cS$, let
\[
	X_S = \begin{cases}
		1 & \text{$S$ is a zero sum set and $S \subseteq [n]_p$}\\
		0 & \text{Otherwise}
	\end{cases}
\]
Then $X(\cS) = \sum_{S \in \cS} X_S$.\\

Note that the probability of any three elements are in $[n]_p$ is given by $p^3$ since each element is chosen independently. Therefore
\[
	\bE[X_S] = \begin{cases}
		p^3 & \text{$S \in \cS$ is a zero sum triplet in $[n]_p$}\\
		0 & \text{$S$ is not a zero sum triplet of $[n]$ or $S \nsubseteq [n]_p$}
	\end{cases}
\]
Moreover, there are in total $\frac{n(n-1)}{6}$ zero sum triplet in $[n]$; we can choose the first and second element, hence we have $n (n-1)$, but once the first two of the three is chosen, by lemma above, there is one element from the remaining $n-2$ elements of $[n]$ that satisfy the equation. On the other hand, the order at which we choose the triplet is indifferent, therefore we need to divide by $3! = 6$.\\

Thus to evaluate $\bE[X]$, we compute the number of zero sum triplet of $[n]$ that is in $[n]_p$.
\[
	\bE[X] = \sum_{S \in \cS} \bE[X_S] = \frac{n(n-1)}{6} p^3 = o(n^2 p^3)
\]
By choosing $p = n^{-\frac{2}{3}}$, we have that $\bE[X(\cS)] = o(1)$. By first moment method, we have that $\bP(X(\cS) = 0)  = 1 - o(1)$. Thus we can choose $p_0(n) = n^{-\frac{2}{3}}$. \qed

\end{document}