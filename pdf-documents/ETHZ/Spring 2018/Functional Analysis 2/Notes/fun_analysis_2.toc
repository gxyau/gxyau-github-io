\contentsline {section}{\numberline {1}Overview}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Heuristics}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Uniqueness}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}First Derivative Test}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Boundary Problem \& Variational Problem}{2}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Existence \& Regularity}{3}{subsection.1.5}
\contentsline {section}{\numberline {2}Boundary Value Problem}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Weak Solutions of Dirichlet Equations}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}First Notion of Weak Solution (Riesz)}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Second Notion of Weak Solution (Closed Range Theorem)}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Distributional Derivatives}{6}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Basic Functional Properties}{9}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Extension Space}{13}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Approximation and 1-Dimensional Sobolev Spaces}{14}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}ODE's With Dirichtlet/Neumann Boundary Conditions}{17}{subsection.2.8}
\contentsline {subsubsection}{\numberline {2.8.1}Dirichlet Problem}{17}{subsubsection.2.8.1}
\contentsline {subsubsection}{\numberline {2.8.2}Neumann Problem}{17}{subsubsection.2.8.2}
\contentsline {subsubsection}{\numberline {2.8.3}Variational Problem}{18}{subsubsection.2.8.3}
\contentsline {subsubsection}{\numberline {2.8.4}Neumann Problem vs. Variational Problem}{19}{subsubsection.2.8.4}
\contentsline {section}{\numberline {3}Sobolev Spaces for $\Omega \subseteq {\mathbb {R}} ^n$}{20}{section.3}
\contentsline {subsection}{\numberline {3.1}Approximation of Sobolev Functions}{22}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Calculus in Sobolev Spaces}{23}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Extension of $W^{1,p}$ Functions}{27}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Local Extension}{27}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Global Extension}{28}{subsubsection.3.3.2}
\contentsline {subsection}{\numberline {3.4}Traces of Sobolev Functions}{30}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Sobolev Embedding Theorems}{33}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Sobolev Inequality}{33}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}Sobolev Embedding Theorem, $p \le n$}{36}{subsubsection.3.6.1}
\contentsline {subsubsection}{\numberline {3.6.2}Sobolev Embedding Theorem, $p > n$}{39}{subsubsection.3.6.2}
\contentsline {subsection}{\numberline {3.7}Higher Sobolev Embeddings \& Weak Solutions}{43}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}Regularity \& Weak Solutions}{43}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}Weak Formulation}{43}{subsubsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.3}Gradient Growth of $C^{0,\alpha }$ Functions}{44}{subsubsection.3.7.3}
\contentsline {subsubsection}{\numberline {3.7.4}Classic Differentiability}{45}{subsubsection.3.7.4}
\contentsline {subsection}{\numberline {3.8}Summary}{45}{subsection.3.8}
\contentsline {section}{\numberline {4}Elliptic Regularity}{46}{section.4}
\contentsline {subsection}{\numberline {4.1}Interior Regularity for Elliptic Problems}{46}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Preliminary}{47}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}A Priori Estimates}{47}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Higher Elliptic Estimates}{50}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Elliptic Operators in Divergence Form}{52}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Boundary and Global Regularity}{53}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Global Regularity for Weak Solutions of Elliptic PDE's}{57}{subsection.4.7}
\contentsline {subsection}{\numberline {4.8}$H^2$-Estimates and Higher}{59}{subsection.4.8}
\contentsline {subsection}{\numberline {4.9}Spectral Theory for Laplace Operator}{60}{subsection.4.9}
\contentsline {section}{\numberline {5}Schauder Theory}{64}{section.5}
\contentsline {subsection}{\numberline {5.1}How to proceed?}{64}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}General Case}{65}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Schauder Estimates Part I - Local Schauder Estimate}{70}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Schauder Estimates Part II - Global Schauder Estimate}{74}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Existence Result for Elliptic Boundary Value Problems}{76}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Maximal Principle for Elliptic Operators}{79}{subsection.5.6}
\contentsline {subsubsection}{\numberline {5.6.1}Inhomogeneous Case}{80}{subsubsection.5.6.1}
\contentsline {subsection}{\numberline {5.7}Strong Maximal Principle}{82}{subsection.5.7}
\contentsline {subsection}{\numberline {5.8}Method of Sub-Super Solution}{84}{subsection.5.8}
\contentsline {section}{\numberline {6}Tutorial}{84}{section.6}
\contentsline {subsection}{\numberline {6.1}February $19^{th}$, 2018}{84}{subsection.6.1}
\contentsline {paragraph}{Problem 1 (Weak Topology)}{84}{section*.28}
\contentsline {paragraph}{Solution:}{85}{section*.29}
\contentsline {paragraph}{Question 2 (Direct Method)}{85}{section*.30}
\contentsline {paragraph}{Solution:}{85}{section*.31}
\contentsline {paragraph}{Problem 3 (Adjoint Operator \& Spectral Theorem)}{85}{section*.32}
\contentsline {paragraph}{Solution:}{85}{section*.33}
\contentsline {subsection}{\numberline {6.2}February $26^{th}$, 2018}{86}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}March $5^{th}$, 2018}{87}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}March $12^{th}$, 2018}{88}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}March $19^{th}$, 2018}{89}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}March $26^{th}$, 2018}{91}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}April $9^{th}$, 2018}{91}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}April $16^{th}$, 2018}{92}{subsection.6.8}
\contentsline {subsection}{\numberline {6.9}April $23^{rd}$, 2018}{94}{subsection.6.9}
\contentsline {subsection}{\numberline {6.10}April $30^{th}$, 2018}{94}{subsection.6.10}
\contentsline {subsection}{\numberline {6.11}May $7^{th}$, 2018}{96}{subsection.6.11}
\contentsline {subsection}{\numberline {6.12}May $14^{th}$, 2018}{97}{subsection.6.12}
\contentsline {subsection}{\numberline {6.13}May $28^{th}$, 2018}{99}{subsection.6.13}
