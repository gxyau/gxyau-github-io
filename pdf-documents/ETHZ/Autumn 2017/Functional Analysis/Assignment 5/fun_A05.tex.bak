\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 5}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 5}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

As usual, we will prove several lemmas before continuing with the assignments.

\begin{lemma}
	Every finite dimensional normed space is closed.
\end{lemma}
\begin{proof}
	Let $(X,\norm{\cdot})$ be a finite dimensional normed space. From class, we know that it is linearly isomorphic to $\bR^n$ for some finite $n \in \bN$, so without loss of generality we may assume $X = \bR^n$. Let $(x_n)_n$ be a converging sequence in $X$, say to $x$. For $\ee > 0$, choose $N \in \bN$ such that $\norm{x_n - x} < \frac{\ee}{2}$ for all $n > N$. Then $\norm{x_n - x_m} \le \norm{x_n - x} + \norm{x - x_m} < \ee$, so $(x_n)_n$ is Cauchy, by completeness, $x \in X$, and hence $X$ is closed.
\end{proof}

\begin{lemma}
	Let $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ be normed spaces. Suppose $T \in L(X,Y)$ such that $\norm{T}_{op} < \infty$, then for each $x \in X$, $\norm{Tx}_{Y} \le \norm{T}_{op} \norm{x}_{X}$.
\end{lemma}
\begin{proof}
	Let $x \in X$ be arbitrary. Now $\norm{Tx}_{X} = \norm{x} \cdot \norm{T \frac{x}{\norm{x}}} \le \norm{T}_{op} \norm{x}_{X}$.
\end{proof}

\begin{lemma}
	Let $(X,\norm{\cdot}_X)$, $(Y,\norm{\cdot}_Y)$ be a normed spaces. Consider $(X \times Y,\norm{\cdot}_{X \times Y})$, with $\norm{(x,y)}_{X \times Y} = \norm{x}_X + \norm{y}_{Y}$. Then $\norm{x}_X,\norm{y}_Y \le \norm{(x,y)}_{X \times Y}$.W
\end{lemma}
\begin{proof}
	Since $\norm{\cdot}_X, \norm{\cdot}_Y \ge 0$ for all $x \in X$ and for all $y \in Y$, so we have the inequality $\norm{x}_X \le \norm{x}_X + \norm{y}_Y = \norm{(x,y)}_{X \times Y}$. It is analogous for $\norm{y}_{Y}$.
\end{proof}

\paragraph*{Problem 5.1} Let $T:X \to Y$ be a linear operator, where $X,Y$ are normed spaces. Then we define the operator norm to be $\norm{T}_{op} = \sup_{\norm{x}_{X} = 1} \norm{Tx}_{Y}$.
\begin{enumerate}[(a)]
	\item Let $A \in L(\bR^n,\bR^n)$, and $\lambda_1,\cdots,\lambda_n$ be eigenvalues of $A^T A$, in decreasing order, i.e. the order is $\lambda_1 \lg \cdots \ge \lambda_n$. Then $\lambda_{\max} = \lambda_1$.\\
	
	Now since $A^T A$ is symmetric, in particular, it is Hermitian in real inner product space", so we can find an orthonormal eigenbasis $B := \{e_i\}_{i=1}^{n}$ with respect to $\inp{\cdot}{\cdot}$, where $e_i$ is an eigenvector with corresponding eigenvalue $\lambda_i$. Let $x \in \bR^n$ be arbitrary, and write $x = \sum_{i=1}^{n} a_i e_i$. Consider $\norm{Ax}^2 = \inp{Ax}{Ax}$, we have
	\begin{align*}
		\norm{Ax}^2 = \inp{Ax}{Ax} = & \inp{A^*A x}{x} = \sum_{i,j = 1}^{n} \inp{A^T A a_i e_i}{a_j e_j}\\
		= & \sum_{i,j = 1}^{n} |\lambda_i| |a_i| |a_j| \dd_{ij} = \sum_{i=1}^{n} |\lambda_i| (a_i)^2\\
		\le & |\lambda_1| \sum_{i=1}^{n} (a_i)^2 = |\lambda_1| \norm{x}
	\end{align*}
	Where $\dd_{ij}$ is the Kronecker delta. So $\norm{A}_{op}^2 = \sup_{\norm{x} = 1} \norm{Ax} \le \sup_{\norm{x} = 1} |\lambda_1| \norm{x} = |\lambda_1|$.\\
	
	On the other hand, $\norm{e_1} = 1$. Thus $\norm{A e_i} \le \norm{A}_{op}^2$. This means that
	\[
		|\lambda_1| = \norm{A e_1}^2 \le \norm{A}_{op}^2
	\]
	Therefore $\norm{A}_{op}^2 = |\lambda_1|$. \qed
	
	\item Suppose $A,B$ are symmetric linear operators as defined in the question. Note that $Ax \in \bR^{2017}$, and then we have
	\begin{align*}
		\norm{BA}_{op} = & \sup_{\norm{x} = 1} \norm{(BA) x} = \sup_{\norm{x} = 1} \norm{B (Ax)} \le \sup_{\norm{x} = 1} \norm{B}_{op} \norm{A x}\\
		= & \norm{B}_{op} \cdot \sup_{\norm{x} = 1} \norm{A x} = \norm{B}_{op} \cdot \norm{A}_{op}
	\end{align*}
	Where the inequality is justified by lemma 2. By part (a), $\norm{A}_{op},\norm{B}_{op} = \sqrt{2017^2} = 2017$ since $A^T$ and $A$ shares the same eigenvalues. Thus
	\[
		\norm{BA}_{op} \le \norm{B}_{op} \cdot \norm{A}_{op} = 2017^2 < (2100)^2 = 4~ 410 ~ 000
	\]
	And we are done. \qed
\end{enumerate}

\paragraph*{Problem 5.2} (Volterra Equation) Let $k: [0,1]^2 \to \bR$ be continuous. Let $X = C^0([0,1])$, and $V:X \to X$, $(Vf)(t) = \int_0^t k(t,s) f(s) ~ ds$. We claim that if the spectral radius $r(V) < 1$, we can solve the problem by defining
\[
	g := \left[ \sum_{n \in \bN} (-V)^n \right]^{-1} f ~ ~ ~ \forall t \in [0,1]
\]
Indeed, since $r(V) < 1$, so $r(-V) < 1$, and we get 
\[
	g (t) =\left[ \left((\idf - (-V))^{-1}\right)^{-1} f \right] (t) = f(t) + (Vf) (t)
\]
which is what we want.\\

So it remains to prove that $r(V) < 1$. Note that the general formula for $\norm{V^n}_{op}$ is the following
\begin{align*}
	\norm{V^n}_{op} = & \sup_{\norm{f} = 1} \norm{\int_0^t k(t,s_n) \int_0^{s_n} k(s_{n}, s_{n-1})\cdots \int_{0}^{s_2} k(s_2,s_1) f(s_1) ~ d s_1 \cdots d s_n}\\
	\le & \norm{\int_0^t k(t,s_n) \int_0^{s_n} k(s_{n}, s_{n-1})\cdots \int_{0}^{s_2} k(s_2,s_1)~ d s_1 \cdots d s_n}\\
	\le & \norm{k}_{op} \cdot \norm{\int_0^t k(t,s_n) \int_0^{s_n} k(s_{n}, s_{n-1})\cdots \int_{0}^{s_2} 1 ~ d s_1 \cdots d s_n}\\
	= & \norm{k}_{op} \cdot \norm{\int_0^t k(t,s_n) \int_0^{s_n} k(s_{n}, s_{n-1})\cdots \int_{0}^{s_3} k(s_3,s_2) s_2 ~ ds_2 \cdots d s_n}\\
	\le & \norm{k}_{op}^{2} \cdot \norm{\int_0^t k(t,s_n) \int_0^{s_n} k(s_{n}, s_{n-1})\cdots \int_{0}^{s_4} \frac{s_3^2}{2!} ~ ds_2 \cdots d s_n}\\
	\le \cdots \le \frac{t^n}{n!} \cdot \norm{k}_{op}^n
\end{align*}
So $\norm{V^n f} \le \frac{\norm{k}^n}{n!} \to 0$ as $n \to \infty$. Thus $r(V) = 0 < 1$, and we are done. \qed

\paragraph*{Problem 5.3} (Right Shift Operator) Let $S: \ell^2 \to \ell^2$, $(x_1,x_2,\cdots) \mapsto (0,x_1,x_2,\cdots)$ be the right shift operator. For simplicity assume $\norm{\cdot}$ is the norm on $\ell^2$.
\begin{enumerate}[(a)]
	\item To prove that $S$ is continuous linear operator with $\norm{S}_{op} = 1$, we claim that it suffices to show the latter since by lemma 2, $\norm{Sx - Sy} \le \norm{S}_{op} \norm{x-y}$ is 1-Lipschitz and hence continuous, where $x,y \in \ell^2$ are arbitrary.\\
	
	Let $x = (x_n)_n \in \ell^2$ be arbitrary and suppose $x' := Sx = (x_n')_n$. Then we have
	\[
		\norm{Sx}^2 = 0^2 + \sum_{n \ge 2} |x_n'|^2 = \sum_{n \in \bN} |x_n|^2 = \norm{x}^2
	\]
	So we may conclude $\norm{Sx} = \norm{x}$ since $\norm{\cdot} \ge 0$. So we get 
	\[
		\norm{S}_{op} = \sup_{\norm{x} = 1} \norm{Sx} = \sup_{\norm{x} = 1} \norm{x} = 1
	\]
	And we are done. \qed
	
	\item Let us first compute the spectral radius. First, notice that
	\[
		\norm{S^n}_{op} = \sup_{\norm{x} = 1} \norm{S^n x} = \sup_{\norm{x} = 1} \left[0^2 + \norm{S^{n-1} x} \right] = \norm{S^{n-1}}_{op}
	\]
	Therefore by induction, $\norm{S^n}_{op} = \norm{S}_{op} = 1$. Thus $r(S) = \lim_n (\norm{S^n})^{\frac{1}{n}} = \lim_n 1^{\frac{1}{n}} = 1$.\\
	
	We claim that there are no eigenvalues. Suppose towards a contradiction that there exists $\lambda \in \bR$ is an eigenvalue of $S$, paired with the eigenvector $v = (v_n)_n \in \ell^2$. Then
	\[
		(\lambda I - S) v = 0 \iff (v_1, v_2 - \lambda v_1, v_3 - \lambda v_2,\cdots) = (0,0,\cdots)
	\]
	Since eigenvalues are non-zero values, so $v_1 = 0$. This means that $v_2 - \lambda v_1 = v_2 = 0$. In general $v_n - \lambda v_{n-1} = v_n = 0$ for all $n \in \bN$. Hence $v = (0,0,\cdots)$, contradicting $v$ being non-trivial solution to the characteristic polynomial $(\lambda I - S) v = 0$. Thus there are no eigenvalues of $S$. \qed
	
	\item Let $T: \ell^2 \to \ell^2$, $(x_1,x_2,\cdots) \mapsto (x_2,x_3,\cdots)$ be the left-shift operator. Let $x = (x_n)_n \in \ell^2$ be arbitrary. Then $TS(x) = T((0,x_1,x_2,\cdots)) = x$. Therefore $T \circ S = \id$. However, $ST(x) = S((x_2,x_3,\cdots)) = (0,x_2,x_3,\cdots) \ne x$. Hence $S \circ T \ne \id$.
\end{enumerate}

\paragraph*{Problem 5.4} (Closed Subspaces) Let $(X,\norm{\cdot}_{X})$ be a normed space and $U,V \subseteq X$ be subspaces. Let $Y \subseteq X$ be any subspace. The canonical projection map $\pi_Y : X \to X/Y$ is continuous as it is defined the finest topology in which $\pi_Y$ is continuous.
\begin{enumerate}[(a)]
	\item Consider $\pi_V: X \to X/V$, the canonical projection onto the subspace $V$. So $X/V$ is a normed space with the norm and topology inherited from $X$. Now consider $\pi_V(U)$, this is a finite dimensional subspace of the normed space $X/V$, hence by lemma 1, it is closed. Therefore $\pi^{-1} \circ \pi (U) = U+V$ is closed since $\pi$ is continuous. \qed
	
	\item Consider again the canonical projection $\pi_V$. We know that $V$ has finite codimension, so $\dim (X/V) = n < \infty$. We have $\pi_V(U) \subseteq X/V$, therefore $\dim(\pi_V(U)) \le \dim (X/V) = n$, i.e. $U$ is finite dimensional. By (a), $U + V$ is closed.
\end{enumerate}

\paragraph*{Problem 5.5} (Vanishing Boundary Values) Let $X = C^0([0,1])$, and the subset 
\[
	U := C_0^0([0,1]) = \set{ f \in X \big| f(0) = 0 = f(1)}
\]
\begin{enumerate}[(a)]
	\item We will first prove that $U$ is a subspace. Let $r \in \bR$, $f,g \in U$ be arbitrary. Then
	\begin{itemize}
		\item First, $f + g$ is continuous as a sum of continuous functions. Moreover, 
		\[
			(f+g) (0) = f(0) + g(0) = f(1) + g(1) = 0 + 0 = 0
		\]
		Hence it is closed under addition.
		
		\item We also have $r f(0) = rf(1) = r \cdot 0 = 0$, so $rf \in U$, and it is closed under scalar multiplication.
	\end{itemize}
	Hence $U$ is a subspace of $X$. Now we will proceed to prove that $U$ is closed.\\
	
	Let $g \in X \setminus U$, then either $f(0) \ne 0$ or $f(1) \ne 0$. There are two cases
	\begin{enumerate}[1]
		\item $f(1) = f(0) = a \ne 0$
		
		\item $f(0) \ne f(1)$, and without loss of generality, $f(0) - f(1) > 0$.
	\end{enumerate}
	In case 1, let $g \in B(f,r)$ for some $r > 0$. In particular,
	\[
		|f(0)| - |g(0)| \le |f(0) - g(0)| \le |f(0)| + |g(0)| <  a + |g(0)|
	\]
	So $|g(0)| +  a > a - |g(0)| \implies 2|g(0)| > 0$, thus $|g(0)| > 0$, i.e. $g(0) \ne 0$, and $g \notin U$. Thus $B(f,r) \subseteq X \setminus U$.\\
	
	In case 2, let $r := \frac{1}{3} |f(0) - f(1)| > 0$, and again, let $g \in B(f,r)$. Then we have 
	\[
		3r = |f(0) - f(1)| \le |f(0) - g(0)| + |g(0) - g(1)| + |g(1) - f(1)| < 2r + |g(0) - g(1)|
	\]
	So $|g(0) - g(1)| > r = \frac{1}{3} |f(0) - f(1)| > 0$, i.e. at least one of $g(0)$ or $g(1)$ is non-zero. Hence $g \notin U$, and $B(f,r) \subseteq X \setminus U$. By definition, $X \setminus U$ is open, hence the complement $U$ is closed.
	
	\item Let $f \in X/U$, such that $f \not \equiv 0_{X/U}$. Then at least one of $f(0) \ne 0$ or $f(1) \ne 0$ is true. Let $g_1$ and $g_2$ be defined by $g_1 = \id$ and $g_2 = 1 - \id$ for all $x \in [0,1]$. Then $g_1,g_2 \notin U$, i.e. $g_1,g_2 \in X/U$ such that $g_1,g_2 \not \equiv 0_{X/U}$. In fact, $g_1, g_2$ are the basis of $X/U$, since we can scale them. Therefore $\dim(X/U) = 2$.
\end{enumerate}

\paragraph*{Problem 5.6} (Topological Complement) Let $(X,\norm{\cdot})$ be a Banach space. Let $U \subseteq X$ be a subspace of $X$. 
\begin{enumerate}[(a)]
	\item Suppose $U$ is complemented, then there exists a subspace $V \subseteq X$, such that the linear $L: U \times V$, $(u,v) \mapsto u + v$ is a homeomorphism. Therefore $U \times V \iso X$, and for all $x \in X$, we can write $x = u + v$ where $u \in U$ and $v \in V$. Define a map $\pi_U : X \to X$, $x = u + v \mapsto u$. This map is clearly linear. We claim that $\pi_U$ is continuous.\\
	
	Suppose $x \in X$ has unit length, then $\norm{x}_X = 1$. This means that $\norm{u+v}_X = 1$ for some $u \in U$ and some $v \in V$. But we know $X \iso U \times V$,  by lemma 3, $\norm{u}_X,\norm{v}_X \le \norm{x}_X$. In particular, $\norm{\pi_U x}_X \le \norm{x}_X$ if $\norm{x}_X = 1$. Therefore
	\[
		\norm{\pi_U}_{op} := \sup_{\norm{x}=1} \norm{\pi_U x}_X = \sup_{\norm{x}=1} \norm{\pi_U x}_X \le \sup_{\norm{x} = 1} \norm{x}_X = 1 < \infty
	\]
	By continuity criteria on normed spaces, $\pi_U$ is continuous. Letting $P = \pi_U$ and voil\`{a}, this implication is done.\\
	
	Now let $P:X \to X$ be a continuous, linear, idempotent map such that $\im(P) = U$. We need to find a subspace $V$ that complements $U$. Let $V := X/U$, and define the map $I : U \times V \to X$ by $(u,v+U) \mapsto u + v$. If there exists $I^{-1}$ such that $I \circ I^{-1} = \id_X$ and $I^{-1} \circ I = \id_{U \times V}$, then it is a bijection. Indeed, define the map
	\[
		I^{-1} : X \to U \times V, ~ ~ ~ x \mapsto (P(x), x-P(x))
	\]
	Then for all $x \in X$, and $(u,v) \in U \times V$, we have
	\begin{itemize}[$\star$]
		\item $I \circ I^{-1} (x) = I \left[ (P(x), x - P(x) \right] = P(x) + x - P(x) = x = \id_X(x)$
		
		\item $I^{-1} \circ I (u,v) = I^{-1} (u + v) = (u, (u+v) - u) = (u,v) = \id_{U \times V}(u,v)$
	\end{itemize}
	So $I \circ I^{-1} \equiv \id$, $I$ is a bijection. Moreover,
	\begin{align*}
		\norm{I}_{op} = & \sup_{\norm{(u,v)}_{U,V} = 1} \norm{I(u,v)}_X = \sup_{\norm{(u,v)}_{U,V} = 1} \norm{u+v}_X \le \norm{I(u,v)}_X\\
		= & \sup_{\norm{(u,v)}_{U,V} = 1} \norm{u}_X + \norm{v}_X = 1 < \infty
	\end{align*}
	So by continuity criteria on normed spaces, $I$ is a continuous isomorphism. If we can also prove $I^{-1}$ is continuous, then $V$ is indeed a complementary subspace to $U$ and we are done. By lemma 3, $\norm{P(x)}_X,\norm{x - P(x)}_X \le \norm{x}_X$, and we are done by applying continuity criteria for normed spaces. \qed
	
	\item Let $U$ be a topologically complemented subspace. Then by (a), there exists continuous, linear, idempotent map $P:X \to X$ such that $\im(P) = U$. Let $(u_n)_n \in U^{\bN}$ be a sequence such that $\lim_n u_n = u \in X$. Note that since $P$ is idempotent, then $P(P-I) \equiv 0$. In other words, if $x \in U$, then $(I - P) (x) = 0$, i.e. $P(x) = x$; otherwise, $P(x) = 0$. So
	\[
		P(u) = P(\lim_n u_n) \overset{(*)}{=} \lim_n P(u_n) = \lim_n u_n = u
	\]
	So $u \in U$ and $U$ is closed. Note that (*) holds because $P$ is continuous. \qed
\end{enumerate}

\paragraph*{Problem 5.7} (Continuity of Bilinear Maps) Let $(X,\norm{\cdot}_X)$, $(Y,\norm{\cdot}_Y$, and $(Z,\norm{\cdot}_Z)$ be normed spaces. Consider $(X \times Y, \norm{\cdot}_{X \times Y})$ equipped with $\norm{(x,y)}_{X \times Y} = \norm{x}_{X} + \norm{y}_Y$, and suppose that $B:X \times Y \to Z$ is a bilinear map.
\begin{enumerate}[(a)]
	\item Suppose $\norm{(x,y)}_{X \times Y} = 1$, then by lemma 3, $\norm{x}_X,\norm{y}_Y \le 1$. So
	\begin{align*}
		\norm{B}_{op} = & \sup_{\norm{x}_X + \norm{y}_Y = 1} \norm{B(x,y)} \le \sup_{\norm{x}_X + \norm{y}_Y = 1} C \norm{x}_{X} \norm{y}_{Y}\\
		\le & \sup_{\norm{x}_X + \norm{y}_Y = 1} C = C < \infty
	\end{align*}
	So by continuity criteria, $B$ is continuous. \qed
	
	\item We know that the maps $f_{y'}: X \to Z$, $x \mapsto B(x,y')$ and $g_{x'}: Y \to Z$, $y \mapsto B(x',y)$ are continuous, for all $y' \in Y$ and for all $x' \in X$. So by continuity criteria, we have $\norm{f_y}_{op}$ and $\norm{g_x}_{op}$ are bounded. In particular, $\norm{f_y}_{op} = \sup_{\norm{x}_X = 1} \norm{f_y(x)} < M_y$, i.e. $f_y$'s are pointwise bounded on the unit sphere. Consider $B(x,y) = B(\frac{x}{\norm{x}_X}, \frac{y}{\norm{y}_Y}) \cdot \norm{x}_X \cdot \norm{y}_Y$, let us denote by $x' = \frac{x}{\norm{x}}$ and $y' := \frac{y}{\norm{y}}$, we have
	\[
		\norm{B(x,y)}_{Z} = \norm{B(x', y')} \cdot \norm{x}_X \norm{y}_Y 
	\]
	Now since $X$ is Banach, by Banach-Steinhaus, we have
	\[
		\norm{B}_{op} = \sup_{y \in Y} \sup_{x \in X} \norm{B(x,y)} = \sup_{\norm{y'} = 1} \sup_{\norm{x'}_X = 1} \norm{f_{y'}(x')} \norm{x}_X \norm{y}_Y \le M \norm{x}_X \norm{y}_Y
	\]
	where and we are done.
\end{enumerate}




\end{document}