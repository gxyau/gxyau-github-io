\contentsline {part}{I\hspace {1em}General Topology}{1}{part.1}
\contentsline {section}{\numberline {1}Set Theory \& Logic}{1}{section.1}
\contentsline {section}{\numberline {2}Metric Spaces \& Normed Linear Spaces}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Metric Spaces}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Normed Linear Spaces}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Topological Spaces}{4}{section.3}
\contentsline {section}{\numberline {4}Topological Bases}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Subbases}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Exercises}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Order Topology}{10}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Subspace Topology}{11}{subsection.4.4}
\contentsline {section}{\numberline {5}Closed Sets and Limit Points}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Closed Sets}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Limit Points}{14}{subsection.5.2}
\contentsline {section}{\numberline {6}Continuous Functions}{15}{section.6}
\contentsline {section}{\numberline {7}Separation Axioms}{15}{section.7}
\contentsline {section}{\numberline {8}Compactness \& Connectedness}{15}{section.8}
