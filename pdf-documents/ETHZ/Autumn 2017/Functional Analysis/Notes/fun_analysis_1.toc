\contentsline {part}{I\hspace {1em}Introduction \& Overview}{1}
\contentsline {section}{\numberline {1}Overview}{1}
\contentsline {subsection}{\numberline {1.1}Motivation}{1}
\contentsline {subsection}{\numberline {1.2}Structures}{2}
\contentsline {subsection}{\numberline {1.3}Hierarchy of Spaces}{3}
\contentsline {subsection}{\numberline {1.4}Solving Equations}{4}
\contentsline {section}{\numberline {2}Introduction}{5}
\contentsline {subsection}{\numberline {2.1}Baire Category Theorem for Complete Metric Spaces}{5}
\contentsline {subsection}{\numberline {2.2}Uniform Boundedness Principle}{8}
\contentsline {subsection}{\numberline {2.3}Different Notions of Smallness}{10}
\contentsline {part}{II\hspace {1em}Banach Spaces \& Hilbert Spaces}{11}
\contentsline {section}{\numberline {3}Banach Spaces}{11}
\contentsline {subsection}{\numberline {3.1}Subspaces}{15}
\contentsline {subsection}{\numberline {3.2}Unit Spheres}{15}
\contentsline {subsection}{\numberline {3.3}Series in Banach Spaces}{19}
\contentsline {subsection}{\numberline {3.4}Linear Group in a Banach Space}{21}
\contentsline {subsection}{\numberline {3.5}Operations on Banach Spaces}{21}
\contentsline {section}{\numberline {4}Hilbert Spaces}{26}
\contentsline {subsection}{\numberline {4.1}Projection Lemma}{27}
\contentsline {subsection}{\numberline {4.2}Orthonormal System \& Hilbertean Basis}{29}
\contentsline {part}{III\hspace {1em}Fundamental Principles of Functional Analysis}{30}
\contentsline {section}{\numberline {5}Uniform Boundedness Principle for Banach Spaces}{30}
\contentsline {subsection}{\numberline {5.1}Open Mapping Theorem}{32}
\contentsline {subsection}{\numberline {5.2}Closed Graph Theorem}{34}
\contentsline {subsection}{\numberline {5.3}T\"{o}plitz Criterion}{35}
\contentsline {subsection}{\numberline {5.4}Unbounded Operators}{36}
\contentsline {subsection}{\numberline {5.5}Closable Operators}{37}
\contentsline {part}{IV\hspace {1em}Hahn-Banach Theorems, Convexity, Extremal Points}{41}
\contentsline {section}{\numberline {6}Hahn-Banach Theorems}{42}
\contentsline {subsection}{\numberline {6.1}Dual Spaces}{45}
\contentsline {subsection}{\numberline {6.2}Geometric Forms of Hahn-Banach}{46}
\contentsline {subsection}{\numberline {6.3}Duality of Hilbert Spaces}{47}
\contentsline {subsection}{\numberline {6.4}Duality for $L^p$-Spaces}{49}
\contentsline {section}{\numberline {7}Convexity in Infinite Dimensional Vector Spaces}{55}
\contentsline {section}{\numberline {8}Extremal Points}{57}
\contentsline {part}{V\hspace {1em}Weak Topologies, Reflexility \& Separability}{59}
\contentsline {section}{\numberline {9}Weak \& Strong Topologies}{59}
\contentsline {subsection}{\numberline {9.1}Weak Topology}{59}
\contentsline {subsection}{\numberline {9.2}Closure \& Convexity}{62}
\contentsline {section}{\numberline {10}Reflexivity \& Separability}{64}
\contentsline {subsection}{\numberline {10.1}Reflexivity}{64}
\contentsline {subsection}{\numberline {10.2}Separability}{66}
\contentsline {subsection}{\numberline {10.3}Application of Reflexivity \& Separability}{67}
\contentsline {paragraph}{$\text {Weak}^*$-Topology:}{67}
\contentsline {subsection}{\numberline {10.4}Lower Semicontinuity}{70}
\contentsline {part}{VI\hspace {1em}Adjoint Operators}{73}
\contentsline {section}{\numberline {11}Dual \& Compact Operators}{73}
\contentsline {subsection}{\numberline {11.1}Dual Operators}{73}
\contentsline {subsection}{\numberline {11.2}Isometry Property}{73}
\contentsline {subsection}{\numberline {11.3}Compact Operators}{76}
\contentsline {subsection}{\numberline {11.4}Summary of Compactness Theorems \& Compact Operators}{80}
\contentsline {subsection}{\numberline {11.5}Adjunctions in Hilbert Spaces}{81}
\contentsline {section}{\numberline {12}Spectral Theory}{85}
\contentsline {subsection}{\numberline {12.1}Spectrum \& Resolvent}{85}
\contentsline {subsection}{\numberline {12.2}Structure of the Spectrum}{87}
\contentsline {subsection}{\numberline {12.3}Spectral Calculus}{89}
\contentsline {subsection}{\numberline {12.4}Spectral Theory in Hilbert Spaces}{90}
\contentsline {subsection}{\numberline {12.5}Spectral Theorem for Bounded Operators}{93}
\contentsline {subsection}{\numberline {12.6}Spectral Theory Over $ {\mathbb {R}} $}{97}
\contentsline {section}{\numberline {13}Christmas Special}{98}
\contentsline {subsection}{\numberline {13.1}Dirichlet Spectrum of $A$}{98}
