function cannon_trajectory

%Student:

%set angle alpha (in radians)
alpha=2*pi/360*20;

%set constants
v0=100;
g=10;
m=20;
rho=0.1;

%set initial condition
%(attention: this is a 4x1 vector!)
y0=[0;0;v0*cos(alpha);v0*sin(alpha)];

%specify time interval
tspan=[0 5];

%specify right hand side
%(attention: output needs to be a 4x1 vector!)
rhs=@(t,y) [y(3:4);[0;-g]-rho/m*norm(y(3:4))*y(3:4)];

%carry out integration using ode45
[tout,yout]=ode45(rhs,tspan,y0);


%plot the trajectory
figure;
plot(yout(:,1),yout(:,2));
hold on;

%overwrite alpha with new value (in radians)
alpha=2*pi/360*40;

%specify intitial condition
%(attention: this is a 4x1 vector!)
y0=[0;0;v0*cos(alpha);v0*sin(alpha)];

%specify time interval
tspan=[0 8.5];

%specify right hand side
%(attention: output needs to be a 4x1 vector!)
rhs=@(t,y) [y(3:4);[0;-g]-rho/m*norm(y(3:4))*y(3:4)];


%carry out integration using ode45
[tout,yout]=ode45(rhs,tspan,y0);

%plot the trajectory (in green)
plot(yout(:,1),yout(:,2),'g');
legend('alpha=20','alpha=40');