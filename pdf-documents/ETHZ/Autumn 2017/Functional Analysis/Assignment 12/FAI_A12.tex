\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 12}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 12}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\begin{lemma}
	(Heine-Cantor Theorem) Let $(M,d_M)$ and $(N,d_N)$ be two metric spaces. If $M$ is a compact space and $f: M \to N$ is a continuous function, then $f$ is uniformly continuous.
\end{lemma}
\begin{proof}
	(Proof of Heine-Cantor Theorem) Since $f$ is continuous, so fix an arbitrary $x_0 \in M$. For any $\ee > 0$, there exists $\dd := \dd(x,\ee) > 0$ such that if $d_M(x_0,x) < \dd$, then $d_N(f(x_0),f(x)) < \ee$.\\
	
	Fix $\ee > 0$. Now since $M$ is compact, for arbitrary $r > 0$, there exists $x_1,\cdots,x_m$ such that $\set{B(x_i,\frac{\dd_i}{2})}_{i=1}^{m}$ covers $M$, where $\dd_i$ be chosen such that $d_N(f(x_i),f(x)) < \frac{\ee}{2}$ whenever $d_M(x_i,x) < \dd_i$. Now define $\dd := \min_{i=1,\cdots,m} \set{\frac{\dd_i}{2}}$; $\dd > 0$ is well defined because there are only finitely many such $\dd_i$'s. Then pick any $x,y \in M$ such that $d_M(x,y) < \dd$. We know that $\set{B(x_i,\frac{\dd_i}{2})}_{i=1}^{m}$ is a cover of $M$, thus there exists $i_0 \in \set{1,\cdots,n}$ such that $d_M(x,x_{i_0}) \le \frac{\dd_i}{2}$
	\[
		d_M(y,x_{i_0}) \le d_M(y,x) + d_M(x,x_{i_0}) \le \dd + \frac{\dd_i}{2} < \dd_i
	\]
	Then $d_N(f(x),f(y)) \le d_N(f(x), f(x_{i_0})) + d_N(f(x_{i_0}),f(y)) < \frac{\ee}{2} + \frac{\ee}{2} = \ee$. Thus $f$ is uniformly continuous as we desired.
\end{proof}

\paragraph*{Problem 12.1} (Integral Operators) Let $m \in \bN$, and $\Omega \subseteq \bR^n$ be a bounded set. Denote $(\cH,\inp{\cdot}{\cdot}_{\cH}) = (L^2(\Omega),\inp{\cdot}{\cdot}_{L^2(\Omega)})$. Let $k \in L^2(\Omega \times \Omega)$ be arbitrary, and define
\[
	K: L^2(\Omega) \to L^2(\Omega), ~ ~ ~ (Kf)(x) = \int_{\Omega} k(x,y) f(y) ~ dy
\]
be the kernel transformation.
\begin{enumerate}[(a)]
	\item To prove that $K$ is well defined, it suffices to show that $Kf \in L^2(\Omega)$ for any $f \in L^2(\Omega)$. Note that for arbitrary $x \in \Omega$, we can define $l_x: L^2(\Omega)$ by $l_x(y) = k(x,y)$ for all $y \in \Omega$. If we take $p = q = 2$ as the H\"{o}lder's conjugates, we have
	\begin{align*}
		\norm{Kf}_{\cH}^2 = & \int_{\Omega} |(Kf)(x)|^2 ~ dx = \int_{\Omega} \left| \int_{\Omega} l_x(y) f(y) ~ dy \right|^2 dx\\
		\le & \int_{\Omega} \left( \int_{\Omega} |l_x(y) f(y)| ~ dy  \right)^2 dx\\
		\overset{*}{\le} & \int_{\Omega} \left[ \left( \int_{\Omega} |l_x(y)|^2 ~ dy \right)^{\frac{1}{2}} \left( \int_{\Omega} |l_x(y)|^2 ~ dy \right)^{\frac{1}{2}} \right]^2 ~ dx\\
		= & \int_{\Omega} |f(y)|^2 dy \cdot \int_{\Omega \times \Omega} |k(x,y)|^2 ~ dy dx\\
		= & \norm{f}_{L^2(\Omega)}^2 \cdot \norm{k}_{L^2(\Omega \times \Omega)}^2 < \infty
	\end{align*}
	Therefore $\norm{Kf}_{\cH} < \infty$ for all $f \in L^2(\Omega)$ and $Kf$ is indeed well defined. \qed
	
	\item Let $B := B_{L^2(\Omega)} (0,1)$ be the unit ball of $\cH$. Note that $0$ is the zero functional, i.e. $0(x) = 0$ for all $x \in \Omega$. We want to show that $\bar{K(B)}$ is compact. From exercise 11.4, relative compactness in normed spaces is equivalent to the statement that every bounded sequence has a converging subsequence. So let $(f_n)_n$ be a bounded sequence in $\cH$, i.e. $\sup_{n \in \bN} \norm{f_n}_{\cH} = C < \infty$ for some $C \ge 0$.
\end{enumerate}

\paragraph*{Problem 12.2} (Uniform Subconvergence) Let $(X,\norm{\cdot}_X) = (C^1([0,1]), \norm{\cdot}_{C^0([0,1])})$. Let $(f_n)_n \in X^{\bN}$ be a sequence that satisfies the following two properties:
\begin{itemize}
	\item $\forall n \in \bN$, $f_n(0) = f_n'(0)$.
	
	\item $\exists C > 0$ s.t. $\forall x \in [0,1], ~  \forall n \in \bN$, $|f_n'(x)| \le C$
\end{itemize}
Let $F := \set{f_n: n \in \bN}$. We will show that $F$ satisfies one of the two hypotheses of Arzel\`{a}-Ascoli; if that is true, then $F$ is relatively compact and we are done.\\

We need to show that $F$ is $C_0$-uniformly bounded, and $C_0$-uniformly continuous. Before we continue, we prove a lemma.

Now by using the lemma 1 above with $M = [0,1]$, and since $F \subseteq C^1([0,1])$, thus each $f \in F$ is uniformly continuous. Hence $f$ is uniformly continuous. On the other hand, by second property, we have $|f_n'(x)| \le C$, where $C$ is given. Then
\[
	|f_n'(x)| \le C \iff -C \le f_n'(x) \le C \iff -Cx + k_n \le f_n(x) \le Cx + k_n
\]
Setting $x = 0$ we get that $k_n = f_n(0) = f_n'(0)$. Thus we have
\[
	|f_n(x)| - |f_n(0)| \le |f_n(x) - f_n(0)| \le C \implies |f_n(x)| \le C + |f_n(0)| = C + |f_n'(0)|
\]
So $\sup_{n \in \bN} |f_n(x)| \le C + \sup_{n \in \bN} |f_n'(0)| \le 2C < \infty$, and therefore $F$ is also uniformly bounded. Hence by Arzel\`{a}-Ascoli, we are done. \qed

\paragraph*{Problem 12.3} (Multiplication Operators on Complex-Valued Sequences) Let us define
\[
	\ell_{\bC}^{p} := \set{x : \bN \to \bC, n \mapsto x_n | \sum_{n \in \bN} |x_n|^p < \infty}
\]
In other words, $\ell_{\bC}^p$ is are the complex $\ell^p$-spaces. Let $a \in \ell_{\bC}^{\infty}$ be arbitrary, and define $T: \ell_{\bC}^2 \to \ell_{\bC}^2$, $x \mapsto (Tx)_n = (a_n x_n)_n$. Let $M := \norm{a}_{\ell_{\bC}^{\infty}} = \sup_{n \in \ bN} |a_n|$. Since $a \in \ell_{\bC}^{\infty}$, so $M < \infty$. We will denote by $(\cH,\norm{\cdot}_\cH) = (\ell_{\bC}^{2},\norm{\cdot}_{\ell_{\bC}^{2}})$.
\begin{enumerate}[(a)]
	\item Since $\cH$ is a normed space and $T$ is (clearly) linear, and if we want to prove $T \in L(\cH,\cH)$, it suffices to prove that $T$ is bounded. Indeed, we have
	\begin{align*}
		\norm{T}_{L(\cH)} = & \sup_{\norm{x} = 1} \norm{Tx}_{\cH} = \sup_{\norm{x} = 1} \sum_{n \in \bN} |a_n x_n|^2 = \sup_{\norm{x} = 1} \sum_{n \in \bN} |a_n|^2 \cdot |x_n|^2\\
		\le &  \sup_{\norm{x} = 1} M^2 \sum_{n \in \bN} |x_n|^2 = \sup_{\norm{x} = 1} M^2 \norm{x}_{\cH}  = M^2 < \infty 
	\end{align*}
	Therefore $T$ is bounded and hence continuous. We claim that $M^2 \le \norm{T}_{L(\cH)}$. Let $e_k$ be the $k^{th}$ coordinate vectors, i.e. 1 in its $k^{th}$ position and 0 everywhere else. Then
	\[
		\norm{Te_k}_{\cH} = |a_k|^2 \le M^2, ~ ~ ~ \forall k \in \bN
	\]
	Therefore by definition of $\sup$ being the upper bound, we have
	\[
		\norm{T}_{L(\cH)} = \sup_{\norm{x}_\cH = 1} \norm{Tx}_\cH \ge \sup_{k \in \bN} \norm{T e_k}_\cH = M^2
	\]
	Thus $\norm{T}_{L(\cH)} = M^2$. \qed
	
	\item We want to prove that $T$ is self-adjoint if and only if $a_n \in \bR$ for all $n \in \bN$. For $c \in \bC$, let us denote $\bar{c}$ to be its conjugate. Then we have $T^*(x) = \sum |\bar{a}_n x_n|^2$, and
	\begin{align*}
		T = T^* \iff & \norm{(T - T^*)x}_X^2 = 0 ~ ~ ~ \forall x \in \ell_{\bC}^2\\
		\iff & \sum_{n \in \bN} |(a_n - \bar{a}_n) x_n|^2 = 0 ~ ~ ~ \forall x \in \cH\\
		\iff & |(a_n - \bar{a}_n) x_n| = 0 ~ ~ ~ \forall x \in \cH, \forall n \in \bN\\
		\iff & \bar{a}_n = a_n ~ ~ ~ \forall n \in \bN
	\end{align*}
	In other words, $T$ is self-adjoint if and only if all $a_n$'s are real valued, as we desired.
	
	\item \textcolor{red}{(Incomplete)} Let $T$ be a compact operator, then any bounded sequence $(x^{(k)})_k$ is such that $(Tx^{k(m)})_{m}$ is a converging subsequence by problem 11.4. Thus
	
	By exercise 11.4, it suffices to show that if $(x_n)_n$ is a bounded sequence in $X$, then $(Tx_n)_n$ has a converging subsequencce. Now since $(x_n)_n$ is a bounded sequence, therefore $(Tx_n)_n$ is also bounded, in particular by $C \cdot \norm{T}$ where $(x_n)_n$ is bounded by $C$.\\
	
	Since $\cH$ is a Hilbert space, so in particular $\cH$ is reflexive, and by Eberlein-\v{S}mulian, $(Tx_n)_n$ is a weakly convergent subsequence, say $Tx_{n_k} \xrightarrow[k \to \infty]{w} y \in \cH$.
\end{enumerate}

\paragraph*{Problem 12.4} (Compact Operators on Continuous Functions) Let us denote by the normed space $(X,\norm{\cdot}_X) = (C^0([a,b]),\norm{\cdot}_{C^*0([a,b])})$, $T: X \to X$ be defined by
\[
	(Tf) (x) = \int_{a}^{x} \frac{f(t)}{\sqrt{x-t}} ~ dt
\]
It is easy to see that $T$ is linear.
\begin{enumerate}[(a)]
	\item Computing the operator norm, we
	\begin{align*}
		\sup_{\norm{f}_X = 1} \norm{Tf}_X = & \sup_{\norm{f}_X = 1} \sup_{x \in [a,b]} \left| \int_{a}^{x} \frac{f(t)}{\sqrt{x-t}} ~ dt \right| \le \sup_{\norm{f}_X = 1} \sup_{x \in [a,b]} \int_{a}^{x} \left| \frac{f(t)}{\sqrt{x-t}} \right| ~ dt\\
		\overset{*}{=} &  \sup_{x \in [a,b]} \sup_{\norm{f}_X = 1} \int_{a}^{x} \left| \frac{f(t)}{\sqrt{x-t}} \right| ~ dt \le \sup_{x \in [a,b]} \int_{a}^{x} \frac{1}{\sqrt{x-t}} dt\\
		= & \sup_{x \in [a,b]} (-2) \cdot \eval{\sqrt{x-t}}_{t=a}^{x} = \sup_{x \in [a,b]} (-2) \cdot (0 - \sqrt{x-a})\\
		= & \sup_{x \in [a,b]} 2 \sqrt{x-a} = 2 \sqrt{b-a} < \infty
	\end{align*}
	Where we use Tonelli's theorem to interchange the supremum at (*). So $T$ is bounded and hence continuous. Let $g \equiv 1$ be the constant 1 function on $[a,b]$. Then $\norm{g}_{X} = 1$, and therefore
	\begin{align*}
		\norm{T}_{L(X)} = & \sup_{\norm{f}_X = 1} \norm{Tf}_{X} \ge \norm{Tg}_{X}\\
		= & \sup_{x \in [a,b]} |Tg(x)| = \sup_{x \in [a,b]} \left| \int_{a}^{x} \frac{1}{\sqrt{x-t}} ~ dt \right|\\
		= & \sup_{x \in [a,b]} |2 \sqrt{x-a}| = 2 \sqrt{b-a}
	\end{align*}
	Therefore $\norm{T}_{L(X)} = 2 \sqrt{b-a}$. \qed
	
	\item Suppose $(f_n)_n$ is a bounded sequence in $X$, then if we denote $g_n := T f_n$ for all $n \in \bN$, then $(Tf_n)_n$ is also a bounded sequence in $X$. Define $F := \set{g_n: n \in \bN}$, we will show that $F$ is a family of functions that is uniformly equicontinuous, and uniformly bounded.\\
	
	Since $(f_n)_n$ is a bounded sequence, so there exists $C' > 0$ such that $\norm{f_n}_X \le C$. Let us define $C := 2C' \sqrt{b - a} = C' \norm{T}_{L(X)}$; we claim that $(g_n)_n$ is uniformly bounded by $C$. We have
	\[
		\norm{g_n} \le \norm{T}_{L(X)} \cdot \norm{f_n}_X = 2C' \sqrt{b-a}
	\]
	for all $n \in \bN$. Hence $C$ is a uniform bound for $(g_n)_n$; in particular, $C$ is a uniform bound for the family $F$. Now let $\ee > 0$, since $f_n \in X = C^0([a,b])$, and $[a,b]$ is compact, so by lemma 1, $f_n$ is uniformly continuous for all $n \in \bN$; in other words, $f$ is a uniformly equicontinuous family of functions.\\
	
	By Arzel\`{a}-Ascoli theorem, $T(B_X(0,1))$ is relatively compact, and hence $T$ is compact. \qed
	
	\item For a fixed, arbitrary $n \in \bN$, we have
	\[
		\norm{T^n}_{L(X)}^{\frac{1}{n}} \le \left( \norm{T}_{L(X)}^n \right)^{\frac{1}{n}} = \norm{T}_{L(X)} =  2 \sqrt{b-a}
	\]
	Therefore $r(T) := \lim_n \norm{T^n}^{\frac{1}{n}} \le \lim_n 2 \sqrt{b-a} = 2 \sqrt{b-a}$ and voil\`{a}. \qed
\end{enumerate}

\paragraph*{Problem 12.5} (Multiplication Operators on Square Integrable Functions)


\end{document}