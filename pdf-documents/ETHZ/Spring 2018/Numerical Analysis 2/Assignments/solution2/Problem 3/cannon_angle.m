function alpha=cannon_angle
%determine firing angle for which
%the target is hit

%Student:

%set initial values for secant method
%careful: the values eventually need
%to be written in radiants!
alpha(1)=20*2*pi/360;
alpha(2)=40*2*pi/360;
alpha(3)=0;

%write a function for the secant method
f=@(alpha) 250-cannon_hit(alpha);

%stops the while-loop as soon as the absolute
%value of the function 'f' is smaller than 1e-5
while abs(f(alpha(2)))>1e-5
    %carry out one step of the secant method
    alpha(3)= alpha(2)-f(alpha(2))*(alpha(2)-alpha(1))/...
        (f(alpha(2))-f(alpha(1)));
    %update the values
    alpha(1)=alpha(2);
    alpha(2)=alpha(3);
end
%write output in degrees
alpha=alpha(2)/2/pi*360;


    

