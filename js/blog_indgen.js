$("#post-list").html('<li style="background:#fff">Building index...</li>');

var bposts = null;
$.getJSON("http://hxhl95.github.io/blog/data.json", function(data) {bposts = data.posts;})
.error(function() {alert("Error: failed getJSON call for blog index data!");})
.complete(function(){
    var btag = getURLParameter("tag");
    var bOrder = getURLParameter("order");
    var bSortBy = getURLParameter("sort") || "desc";

    var bAllTags = [];
    $.each(bposts, function(ind, post){
        $.merge(bAllTags, post.tags);
        post.date = new Date(post.date);
    });
    bAllTags = $.grep(bAllTags,function(tag, ind){
        return ind == $.inArray(tag, bAllTags);
    });
    $.each(bAllTags, function(ind, tag){
        $("select[name=tag]").append('<option value="' + tag + '">' + tag + '</option>');
    });
    if (btag != null)
        $("select[name=tag]").val(btag);
    if (bOrder != null)
        $("select[name=order]").val(bOrder);
    if (bSortBy != null)
        $("select[name=sort]").val(bSortBy);

    if (btag != null && btag != "all")
    {
        bposts = $.grep(bposts, function(post, ind){
            return $.inArray(btag, post.tags) > -1;
        });
    }
    if (bOrder != null)
    {
        var bSfa = (bSortBy == "asc") ? -1 : 1;
        bposts.sort(function(a, b){
            return (a[bOrder] < b[bOrder]) ? bSfa : -bSfa;
        });
    }
    
    displayMore(bposts, 2*bPostsPerPage, "url", "title", "date", "tags", true, false);
});