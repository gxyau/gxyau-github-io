\contentsline {section}{\numberline {1}Basics}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Ramsey Theory}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Hypergraph Colouring}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Methods Using Expectations}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Maximum Cut}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Tournaments \& Hamiltonian Paths}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Combinatorial Number Theory}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Br\'{e}gman's Theorem}{5}{subsection.2.4}
\contentsline {section}{\numberline {3}Alteration Methods}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Graphs with Large Girth Large Chromatic Number}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Non 2-Colourable $n$-Uniform Hypergraph}{8}{subsection.3.2}
\contentsline {section}{\numberline {4}Dependent Random Choice}{9}{section.4}
\contentsline {section}{\numberline {5}Methods of Moments}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Second Moment Method}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Sum-Free Sets}{11}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Prime Divisors of a Number}{12}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Random Graphs}{13}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Chromatic Number of $G(n,\frac {1}{2})$}{14}{subsection.5.5}
\contentsline {section}{\numberline {6}Convergence \& Concentration}{15}{section.6}
\contentsline {subsection}{\numberline {6.1}Chernoff's Inequality}{15}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Tournaments}{16}{subsection.6.2}
\contentsline {section}{\numberline {7}Lov\'{a}sz Local Lemma}{18}{section.7}
\contentsline {subsection}{\numberline {7.1}Motivating Examples}{19}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Proof of Local Lemma}{20}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Post Proof Motivating Example}{20}{subsection.7.3}
\contentsline {section}{\numberline {8}Martingale}{21}{section.8}
\contentsline {subsection}{\numberline {8.1}Azuma-Hoeffding Inequality}{21}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Concentration of Chromatic Number of Random Graphs}{23}{subsection.8.2}
\contentsline {section}{\numberline {9}Correlation Inequalities}{26}{section.9}
\contentsline {subsection}{\numberline {9.1}FKG Inequalities}{26}{subsection.9.1}
\contentsline {section}{\numberline {10}Erd\"{o}s-Stone Theorem}{28}{section.10}
\contentsline {section}{\numberline {11}Exercise Sessions}{30}{section.11}
