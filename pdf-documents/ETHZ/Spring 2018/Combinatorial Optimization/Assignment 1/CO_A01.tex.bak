\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Combinatorial Optimization - Problem Set 1}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Combinatorial Optimization - Problem Set 1}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

\pagestyle{fancy}

\begin{document}

\maketitle

\paragraph{Problem 1} Let $G$ be a triangle, and let $x(e) = \frac{1}{2}$ for all $e \in E(G)$. Then any pair of edges is an element of $P \setminus P^{\cM}$.

\paragraph{Problem 2}
Let us denote $c,x \in \bR_{\ge 0}^3$, $b,y \in \bR_{\ge 0 }^6$, $A \in M_{6 \times 3}(\bR)$ as follows:
\[
	x = \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} ~
	c = \begin{bmatrix} -1 \\ 2 \\ -1 \end{bmatrix} ~
	b = \begin{bmatrix} y_1 \\ y_2 \\ y_3 \\ y_4 \\ y_5 \\ y_6 \end{bmatrix} ~
	x = \begin{bmatrix} 1 \\ -10 \\ 2 \\ -4 \\ 7 \\ 11 \end{bmatrix} ~
	A = \begin{bmatrix}
		2 & -1 & 1 \\ 
		-5 & 1 & 13\\
		-4 & 1 & 6\\
		-3 & 1 & 7\\
		-2 & 1 & 2\\
		-1 & 1 & 5
	\end{bmatrix}
\]
Then the maximization problem becomes
\[
	\max c^T x ~ s.t. ~ Ax \le b, x \ge 0
\]
Where we use ``$\ge$" compares the two vectors component-wise positive, and 0 here is the zero vector.
\begin{enumerate}[(a)]
	\item $\min b^T y$ subject to $A^T y \ge c, y \ge 0$
	
	\item Let $y^* = (1,0,0,0,0,3)^T$. Then $A^T y = \begin{bmatrix} -1 \\ 2 \\ 16 \end{bmatrix} \ge c$, and $y^*$ is indeed feasible. Moreover,
	\[
		b^T y = 1 \times 1 + 3 \times 11 = 34 = -12 + 46 = c^T x
	\]
	So by complementary slackness, $x^*$ and $y^*$ are the optimal solution to primal and dual problem respectively.
\end{enumerate}

\end{document}