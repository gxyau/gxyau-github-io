\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Randomized Algorithms - Problem Set 2}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Randomized Algorithms - Problem Set 2}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Exercise 2.1} Let $n \in \bN$, and $\pi \in S_n$ be drawn uniformly at random. Suppose $F$ is the random variable as described. For each $i \in [n]$, we define
\[
	F_i := \begin{cases}
		1 & \pi(i) = i\\
		0 & \pi(i) \ne i
	\end{cases}
\]
That is, $F_i$ is the indicator variable that $i$ is fixed under permutation $\pi$. Then $\bP(F_i = 1) = \frac{1}{n}$ for each $i \in [n]$. Moreover, since $\pi$ is chosen uniformly at random, $\bP(F_i)$ is independent of $\bP(F_j)$. It is easy to see that $F = \sum_{i=1}^{n} F_i$, therefore
\begin{itemize}
	\item $\bE(F) = \sum_{i=1}^{n} \bE(F_i) = \frac{1}{n} \cdot n = 1$
	
	\item $\var(F) \overset{(*)}{=} \sum_{i=1}^{n} \var(F_i) = n \cdot (\frac{1}{n} - \frac{1}{n^2}) = \frac{n-1}{n}$
\end{itemize}
Note that $(*)$ is true due to independence of each $F_i$, as we have $\cov(F_i,F_j) = 0$ for all $i \ne j$. \qed

\paragraph{Exercise 2.2}	Let $s,t \in \bN$, $s,t > 2$.
\begin{enumerate}[(a)]
	\item Let $0 \le p \le 1$, $s,t \in \bN$ such that $s + t =: n$, and
	\[
		{n \choose s} p^{s \choose 2} + {n \choose t} (1-p)^{t \choose 2} < 1
	\]
	Consider $K_n$, and colour the edges $e \in E(K_n)$ red with probability $p$, and colour $e$ blue with probability $(1-p)$. For fixed $S \subseteq V$ with $|S| = s$, the probability that $S$ is monochromatic red is $p^{s \choose 2}$; similarly, for any $T \subseteq V$, $|T| = t$, probability of $T$ being monochromatic blue is $(1-p)^{t \choose 2}$. Since there are $n \choose s$ ways to choose a subset of size $s$ and $n \choose t$ ways to choose a subset of size $T$, therefore
	\begin{gather}
		\setcounter{equation}{0}
		\bP(\text{$K_n$ has a red $s$-clique or a blue $t$-clique})\\
		\le \bP(\text{$K_n$ has a red $s$-clique}) + \bP(\text{$K_n$ has a blue $t$-clique})\\
		= {n \choose s} p^{s \choose 2} + {n \choose t} (1-p)^{t \choose 2} < 1
	\end{gather}
	Note that we have used the union bound from (1) to (2). This means that
	\begin{gather*}
		\bP(\text{$K_n$ does not have a red $s$-clique nor a blue $t$-clique})\\
		= 1 - \bP(\text{$K_n$ has a red $s$-clique or a blue $t$-clique})\\
		> 1 - 1 = 0
	\end{gather*}
	Therefore there is a colouring of $K_n$ without a red $s$-clique or a blue $t$-clique. \qed

	\item Let $s = 4$, and $n = c (\frac{t}{\log t})^{\frac{3}{2}}$ for some $c > 0$, then  we have
	\begin{gather*}
		{n \choose 4} p^{4 \choose 2} + {n \choose t} (1-p)^{t \choose 2} \le n^4 p^6 + {n \choose t} (e^{-p})^{t \choose 2}\\
		\le n^4 p^6 + (\frac{en}{t})^t \exp(-p {t \choose 2})\\
		= n^4 p^6 + (\frac{n}{t})^t \exp(\frac{2t - pt (t-1)}{2})
	\end{gather*}
	Choose $p = \frac{\log t}{t}$, then we get
	\[
		{n \choose 4} p^{4 \choose 2} + {n \choose t} (1-p)^{t \choose 2} \le c^4 + (\frac{n}{t})^t \exp(\frac{2t - (\log t)(t-1)}{2})
	\]
	as $t \to \infty$, $2t < (\log t)(t-1)$, and the second term $\to 0$. Thus if we choose $c$ small enough (say $c < 1$), then we are done. \qed
	
	\item Let $N_1 := R(s-1,t)$, $N_2 := R(s,t-1)$, $N := N_1 + N_2$, if we can show that random edge-colouring of $K_N$ has a red $K_s$ or a blue $K_t$, then we are done. Thus let us colour $E(K_n)$ with red-blue colouring uniformly at random. Fix $v \in V(K_N)$, since $|N(v)| = N-1$, so there are at least $N_1$ red edges incident to $v$ or at least $N_2$ blue edges incident to $v$.\\
	
	Without loss of generality, assume there are $N_1$ red edges. Then consider $u_1,\cdots,u_{N_1} \in N(v)$ such that the edge $u_i v$ is coloured red, $i \in [N_1]$. By definition, there are either a red $K_{s-1}$, or a blue $K_t$; if it is the latter, we have a blue $K_t$ as desired; otherwise, $K_{N_1} \cup \set{v}$ yields a red $K_{s}$. \qed
	
	\item Let $n \in \bN$, we claim that $R(s,s) \ge (s-1)^2$. Let $N := (s-1)^2$, we will prove that there is a colouring of $K_N$ such that there is neither a red $K_s$ or a blue $K_s$. Let us first divide the vertices into $s-1$ sets of $s-1$ vertices. Colour the edges in between the same set with red edges, and the ones in between different sets blue. Then it has neither a red $K_s$ since the only red edges are the ones within the same set and there are only $s-1$ vertices, nor a blue $K_s$ since any subset of size $s$ have at least a pair of vertices in one set using pigeon hole principle. Hence $R(s,s) \ge (s-1)^2$, i.e. $R(s,s) \in \Omega(s^2)$. \qed
\end{enumerate}

\paragraph{Exercise 2.3} Let $G = (V,E)$, $|V| = n$, let $d_i$ denote the degree of $v_i \in V$, $|V| = n$, and $d_{\max} = \max_{i \in [n]} d_i$. With probability $\frac{d_i}{1+d_i}$, we delete the vertex $v_i$ from the graph. Let $X$ be the number of vertex that survives, and $Y$ be the number of edges that survives. Then
\begin{itemize}
	\item $\bE(X) = \sum_{i=1}^{n} \frac{1}{1 + d_i} = \sum_{i=1}^{n} \frac{1}{1 + d_i} \cdot \left( \sum_{j \in N(i)} \frac{1}{d_i} \right)$
	
	\item $\bE(Y) = \sum_{i=1}^{n} \sum_{j \in N(i)} \frac{1}{1 + d_i} \cdot \frac{1}{1+d_j} = \sum_{i=1}^{n} \frac{1}{1+d_i} \sum_{j \in N(i)} \frac{1}{1+d_j}$
\end{itemize}
Then $\bE(X - Y) = \sum_{i=1}^{n} \frac{1}{1+d_i} \cdot \left(1 - \sum_{j \in N(i)} \frac{1}{1+d_j}\right) \ge \sum_{i=1}^{n} \frac{1}{1+d_i} (1 - \frac{d_i}{1+d_{\max}})$. Now as $d_{\max} \to \infty$, We have $\lim_{d_{\max} \to \infty} \bE(X - Y) \ge \lim_{d_{\max} \to \infty} \sum_{i=1}^{n} \frac{1}{1+d_i} (1 - \frac{d_i}{1+d_{\max}}) = \sum_{i=1}^{n} \frac{1}{1+d_i}$. Thus if $d_{\max}$ is very large, this holds. \textcolor{orange}{Sorry I don't know how to continue}.

\paragraph{Exercise 2.4} \begin{enumerate}[(a)]
	\item Let $G = (V,E)$ be a graph, and $k$ be the number of triangles in $G$. Let $X$ be the random variable that counts the number of rainbow triangles in $G$. Colour each $v \in V$ with colours $1,2,3$ uniformly at random. Define the random variable $X_i$ for each $i \in [k]$ as follows
	\[
		X_i = \begin{cases}
			1 & \text{Triangle $i$ is rainbow}\\
			0 & \text{Triangle $i$ is not rainbow}
		\end{cases}
	\]
	Then $X = \sum_{i=1}^{k} X_i$. Moreover, $\bP(X_i = 1) = \frac{3!}{3^3} = \frac{2}{9}$ since there are $3! = 6$ permutations to colour triangle $i$ with 3 colours, and there are $3^3 = 27$ ways to colour a triangle in total. So $\bE(X) = \sum_{i=1}^{k} \bE(X_i) = \frac{2}{9} k$. Therefore there exists a 3-colouring of $G$ such that at least $\frac{2}{9}$ of its triangles are rainbow.
\end{enumerate}

\end{document}