\babel@toc {english}{}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Basic Model}{5}{0}{2}
\beamer@sectionintoc {3}{ICM \& LTM}{6}{0}{3}
\beamer@sectionintoc {4}{Algorithms}{17}{0}{4}
\beamer@sectionintoc {5}{$\sigma (\cdot )$}{20}{0}{5}
\beamer@sectionintoc {6}{Generalisations}{25}{0}{6}
\beamer@sectionintoc {7}{References}{26}{0}{7}
