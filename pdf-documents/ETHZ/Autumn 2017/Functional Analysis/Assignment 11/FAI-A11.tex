\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 11}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 11}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\paragraph*{Problem 11.1} (Dual Operators) Let $(X,\norm{\cdot}_X)$, $(Y,\norm{\cdot}_Y)$, and $(Z,\norm{\cdot}_Z)$ be normed spaces. Recall that if $T \in L(X,Y)$, then its adjoint $T^* \in L(Y^*, X^*)$ is such that 
\[
	\inp{T^* y^*}{x}_{X^* \times X} = \inp{y^*}{Tx}_{Y^* \times Y}
\]
for all $x \in X$ and all $y^* \in Y^*$.
\begin{enumerate}[(a)]
	\item We want to show that $\id_{X^*} = (\id_X)^*$. Let $x \in X$ and $x^* \in X^*$ be arbitrary. We have
	\begin{gather*}
		\inp{(\id_X)^* x^*}{x}_{X^* \times X} = \inp{x^*}{\id_X x}_{X^* \times X} = \inp{x^*}{x}_{X^* \times X} = \inp{\id_{X^*} x^*}{x}\\
		\implies \inp{[(\id_X)^* - \id_{X^*}] x^*}{x}_{X^* \times X} \equiv 0 ~ ~ ~ \forall x \in X, ~ \forall x^* \in X^*
	\end{gather*}
	Without loss of generality, we may assume that $x \ne 0$ and $x^* \not \equiv 0$, thus $(\id_{X})^* = \id_{X^*}$. \qed
	
	\item Let $T \in L(X,Y)$, $S \in L(Y,Z)$. Fix arbitrary $x \in X$ and $z^* \in Z^*$, then
	\begin{align*}
		\inp{(S \circ T)^* z^*}{x}_{X^* \times X} = & \inp{z^*}{(S \circ T)(x)}_{Z^* \times Z} = \inp{S^* z^*}{T x}_{Y^* \times Y}\\
		= & \inp{(T^* \circ S^*) z^*}{x}_{X^* \times X}
	\end{align*}
	Thus we get that $(S \circ T)^* - T^* \circ S^* \equiv 0$ for all $x \in X$ and all $z^* \in Z^*$. \qed
	
	\item Let $T \in L(X,Y)$ be a bijection with continuous inverse i.e. $T^{-1} \in L(Y,X)$. Let $x \in X$ and $x^* \in X^*$ be arbitrary. Note that $T^* \circ (T^*)^{-1} = \id_{X^*}$ and $T^{-1} \circ T = \id_X$. Using (a) and (b), we have the following
	\begin{gather*}
		\inp{(\id_{X})^* x^*}{x}_{X^* \times X} = \inp{(T^{-1} \circ T)^* x^*}{x}_{X^* \times X} = \inp{T^* \circ (T^{-1})^* x^*}{x}_{X^* \times X}\\
		\inp{\id_{X^*} x^*}{x}_{X^* \times X} = \inp{(T^*) \circ (T^*)^{-1} x^*}{x}_{X^* \times X}
	\end{gather*}
	Therefore $\inp{[T^* \circ (T^{-1})^* - T^* \circ (T^*)^{-1}] x^*}{x}_{X^* \times X} \equiv 0$ for all $x \in X$ and all $x^* \in X^*$. Thus $T^* \circ [(T^{-1})^* - (T^*)^{-1}] \equiv 0$; but since $T$ is bijective, thus $T \not \equiv 0$, and therefore $T^* \not \equiv 0$. So $(T^{-1})^* - (T^*)^{-1} \equiv 0$. \qed
	
	\item Let $\cI_X : X \hookrightarrow X^{**}$ and $\cI_Y : Y \hookrightarrow Y^{**}$ be canonical inclusions. We want to prove that $\cI_Y \circ T = T^{**} \circ \cI_X$ for all $T \in L(X,Y)$, i.e. the following diagram commutes.
	\[
		\begin{tikzcd}
			X \ar{r}{T} \ar{d}[swap]{\cI_X} & Y \ar{d}{\cI_Y}\\
			X^{**} \ar{r}{T^{**}} & Y^{**}
		\end{tikzcd}
	\]
	Let $x \in X$ be arbitrary, $y \in Y$ be such that $Tx = y$, with $\hat{x} \in X^{**}$ and $\hat{y} \in Y^{**}$ are such that $\cI_X (x) = \hat{x}$ and $\cI_Y(y) = \hat{y}$. Therefore $\hat{y} = \cI_{Y} \circ T (x)$.\\
	
	Since $T \in L(X,Y)$, hence $T^* \in L(Y^*,X^*)$, and $T^{**} \in L(X^{**},Y^{**})$. Let $y^* \in Y^*$ be arbitrary, then we have
	\begin{align*}
		[T^{**} \circ \cI_X (x)] (y^*) = & T^{**} \hat{x} (y^*) = \inp{T^{**} \hat{x}}{y^*}_{Y^{**} \times Y^*} = \inp{\hat{x}}{T^* y^*}_{X^{**} \times X^*}\\
		= & \inp{x}{T^* y^*}_{X \times X^*} = \inp{y}{y^*}_{Y \times Y^*} = \inp{\hat{y}}{y^*}_{Y^{**} \times Y^*}\\
		= & \hat{y} (y^*) = [\cI_Y \circ T (y)] (y^*)
	\end{align*}
	Thus the diagram commutes. \qed
\end{enumerate}

\paragraph*{Problem 11.2} (Isomorphisms and Isometries) Let $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ be normed spaces, and $T \in L(X,Y)$.
\begin{enumerate}[(a)]
	\item Let $T$ be an isomorphism with $T^{-1} \in L(Y,X)$. Then $T^{-1} \circ T = \id_X$. By problem 11.1
	\[
		\id_{X^*} = (\id_X)^* = (T^{-1} \circ T)^* = T^* \circ (T^*)^{-1}
	\]
	And analogously, $(T^*)^{-1} \circ T^* = \id_{Y^*}$. Moreover, since $T \in L(X,Y)$, hence $T^* \in L(Y^*, X^*)$. Thus $T^*$ is an isomorphism. \qed
	
	\item Suppose $T^*$ is an isometric isomorphism. By part (a), $T^*$ is an isomorphism, so it remains to show that $T^*$ is isometric. Since $T$ is isometric, so for all $x_1,x_2 \in X$ arbitrary, $\norm{T(x_1 - x_2)}_Y = \norm{x_1 - x_2}_X$. Let $y_1^* y_2^* \in Y^*$ be arbitrary, we have
	\begin{align*}
		\norm{T^*(y_1^* - y_2^*)}_{X^*} = & \sup_{\norm{x}_X = 1} |\inp{T^* (y_1^* - y_2^*)}{x}_{X^* \times X}| = \sup_{\norm{x}_X = 1} |\inp{y_1^* - y_2^*}{T x}_{Y^* \times Y}|\\
		\overset{*}{=} & \sup_{\norm{Tx}_Y = 1} |\inp{y_1^* - y_2^*}{Tx}_{Y^* \times Y}| = \norm{y_1^* - y_2^*}_{Y^*}
	\end{align*}
	where (*) holds because of isometry. Thus $T^*$ is indeed an isometry. \qed
	
	\item Suppose $X \iso X^{**}$ and $Y \iso Y^{**}$ are reflexive. Then we can identify $T^{**}$ with $T$; in particular, the commutative diagram in 11.1(d) holds, with $\cI_X$ and $\cI_Y$ are isomorphism. So $T = T^{**}$. Now by part (a) and (b), if $T^*$ is isometric isomorphism, then $(T^*)^* = T^{**} = T$ is also isometric isomorphism. Thus the reverse implications are true. \qed
	
	\item Suppose $(X,\norm{\cdot}_X)$ is a reflexive Banach space, and that $(X,\norm{\cdot}_X) \iso (Y, \norm{\cdot}_Y)$. Let $T : X \to Y$ be an isomorphism. Then $T^{**}$ in 11.1(d) is such that $T^{**} = \cI_Y^{-1} \circ T \circ \cI_Y$. In particular, since $\cI_X^{-1},\cI_Y$ and $T$ are isomorphisms, their composition is also an isomorphism, i.e. $T$ is isomorphism. Hence $Y \iso X \iso X^{**} \iso Y^{**}$, and therefore $Y$ is isomorphic to $Y^{**}$, i.e. $Y$ is reflexive. \qed
\end{enumerate}

\paragraph*{Problem 11.3} (Minimal Energy) Let $m \in \bN$< $\Omega \subseteq \bR^m$ be a bounded set. Let $g \in L^2(\bR^m)$, $h \in L^2(\Omega)$. Define the following maps
\begin{itemize}
	\item $V:L^2(\Omega) \to \bR$, $f \mapsto \int_{\Omega} \int_{\Omega} g(x-y) f(x) f(y) ~ dy dx$
	
	\item $E: L^2(\Omega) \to \bR$, $f \mapsto \norm{f - h}_{L^2(\Omega)}^2 + V(f)$
\end{itemize}
Prove the following:
\begin{enumerate}[(a)]
	\item Prove that $V$ is weakly sequentially continuous, i.e. if $f_n \xrightarrow[]{w} f$, then $V(f_n) \xrightarrow[]{w} V(f)$.
	
	\item Suppose $g \ge 0$, and that
	\[
		L_{+}^2(\Omega) := \set{f \in L^2(\Omega)|\text{$f(x) \ge 0$ almost everywhere}}
	\]
	Prove that $\restr{E}{L_{+}^{2}(\Omega)}$ attains global minimum.
\end{enumerate}
First note that $V(f) = \int_{\Omega} f(x) \int_{\Omega} g(x-y) f(y) ~ dy dx = \int_{\Omega} (g*f)(x) f(x) ~ dx$ where
\[
	g * f (x) = \int_{\Omega} g(x-y) f(y) ~ dy
\]
is the convolution of the integrable functions $f$ and $g$. Note that $g * f$ is well defined by using Fubini-Tonelli Theorem; suppose $f,g \ge 0$, then by applying Tonelli's theorem at (*), we have
\begin{align*}
	\norm{g * f}_{L^2(\Omega)}^2 = & \int_{\Omega} \left| (g * f)(x) \right|^2 ~ dx \overset{*}{=} \int_{\Omega} \left| \int_{\Omega} g(x-y) f(y)~ dy \right|^2 dx\\
	\le  & \int_{\Omega} \norm{f}_{L^2(\Omega)}^2 \norm{g}_{L^2(\Omega)}^2 ~ dx dy = \norm{f}_{L^2(\Omega)}^2 \norm{g}_{L^2(\Omega)}^2 \cdot |\Omega|
\end{align*}
Thus $g*f$ is integrable if $f,g \ge 0$. We can say the same thing if $f,g$ are absolutely integrable. Thus $g * f$ is well defined.

\paragraph*{Problem 11.4} (Compact Operators) Let $(X,\norm{\cdot}_X)$, $(Y,\norm{\cdot}_Y)$, and $(Z,\norm{\cdot}_Z)$ be normed spaces. We denote by
\[
	K(X,Y) := \set{T \in L(X,Y): \text{$\bar{T(B_X(0,1))} \subseteq Y$ is compact}}
\]
the set of compact operators between $X$ and $Y$.
\begin{enumerate}[(a)]
	\item Let $T \in L(X,Y)$. Suppose $T$ is a compact operator. Let $(x_n)_n \in X^{\bN}$ be a bounded sequence, i.e. there exists $C \in \bR_{\ge 0}$ such that $\norm{x_n}_X \le C < \infty$. We claim that the sequence $(Tx_n)_n$ is a bounded sequence in $Y$. Indeed, we have
	\[
		\norm{T x_n}_Y \le \norm{T}_{L(X,Y)} \norm{x_n}_X \le C \cdot \underbrace{\norm{T}_{L(X<Y)}}_{< \infty}
	\]
	note that $\norm{T}_{L(X,Y)} < \infty$ since $T$ is continuous, and hence bounded. Therefore $(Tx_n)_n$ is a bounded sequence. Since $Y$ is a normed space, and hence a metric space, compactness and sequential compactness are equivalent. Hence $(Tx_n)_n$ has a converging subsequence.\\
	
	Now suppose that for all bounded sequence $(x_n)_n$ in $X$, the sequence $(T x_n)_n$ has a converging subsequence in $Y$. Fix an arbitrary bounded sequence $(x_n)_n \in X^{\bN}$. Suppose that $\sup_{n \in \bN} \norm{x_n}_X = M$, and, without loss of generality, we may assume that $M \le 1$; otherwise, we work with the sequence $(\frac{x_n}{M})_n$ instead.\\
	
	Moreover, since $\norm{x_n}_X \le 1$, thus $(x_n)_n \in B_X(0,1)^{\bN}$. By assumption, the sequence $(Tx_n)_{n \in \bN}$ has a converging subsequence, hence every sequence $(Tx_n)_n$ that is bounded in $\bar{T(B_X(0,1))}$ has a converging subsequence; hence $\bar{T(B_X(0,1))}$ is sequentially compact, and hence compact. Thus $T$ is a compact operator. \qed
	
	\item Suppose $(Y,\norm{\cdot}_Y)$ is complete, i.e. $Y$ is a Banach space. We want to prove that $K(X,Y)$ is a closed subspace of $L(X,Y)$.\\
	
	Let $(T_n)_n \in K(X,Y)^{\bN}$ be a sequence of compact operators, such that 
	\[
		\lim_n T_n = T \in L(X,Y)
	\]
	Then $\bar{T(B_X(0,1))} = \lim_n \bar{T_n(B_X(0,1))}$. Fix an arbitrary $x \in B_X(0,1)$, since 
	\[
		T_n \xrightarrow[n \to \infty]{\norm{\cdot}_{L(X,Y)}} T
	\]
	So we have $T_n(x) \to T(x)$ as $n \to \infty$ for all $x \in X$. Let $\ee > 0$ bar arbitrary, the set $\cU := \set{B_Y(y,\ee) : y \in \bar{T(B_X(0,1))}}$ is an open cover for $B := \bar{T(B_X(0,1))}$; we want to find a finite subcover for $\cU$.\\
	
	By definition, we can find $N(\ee) \in \bN$ such that $\norm{(T - T_n) x}_{Y} < \ee$ for all $x \in X$ whenever $n > N(\ee)$. Since $T_n$'s are compact operators, thus $\bar{T_n(B_X(0,1))}$ is compact. In particular, there exists $k(n) \in \bN$ and $y_1,\cdots,y_{k(n)} \in B$ such that $\bar{T_n(B_X(0,1))} \subseteq \bigcup_{i=1}^{k(n)} B_Y(y_i,\ee)$.\\
	
	In particular, let $y \in B$ be arbitrary. Without loss of generality, we may assume that $y \in B_Y(y_{i_0},\ee)$. Then we have the following inequality
	\[
		\norm{T(y - y_{i_0})}_{Y} \le \norm{(T - T_n) y}_Y + \norm{T_n (y - y_{i_0})}_{Y} + \norm{(T - T_n) y_{i_0}}_Y < \ee + \ee + \ee = 3\ee
	\]
	This means that $\set{B(y_i,\ee)}_{i=1}^{k(n)}$ is a $3\ee$-neighbourhood of $T$, and it covers $B$. By replacing $\ee$ with $\frac{\ee}{3}$ (as well as the radii of the neighbourhoods in $\cU$ with $\frac{\ee}{3}$), we have a finite subcover of $\cU$ such that $B \subseteq \bigcup_{i=1}^{k(n)} B(y_i,\frac{\ee}{3})$, and hence $T$ is indeed compact. So $K(X,Y)$ contains all its limit points and it is closed. \qed
	
	\item Since $T(X) \subseteq Y$ is finite dimensional normed space, so it is isomorphic to $\bR^n$ for some $n \in \bN$. We claim that $T(X)$ is compact. By Heine-Borel, it suffices to show that $T(X)$ is closed and bounded.\\
	
	Since $T \in L(X,Y)$, thus $T$ is bounded. Moreover, since $T(X)$ is a finite dimensional normed space, so it is closed as finite dimensional subspace of normed space is complete, and hence closed. Therefore $T(X)$ is compact. Note that $\bar{T(B_X(0,1))} \subseteq T(X)$ is a compact set as closed subset of a compact space. Hence $T \in K(X,Y)$. \qed
	
	\item Let $T \in L(X,Y)$ and $S \in L(Y,Z)$. If $T \in K(X,Y)$, then $\bar{T(B_X(0,1))}$ is compact, and hence $S \circ T(B_X(0,1))$ is compact as continuous image of compact set. Hence $\bar{S \circ T(B_X(0,1))}$ is also compact as closure of compact set.\\
	
	If $S$ is a compact operator, then by (a), every bounded sequence in $Y$ is such that the image contains a converging subsequence. Now let $(x_n)_n$ be a bounded sequence in $X$, say for all $n \in \bN$, $\norm{x_n} \le M < \infty$ for some $M \in \bR$. Then $(S \circ T x_n)_n$ is a bounded sequence in $Z$, bounded by $M \cdot \norm{S}_{L(Y,Z)} \cdot \norm{T}_{L(X,Y)}$.\\
	
	Define $x_n' := \frac{x_n}{M \cdot \norm{S}_{L(Y,Z)} \cdot \norm{T}_{L(X,Y)}}$, then $x_n'$ is a bounded sequence in $X$ such that its image under $S \circ T$ is in the unit ball of $Z$, i.e. $x_n' \in B_Z(0,1)$. Since $\bar{S(B_Y(0,1))}$ is compact, $(S \circ T x_n')_n$ has a converging subsequence in $\bar{S(B_Y(0,1))}$, therefore $(S \circ T x_n)_n$ has a converging subsequence and $\bar{S \circ T(B_X(0,1))}$ is compact. \qed
	
	\item Suppose $X$ is reflexice, and let $(x_n)_n$ be a bounded sequence in $X$. By Eberlein-\v{S}mulian, $x_n \xrightarrow[]{w} x$ for some $x \in X$. Then let $T \in L(X,Y)$ be arbitrary such that $T$ maps weakly convergent sequence to norm-convergent sequence. Then $(Tx_n)_n \to Tx$ in $Y$. Taking the subsequence to be itself, we have a converging subsequence, so by (a), $T$ is compact. \qed
\end{enumerate}

\end{document}