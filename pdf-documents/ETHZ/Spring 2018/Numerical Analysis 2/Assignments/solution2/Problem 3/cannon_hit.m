function x1 = cannon_hit(alpha)

% given data
v0 = 100;
g = 10;
m = 20;
rho = 0.1;

%define right hand side
odefun = @(tt, xx) [xx(3:4); -rho/m*norm(xx(3:4))*xx(3:4) + [0; -g]];

% specify integration interval
tspan = [0 100];

% determine initial conditions
x0 = [0; 0; v0*cos(alpha); v0*sin(alpha)];

% activate event-function
opts = odeset('Events', @cannon_event);

% carry out integration
[t, x, te, xe, ie] = ode45(odefun, tspan, x0, opts);

% return point of impact
x1 = xe(1);

function [value, isterminal, direction] = cannon_event(t, y)

% return height above ground of projectile
value = y(2);

% stop integration, if height = 0 is attained
isterminal = 1;

% specify direction through zero
direction = -1;