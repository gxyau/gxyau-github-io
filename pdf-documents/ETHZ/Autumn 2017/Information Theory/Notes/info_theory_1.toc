\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Entropy}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Properties of Entropy}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Relative Entropy}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Jensen's Inequality}{3}{subsection.1.4}
\contentsline {section}{\numberline {2}Chain Rule}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Mutual Information}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Conditional Mutual Information}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Fano's Inequality}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Encoding \& Decoding}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}One-to-variable Encoding}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Prefix Free Codes and Rooted Binary Trees}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Fixed-to-variable Uniquely Decodable Code}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Huffman Procedure}{10}{subsection.3.4}
\contentsline {section}{\numberline {4}Typicality}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Expected Description Length}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Entropy is Concave}{14}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Log-Sum Inequality}{14}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Relative Entropy is Convex}{14}{subsection.4.4}
\contentsline {section}{\numberline {5}Channel}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}Weakly Symmetric Channel}{17}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Conditional Independence \& Data Processing Inequality}{19}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Joint Weak Typicality}{19}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Weak Typicality Decoding}{20}{subsection.5.4}
\contentsline {section}{\numberline {6}Lossy Compression}{23}{section.6}
