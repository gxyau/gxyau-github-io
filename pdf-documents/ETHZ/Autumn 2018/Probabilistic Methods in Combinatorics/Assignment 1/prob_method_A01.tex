\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Probabilistic Method in Combinatorics}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Probabilistic Method in Combinatorics \\ Problem Set 1}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Problem 1.1} Let $H$ be an $n$-uniform hypergraph, with $E(H) < \frac{4^{n-1}}{3^n}$, and let $V(H) = \set{v_i}_{i=1}^{m}$, i.e. a hypergraph on $m$ vertices. For each $v \in V(H)$, let us colour $v$ by colour $1,\cdots,4$ uniformly at random; that is, each vertex has probability $\frac{1}{4}$ to get coloured by a colour from $[4]$.\\

Let $e \in E(H)$, then
\[
	\bP(\text{$e$ has at most 3 colours}) = \frac{1}{4^n} \cdot {4 \choose 3} \cdot 3^n = \frac{4^{n-1}}{3^n}
\]
Therefore the probability that every edge contains all 4 colours is
\begin{gather*}
	\bP(\text{All $e \in E(H)$ contains 4 colours})\\
	= 1 - \bP(\text{At least one $e \in E(H)$ has at most 3 colours})\\
	\overset{(*)}{\ge} 1 - \sum_{e \in E(H)} \bP(\text{$e$ is coloured by at most 3 colours})\\
	=  1 - \frac{3^n}{4^{n-1}} \cdot |E| > 1 - 1 = 0
\end{gather*}
where $(*)$ is due to union bound. Since $\bP(\text{All $e \in E(H)$ contains 4 colours}) > 0$, hence there exists a colouring such that each edge is represented by exactly 4 colours.

\paragraph{Problem 1.2} Let $F$ be a finite collection of prefix-free binary strings, and let $k \in \bN$ be the length of the longest binary string in $F$; such $k$ exists because $F$ is finite. Let $N_i$ denote the number of strings in $F$ of length $i$ for $i \in \bN$. Let $\cC$ be the collection of binary strings of length $k$, with each bit $\sigma_i$ having probability $\frac{1}{2}$ being 0 and with probability $\frac{1}{2}$ being 1. So $|\cC| = 2^k$.\\

Now let $\sigma \in \cC$ be an arbitrary binary string. Let $E_i$ denote the event that $\sigma$ contains a substring of length $i$ in $F$. Since there are $N_i$ many substrings of length $i$ in $F$, so
\[
	|E_i| = N_i \cdot \frac{2^k}{2^i} = N_i \cdot 2^{k-i}
\]
Note that for a fix string of length $i$ to be a prefix of $\sigma$, the first $i$ bits are fixed, and since there are $N_i$ of them, the above equality holds. The $\bigcup_{i=1}^{k} E_i$ is the collection of all binary strings in $|\cC|$ contains a substring as prefix in $F$. Now note that $F$ is prefix free, hence $E_i \cap E_j = \emptyset$ for $1 \le i < j \le k$, thus we have
\[
	\sum_{i=1}^{\infty} N_i \cdot 2^{k-i} = \sum_{i=1}^{k} N_i \cdot 2^{k-i} = \left| \bigcup_{i=1}^{k} E_i \right| \le |\cC| = 2^k
\]
Dividing $2^k$ on both sides, we get indeed $\sum_{i=1}^{\infty} \frac{N_i}{2^i} \le 1$. \qed

\paragraph{Problem 1.3} Let $0 \le p \le 1$, $s,t \in \bN$ such that $s + t =: n$, and
\[
	{n \choose s} p^{s \choose 2} + {n \choose t} (1-p)^{t \choose 2} < 1
\]
Consider $K_n$, and colour the edges $e \in E(K_n)$ red with probability $p$, and colour $e$ blue with probability $(1-p)$. For fixed $S \subseteq V$ with $|S| = s$, the probability that $S$ is monochromatic red is $p^{s \choose 2}$; similarly, for any $T \subseteq V$, $|T| = t$, probability of $T$ being monochromatic blue is $(1-p)^{t \choose 2}$. Since there are $n \choose s$ ways to choose a subset of size $s$ and $n \choose t$ ways to choose a subset of size $T$, therefore
\begin{gather}
	\setcounter{equation}{0}
	\bP(\text{$K_n$ has a red $s$-clique or a blue $t$-clique})\\
	\le \bP(\text{$K_n$ has a red $s$-clique}) + \bP(\text{$K_n$ has a blue $t$-clique})\\
	= {n \choose s} p^{s \choose 2} + {n \choose t} (1-p)^{t \choose 2} < 1
\end{gather}
Note that we have used the union bound from (1) to (2). This means that
\begin{gather*}
	\bP(\text{$K_n$ does not have a red $s$-clique nor a blue $t$-clique})\\
	= 1 - \bP(\text{$K_n$ has a red $s$-clique or a blue $t$-clique})\\
	> 1 - 1 = 0
\end{gather*}
Therefore there is a colouring of $K_n$ without a red $s$-clique or a blue $t$-clique.\\

To prove the second part, let $s = 4$, and $n = c (\frac{t}{\log t})^{\frac{3}{2}}$ for some $c > 0$, then  we have
\begin{gather*}
	{n \choose 4} p^{4 \choose 2} + {n \choose t} (1-p)^{t \choose 2} \le n^4 p^6 + {n \choose t} (e^{-p})^{t \choose 2}\\
	\le n^4 p^6 + (\frac{en}{t})^t \exp(-p {t \choose 2})\\
	= n^4 p^6 + (\frac{n}{t})^t \exp(\frac{2t - pt (t-1)}{2})
\end{gather*}
Choose $p = \frac{\log t}{t}$, then we get
\[
	{n \choose 4} p^{4 \choose 2} + {n \choose t} (1-p)^{t \choose 2} \le c^4 + (\frac{n}{t})^t \exp(\frac{2t - (\log t)(t-1)}{2})
\]
as $t \to \infty$, $2t < (\log t)(t-1)$, and the second term $\to 0$. Thus if we choose $c$ small enough (say $c < 1$), then we are done.

\end{document}