 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% writeLaTeX Example: A quick guide to LaTeX
%
% Source: Dave Richeson (divisbyzero.com), Dickinson College
% 
% A one-size-fits-all LaTeX cheat sheet. Kept to two pages, so it 
% can be printed (double-sided) on one piece of paper
% 
% Feel free to distribute this example, but please keep the referral
% to divisbyzero.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use writeLaTeX: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[10pt,landscape]{article}
\usepackage{hedgehog}


\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}
\pagestyle{empty}
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother
\setcounter{secnumdepth}{1}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}
% -----------------------------------------------------------------------

\title{Summary of Graph Theory}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem*{thm}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{coro}{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem*{defn}{Definition}

\begin{document}

\raggedright
\footnotesize

\begin{center}
     \Large{\textbf{Computational Statistics}} \\
\end{center}
\begin{multicols}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Regression}
\subsection{Linear Regression}
For simple linear regression, we have
\[
	y = \beta_1 x + \beta_0 \ee
\]
where $\beta_1$ is the gradient and $\beta_0$ is the intercept. For multiple linear regression, we have
\[
	Y_i = x_i^T \beta + \ee_i ~ ~ ~ \forall i \in \set{1,\cdots,n}
\]
where each $i \in \set{1,\cdots,n}$ is an observation. In matrix form, we have
\[
	Y = X \beta + \ee
\]
where $Y, \ee \in \bR^{n \times 1}$, $X \in \bR^{n \times p}$, $\beta \in \bR^{p \times 1}$.

\subsection{Least Squares Model}
Given linear model $Y = X \beta + \ee$, the least squares estimator, $\hat{\beta}$, of $\beta$ is as follows:
\[
	\hat{\beta} = \min_{b \in \bR^p} \norm{Y - X b}_2^2
\]
where $\norm{\cdot}_2$ is the 2-norm (Euclidean norm); $\hat{\beta}$ is given by
\[
	\hat{\beta} = (X^T X)^{-1} X^T Y
\]
Mean and standard error of $\hat{\beta}$ is as follows:
\begin{itemize}
	\item $\bE(\hat{\beta}) = \beta$
	
	\item $S.E.(\hat{\beta}) = \sqrt{\sigma^2 (X^T X)^{-1}}$
\end{itemize}
In single variable, we have $\hat{\beta}_j = \frac{1}{\sum_{i=1}^{n}X_{ij}^2} \sum \limits_{i=1}^{n} X_{ij} y_i$

\begin{defn}
	The t-statistics is given by $t(\hat{\beta}) = \frac{\hat{\beta} - \beta_0}{S.E.(\hat{\beta})}$, where $\hat{\beta}$ is the estimated variable $\beta$, and  $\beta_0$ is theoretical value of $\beta$ (normally this is what we want to test), i.e. population mean.
\end{defn}

\begin{defn}
	Let $t_0 := t(\hat{\beta})$ be the t-statistic of $\hat{\beta}$. Given $\alpha \in [0,100]$, the p-value of $\hat{\beta}$ is $2 \cdot \bP(|t| \ge t_0)$ for two tail test, and $\bP(t \ge t_0)$ for one tail test.
\end{defn}

\begin{defn}
	The confidence interval for $\hat{\beta}_j$ at $(1-\alpha) \%$ is $\hat{\beta}_j \pm \hat{S.E.}(\hat{\beta}_j) \cdot t_{1-\frac{\alpha}{2},n-p}$.
\end{defn}

\section{KNN \& CV}
\begin{defn}
	Let $y = X \hat{\beta}$ be a submodel $y = X \tilde{\beta}$ with $p_0$ and $p_1$ predictors, i.e. $X \hat{\beta}$ is null hypothesis and $X \tilde{\beta}$ is the alternative hypothesis. The F-statistics is given by 
	\[
		F = \frac{\norm{X \hat{\beta} - X \tilde{\beta}}^2/(p_1 - p_0)}{\norm{y - X \beta}^2/(n-p_1)}
	\]
	The degrees of freedom are $p_0,p_1$ respectively. Use $anova()$ in R to compute by hand.
\end{defn}

\begin{defn}
	Mean square error is defined to be
	\[
		MSE = \frac{1}{n} \sum_{i=1}^{n} |y_i - f(x_i)|^2 = \frac{1}{n} \norm{y - X^T \beta}_2^2
	\]
	where ${(x_i,y_i)}_{i=1}^{n}$ are observations. The residual standard error of previously said data is
	\[
		RSE = \sqrt{\frac{\sum_{i=1}^{n} (y_i - f(x_i))^2}{n-df}}
	\]
	where $df$ is the degrees of freedom. Let $fit = lm(y \sim x1)$, then residuals is given by $fit.res$ in $R$.
\end{defn}

\begin{lemma}
	$\text{MSE} = \text{Bias}^2 + \var$, i.e.
	\[
		\bE[(y - \hat{f}(x))^2] = (\bE[y - \hat{f}(x)])^2 + \var(\hat{f}(x))
	\]
	This is the bias-variance trade off formula.
\end{lemma}

We define $RSS = \sum_{i=1}^{n} |y_i - f(x_i)|^2$ and $TSS = \sum_{i=1}^{n} |y_i - \bar{y}|^2$.

\begin{defn}
	The $R^2$-statistics, given by $R^2 = 1 - \frac{RSS}{TSS}$, is the proportion of variability explained by the model. The adjusted $R^2$ is $R_{adj}^2 = 1 - (1-R^2)\frac{n-1}{n-p-1}$.
\end{defn}

\begin{defn}
	Let $\set{(x_i,y_i)}_{i=1}^{n}$ be the training set, and $\hat{m}(x)$ be estimated model of $x$, and $\rho(y,\hat{m}(x))$ be the loss function. The $K$-fold cross validation error is
	\[
		CV ~ error = \frac{1}{K} \sum_{j=1}^{K} \rho(y^{-I_j}, \hat{m}(X^{-I_j}))
	\]
	where $y^{-i}$ denotes all observation except for $i$, $\bigsqcup_{j=1}^{K} I_j = [n]$, $y^{-I_j}$ denotes all observations except for the one indexed by elements of $I_j$.
\end{defn}

\subsection{Double CV}
The idea is to split the dataset into training, tuning, and testing sets; train and tune on the training tuning set, and then testing using the last set.

\section{Bootstrapping}
Suppose $\set{(x_i,y_i)}_{i=1}^{n}$ are observations, which we denote by $Z_i = (x_i,y_i)$. We also denote by $\hat{P}_n$ is the empirical distribution of $\set{Z_i}_{i=1}^{n}$. The procedure to produce bootstrap sample is as follows:
\begin{enumerate}
	\item Generate $\set{Z_i^*}_{i=1}^{n} \overset{i.i.d.}{\sim} \hat{P}_n$ (sampling with replacement)
	
	\item Compute $\hat{\theta}_n^{*} = g(Z_1^*,\cdots,Z_n^*)$
	
	\item Repeat $B$ times to get $\set{\hat{\theta}_n^{*j}}_{j=1}^{B}$
\end{enumerate}

We can then use bootstrap estimators in 3 to get the bootstrap distribution, i.e.
\begin{itemize}
	\item (B. Mean) $\bE^{*}(\hat{\theta}_n^*) = \frac{1}{B} \sum_{i=1}^{B} \hat{\theta}_n^{*i}$
	
	\item (B. Var) $\var^*(\hat{\theta}_n^*) = \frac{1}{B-1} \sum_{i=1}^{B} \left( \hat{\theta}_n^{*i} - \bE^{*}(\hat{\theta}_n^*) \right)^2$
	
	\item (B. Bias) $\bias^*(\hat{\theta}_n^*) = \frac{1}{B} \sum_{i=1}^{B} (\hat{\theta}_n^{*i} - \bE^{*}(\hat{\theta}_n^*))$
	
	\item (B. Q4) $\alpha$-quantile of $\hat{\theta}_n^*$ distribution is approximately empirical  $\alpha$-quantile for $\set{\hat{\theta}_n^{*i}}_{i=1}^{B}$.
\end{itemize}
We write the bootstrap distribution of $\hat{\theta}_n^*$ as $P^*$.

\begin{defn}
	A bootstrap distribution $\hat{\theta}_n^*$ is consistent for $\hat{\theta}_n$ if
	\
	\[
		\bP(\sqrt{n} | \hat{\theta}_n - \theta | \le x) - \bP^*(\sqrt{n} | \hat{\theta}_n - \hat{\theta}_n^* | \le x) \overset{P}{\to} 0
	\]
	where $\bP^*$ comes from the bootstrap distribution $P^*$.
\end{defn}

Consistency of bootstrap usually implies consistency of variance and bias of bootstrap i.e.
\begin{multicols}{2}
	\begin{itemize}
		\item $\frac{\bE(\hat{\theta}_n) - \theta}{\bE(\hat{\theta}_n^*) - \hat{\theta}_n} \xrightarrow[]{P} 1$
		
		\item $\frac{\var(\hat{\theta}_n)}{\var(\hat{\theta}_n^*)} \xrightarrow[]{P} 1$
	\end{itemize}
\end{multicols}
Minima of $\var(\alpha X + (1-\alpha) Y)$ is attained at $\alpha = \frac{\sigma_y^2 - \sigma_{xy}}{\sigma_x^2 + \sigma_y^2 - 2 \sigma_{xy}}$

\subsection{Bootstrap CI}
Given $\hat{\theta}_n^*$ is consistent for $\hat{\theta}_n$. A two side confidence interval with $(1-\alpha)$ coverage for $\theta$ is given by 
\[
	[\hat{\theta}_n - 	q_{1-\frac{\alpha}{2}}(\hat{\theta}_n^* - \hat{\theta}_n), \hat{\theta}_n - \hat{q}_{\frac{\alpha}{2}}(\hat{\theta}_n^* - \hat{\theta}_n) ] = [2 \hat{\theta}_n - q_{1-\frac{\alpha}{2}}^*, 2 \hat{\theta}_n - q_{\frac{\alpha}{2}}^* ]
\]
where $q_{\alpha}^*$ is $\alpha$-bootstrap quantile of $\hat{\theta}_n^*$, $\hat{q}_{\alpha} = q_{\alpha}^* - \hat{\theta}_n$; this is the reversed quantile for bootstrap distributions.\\

The bootstrap $t$-distributions is as follows
\[
	\left[ \hat{\theta}_n -  \hat{q}_{1-\frac{\alpha}{2}}(\frac{\hat{\theta}_n^* - \hat{\theta}_n}{\hat{s.d.}(\hat{\theta}_n^*)}), \hat{\theta}_n - \hat{q}_{\frac{\alpha}{2}}(\frac{\hat{\theta}_n^* - \hat{\theta}_n}{\hat{s.d.}(\hat{\theta}_n^*)})  \right]
\]
where $\hat{s.d.}(\hat{\theta}_n^*) = \sqrt{\hat{\var}^*(\hat{\theta}_n^*)}$ is the sample s.d. of $\hat{\theta}_n$, and $\hat{s.d.}(\hat{\theta}_n^*)$ is the computed via second layer of bootstrap samples. Note that $\hat{q}_{t}(\frac{\hat{\theta}_n^* - \hat{\theta}_n}{\hat{s.d.}(\hat{\theta}_n^*)}) = q_{t} (\frac{\hat{\theta}_n^* - \hat{\theta}_n}{\hat{s.d.}(\hat{\theta}_n^*)}) \cdot \hat{s.d.}(\hat{\theta}_n)$.

\section{Hypothesis Testing}
There are parametric and non-parametric tests:
\begin{itemize}
	\item (Parametric) Assume normal distribution if variance known
	
	\item (Parametric) Assume $t$-distribution if variance unknown
	
	\item (Non-Parametric) Wilcoxon rank sum test
	
	\item (Non-Parametric) Permutation test
\end{itemize}

\subsection{Wilcoxon Rank}
Given $\set{Y_{i}^{(1)}}_{i=1}^{n_1} \overset{i.i.d.}{\sim} F_1$ and $\set{Y_{i}^{(2)}}_{i=1}^{n_2} \overset{i.i.d.}{\sim} F_2$\\
\begin{description}
	\item[$H_0$] $F_1 = F_2$
	
	\item[$H_1$] $F_1$ is shifted version of $F_2$
\end{description}
Procedure:
\begin{enumerate}
	\item Compute ranks of all observations $(1,\cdots,n_1+n_2)$
	
	\item Compute $U$, sum of ranks within group
	
	\item Compute distribution of $U$ under $H_0$
\end{enumerate}
To compute $U$ we permute $(Y_i^{(1)},Y_i^{(2)})$, in all possible combinations, i.e. $2^m$ combinations.

\subsection{Permutation Test}
To get the distribution of $U$ under $H_0$, permute $Y$'s in both groups in all possible ways. For each group assignment compute sum of ranks within group. $H_0$ says the values would've been the same regardless.
\begin{itemize}
	\item Small sample sizes: Compute all permutations
	
	\item Large sample sizes: Simulate large number of simulations.
\end{itemize}
Permutation test assumes the labels are indifferent to the distribution.

\subsection{FDR \& FWER}
Suppose $V$ is the number of times $H_0$ is incorrectly rejected, $R$ is the total number of times $H_0$ is rejected. Then false discovery proportion is $Q := \frac{V}{R}$. False discovery rate (FDR) is $\bE(Q)$. Family wise error rate (FWER) is $\bP(V \ge 1)$. Bonferroni correction is to conduct $H_0$ at $\frac{\alpha}{m}$ to control the FWER at $\alpha$, where $m$ is the total number of test conducted.

\subsection{Information Criteria}
Let $p$ be number of estimated parameters, $\hat{L}$ be max value of likelihood function of model, $n$ observations.
\begin{description}
	\item[AIC] $2p - 2 \ln (\hat{L})$
	
	\item[BIC] $\ln(n) p - 2 \ln(\hat{L})$
	
	\item[Mallow's Cp] $\frac{RSS_p}{S^2} - n + 2p$
\end{description}
where $S^2$ is MSE on $k$ parameters, and we choose $p < k$ parameters.

\section{Ridge \& LASSO}
Ridge regression use $\ell^2$ penalty i.e.
\[
	\min \frac{1}{n} \sum_{i=1}^{n} (y_i - x_i^T \beta)^2 + \lambda \sum_{j=1}^{p} |\beta_j|^2 \equiv \min MSE(\beta) + \lambda \norm{\beta}_{2}^2
\]
Lasso uses the $\ell^1$-norm penalty i.e.
\[
	\min \frac{1}{n} \sum_{i=1}^{n} (y_i - x_i^T \beta)^2 + \lambda \sum_{j=1}^{p} |\beta_j| \equiv \min MSE(\beta) + \lambda \norm{\beta}_{1}
\]
Use LASSO for variable selection and Ridge for shrinkage.

\section{Spline}
For $\bR^d$, a spline of degree $d$ with $k$ knots has $(k+1) \cdot (d+1)$ parameters and $d \cdot k$ constraints. Choice of basis for spline is $(x - \xi)_{+}^3$, $\xi$ is the knot whose $3^{rd}$ derivative is not continuous.

\subsection{GAM \& Backfitting}
Generalised additive models: $y_i = \beta_0 + \sum_{j=1}^{p} f_j (x_{ij}) + \ee_i$; non-linearity makes GAM generalisable. GAM does not allow interactions. Backfitting procedure is as follows:
\begin{enumerate}
	\item Initialise est. mean $\hat{\mu} = \frac{\sum_{i=1}^{n} y_i}{n}$, predictors $\hat{g}_j = 0$, for all $j \in 1,\cdots,p$.
	
	\item For $j$, $\hat{g}_j = S_j(y - \hat{\mu} \i1 - \sum_{k \ne j} \hat{g}_k)$; note $y - \hat{\mu} \i1$ is center. Normalise $\hat{g}_j \leftarrow \hat{g}_j - \frac{\sum_{i=1}^{n} \hat{g}_j (x_{ij})}{n}$.
\end{enumerate}
Repeat 2 until convergence; convergence measure $\max_j \frac{\norm{\hat{g}_{j,new} - \norm{\hat{g}_{j,old}}^2}}{\norm{\hat{g}_{j,old}}^2} \le \text{tolerance}$.

\section{CART \& RF}
Let $\set{R_i}_{i=1}^{M}$ be a partition of $\bR^p$. For CART,
\[
	g_{tree}(x) = \sum_{r=1}^{M} \beta_r\i1_{x \in R_r}
\]
If partitions are given, then $\hat{\beta}_j := \frac{\sum_{i=1}^{n} y_i \i1_{x_i \in R_j}}{\sum_{i=1}^{n} \i1_{x_i \in R_j}}$.\\

Key point of bagging is decrease variance without increasing bias by aggregating estimators. Random forest is more independent than bagging so better performance. Out of bag error on $x_i$ uses only trees that doesn't have $x_i$ in them. Bagging uses bootstap samples, build model and aggregate predictors.

\end{multicols}

\section*{R Cheatsheet \& Packages}
\begin{multicols}{2}
\begin{tabularx}{\columnwidth}{X X}
	\%\% & Remainder\\
	\%/\% & Quotient\\
	\%in\% & Membership\\
	anova(model1,model2) & F-test on the models\\
	abline(a,b,v,h) & a slope, b intersect, v vertical, h horizontal\\
	boot(data,statistic) & statistic is a function\\
	boot.ci(boot.out, conf) & boot.out is boot object\\
	boxplot(x,y) & Boxplot\\
	bs(x,knots,data) & Basis for spline\\
	coef(obj,i) & Coefficient of the model with $i$ predictors\\
	cut(x,breaks,labels) & Cutting a vector into different groups\\
	gam.s(x,y,df,data) & Smoothing spline basis for GAM\\
	glmnet(x,y,alpha,lambda) & alpha = 0 Ridge, alpha = 1 LASSO, lambda is tuning parameter\\
	hist(x,breaks,prob) & Histogram\\
	ifelse(if,then,else) & if then else\\
	kknn(formula, train, test, k) & KNN, train \& test are matrix or df\\
	lines(x,y) & Draw lines on graphs\\
	lm($y \sim \cdot$) & Linear models\\
	lo(x,span) & Basis for using LOOES\\
	mean(vec) & Mean of the vector\\
	model.matrix(form, data) & Create $x$ matrix for Ridge and LASSO\\
	ns(x,knots,data) & Basis for natural spline
\end{tabularx}

\begin{tabularx}{\columnwidth}{X X}
	order(vec) &f\ Order of each element\\
	pairs(formula, data) & Pairs plot\\
	par(mfrow) & Multiple plots diff. graphs\\
	par(new) & Multiple plots same graph\\
	poly(x,n) & Polynomial degrees $n$ in $x$\\
	randomForest(y,x,formula) & Random Forest\\
	rank(x) & Computes the rank of the samples\\
	regsubsets(formula,data,nvmax) & Finding best subset of predictors\\
	replicate(n, expr) & Replicate expr n times\\
	s(x,df,data) & Basis for smoothing spline\\
	sample(x,size,replace) & Sampling from $n$ numbers\\
	sort(vec) & Sorting the vector\\
	var(vec) & Variance of the vector\\
	which(expression) & Which element fulfils the expression\\
	wilcox.test(x,y, alternative=``less") & Wilcoxon Rank test\\
	\hline
	library(kknn) & K nearest neighbours\\
	library(ISLR) & Package for ISLR datasets and stuffs\\
	library(leaps) & For regsubsets, etc\\
	library(spline) & Package for splines, bs() for spline and ns() for natural spline\\
	library(gam) & General additive models\\
	library(cart) & Classification and Regression Trees
\end{tabularx}

\end{multicols}

\end{document}
