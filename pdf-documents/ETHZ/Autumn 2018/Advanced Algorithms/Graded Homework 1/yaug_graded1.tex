\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Advanced Algorithms - Graded Homework 1}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Advanced Algorithms - Graded Homework 1}
\author{Guo Xian Yau}
\date{\today}

\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{prop}{Proposition}

\begin{document}

\maketitle

\begin{center}\small{\textsl{ Collaborator(s): Wonryong Ryou, Fabian Fr\"{a}nz, Barna P\'{a}sztor }} \end{center}

Notations: In this problem set, we will use $[n] := \set{1,\cdots,n}$ for any $n \in \bZ_{+}$, $\bN = \set{1,2,3,\cdots}$, and $\bN_0 = \bN \cup \set{0}$.

\paragraph{Problem 1 - Team Formation} Let $[n]$ be the set of students with $n \in \bN$, and $\cL$ be a list of potential teams of size 3, i.e. $L \in \cL$ is such that $|L| = 3$. We define a hypergraph $H = (V,L)$, i.e. for all students $i,j,k \in [n]$, $i,j,k$ shares a hyperedge if and only if $\set{i,j,k} \in \cL$.\\

The problem of finding the maximum number of teams can then be viewed as finding an maximum matching of the hypergraph $H$. Notice that $H$ is a 3-uniform hypergraph. We claim that the greedy algorithm will yield a 3-approximation PTAS. Fix an arbitrary ordering $L_1,\cdots,L_m$ of elements of $\cL$. We want
\begin{algorithm}[H]
	\caption{3-Approximation for Hypergraph Matching}
	\begin{algorithmic}[1]
		\State $M \gets \emptyset$
		\For{$i \in [m]$}
			\If{$L_i \cap L_j = \emptyset$ for all $j < i$}
				$M \gets M \cup \set{L_i}$
			\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}
Now observe that using this algorithm, we will produce a maximal hypergraph matching $M$ for $H$. Let $M^* := \opt(H)$ be the optimal (i.e. maximum) matching of $H$. Let $e \in M^*$ be arbitrary. We claim that $1 \le |\set{f \in M: e \cap f \ne \emptyset}| \le 3$. If we proved the claim, then
\[
	|M^*| = \sum_{f \in M^*} 1 \le \sum_{f \in M^*} |\set{e \in M: e \cap f \ne \emptyset}| \le \sum_{e \in M} 3 = 3|M|
\]
Rearranging we get $|M| \ge \frac{1}{3} |M^*|$. Hence this is indeed a 3-approximation PTAS for 3-uniform hypergraph matching.\\

It remains to show that the inequality is true. First let us suppose towards a contradiction that for a fixed arbitrary $e \in M^*$, we have $|\set{e \in M: e \cap f \ne \emptyset}| = 0$. This means that vertices of $e$ are disjoint from vertices in $M$; by our algorithm, this would be a contradiction because $e$ would have been added into $M$. Therefore $|\set{e \in M: e \cap f \ne \emptyset}| \ge 1$.\\

On the other hand, for each $e \in M^*$, since $H$ is 3-uniform, so there are at most 3 hyperedge, say $f,g,h$, of $M$ is such that $e \cap e' \ne \emptyset$ for $e' \in \set{f,g,h}$. If there is a fourth edge $i \in M^*$ such that $i \cap e \ne \emptyset$, then we would contradict $M^*$ being a matching since by pigeon hole principle, there is a vertex in $e$ that is shared between two of $\set{f,g,h,i}$. Therefore $|\set{e \in M: e \cap f \ne \emptyset}| \le 3$. This concludes the proof. \qed

\paragraph{Problem 2 - Team Patrolling} Let $L = \set{l_i}_{i=1}^{m}$ be a list of $m$ buildings, and $U = \set{u_i}_{i=1}^{n}$ be the collection of patrolling units, $m,n \in \bN$. Let $c_i$ be the cost to hire unit $u_i$, and $B \in \bN$ be the budget to pay for $k$ units, $k \in \bN$. Since we only have budget to hire at most $k$ units, we may assume $u_i = 1$ and $B = k$. Define $f: \cP(U) \to \bN$ by
\[
	f(V) = \left| \bigcup_{u \in V} \set{l \in L: \text{Building $l$ is covered by $u$}} \right|
\]
We claim that $f$ is monotonic, non-decreasing, submodular function. If we can prove that, then we can use the greedy solution to submodular maximization problem with at most $k$ elements, which is proven in the previous problem sets. The approximation factor is therefore $(1 - \frac{1}{e})$ for this problem.\\

We have to show two things:
\begin{itemize}
	\item $f$ is a monotonic increasing, i.e. non-decreasing function
	
	\item $f$ is a submodular function.
\end{itemize}
Let $V \in \cP(U)$ be arbitrary, and $W \subseteq V$. Since building covered by $W$ must also be covered by $V$, we have indeed $f(V) \ge f(W)$ for all $W \subseteq V$ and for all $V \in \cP(U)$. This proves that $f$ is monotonic increasing.\\

To prove submodularity, let $S,T \in \cP(U)$ with $S \subseteq T$, and $u \in U$ be arbitrary. Our goal is to prove that $f$ has diminishing return, i.e.
\[
	f(S \cup \set{u}) - f(S) \ge f(T \cup \set{u}) - f(T)
\]
If $u \in S$, then we have nothing to prove since we will get zero on both sides of the inequality. So without loss of generality we may assume $u \in U \sm S$. We now have two cases, namely $u \in T$, and $u \notin T$. In the former case, the right hand side yields zero since $Y \cup \set{u} = Y$. Then the inequality is trivially satisfied as $f$ is monotonically increasing.\\

In the latter case, i.e. $u \notin T$, we consider $L'(v) = \set{\text{Buildings covered by $v \in U$}}$, and denote $L'(S) = \bigcup_{v \in S} L'(v)$. By subset relation, we have $L'(S) \subseteq L'(T)$; this is just saying that the set $T$ covers at least the buildings that are covered by $S$. Therefore we have
\[
	L'(T) \cap L'(u) \supseteq L'(S) \cap L'(u) \implies L'(u) \sm (L'(T) \cap L'(u)) \subseteq  L'(u) \sm (L'(S) \cap L'(u))
\]
More importantly, we have the relation $f(S) = |L'(S)|$. Therefore
\begin{gather*}
	f(S \cup \set{u}) - f(S) \ge f(T \cup \set{u}) - f(T)\\
	\overset{(*)}{\iff} (L'(S) \cup L'(u)) \sm L'(S) \supseteq (L'(T) \cup L'(u)) \sm L'(T)\\
	\iff L'(u) \sm (L'(S) \cap L'(u)) \supseteq L'(u) \sm (L'(T) \cap L'(u))
\end{gather*}
which proves that $f$ is submodular. Note that we used the fact that $f(X) = |L'(X)|$ and $f(X) - f(Y) = |L'(X) \sm L'(Y)|$ at $(*)$. This concludes the proof. \qed

\paragraph{Problem 3 - Vertex Cover via DFS} Let $G = (V,E)$ be a simple graph, and $r \in V$ be chosen at random. Suppose $T$ is the depth first search tree rooted at $r$, and $S$ is its internal vertices (including $r$), i.e. $S$ is the set of vertices minus non-root leaves of $T$.\\

We will first show that $S$ is a vertex cover of $G$. Let $e = uv \in E$ be such that $e$ is not covered by $S$. So $\set{u} \cap S = \emptyset = \set{v} \cap S$. Therefore we conclude that $u,v$ are leaves in $T$. Let $w$ be the common ancestors of $u$ and $v$ such that its depth is maximum, we will show that there is a cycle containing $u,v,w$.\\

Let $P_u = u x_2 \cdots x_{n-1} w$ and $P_v = v y_2 \cdots y_{l-1} w$. Then $P_u \cup P_v \cup \set{uv}$ is the cycle desired, since given any two vertex in a tree, the path from one vertex to another vertex is unique. However, this shows a contradiction because by depth first search algorithm, we would go from $u$ to $v$ directly, or vice versa, making $u$ non-leaf, or $v$ non-leaf. Therefore no such $e \in E$ can exists, and $S$ is indeed a vertex cover. We will first prove a lemma about matchings in rooted trees.

\begin{lemma}
	Let $T$ be a tree rooted at $r$ (may be a leaf), and $S$ be its internal vertices including the root $r$. Then there is a matching of size $\ceil{\frac{|S|}{2}}$.
\end{lemma}
\begin{proof}
	Since the decision problem ``Is there a matching of size $k$?" is NP-complete, we will not give an explicit construction; instead we will prove via induction. We will induct on $|S|$. If $|S| = 1$, then $T$ is $K_{1,|V(T)|-1}$, and indeed it has a matching of size $1 = \ceil{\frac{|S|}{2}}$. Therefore the base case is true.\\
	
	Now let the statement to be true whenever we have size of internal vertex $|S| \le n-1$, where $n \in \bN$. Let us define $N(r) = \set{v_i}_{i=1}^{k}$, the vertices of depth 1, and let us denote $B(v) \subseteq V(T)$ by the entire branch of $v$ (including $v$), i.e. $v$ together with its descendants. Consider $T \sm \set{r}$, and let $T_i$ be the component where $v_i$ resides.\\
	
	If we let $v_i$ be the root of $T_i$, then $(T_i,v_i)$ is again a rooted tree. Let $S_i$ be the set of internal vertex union $v_i$, then we have $|S_i| \le |S| - 1$, and $|S| = \sum_{i=1}^{k} |S_i| + 1$, where the 1 comes from the original root $r$. By induction, in $(T_i,v_i)$, there is a matching $M_i$ such that $|M_i| \ge \frac{|S_i|}{2}$.\\
	
	Now let $M' := \bigcup_{i=1}^{k} M_i$, there are two cases we need to consider:
	\begin{enumerate}[(\text{Case} 1)]
		\item There exists $i_0 \in [k]$ such that $v_i$ is not incident to any edge in $M_i$
		
		\item All $i \in [k]$ are such that $v_i$ is incident to some edge $e \in M_i$
	\end{enumerate}
	In case 1, we construct a new matching $M := M' \cup \set{rv_{i_0}}$, then 
	\[
		|M| = 1 + \sum_{i=1}^{k} |M_i| \ge 1 + \sum_{i=1}^{k} \frac{|S_i|}{2} = 1 + \frac{1}{2} (|S| - 1) = \frac{|S|}{2} + \frac{1}{2} > \frac{|S|}{2}
	\]
	and $M$ is the matching that we desired.\\
	
	In case 2, choose an arbitrary subtree $T_i \in \set{T_j}_{j=1}^{k}$. Without loss of generality, assume $T_i = T_1$, and let $T_1',\cdots,T_l'$ be subtrees of $T_1$ after removing $v_i$. Then again by induction, each tree $T_j'$ has a matching $N_j$ such that $|N_j| \ge \ceil{\frac{|T_j'|}{2}}$.\\
	
	Set $M = \bigcup_{j=1}^{l} N_j \cup \bigcup_{i=2}^{k} M_i \cup \set{r v_1}$, we claim that $M$ is a matching since any adjacent pair of edges has to be in $M \sm \bigcup_{i=2}^{k} M_i$ as $M_i$ are matching themselves and $r v_i$ is not in $M$ for $i = 2,\cdots,k$. Moreover, in $M \sm \bigcup_{i=1}^{k} M_i$, no pairs of edges are adjacent, since they are either in $T_j'$, or $r v_1$ itself. Thus $M$ is a matching. Let $S_j'$ be the internal vertices of $T_j'$, then
	\[
		\sum_{j=1}^{l} |S_j'| + \sum_{k=2}^{n} |S_i| = |S| - 2
	\]
	We also have the following chain of inequalities
	\begin{gather*}
		|M| = 1 + \sum_{j=1}^{l} |N_j| + \sum_{i=2}^{k} |M_i| \ge 1 + \sum_{j=1}^{l} \frac{|S_j'|}{2} + \sum_{i=2}^{k} \frac{|S_i|}{2} = 1 + \frac{|S| - 2}{2} = \frac{|S|}{2}
	\end{gather*}
	Combining both cases (case 1 and case 2), we have $|M| \ge \ceil{\frac{|S|}{2}}$. This concludes the lemma.
\end{proof}

Using the lemma, we have a matching $M'$ of size $|M'| \ge \frac{|S|}{2}$. Let $M$ be a maximal matching containing $M$ (could be $M'$ itself), then we have
\[
	\frac{|S|}{2} \le |M'| \le |M| \overset{(*)}{\le} |\opt(G)|
\]
where $\opt(G)$ denotes the optimal (minimal) cover of $G$. Note that $(*)$ holds because at least one vertex of each edge $e \in E(T)$ has to be in the vertex cover $\opt(G)$, therefore $|M| \le |\opt(G)|$. Rearranging, we get $|S| \le 2 |\opt(G)|$, and DFS is indeed a 2 approximation algorithm. \qed

\paragraph{Problem 4 - Server Optimization} Let $\cS = [n]$ be the set of servers, and $\cC = \set{c_i}_{i=1}^{m}$ be the set of clients, and $c_i$ can be served by server $j$ if and only if $j \in S_i$ for $i \in [m]$. So $S_i \subseteq \cS$ for all $i \in [m]$. Let $k \in \bN$ be such that $|S_i| \ge k$ for each $i \in [m]$, i.e. each client has a list of at least $k$ servers who serve them.\\

Let $m_l$ be the number of clients left to be covered at iteration $l$; by definition, we have $m_1 = m$ since we have not chosen any servers yet. We will denote $\Delta_l$ and $\bar{d}_l$ be the maximal degree and average degree of the set of serves at step $l$; for simplicity, we will define $\Delta := \Delta_1$, $\bar{d} := \bar{d}_1$, i.e. the maximal and average degree of the servers before any servers are selected. Then at each step of the algorithm, we will choose the server with degree $\Delta_l$, and remove it from the graph together with all its neighbours (i.e. clients), and repeat until the graph is empty. We now prove a small result using notations we have just defined.

\begin{lemma}
	$m_l \le m(1-\frac{k}{n})^{l}$.
\end{lemma}
\begin{proof}
	First note that in the beginning, the total degree at the server's partition is $n \bar{d}$, and since each client has a preference of at least size $k$, we get that $n\bar{d} \ge m k \equiv \bar{d} \ge \frac{mk}{n}$ In particular, this inequality holds at any step, and we have $\bar{d}_l \ge \frac{m_l k}{n}$.\\
	
	Moreover, at iteration $l$, the number of clients left to cover is 
	\[
		m_l = m_{l-1} - \Delta_l \le m_{l-1} - \bar{d}_l \le m_{l-1} - \frac{m_l k}{n} = m_{l-1} (1 - \frac{k}{n})
	\]
	Applying induction on $l$ gives us indeed $m_l \le m_1 (1-\frac{k}{n})^l = m(1-\frac{k}{n})^l$.
\end{proof}

Now let $l_0 \in \bN$ be such that the last iteration is at $l_0$. This means that $m_{l_0 + 1} = 0$, and $m_{l_0} \ge 1$. Using the lemma, we get
\[
	1 \le m_{l_0} \le m(1-\frac{k}{n})^{l_0} \overset{(*)}{\le} m \cdot \exp(-\frac{k}{n} \cdot l_0)
\]
where $(*)$ is due to $(1+x) \le \exp(x)$ for any $x \in \bR$. Applying $\log$ and rearranging, we get that $l_0 \le \frac{n \cdot \log m}{k} = \frac{n \cdot O(\log m)}{k}$, as we desired.

\paragraph{Problem 5 - Scheduling Quadcore Computer} Let $P_1,\cdots,P_4$ be the processors, and we represent the set of jobs by $[n]$ with processing time $t(j)$ for $j \in [n]$.	Notice that the optimal solution $|\opt| \ge \frac{1}{4} \sum_{j=1}^{n} t(j)$ since in the best case scenario, all the jobs would be distributed evenly across all four processors.\\

Note that using dynamic programming, we can obtain exact solution to this problem. We shall initialise $S(0) = (0,0,0,0) \in \bR^4$, and we use $e_1,\cdots,e_4$ to denote the standard coordinate vectors. Now for each job $j \in [n]$, and $i = 1,\cdots,4$, we can define
\[
	S(k) = \bigcup_{x \in S(k-1)} \set{x + e_i \cdot t(j)}_{i \in [4]}
\]
excluding multiplicity, i.e. $(0,t(j),t(j'),0)$ and $(t(j),0,0,t(j'))$ are considered the same element, but different than $(t(j) + t(j'),0,0,0)$. This means that any $S(k)$ may not contain $(0,t(j),t(j'),0)$ and $(t(j),0,0,t(j'))$ at the same time, but we allow $S(k)$ to contain the elements $(0,t(j),t(j'),0)$ and $(t(j)+t(j'),0,0,0)$ simultaneously. In $S(n)$, we look at the element that minimizes the maximum component; this corresponds to shortest makespan and this proves the correctness of the algorithm.\\

We will now proceed with the approximation algorithm. To do this, we let $M := \sum_{j = 1}^{n} t(j)$. Let $\ee > 0$ be given. We partition the interval $L := [0,M]$ into subinterval of size $\ceil{\frac{4n}{\ee}}$, except for the last interval where it is allowed to have size $< \frac{cn}{\ee}$, call the $i^{th}$ interval $L_i$ for $i = \set{1,\cdots,\ceil{\frac{M\ee}{4n}}}$.\\

For a tuple $x = (x_1,x_2,x_3,x_4)$, let us denote $\max x = \max \set{x_i}_{i=1}^{4}$. Consider $S(0) = (0,0,0,0)$, and for $j \in [n]$, if we are given $S(j-1)$, we can build $S(j)$ by considering all combinations from tuples from $S(j)$. Consider the following set:
\[
	S'(j) = \set{x + t(j) e_i \big| x \in S(j-1), i \in [4]}
\]
Now for $x \in S'(j)$, we assign $x$ to $\max x$ on $[0,M]$, and we obtain $S(j)$ by choosing only one element from each interval $L_i$. Therefore each iteration of $S(j)$ will retain at most $\ceil{\frac{4n}{\ee}}$ tuples. However, this will produce an error of at most $\floor{\frac{M\ee}{4n}}$; this follows from the fact that for any
\[
	\argmax \set{|x - y| : x,y \in \left[ l,l+\ceil{\frac{\ee}{4n}} \right]} = \set{l,l+\ceil{\frac{\ee}{4n}}}
\]
Therefore the total error accumulated at $S(n)$ is $n \floor{\frac{M \ee}{4n}} \le \ee |\opt|$; this means that $- n \floor{\frac{M \ee}{4n}} \ge - \ee |\opt|$, and
\[
	|\opt| - \min \set{\max x: x \in S(n)} \ge |\opt| - n \floor{\frac{M \ee}{4n}} \ge (1-\ee) |\opt|
\]
It remains to show that this algorithm runs in $\poly(n,\frac{1}{\ee})$.\\

Indeed, for each $x \in S(j)$, we need to compute $x + t(j) e_i$ four times, and we have $O(\frac{n}{\ee})$ items, so each iteration runs in $\poly(n,\frac{1}{\ee})$ time; finally, we have $n$ iterations, so the overall run time is $n \cdot \poly(n, \frac{1}{\ee}) = \poly(n,\frac{1}{\ee})$ time, and therefore it is indeed FPTAS.

\paragraph{Problem 6 - Recruiting for Social Welfare} Given $n$ task, $T = \set{t_i}_{i=1}^{n}$, and payoff for $i^{th}$ task, $p(i) \in [P_{\min},P_{\max}]$ for all $i \in [n]$, and $\frac{P_{\max}}{P_{\min}} =: c \in \bR_{+}$. Note that $P_{\min} \ne 0$, for otherwise $\frac{P_{\max}}{P_{\min}}$ does not make any sense. Since we only have $n$ tasks, so we also assume there are only $k \in [n]$ tasks of different payoff.\\

Without loss of generality, we may assume that $P_{\max} = 1$; otherwise we define $P_{\max}' = 1$ and $P_{\min}' := \frac{P_{\min}}{P_{\max}}$, and work with $P_{\min}'$ and $P_{\max}'$ instead. First, notice that let $\opt$ be the optimal configuration of tasks assigned to workers. Then $|\opt| \le \floor{\sum_{i=1}^{n} p(i)}$ since any left over must be distributed among existing workers.\\

Now since each task have payoff at least $P_{\min}$, let $M := \ceil{\frac{1}{P_{\min}}}$, then each worker $j$ has to have at least $M$ tasks assigned to him/her for if $j$ is assigned less than $M$ task (say $N < M$), then we have $N \cdot P_{\min} < M P_{\min} \le 1$, which imply it is not a solution to our problem since we require each worker to get at least $P_{\max} = 1$.\\

Let $R$ be the number of task that we can assign to any worker $j$. Without loss of generality, we may assume there are only $k$ jobs of different payoff since we only have $n < \infty$ jobs in total; clearly $k \le n$ which is again a finite number. Since worker $j$ requires at least $M$ tasks, and there are $k$ different possible payoff for a task, therefore using the solution to the partition problem, we have $R = {M + k \choose k}$. Thus $R = {M + k \choose k} \le (\frac{e(M+k)}{k})^{k}$, and we have at most $(\frac{e(M+k)}{k})^{k}$ many configurations that we need to try to compute the exact configuration of payoff of worker $j$. This problem can then be solved in $O(\frac{1}{c} n^{\frac{k}{c}})$, where $c = P_{\min}$. Call this algorithm $\cA_{c}$.  We will translate this into bin covering problem, with $\cA_{c}$ being the exact algorithm to solve the problem when each payoff (item size) have size at least $c$.\\

Suppose we are given $\ee > 0$. We will work with $\ee' = f(\ee) > 0$ instead, and determine value of $\ee'$ later. We will first consider only jobs of size at least $\ee'$. Set $K := \ceil{\frac{2 P_{\max}}{P_{\min}}}$ and $Q = \floor{\frac{n P_{\min}\ee}{2 P_{\max}}}$. Without loss of generality, we order task $t_i$ in non-decreasing order of $p(i)$, i.e. $p(i) \le p(i+1)$ for all $i \in [n-1]$. Let us call the original instance of social welfare problem $I$. Now divide tasks $T$ into $K$ groups, each with size $Q$. For each group, we round down the payoff of each task to the group minimum. Denote this new group $J_i$, and this new instance of welfare optimization $J$. With similar fashion, we round up payoff of each group to the group maximum, call this new group $J_i'$, and this new instance $J'$. We will first prove a claim.

\begin{lemma}
	$|\opt(J)| \ge |\opt(J')| - Q$
\end{lemma}

Now observe that, trivially, $|\opt(J)| \le |\opt(I)| \le |\opt(J')|$, and $Q \ge n (\ee')^2 \ge \ee' |\opt(I)|$; we have $n$ tasks and the minimum payoff is $\ee'$, therefore $|\opt(I)| \le n \ee'$. We will first proceed with the proof.

\begin{proof}
	(Proof of lemma) Recall that $J_i$ is the round down version of the $i^{th}$ group, and $J_i'$ is the round up version of the $i^{th}$ group. Note that since the tasks are ordered in non-decreasing order, therefore $\max J_i' \le \min J_{i+1}$. Using the configuration of $J_i'$, we can use the configuration of $J_i'$ for group $J_{i+1}$, with the exception of $J_1$ since there is no $J_0'$.\\
	
	For group $J_1$, randomly assign the tasks for any of the group $J_2,\cdots,J_k$; this will only add to total payoff for each worker, hence it is still a valid solution. Now for tasks in group $J_K'$, since we rounded up the payoffs, so the worst case would be that $\max J_K' = P_{\max} = 1$, and each task is assigned to one new worker. Removing these $Q$ tasks in $J_K'$ yields a solution to instance $J$. Hence $|\opt(J)| \ge |\opt(J')| - Q$ as we desired.
\end{proof}

Observe that in the configuration of $J$, at most one worker will have payoff $> 2 P_{\max}$; suppose there are two workers whose payoff is $> 2 P_{\max}$, then each worker has at least 3 jobs assigned. This means that for each worker, there exists a job with $\frac{2}{3} P_{\max} \le p(j) \le P_{\max}$ for some job $j$ assigned to the worker. Hence removing this job from this worker, we will get remaining job has payoff between $[P_{\max},\frac{4}{3} P_{\max}]$. Now since there are two workers, and if we remove jobs of size at least $\frac{2}{3} P_{\max}$, we will be able to hire at least one more worker since the total job removed is $\ge \frac{4}{3} P_{\max}$. Hence at most one worker can have $> 2 P_{\max}$ payoff in the optimal assignment.\\

Now for each worker returned by PTAS of $J$, let $L$ be the collection of jobs $J_1$, and the slacks of each $J_2,\cdots,J_K$, namely the jobs when upon removal will not make $J_i$ less than $P_{\max}$. Let $C_0$ be the total $p(j)$ for all jobs that has payoff less than $\ee$, and all jobs in $L$. Now notice that if $C_0 > P_{\max}$, then we can just add a new worker. So assume $C_0 \le P_{\max}$, then we have
\[
	|\opt(I)| \ge \frac{\sum_{j} p(j) - C_0}{2 P_{\max}} \ge \frac{\sum_{j} p(j) - P_{\max}}{2 P_{\max}} \ge \frac{\sum_{j} p(j)}{2 P_{\max}} - \frac{1}{2} \ge \frac{n P_{\min}}{2 P_{\max}} - \frac{1}{2}
\]
So multiplying $-\ee'$ to the inequality, we get that $- \frac{\ee'}{2} -\ee' |\opt(I)| \le - \frac{n P_{\min} \ee'}{2P_{\max}} \le - Q$. Therefore by the lemma, we get
\[
	|\opt(J)| \ge |\opt(J')| - Q \ge |\opt(I)| - \ee'|\opt(I)|  - \frac{\ee'}{2} = (1-\ee') |\opt(I)| - \frac{\ee'}{2}
\]
Now choosing $\ee' = \frac{\ee}{1+\ee}$, and we get $|\opt(J)| \ge \frac{1}{1+\ee} |\opt(I)| - \frac{\ee}{2(1+\ee)}$, and we are done. \qed

\end{document}