%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% writeLaTeX Example: A quick guide to LaTeX
%
% Source: Dave Richeson (divisbyzero.com), Dickinson College
% 
% A one-size-fits-all LaTeX cheat sheet. Kept to two pages, so it 
% can be printed (double-sided) on one piece of paper
% 
% Feel free to distribute this example, but please keep the referral
% to divisbyzero.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use writeLaTeX: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[10pt,landscape]{article}
\usepackage{hedgehog}


\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}
\pagestyle{empty}
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother
\setcounter{secnumdepth}{1}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}
% -----------------------------------------------------------------------

\title{Summary of Functional Analysis I}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem*{thm}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{coro}{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem*{defn}{Definition}

\begin{document}

\raggedright
\footnotesize

\begin{center}
     \Large{\textbf{Important Theorems and Definitions}} \\
\end{center}
\begin{multicols}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Metric Spaces}
\begin{defn}
	A subset $A \subseteq X$ is \emph{nowhere dense}if one of the following three equivalent conditions, and hence all, are true.
	\begin{enumerate}
		\item $\mathring{\bar{A}} = \emptyset$
		
		\item For all $B \subseteq X$ open ball, $B \setminus \bar{A} \ne \emptyset$
		
		\item For all $U \subseteq X$ open, $U \setminus \bar{A} \ne \emptyset$.
	\end{enumerate}
\end{defn}

\begin{lemma}
	Let $A \subseteq X$. The following are equivalent.
	\begin{enumerate}
		\item $A$ is open and dense;
		
		\item $A^C$ is closed and nowhere dense.
	\end{enumerate}
\end{lemma}

\begin{thm}
	(Baire Category Theorem) Let $(X,d)$ be a \textbf{complete} metric space. Then each one of the following equivalent assertions is true.
	\begin{enumerate}
		\item Given a countable collection of open, dense set, $\{G_n\}_{n \in \bN}$, the intersection of this collection of sets is dense, i.e. $G = \bigcap_{n \in \bN} G_n$ is dense.
		
		\item Let $\{F_n\}_{n \in \bN}$ be a collection of closed, nowhere dense set. Then the union of all these sets has empty interior, i.e. if $F := \bigcup_{n \in \bN} F_n$, then $\mathring{F} = \emptyset$.
		
		\item Let $F = \bigcup_{n \in \bN} F_n$, where $F_n$'s are closed sets. If the interior of $F$ is non-empty, i.e. $\mathring{F} \ne \emptyset$, then there exists $n \in \bN$ such that $\mathring{F}_n \ne \emptyset$.
		
		\item Let $F = \bigcup_{n \in \bN} F_n$. If $F = X$, then there exists $n \in \bN$ such that $\mathring{F}_{n} \ne \emptyset$
	\end{enumerate}
\end{thm}

\begin{thm}
	(Uniform Boundedness Principle)\index{Uniform Boundedness Principle} Let $(X,d)$ be a complete metric space, and $(f_{\lambda})_{\lambda \in \Lambda}$ be continuous maps. If $\exists C_x > 0$ s.t. $|f_{\lambda}(x)| < C_x, ~ \forall \lambda \in \Lambda$, then $\exists B(x_0,r_0) \subseteq X$, $x_0 \in X$, $r_0 > 0$, s.t. $\sup_{\lambda \in \Lambda} \sup_{x \in B} |f_{\lambda} (x)| < \infty$.
\end{thm}

\section{Banach Spaces}
A \emph{Banach spaces} is a complete normed linear space.

\begin{prop}
	Let $(X,\norm{\cdot})$ be a normed space, and $Y \subseteq X$ be a subspace. If $\dim_{\bR} Y < \infty$, then $(Y,\norm{\cdot}_1)$ is complete, and hence $Y \subseteq X$ is closed.
\end{prop}

\subsection{Unit Sphere}
\begin{thm}
	Let $(X,\norm{\cdot})$ is a normed space, and let $S$ be the unit sphere. Then $\dim_{\bR} X < \infty \iff (S,\norm{\cdot})$ is compact.
\end{thm}

\begin{lemma}
	(Riesz Lemma) Let $(X,\norm{\cdot})$ be a normed space, and $\emptyset \subsetneq Y \subsetneq X$ is a closed subspace. $\forall \ee > 0$, $\exists x^* \in X$ s.t.
	\begin{enumerate}
		\item $\norm{x^*} = 1$
		
		\item $d(x^*,y) \ge 1 - \ee$ for all $y \in Y$
	\end{enumerate}
\end{lemma}

\subsection{Continuity \& Convergence}
\begin{prop}
	(Continuity Criteria) Let $(X,\norm{\cdot})$ and $(Y,\norm{\cdot})$ be normed spaces, and let $A : X \to Y$ be linear. The following are equivalent.
		\begin{enumerate}
			\item $A$ is continuous at $0 \in X$.
			
			\item $A$ is continuous at any point $x \in X$.
			
			\item $A$ is uniformly continuous on $X$.
			
			\item $A$ is Lipschitz continuous.
			
			\item $\sup_{\norm{x}_{X} \le 1} \norm{A x}_Y < \infty$.
		\end{enumerate}
\end{prop}

\begin{prop}
	Let $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ be Banach spaces. Then $L(X,Y)$ is Banach.
\end{prop}

\begin{prop}
	(Convergence Criteria) Let $(X, \norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ be Banach spaces. Let $A_k \in L(X,Y), ~ \forall k \in \bN_0$. If $\sum_{k \in \bN} \norm{A_k}_{L(X,Y)} < \infty$, then $\exists \sum_{k=1}^{\infty} A_k \in L(X,Y)$.
\end{prop}

\subsection{Operations on Banach Spaces}
\subsubsection{Product of Banach Spaces}
Let $(X_i,\norm{\cdot}_i)$ be Banach spaces $\forall i = 1,\cdots,n$. Then $(\prod_{i=1}^{n} X_i, \norm{\cdot}_{X_p})$ are Banach spaces, where
\[
	\norm{x}_{X,p} = \begin{cases}
		\left( \sum_{i=1}^n \norm{x_i}_i^p \right)^{\frac{1}{p}} & 1 \le p < \infty\\
		\sup \{\norm{x_1},\cdots,\norm{x_n}\} & p = \infty
	\end{cases}
\]

\subsubsection{Intersection of Banach Spaces}
Let $X$ be a vector space, $Y_1,Y_2 \subseteq X$ are normed subspaces. Let $\norm{\cdot}_Y := \norm{\cdot}_1,\norm{\cdot}_2$. Then $(Y_1 \cap Y_2,\norm{\cdot}_Y)$ is a normed space. If $Y_1,Y_2$ are Banach, then $Y_1 \cap Y_2$ is also Banach.

\subsubsection{Quotient of Banach Spaces}
If $Y \subseteq X$ is closed, and $\norm{[x]}_{X/Y} := \inf_{y \in Y} \norm{x-y}_{X}$. Then $(X/Y,\norm{\cdot}_{X/Y})$ is a normed space. If $X$ is a Banach space then $X/Y$ is also Banach.

\section{Hilbert Spaces}
A \textsl{Hilbert space} is a complete inner product space.

\begin{defn}
	Let $X$ be a complex Hilbert space, and $Y \subseteq X$ be a subspace. Then $Y^{\perp} := \set{x \in X \big| \inp{x}{y} = 0 ~ \forall y \in Y}$.
\end{defn}

\subsection{Projection Lemma}
\begin{thm}
	(Projection Lemma) Let $(X,\inp{\cdot}{\cdot})$ be a Hilbert space, and $Y \subseteq X$ be closed subspace. TFAE
	\begin{enumerate}
		\item $\forall x_0 \in X$, $\exists ! y_0 \in Y$ s.t. $\inp{x_0 - y_0}{y} = 0, \forall y \in Y$.
		
		\item $d(x_0,Y) := \inf_{y \in Y} d(x,y)$ attains its unique minimum at $y_0 \in Y$, and $y_0$ is the unique minimizer.
	\end{enumerate}
\end{thm}

If $X$ is a Hilbert space, and $Y \subseteq X$ is closed subspace, then:
\begin{enumerate}
	\item $X = Y \oplus Y^{\perp}$.
	
	\item $\id_X = \pi_Y \oplus \pi_{Y^{\perp}}$, where $\pi_Y : X \to Y$, $\norm{\pi_Y}_{L(X,Y)} = 1$, $\pi_{Y^{\perp}} : X \to Y^{\perp}$, $\norm{\pi_{Y^{\perp}}}_{L(X,Y^{\perp})} = 1$ are projections.
	
	\item $X/Y \iso Y^{\perp}$.
\end{enumerate}
This can be proven with projection lemma.

\begin{prop}
	Let $X$ be a Hilbert space, and let $Y \subseteq X$ be a subspace. Then $\bar{Y} = Y^{\perp \perp}$; $\bar{Y} = Y \iff Y = \bar{Y} = Y^{\perp \perp}$.
\end{prop}

\subsection{Orthonormal System \& Hilbertean Basis}
\begin{defn}
	Let $(\cH,\inp{\cdot}{\cdot})$ be a real Hilbert space. Then
	\begin{enumerate}
		\item $(e_i)_{i \in I}$ is an \emph{orthonormal family} if $\inp{e_i}{e_j} = \dd_{ij}$.
		
		\item If $I = \bN$, we call $(e_k)_{k \in \bN}$ is an \emph{orthonormal system}.
	\end{enumerate}
\end{defn}

\begin{defn}
	Given a set $B = \{e_k\}_{k \in \bN}$, we say that $B$ is \emph{Hilbertean basis} if $x = \sum_{k \in \bN} \inp{x}{e_k} e_k$ for all $x \in \cH$.
\end{defn}

\begin{thm}
	Let $(\cH,\inp{\cdot}{\cdot})$ be a real Hilbert space, $\{e_k\}_{k \in \bN}$ be an orthonormal system. Then
	\begin{enumerate}
		\item $\sum_{k \in \bN} |\inp{x}{e_k}|^2 \le \norm{x}^2, \forall x \in \cH$.
		
		\item The series $\sum_{k \in \bN} \inp{x}{e_k} e_k \in \cH$ converges.
		
		\item Equality in 1 holds $\iff x = \sum_{k \in \bN} \inp{x}{e_k} e_k$.
	\end{enumerate}
\end{thm}

\begin{thm}
	A Hilbert space $(\cH,\inp{\cdot}{\cdot})$ admits a Hilbertean basis if and only if it is separable.
\end{thm}

\section{Fundamental Principles}
\subsection{Banach Steinhaus Theorem}
\begin{thm}
	(Banach Steinhaus Theorem) Let $X,Y$ be Banach spaces, and let $\set{A_{\lambda}}_{\lambda \in \Lambda} \subseteq L(X,Y)$ be a family of linear operators on the Banach spaces. If $\forall x \in X$, $\exists C_x > 0$ s.t. $\sup_{\lambda \in \Lambda} |A_{\lambda}(x)| \le C_x < \infty$, then $\sup_{\lambda \in \Lambda} \norm{A_{\lambda}}_{L(X,Y)} < \infty$.
\end{thm}

\subsection{Open Mapping Theorem}
\begin{defn}
	Let $X,Y$ be normed spaces. A map $A:X \to Y$ is open if for all $U \in \tau_X$ open, $A(U)$ is open in $\tau_Y$.
\end{defn}

\begin{thm}
	(Open Mapping Theorem) Let $(X,\norm{\cdot}_X$ and $(Y,\norm{\cdot}_Y)$ be Banach spaces. Let $A \in C^0(X,Y)$. Then.
	\begin{enumerate}
		\item If $A$ is surjective, then $A$ is open.
		
		\item If $A$ is a bijection, then $A^{-1} \in L(X,Y)$
	\end{enumerate}
\end{thm}

\subsection{Closed Graph Theorem}
\begin{defn}
	Given two normed spaces, $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$, and $A: D(A) \subseteq X \to Y$. The \emph{graph of $A$} is $\Gamma_A := \set{(x,Ax) \in X \times Y \big| x \in D(A)}$.
\end{defn}

\begin{thm}
	(Closed Graph Theorem) Let $X,Y$ be Banach spaces, $A:X \to Y$ be a linear map. TFAE:
	\begin{enumerate}
		\item $A \in L(X,Y)$
		
		\item $\Gamma_A \subseteq X \times Y$ is closed
	\end{enumerate}
\end{thm}

\subsection{T\"{o}plitz Criterion}
\begin{defn}
	Let $(\cH,\inp{\cdot}{\cdot})$ be a real inner product space, $A: \cH \to \cH$ be a linear map. Then $A$ is \emph{symmetric}\index{Operator! Symmetric Operator} if $\inp{Ax}{y} = \inp{x}{Ay} ~ \forall x,y \in \cH$.
\end{defn}

\begin{thm}
	Let $(\cH,\inp{\cdot}{\cdot})$ real Hilbert space. If a linear map $A: \cH \to \cH$ is symmetric, then it is continuous i.e. $A \in L(\cH)$.
\end{thm}

\subsection{Unbounded Operators}\begin{thm}
	(Continuous Inverse Theorem) Let $A: D(A) \to Y$ be injective and linear with $D(A) \subseteq X$, and $\Gamma_A \subseteq X \times Y$ is closed. If $X$ and $Y$ are Banach, then there exists a continuous map $B : Y \to D(A)$ with $BA = \restr{BA}{D(A)}$ and $AB = \restr{\id}{Y}$.
\end{thm}

\subsection{Closable Operators}
Let $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ are normed spaces. Let $\Gamma \subseteq X \times Y$ be a linear subspace.

\begin{defn}
	A graph $\Gamma$ is a \emph{linear graph} if $(x,y_1), (x,y_2) \in \Gamma \implies y_1 = y_2$.
\end{defn}

\begin{lemma}
	$\Gamma$ is a linear graph $\iff (0,y) \in \Gamma \implies y = 0$.
\end{lemma}

\begin{defn}
	Given $A: D(A) \subseteq X \to Y$ and $B:D(B) \subseteq X \to Y$ are two linear maps, then $B$ is an \emph{extension} of $A$ if $D(B) \supseteq D(A)$ and $\restr{B}{D(A)} = A$.
\end{defn}

\begin{defn}
	Let $A: D(A) \subseteq X \to Y$ be a linear map. Then $A$ is \emph{closable} if $\exists \bar{A} \supseteq A$ s.t. $\Gamma_{\bar{A}} \subseteq X \times Y$ is a linear graph. Then $\bar{A}$ is the unique closure of $A$ associated to $\Gamma_{\bar{A}}$.
\end{defn}

\begin{prop}
	Let $A$ be a linear operator. $A$ is closable $\iff \forall (x_k)_k \in D_A^{\bN}$ s.t. $x_k \to 0$, $Ax_k \to y \implies y = 0$.
\end{prop}

\begin{prop}
	Let $A :D(A) \to Y$ be continuous. Then $A$ is closable.
\end{prop}

\section{Hahn-Banach Theorems}
\begin{defn}
	Let $X$ be a real vector space. A map $p: X \to \bR$ is \emph{sublinear} if
	\begin{enumerate}
		\item $p(x+y) \le p(x) + p(y)$ for all $x,y \in X$
		
		\item $p(\alpha x) = \alpha p(x)$ for all $x \in X$, and for all $\alpha \ge 0$.
	\end{enumerate}
\end{defn}

\begin{thm}
	(Hahn-Banach over $\bR$) Let $X$ be a real vector space, $M \subseteq X$ be a vector subspace. If $p:X \to \bR$ is sublinear and $f:M \to \bR$ is linear, s.t. $f(x) \le p(x) ~ \forall x \in M$, then $\exists F:X \to \bR$ \textbf{linear} s.t. $\restr{F}{M} = f$ and $F(x) \le p(x), ~ \forall x \in X$.
\end{thm}

\begin{coro}
	Let $(X,\norm{\cdot})$ be a normed space, and $f:M \to \bR$ be linear and continuous. Then there $\exists F:X \to \bR$ linear s.t. $\restr{F}{M} = f$ and $\norm{F}_{L(X,\bR)} = \norm{f}_{L(M,\bR)}$.
\end{coro}

\begin{defn}
	Suppose $X$ is a  complex vector space. A map $p:X \to \bR$ is $\bC$-sublinear if
	\begin{enumerate}
		\item $p(x+y) \le p(x) + p(y), ~ \forall x,y \in X$
		
		\item $p(\alpha x) = |\alpha| p(x), ~ \forall x \in X, ~ \forall \alpha \in \bC$.
	\end{enumerate}
\end{defn}

\begin{thm}
	Let $X$ be a complex vector space, and $M \subseteq X$ be a vector subspace. If $p:X \to \bR$ is $\bC$-sublinear and $f:M \to \bC$ is $\bC$-linear, s.t. $|f(x)| \le p(x), ~ \forall x \in M$, then $ \exists F:X \to \bC$ a $\bC$-linear map s.t. $\restr{F}{M} = f$ and $|F(x)| \le p(x), ~ \forall x \in \bC$
\end{thm}

\begin{coro}
	Let $(X,\norm{\cdot})$ be a normed space, and $f:M \to \bC$ be linear and continuous. Then there $\exists F:X \to \bC$ linear s.t. $\restr{F}{M} = f$ and $\norm{F}_{L(X,\bC)} = \norm{f}_{L(M,\bC)}$.
\end{coro}

\subsection{Dual Spaces}
Let $(X,\norm{\cdot}_X)$ be a normed space.
\begin{prop}
	(Abundance Lemma) Given $x \in X$, $\exists x^* \in X^*$ s.t. $\inp{x^*}{x}_{X^* \times X} = \norm{x}_X^2 = \norm{x^*}_{X^*}^2 $
\end{prop}

\begin{prop}
	(Dual characterisation of norms).
	\begin{enumerate}
		\item $\norm{x}_X = \sup_{\norm{x^*}_{X^*} \le 1} |\inp{x^*}{x}|$
		
		\item $\norm{x^*}_{X^*} = \sup_{\norm{x}_{X} \le 1} |\inp{x^*}{x}|$
	\end{enumerate}
\end{prop}

\subsection{Geometric Forms of Hahn-Banach}
\begin{prop}
	Let $(X,\norm{\cdot}_X)$ be a normed space. Given any $x,y \in X$, s.t. $x \ne y$, $\exists l \in X^*$ s.t. $l(x) \ne l(y)$.
\end{prop}

\begin{prop}
	Let $(X,\norm{\cdot}_X)$ be a normed space, $M \subseteq X$ be a closed subspace. Suppose $x_0 \in X \setminus M$ s.t. $d = d(x_0,M)$, then $\exists l \in X^*$ s.t. $\norm{l}_{X^*} = 1$, $l(x_0) = d$, and that $\restr{l}{M} = 0$.
\end{prop}

\begin{defn}
	Let $A \subseteq X$ be a subset. The \emph{annihilator} of $A$ is $\ann(A) = A^{\perp} = \set{f \in X^* : \restr{f}{A} = 0}$.
\end{defn}

\begin{lemma}
	Let $M \subseteq X$ be a subspace. Then $M^{\perp} = \bar{M}^{\perp}$.
\end{lemma}

\begin{defn}
	Given $L \subseteq X^*$ a subset. The \emph{dual annihilator} of $L$ is $\prescript{\perp}{}{L} = \set{x \in X: \forall l \in L, l(x) = 0}$.
\end{defn}

\begin{prop}
	For all $M \subseteq X$ subspace, $\prescript{\perp}{}{(M^{\perp})} = \bar{M}$.
\end{prop}

\begin{prop}
	Let $M \subseteq X$ be a subspace, and $x_0 \in X$. Then $x_0 \in \bar{M} \iff \forall f \in M^{\perp}, ~ f(x_0) = 0$.
\end{prop}

\begin{prop}
	Let $M \subseteq X$ be a linear subspace. Then $\bar{M} = X \iff M^{\perp} = \{0\}$.
\end{prop}

\section{Duality \& Convexity}
\subsection{Duality of Hilbert Spaces}
\begin{thm}
	(Riesz Representation Theorem) $\forall l \in \cH^*$, $\exists ! y \in \cH$ s.t. $l(x) = \inp{x}{y}_{\cH}, ~ \forall x \in \cH$.
\end{thm}

\begin{thm}
	(Lax-Milgram Theorem) Let $a: \cH \times \cH \to \bR$ be a continuous bilinear form, s.t. $a(x,x) \ge \lambda \norm{x}^2, ~ \forall x \in \cH$. Then $\exists A \in L(\cH)$ s.t. $a(x,y) = \inp{Ax}{y}_{\cH}, ~ \forall x,y \in \cH$. Furthermore, $\norm{A}_{L(\cH)} \le \Lambda$, and $\norm{A^{-1}}_{L(\cH)} \le \lambda^{-1}$.
\end{thm}

\begin{coro}
	Let $\cH$ be a real Hilbert space, and $a: \cH \times \cH \to \bR$ as above, then $\exists l \in \cH^*$ s.t. $\exists ! x \in \cH$ s.t. $l(y) = a(x,y), ~ \forall y \in \cH$.
\end{coro}

\subsection{Duality for $L^p$-Spaces}
\begin{thm}
	Let $\Omega \subseteq \bR^n$ be an open subset, $\mu$ be a Radon measure. Given $1 \le p < \infty$, there is an surjective isometry $L^p(\Omega))^* \iso L^q(\Omega)$ where $\frac{1}{p} + \frac{1}{q} = 1$.
\end{thm}

\subsection{Convexity}
\begin{thm}
	(Separation of Convex Sets) Let $(X,\norm{\cdot})$ be a real valued normed space. Let $A,B$ be disjoint, non-empty, convex subsets. Then
	\begin{enumerate}
		\item If $A$ is open, then $\exists l \in X^*$ and $\lambda \in \bR$ s.t. $\forall a \in A$ and $\forall b \in B$, $l(a) < \lambda \le l(b)$.
		
		\item If $A$ is compact, and $B$ is closed, then $\exists l \in X^*$ and $\lambda \in \bR$ s.t. $\sup_{a \in A} l(a) < \lambda < \inf_{b \in B} l(b)$.
	\end{enumerate}
\end{thm}

\begin{defn}
	Let $C \subseteq X$ be a convex set that contains $0$. The \emph{Minkowski functional} is a map $p:X \to \bR$, $x \mapsto \inf \set{\lambda > 0: x \in \lambda C}$.
\end{defn}

\begin{lemma}
	The Minkowski functional is $\bR$-sublinear.
\end{lemma}

\section{Extremal Points}
\begin{defn}
	Let $X$ be a normed space, and $K \subseteq X$. A subset $M \subseteq K$ is \emph{extremal} for $K$ if $\forall \alpha \in (0,1)$ and $\forall x_0,x_1 \in K$, $x_{\alpha} = \alpha x_1 + (1-\alpha) x_2 \in M \implies x_0,x_1 \in M$. A point $x \in K$ is called \emph{extremal point} if $\set{x}$ is an extremal subset for $K$.
\end{defn}

\begin{lemma}
	Let $X$ be a normed space, and $K \subseteq X$. If $M \subseteq K$ is an extremal subset of $K$ and $L \subseteq M$ is an extremal subset of $M$, then $L$ is an extremal subset of $K$.
\end{lemma}

\begin{lemma}
	Let $(X,\norm{\cdot})$ be a real normed space, $K \subseteq X$ be compact subset, $l \in X^*$. If $\lambda = \min_{x \in K} l(x)$, then $K_{\lambda} = \set{x \in K : l(x) = \lambda}$ is an extremal subset of $K$.
\end{lemma}

\begin{thm}
	(Krein Milman I) Suppose $X$ is a normed space, and $\emptyset \ne K \subseteq X$ is compact. Then $K$ has extremal points.
\end{thm}

\begin{defn}
	Let $A \subseteq X$. The \emph{(closed) convex hull} of $A$ is the intersection of all closed, convex subset containing $A$.
\end{defn}

\begin{thm}
	(Krein Milman II) Let $(X,\norm{\cdot})$ be a real normed space. If $K$ is compact and convex, then $K = \bar{\conv(\ext(K))}$.
\end{thm}

\section{Weak \& Strong Topologies}
\subsection{Weak Topology}
\begin{defn}
	Let $(X,\norm{\cdot})$ be a real normed space. A sequence $(x_k)_k$ in $X$ \emph{converges weakly}\index{Convergence! Weak-Convergence}, written $x_k \xrightarrow[]{W} x \in  X$, if for all $l \in X^*$, $l(x_k) \to l(x)$.
\end{defn}

\begin{thm}
	(Lower Semi-Continuity of Norm w.r.t. $w$-Convergence) Suppose $x_k \xrightarrow[]{w} x$. Then $(x_k)_k$ is bounded, and $\norm{x} \le \liminf_{k \to \infty} \norm{x_k}$.
\end{thm}

\begin{defn}
	Given a normed space $(X,\norm{\cdot})$, the \emph{weak topology} on $X$, $\tau_w$, is the smallest (coarsest) topology s.t. all sets of the form $\Omega_{l,U} = l^{-1}(U)$ are open for all $l \in X^*$, and $U \subseteq \bR$ open.
\end{defn}

Here are some facts about the weak topology $\tau_W$.
\begin{enumerate}
	\item $\tau_w$ is Hausdorff.
	
	\item $\tau_w \subseteq \tau_{\norm{\cdot}}$, where $\tau_{\norm{\cdot}}$ is norm induced Banach topology.
	
	\item $\tau_w$ is never metrizable  if $\dim X = \infty$, i.e. no metric can induce the topology $\tau_w$.
\end{enumerate}

\begin{prop}
	Let $(S,\tau)$ be a topological space, $T \subseteq S$. If $T$ is closed, then $T$ is sequentially closed. Converse is not true.
\end{prop}

\begin{lemma}
	$\bar{\Omega}_{w,seq} \supseteq \bar{\Omega}_w \supseteq \bar{\Omega}$
\end{lemma}

\begin{defn}
	A sequence $(l_k)_k$ in $X^*$ $w^*$-converges if for all $x \in X$, $l_k(x) \to l(x)$.
\end{defn}

We now have three notions of convergence on $X^*$.
\begin{enumerate}
	\item (Strong convergence - Convergence in norm) $l_k \to l$ if $\norm{l - l_k}_{X^*} \to 0$.
	
	\item (Weak Convergence) $l_k \xrightarrow[]{w} l$ if for all $w \in X^{**}$, $w(l_k) \to w(l)$.
	
	\item ($\text{Weak}^*$ Convergence - Pointwise Convergence) $l_k \xrightarrow[]{w^*} l$ if for all $x \in X$, $l_k (x) \to l(x)$.
\end{enumerate}
By definition, $[l_k \to l] \implies [l_k \xrightarrow[]{w} l] \implies [l_k \xrightarrow[]{w^*} l]$.

\subsection{Closure \& Convexity}
Let $(X,\norm{\cdot})$ be a real normed space.

\begin{thm}
	Let $\Omega \subseteq X$ be convex. Then $\bar{\Omega} \equiv \bar{\Omega}_W \equiv \bar{\Omega}_{w,seq}$.
\end{thm}

\section{Reflexivity \& Separability}
\subsection{Reflexivity}
Let $(X,\norm{\cdot}_X)$ be a real normed space. There is a canonical map, $\cI: X \to X^{**}$, defined by $x \mapsto \cI_x$ where $\cI_x: X^* \to \bR$, $\cI_x (l) = l(x)$.

\begin{prop}
	The map $\cI$ is a \emph{linear isometry}.
\end{prop}

\begin{defn}
	The normed space $(X,\norm{\cdot}_X)$ is called \emph{reflexive} if $\cI: X \to X^{**}$ is surjective, i.e. $X \iso X^{**}$ are isomorphic.
\end{defn}

\begin{prop}
	If $X$ is reflexive, then $X^*$ is reflexive. If $X^*$ is reflexive, and $X$ is complete, then $X$ is reflexive.
\end{prop}

\begin{prop}
	Suppose $X$ is reflexive, and $Y \subseteq X$ be a closed subspace. Then $Y$ is reflexive.
\end{prop}

\subsection{Separability}
\begin{defn}
	Let $(M,d)$ be a metric space. We say that $(M,d)$ is \emph{separable} if it contains a countable dense subset.
\end{defn}

\begin{prop}
	Let $(M,d)$ be a separable metric space. Let $A \subseteq M$ be a subset. Then $(A, \restr{d}{A \times A})$ is separable.
\end{prop}

\begin{prop}
	Let $(X,\norm{\cdot}_X)$ be a real normed space.
	\begin{enumerate}
		\item If $X^*$ is separable, then so is $X$.
		
		\item If $X$ is separable and reflexive, then $X^*$ is separable.
	\end{enumerate}
\end{prop}

\subsection{Application of Reflexivity \& Separability}
Let $(X,\norm{\cdot}_X)$ be a real normed space, consider $X^*$, $X^{**}$, and $\cI:X \to X^{**}$.

\begin{thm}
	(Banach-Alaoglu Theorem) Let $(X,\norm{\cdot}_X)$ be separable, let $(l_k)_k$ be a \textbf{bounded} sequence in $X^*$. Then there exists $\Lambda \subseteq \bN$ unbounded, and $l \in X^*$ such that $l_k \xrightarrow[k \in \Lambda]{w^*} l$.
\end{thm}

\begin{thm}
	(Eberlein-\v{S}mulian Theorem) Let $(X,\norm{\cdot}_X)$ be a reflexive normed space. Let $(x_k)_k$ be a sequence in $X$ that is bounded, i.e. $\norm{x_k}_{X} \le C$ for all $k \in \bN$. Then there exists $x \in X$ and $\Lambda \subseteq \bN$ such that $x_k \xrightarrow[]{w} x$ as $k \to \infty$, $k \in \Lambda$.
\end{thm}

\begin{thm}
	(Approximation Theorem) Let $X$ be a reflexive normed space, let $M \subseteq X$ be a non-empty closed, and convex subset, and $x_0 \in X \setminus M$. Then $\exists m_0 \in M$ s.t. $\norm{x_0 - m_0}_X = \inf_{m \in M} \norm{x_0 - m}_X$.
\end{thm}

\begin{prop}
	(Mazur's Lemma) Let $(x_k)_k$ be a sequence of $X$ with $x_k \xrightarrow[]{w} x$. Then there exists a sequence of convex linear combination $y_l := \sum_{k=1}^{N(l)} a_{kl} x_k$, with $\sum_{k=1}^{N(l)} a_{kl} = 1$, with $x_l \to x$ as $l \to \infty$.
\end{prop}

\subsection{Lower Semicontinuity}
Let $(X,\norm{\cdot}_X)$ be a real normed space, and $M \subseteq X$ be a subspace, and $F: M \to \bR$ is a (not necessarily continuous) functional.

\begin{defn}
	$F$ is weakly sequentially \emph{lower semi-continuous} if for any sequence $(x_k)_k$ in $M$ such that $x_k \xrightarrow[]{w} x_0$, one has $F(x_0) \le \liminf_{k} F(x_k)$.
\end{defn}

\begin{defn}
	A functional $F: M \to \bR$ is called \emph{coercive} if $\lim_{\norm{x} \to \infty} F(x) = \infty$.
\end{defn}

\begin{thm}
	Let $X$ be reflexive, $M \subseteq X$ be a weakly sequentially closed subspace, and $F:M \to \bR$ is a coercive, $w$-sequential lower semi-continuous functional. Then $F$ has a global minimum on $M$.
\end{thm}

\section{Dual \& Compact Operators}
\subsection{Dual Operators}
\begin{defn}
	Let $X,Y$ be normed spaces, $A:X \to Y$ is a linear operator. The \emph{dual operator} of $A$ is $A^*: Y^* \to X^*$ such that $\inp{y^*}{Ax}_{Y^* \times Y} = \inp{A^* y^*}{x}_{X^* \times X}$.
\end{defn}


\begin{thm}
	Let $X,Y$ be real normed spaces. If $A \in L(X,Y)$, then $A^* \in L(Y^*,X^*)$ with $\norm{A}_{L(X,Y)} = \norm{A^*}_{L(Y^*,X^*)}$.
\end{thm}

\begin{lemma}
	Suppose $X,Y$ be normed spaces. Let $A: D_A \subseteq X \to Y$ be a linear operator, $\bar{D}_A = X$. Then
	\begin{enumerate}
		\item $A^*: D_{A^*} \subseteq Y^* \to X^*$ has closed graph.
		
		\item $A \subseteq B \implies B^* \subseteq A^*$
	\end{enumerate}
\end{lemma}

\begin{prop}
	(Closed Range Theorem) Let $X,Y$ be Banach spaces, $A: D_A \subseteq X \to Y$ be a linear operator, $\bar{D}_A = X$, $\Gamma_A \subseteq X \times Y$ is closed. The following are equivalent.
	\begin{enumerate}
		\item $\im(A)$ is closed in $Y$
		
		\item $\im(A^*)$ is closed in $X^*$
		
		\item $\im(A) = \prescript{\perp}{}{\ker(A^*)} = \set{y \in Y: ~ \forall y^* \in \ker(A^*), \inp{y^*}{y} = 0} \subseteq Y$
		
		\item $\im(A^*) = \ker(A)^{\perp} = \set{x^* \in X^*: \forall x \in \ker(A), ~ \inp{x^*}{x} = 0} \subseteq X^*$
	\end{enumerate}
\end{prop}

\begin{thm}
	(Solvability Criterion) Let $X,Y$ be Banach spaces, $A: D_A \subseteq X \to Y$ be a linear operator, $\bar{D}_A = X$, $\Gamma_A \subseteq X \times Y$ is closed. The following are equivalent:
	\begin{enumerate}
		\item $A$ is surjective
		
		\item $A^*$ is injective and $\im(A^*)$ is closed
		
		\item There exists $c_0 > 0$ and for all $y^* \in D_{A^*}$ such that $c_0 \norm{y^*}_{Y^*} \le \norm{A^* y^*}_{X^*}$.
	\end{enumerate}
\end{thm}

\subsection{Compact Operators}
\begin{defn}
	Let $X,Y$ be real normed spaces. $T \in L(X,Y)$ is \emph{compact} if $\bar{T(B_X(0,1))}$ is compact in $Y$.
\end{defn}

\begin{lemma}
	If $T \in K(X,Y)$, then $x_k \xrightarrow[]{w} x \implies Tx_k \to Tx$.
\end{lemma}

\begin{thm}
	Let $(X,\norm{\cdot})$ be a Banach space, and $T \in K(X)$. Then $\im(\id - T)$ is closed; in particular, the following holds:
	\begin{enumerate}
		\item $\im(\id - T) = \prescript{\perp}{}{\ker((\id - T)^*)}$
		
		\item $\ker(\id - T) = \im((\id - T)^*)^{\perp}$
	\end{enumerate}
\end{thm}

\begin{defn}
	Let $X$ be a normed space. A subset $Y \subseteq X$ is \emph{uniformly bounded} if $\exists C > 0$ s.t. $\sup_{x \in Y} \norm{x}_{X} = C < \infty$.
\end{defn}

\begin{defn}
	Let $X,Y$ be a normed space. A family of functions $\set{f_{\lambda}}_{\lambda \in \Lambda}$ is \emph{uniformly equicontinuous} if $\forall \ee > 0$, $\exists \dd > 0$ s.t. $\norm{x-y}_X < \dd \implies \norm{f_{\lambda}(x) - f_{\lambda}(y)}_{Y} < \ee$, $\forall x,y \in X$ and all $\lambda \in \Lambda$.
\end{defn}

\begin{thm}
	(Arzel\`{a}-Ascoli Theorem) Let $\Omega \subseteq \bR^n$ be an open bounded set. $F \subseteq C^0(\bar{\Omega})$ is a subset. The following are equivalent.
		\begin{enumerate}
			\item $F$ is \emph{relatively compact}
			
			\item $F$ is a uniformly bounded in $C^0(\bar{\Omega})$, and $F$ is uniformly equicontinuous in $C^0(\bar{\Omega})$.
		\end{enumerate}
\end{thm}

\begin{thm}
	(Frech\'{e}t-Kolmogorov Theorem)  Let $\Omega \subseteq \bR^n$ be an open bounded subset, and $1 \le p < \infty$, and $F \subseteq L^p(\Omega)$. The following are equivalent:
		\begin{enumerate}
			\item $F$ is relatively compact.
			
			\item $F$ is uniformly bounded in $L^p$, and $F$ is uniformly equicontinuous in $L^p$.
		\end{enumerate}
		So $f: \Omega \to \bR$ can be extended and equal on $\bR^n \setminus \Omega$, and $T_nf(x) := f(x+h)$.
\end{thm}

\subsection{Adjunctions in Hilbert Spaces}
Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space.

\begin{defn}
	Let $A: D_A \subseteq \cH \to \cH$. The \emph{adjoint} $A^* \supseteq A$ is a map $D_{A^*} \to \cH$ s.t. $\inp{A^* y}{x}_{\cH} = \inp{y}{Ax}_{\cH}$, $\forall x,y \in \cH$.
\end{defn}

\begin{defn}
	An operator $A: D_A \subseteq \cH \to \cH$ is called
	\begin{enumerate}
		\item  \emph{Symmetric} if $D_A \subseteq D_{A^*}$ and $\restr{A^*}{D_A} = A$.
		
		\item \emph{Self-adjoint} if $D_A = D_{A^*}$ and $\restr{A^*}{D_A} = A$.
	\end{enumerate}
\end{defn}

\section{Spectral Theory}
Let $(X,\norm{\cdot})$ is a complex Banach space, and $A:D_A \subseteq X \to X$ be a linear operator.

\subsection{Spectrum \& Resolvent}
\begin{defn}
	The \emph{resolvent set} $\rho(A)$ of $A$ is the set 
	\[
		\set{\lambda \in \bC \big| (\lambda I - A) \in L(D_A,X), (\lambda I - A)^{-1} \in L(X)}
	\]
	and the \emph{spectrum} of $A$ is $\sigma(A) = \bC \setminus \rho(A)$.
\end{defn}

\begin{defn}
	The \emph{resolvent} of $A$ is the map $R: \rho(A) \to L(X)$, $\lambda \mapsto (\lambda I - A)^{-1}$.
\end{defn}

\begin{thm}
	If $z_0 \in \rho(A)$, then $\rho(A) \supseteq B_{\bC}(z_0,\norm{R z_0}_{L(X)}^{-1})$ and $\rho(A)$ is open.  $\forall z \in \rho(A)$, $d(z,\rho(A)) \ge \norm{R z}_{L(X)}^{-1}$. The map $\rho(A) \to L(X)$, $z \mapsto Rz$ is continuous.
\end{thm}

\begin{prop}
	(Basic Resolvent Identities) Let $\lambda, \mu \in \rho(A)$.
	\begin{enumerate}
		\item $R (\lambda) A \subseteq A R (\lambda) = \frac{\lambda I - \lambda I + A}{\lambda I - A} = \lambda R (\lambda) - \id_X \in L(X)$
		
		\item $R(\lambda) - R(\mu) = (\mu - \lambda) R(\mu) R(\lambda)$
		
		\item $R(\mu) R(\lambda) = R (\lambda) R(\mu)$
	\end{enumerate}
\end{prop}

\subsection{Structure of the Spectrum}
Let $(X,\norm{\cdot}_X)$ be a complex Banach space, $A: D(A) \to X$ be a linear operator, where $\bar{D(A)} = X$. We can decompose $\sigma(A)$ into three components, namely $\sigma(A) = \sigma_p(A) \sqcup \sigma_c(A) \sqcup \sigma_r(A)$.

\begin{defn}
	\begin{description}
		\item[Point Spectrum\index{Spectrum! Point Spectrum}] $\sigma_p(A) = \set{\lambda \in \bC: \text{$(\lambda I - A): D_A \to X$ is not injective}}$
		
		\item[Continuum Spectrum\index{Spectrum! Continuum Spectrum}] $\sigma_c(A) = \set{\lambda \in \bC: \substack{\text{$(\lambda I - A): D_A \to X$ is injective, with}\\ \text{dense image (but not all) in $X$.}}}$
		
		\item[Residual Spectrum\index{Spectrum! Residual Spectrum}] $\sigma_r(A) = \sigma(A) \setminus (\sigma_p(A) \sqcup \sigma_c(A)) = \set{\lambda \in \bC: \substack{\text{$(\lambda I - A) : D_A \to X$ is injective,}\\ \text{but image is not dense.}}}$
	\end{description}
\end{defn}

\begin{defn}
	Let $A \in L(X)$, and $r_A = \lim_{n \to \infty} \norm{A^n}^{\frac{1}{n}}$. The number $r_A$, if exists, is call the \emph{spectral radius of $A$}. We can check that $r_A = \inf_{n \in \bN} \norm{A^n}^{\frac{1}{n}}$.
\end{defn}

\begin{prop}
	Let $A \in L(X)$ with $r_A < 1$. There exists $(\idf - A)^{-1} \in L(X)$.
\end{prop}

\begin{thm}
	Let $A \in L(X)$ . Then $\rho(A), \sigma(A) \ne \emptyset$, and
	\begin{enumerate}
		\item If $|z| > r_A := \lim_n \norm{A^n}^{\frac{1}{n}}$, then $z \in \rho(A)$
		
		\item $r_A = \sup_{z \in \sigma(A)} |z|$
	\end{enumerate}
	So $\sigma(A) \subseteq \bC$ is closed and bounded, therefore compact.
\end{thm}

\subsection{Spectral Calculus}
Let $X$ be a complex Banach space, $A \in L(X)$, and $\sigma(A) \subseteq \Omega$ is open and bounded.

\begin{thm}
	(Spectral Mapping Theorem) Given a function $f(z) = p(z) = \sum_{m = 1} c_m (\alpha_m - z)^{k_m}$ with $p \in \bC[z]$, $c_m \in \bC$, $k_m \in \bN$, and $\alpha_m \in \bC$, $\alpha_m \notin \Omega$. Then
	\begin{enumerate}
		\item $f(A) \in GL(X)$ if and only if for all $z \in \sigma(A)$, $f(z) \ne 0$.
		
		\item $\sigma(f(A)) = f(\sigma(A))$.
	\end{enumerate}
\end{thm}

\subsection{Spectral Theory in Hilbert Spaces}
Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space, $A: D_A \subseteq \cH \to \cH$ be a densely defined operator.

\begin{prop}
	Let $A$ be a symmetric (i.e. $A \subseteq A^*$), then its eigenvalues are real, i.e. $\sigma_p(A) \subseteq \bR$.
\end{prop}

\begin{thm}
	Let $A \subseteq A^*$ be symmetric. TFAE:
	\begin{enumerate}
		\item $A$ is self-adjoint, i.e. $A = A^*$;
		
		\item $\sigma(A) \subseteq \bR$
		
		\item $\exists z_1,z_2 \in \rho(A)$, w.t. $\im(z_1) < 0 < \im(z_2)$.
	\end{enumerate}
\end{thm}

\subsection{Spectral Theorem for Bounded Operators}
Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space, $T \in L(\cH)$ be a bounded linear operator.

\begin{defn}
	We say that
	\begin{itemize}
		\item $T$ is \emph{normal}\index{Operators! Normal Operator} if $TT^* = T^* T$;
		
		\item $T$ is \emph{unitary}\index{Operators! Unitary Operator} if $T^* = T^{-1}$;
		
		\item $T$ is \emph{self-adjoint}\index{Operators! Self-Adjoint Operators} if $T = T^*$
	\end{itemize}
	If $T$ is unitary or self-adjoint, then $T$ is normal.
\end{defn}

\begin{prop}
	$T^* = T^{-1} \iff \lambda \in \sigma_p(T) \implies |\lambda| = 1$
\end{prop}

\begin{prop}
	If $T \in L(\cH)$ is normal, then $\norm{T}_{L(\cH)} = r(T)$.
\end{prop}

\begin{thm}
	(Spectral Theorem) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be infinite dimensional, $T$ be self-adjoint and compact. Then $\forall k \in \bN$, $\exists \lambda_k \in \bR \setminus \{0\}$, s.t. $\lambda_k \to 0$ as $k \to \infty$. Moreover, the associated eigenvectors $e_k$ are such that
	\[
		\cH = \ker(T) \oplus^{\perp} \bar{\Span_{\bR} \set{e_k: k \in \bN}}
	\]
	Where $Tx = \sum_{n \in \bN} \lambda_k \inp{x}{e_k} e_k$.
\end{thm}

\begin{defn}
	$T$ is \emph{positive definite} if $\inp{Tx}{x}_{\cH} > 0$, $\forall x \in \cH$.
\end{defn}

\begin{prop}
	Assume $T$ is positive definite, then in the previous theorem, we can order the eigenvalues s.t. $\lambda_k \ge \lambda_{k+1}$ for all $k \in \bN$, $\lambda_k \to 0$, and that
	\[
		\lambda_k = \sup_{\substack{M \subseteq \cH\\ \dim_{\bC} M \ge k}} \inf_{\substack{x \in M\\ \norm{x} = 1}} \inp{x}{Tx}_{\cH}
	\]
\end{prop}

\end{multicols}

\end{document}
