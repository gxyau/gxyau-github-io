\contentsline {section}{\numberline {1}Linear Regression}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Simple Linear Regression}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Parameter Estimation}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Multiple Linear Regression}{1}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Summary}{2}{subsection.1.4}
\contentsline {section}{\numberline {2}Techniques in Regressions}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Variances \& R Square Value}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Categorical Variables}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Cross Validations}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}$K$-Nearest Neighbours (KNN)}{6}{subsection.3.1}
\contentsline {section}{\numberline {4}Bootstrapping}{8}{section.4}
