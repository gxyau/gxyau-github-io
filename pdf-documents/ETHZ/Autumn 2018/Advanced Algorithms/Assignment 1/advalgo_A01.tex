\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Advanced Algorithms - Series 1}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Advanced Algorithms - Series 1}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Problem 1.1} Let $\cU$ be an $n$-element set, $f: 2^{\cU} \to \bR_{+}$ is submodular function that determines our profit. Consider the following greedy algorithm
\begin{algorithm}
	\caption{Greedy Algorithm for Submodular Function $f$}
	\begin{algorithmic}[1]
		\State $S \gets \emptyset$
		
		\While{$|S| < k$}
			
			$u = \argmax_{v \in \cU} \set{f(S \cup \set{v}) - f(S)}$
			
			$S \gets S \cup \set{u}$
			
			$\cU \gets \cU \sm \set{u}$
			
		\EndWhile
	\end{algorithmic}
\end{algorithm}
By the end of the algorithm, $|S| = k$, and it is indeed a solution to the problem. By results proven by Nemhauser, Wolsey, Fisher in 1978, greedy algorithm for submodular functions has approximation factor $\alpha = (1-\frac{1}{e})$.\\

More precisely, let $S \subseteq T$. Then by submodularity, we have $f(T) - f(S) \le \sum_{x \in T \sm S} \Delta(x,S)$, where $\Delta(x,S) = f(S \cup \set{x}) - f(S)$, since for any $x,y \in T \sm S$, we have
\[
	f(S \cup \set{x,y}) - f(S \cup \set{y}) \le f(S \cup \set{y}) - f(S)
\]
Then at step $i$, we have $1 - f(S_{i+1}) \le (1-\frac{1}{k}) (1-f(S_i))$ using the inequality above. \qed

\paragraph{Problem 1.2} Let $G = (V,E)$ with $|V| = n$. For each $S \subseteq V$ and each $v \in V$, let us define
\[
	x_S(v) = \begin{cases}
		1 & \text{$v \notin S$ and has no neighbours in $S$}\\
		0 & \text{$v \in S$ or $v$ has a neighbour in $S$}
	\end{cases}
\]
Now define a function $p: 2^V \to \bZ_{\ge 0}$ by $p(S) = \sum_{v \in V} x_S(v)$. The algorithm is as follows:
\begin{algorithm}
	\caption{Greedy Algorithm for CDS}
	\begin{algorithmic}[1]
		\State $S \gets \emptyset$
		
		\While{$p(S) > 0$}
			
			$u = \argmax_{v \in V} \set{p(S \cup \set{v}) - p(S)}$
			
			$S \gets S \cup \set{u}$
			
		\EndWhile
	\end{algorithmic}
\end{algorithm}
The resulting set $S$ is clearly a dominating set, since there is all vertex are either in $S$ or has a neighbour in $S$; if there exists $v \in V$ with such property then $p(S) > 0$ since $X_v = 1$ by definition. The resulting set $S$, however, may not be connected.\\

We claim that for any $v_1 \in S$, there exists a minimally separated $v_2 \in S$ such that $v_1,v_2$ are separated by at most 2 vertices not in $S$. In other words $v_2$ is the closest vertex in $S$ to $v_1$ using the discrete metric. Suppose otherwise, then there exists $u_1,u_2,u_3 \in V$ such that $u_i \notin S$, $i = 1,2,3$ is in between $v_1,v_2$, and assume the $v_1v_2$-path is $v_1 u_1 u_2 u_3 v_2$. Since $p(S) = 0$, either $u_2 \in S$, or $u_2$ has a neighbour in $S$; both cases contradicts minimality of $v_2$.\\

Now for each pair of components $C_i, C_j$ of $S$, we connect them by adding 2 vertices into the collection $S$. Let us call the final collection of connected subgraph $S'$. Since the worse case is that $S$ induces an empty graph, hence the resulting set is at most 3 times the original set $S$, and it is connected. Therefore $S'$ is a CDS.

\paragraph{Problem 1.3} Let $M = (E,\cI)$ be a matroid, with elements $e \in E$ having weight $w_e$. Let $B \subseteq \cI$ be a basis of the matroid. Without loss of generality, order $E$ by decreasing order of $w_e$.\\

\begin{algorithm}[H]
	\caption{Algorithm for Maximum Weight Matroid}
	\begin{algorithmic}[1]
		\State $B \gets \emptyset$
		
		\For{$e \in E$}
			\If{$B + e \in \cI$}
				$B \gets B + e$
			\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}

We claim that the algorithm above produces a maximum weight basis $B$ for the matroid. We need to prove two things. By construction, $B$ is an independent set. Moreover, $B$ has maximal size; by construction, we only add element $e$ to $B$ if $B + e$ is independent, so if there exists $e \in B$ such that $B + e \in \cI$, it would have been in $B$ originally, contradicting such element exists. Thus $B$ is maximal size. It remains to show that it has maximum weight.\\

Note that for each $e \in E \sm B$, there exists $f \in B + e$ such that $B + e - f$ is again independent; just remove an element from the circuit in $B + e$. Now let $e \in E \sm B$, let $f := \argmin_{f' \in C} w_{f'}$, where $C$ denotes the circuit in $B + e$. Notice that if $B + e - f$ has a larger weight than $B$, then $w_e \ge w_f$, and by the ordering, $e$ would have been added into $B$ before $f$, contradiction. Hence $B$ is indeed maximum weight basis.

\paragraph{Problem 1.4} Recall that $Exp(\lambda)$ distribution has mean $\frac{1}{\lambda}$, so $Exp(1)$ has a mean of $1$. For $x,y \in \set{0,1}^d$ adjacent (i.e. $x$ differs from $y$ by precisely one single entry), let us denote the length by $l(x,y) \sim Exp(1)$. Let $D := \set{0,1}^d$ be the hypercube.
\begin{algorithm}[H]
	\caption{Greedy Algorithm for Hypercube Walking}
	\begin{algorithmic}[1]
		\State $x \gets (0,\cdots,0)$
		
		\While{$\sum_{i=1}^{d} x_i < d$}
			\State $y \gets \min_{z \in N(x)} \set{l(x,z) : \sum_{i=1}^{d} (z_i - x_i) = 1}$
			
			\State $x \gets y$
		\EndWhile
	\end{algorithmic}
\end{algorithm}
Where $N(x) := \set{y \in D: \text{$y$ is adjacent to $x$}}$. Now we will prove that the expected length is $O(\log(d))$. First notice that at each iteration of the algorithm, we eliminate half of the possible edges since we either increment the number of 1's by 1 or decrease the number of 1's by one. In our algorithm, we require the  solution to increase the number of 1's by one. Thus it takes $O(\log(d))$ steps to finish the algorithm. Moreover, expected length of each edge is 1, thus the overall expected length i s $O(1) \cdot O(\log(d)) = O(\log(d))$.

\paragraph{Problem 1.5} Let $P = \set{p_i}_{i=1}^{n}$ be a set of $n$ points, $d : P^2 \to \bR_{\ge 0}$ be a metric. Let $p_i \in P$ be an arbitrary point, and $k \in \bN$. Consider the following algorithm
\begin{algorithm}[H]
	\caption{Greedy Algorithm for $k$-Center Problem}
	\begin{algorithmic}[1]
		\State $S \gets \set{p_i}$
		
		\While{$|S| < k$}
			
			\State $p_j \gets \argmax_{p \in P} d(p,S)$
			
			\State $S \gets S \cup \set{p_j}$
			
		\EndWhile
	\end{algorithmic}
\end{algorithm}
Let $S^* \subseteq P$ denote the optimal subset for $k$-center, i.e. $S^* := \argmin_{S \subseteq P} \max_{p \in P} d(p,S)$. Let $S = \set{p_i}_{i=1}^{k}$ be our solution from the algorithm, with $p_i^* := \argmin_{p' \in S} d(p_i,p')$ for each $p_i \in S$. For $p \in P$, if $p_i := \argmin_{p' \in S} d(p,p')$, then
\[
	d(p,S) = d(p,p_i) \le d(p,S^*) + d(p_i,S^*) \le 2 \opt
\]
Since this is true for any $p \in P$, the algorithm above is indeed 2 approximation algorithm.


\end{document}