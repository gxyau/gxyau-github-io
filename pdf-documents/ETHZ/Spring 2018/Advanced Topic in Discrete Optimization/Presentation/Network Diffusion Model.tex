\documentclass{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%

\usepackage{algorithmic}
\usepackage{amsmath, amssymb, amsfonts, amsthm}
\usepackage{tkz-graph}
\usepackage{tikz}

% Custom commands
\newcommand{\bA}{ {\mathbb{A}} }
\newcommand{\bB}{ {\mathbb{B}} }
\newcommand{\bC}{ {\mathbb{C}} }
\newcommand{\bD}{ {\mathbb{D}} }
\newcommand{\bE}{ {\mathbb{E}} }
\newcommand{\bF}{ {\mathbb{F}} }
\newcommand{\bG}{ {\mathbb{G}} }
\newcommand{\bH}{ {\mathbb{H}} }
\newcommand{\bI}{ {\mathbb{I}} }
\newcommand{\bJ}{ {\mathbb{J}} }
\newcommand{\bK}{ {\mathbb{K}} }
\newcommand{\bL}{ {\mathbb{L}} }
\newcommand{\bM}{ {\mathbb{M}} }
\newcommand{\bN}{ {\mathbb{N}} }
\newcommand{\bO}{ {\mathbb{O}} }
\newcommand{\bP}{ {\mathbb{P}} }
\newcommand{\bQ}{ {\mathbb{Q}} }
\newcommand{\bR}{ {\mathbb{R}} }
\newcommand{\bS}{ {\mathbb{S}} }
\newcommand{\bT}{ {\mathbb{T}} }
\newcommand{\bU}{ {\mathbb{U}} }
\newcommand{\bV}{ {\mathbb{V}} }
\newcommand{\bW}{ {\mathbb{W}} }
\newcommand{\bX}{ {\mathbb{X}} }
\newcommand{\bY}{ {\mathbb{Y}} }
\newcommand{\bZ}{ {\mathbb{Z}} }

\newcommand{\dd}{\delta}
\newcommand{\ee}{\varepsilon}
\newcommand{\ph}{\varphi}
\newcommand{\set}[1]{\left \{ #1 \right \}}
\newcommand{\sm}{\setminus}

\DeclareMathOperator{\argmax}{arg ~ max}
\DeclareMathOperator{\opt}{OPT}

\mode<presentation>
{
  \usetheme{Frankfurt}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}

\title[Network Diffusion Models \& Influence Maximization]{Maximizing the Spread of Influence through a Social Network}
\author{Guo Xian Yau}
\institute{ETH Zurich}
\date{March $28^{th}$, 2018}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% Uncomment these lines for an automatically generated outline.
\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Introduction}

Network diffusion models are useful to study social networks.  They are widely used to model
\begin{itemize}
	\item Spread of new product in market
	
	\item Spread of a new disease
	
	\item Spread of a new idea
\end{itemize}
The goal is to choose a sample of people in the network, and maximize the influence spread. We will consider time as a discrete quantity to simplify things.
\end{frame}

\begin{frame}{Notations}
	We will use the following notations throughout the talk. We first let $G = (V,E)$ be a directed graph, $u,v \in V$, $A_t,A,X \subseteq X$.
	\begin{itemize}
		\item $N^{+}(u) = N^{out}(u) := \set{v \in V: uv \in E}$
		
		\item $N^{-}(v) = N^{in}(v) := \set{u \in V: uv \in E}$
		
		\item The set $A_t$ is the active set at time $t \in \bN$. $A := A_0$ denotes the initial active set, and $A_t^{new} := A_t \sm A_{t-1}$
		
		%\item $\ph(A)$, denotes the distribution of final active set.
		
		\item Let $A := A_0$, the influence measure is $\sigma(A)$.
		
		\item $R(u,X)$ is the set of all nodes that is reachable from $u$ consisting entirely of live edges.
	\end{itemize}
\end{frame}

\section{Basic Model}
\begin{frame}{Basic Model}
	Given a directed graph $G = (V,E)$, which we can use to represent the social network. 
	\begin{itemize}
		\item Each vertex $v \in V$ represents a person in the network.
		
		\item Each $uv \in E$ represents the influence of $u$ on $v$.
		
		\item Set $A \subseteq V$ is called the ``activation set" of $G$. 
		
		\item If $v$ is activated by $u$, then the we declare $uv \in E$ to be a live edge.
		
		\item $\sigma(A)$ is the expected size of the active nodes when the process ends.
	\end{itemize}

	\begin{block}{Summary}
		\begin{itemize}
			\item Input: A directed graph $G = (V,E)$, and a number $k \in \bN$
			
			\item Goal: To maximize the spread of influence, $\sigma(A)$
			
			\item Output: The set of initial active nodes, $A \subseteq V$
		\end{itemize}
	\end{block}
\end{frame}

\section{ICM \& LTM}
\begin{frame}{Independent Cascade Model (ICM)}
	In Independent Cascade Model, the idea is to assign a probability on each edge; each given $uv \in E$
	\begin{itemize}
		\item $p_{uv}$ denotes  the probability denotes the probability of $u$ activating $v$.
		
		\item $p_{uv} \overset{i.i.d.}{\sim} Unif([0,1])$
	\end{itemize}
	\begin{block}{Remark}
		If $v \notin N^{+}(u)$, we simply set the probability to $p_{uv} = 0$; equivalently, we can just remove arcs with 0 probability and get a smaller graph.
	\end{block}
\end{frame}

\begin{frame}{Rule of Propagation (LTM)}
	The rule of propagation of active nodes is as follows:
	\begin{itemize}
		\item At time $t \in \bN$, $u \in A_{t}^{new}$ gets one chance to activate its inactive neighbours $v \in N^{+}(u) \sm A_t$
		
		\item The process stop at time $t = n+1$, where $k \in \bN$ is such that $A_{k}^{new} = \emptyset$, or equivalently if $A_{k+1} = A_{k}$.
	\end{itemize}
\end{frame}

\begin{frame}{Example of ICM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex] (p) at  (0,0) {P};
			\node[vertex] (q) at  (2.5,0) {Q};
			\node[vertex] (r) at  (0, 2.5) {R};
			\node[vertex] (u) at  (-2.5, -2.5) {U};
			\node[vertex] (v) at (-2.5, 1) {V};
			\node[vertex] (w) at (0,-2.5) {W};
			% Edges
			\draw[edge] (p) to node[above] {0.95} (q);
			\draw[edge] (p) to node[right] {0.59} (r);
			\draw[edge] (p) to node[below right] {0.77} (u);
			\draw[edge] (q) to node[above right] {0.05} (r);
			\draw[edge] (r) to node[above left] {0.04} (u);
			\draw[edge] (q) to node[below right] {0.25} (w);
			\draw[edge] (w) to node[below] {0.42} (u);
			\draw[edge] (u) to node[left] {0.17} (v);
		\end{tikzpicture}\\
		In the digraph above, the number on each arc is the probability that the tail will activate the head.
	\end{center}
\end{frame}

\begin{frame}{Example of ICM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex, fill=green!25] (p) at  (0,0) {P};
			\node[vertex] (q) at  (2.5,0) {Q};
			\node[vertex] (r) at  (0, 2.5) {R};
			\node[vertex] (u) at  (-2.5, -2.5) {U};
			\node[vertex] (v) at (-2.5, 1) {V};
			\node[vertex] (w) at (0,-2.5) {W};
			% Edges
			\draw[edge] (p) to node[above] {0.95} (q);
			\draw[edge] (p) to node[right] {0.59} (r);
			\draw[edge] (p) to node[below right] {0.77} (u);
			\draw[edge] (q) to node[above right] {0.05} (r);
			\draw[edge] (r) to node[above left] {0.04} (u);
			\draw[edge] (q) to node[below right] {0.25} (w);
			\draw[edge] (w) to node[below] {0.42} (u);
			\draw[edge] (u) to node[left] {0.17} (v);
		\end{tikzpicture}\\
		We choose $A_0 = \set{P}$.
	\end{center}
\end{frame}

\begin{frame}{Example of ICM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex, color=green] (p) at  (0,0) {P};
			\node[vertex] (q) at  (2.5,0) {Q};
			\node[vertex, fill=green!25] (r) at  (0, 2.5) {R};
			\node[vertex, fill=green!25] (u) at  (-2.5, -2.5) {U};
			\node[vertex] (v) at (-2.5, 1) {V};
			\node[vertex] (w) at (0,-2.5) {W};
			% Edges
			\draw[edge, color=red] (p) to node[above] {0.95} (q);
			\draw[edge, color=green] (p) to node[right] {0.59} (r);
			\draw[edge, color=green] (p) to node[below right] {0.77} (u);
			\draw[edge] (q) to node[above right] {0.05} (r);
			\draw[edge] (r) to node[above left] {0.04} (u);
			\draw[edge] (q) to node[below right] {0.25} (w);
			\draw[edge] (w) to node[below] {0.42} (u);
			\draw[edge] (u) to node[left] {0.17} (v);
		\end{tikzpicture}\\
		$P$ activates $R$ and $U$ but not $Q$.
	\end{center}
\end{frame}

\begin{frame}{Example of ICM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex, color=green] (p) at  (0,0) {P};
			\node[vertex] (q) at  (2.5,0) {Q};
			\node[vertex, color=green] (r) at  (0, 2.5) {R};
			\node[vertex, color=green] (u) at  (-2.5, -2.5) {U};
			\node[vertex] (v) at (-2.5, 1) {V};
			\node[vertex] (w) at (0,-2.5) {W};
			% Edges
			\draw[edge, color=red] (p) to node[above] {0.95} (q);
			\draw[edge, color=green] (p) to node[right] {0.59} (r);
			\draw[edge, color=green] (p) to node[below right] {0.77} (u);
			\draw[edge] (q) to node[above right] {0.05} (r);
			\draw[edge] (r) to node[above left] {0.04} (u);
			\draw[edge] (q) to node[below right] {0.25} (w);
			\draw[edge] (w) to node[below] {0.42} (u);
			\draw[edge, color=red] (u) to node[left] {0.17} (v);
		\end{tikzpicture}\\
		$U$ fails to activate $V$. No more newly activated nodes, process ended. Green edges are live edges, and green nodes are final active nodes.
	\end{center}
\end{frame}

\begin{frame}{Linear Threshold Model (LTM)}
	Linear Threshold Model (LTM) is another frequently used diffusion model in social network. For $G = (V,E)$, we have
	\begin{itemize}
		\item For each $v \in V$, $\theta_{v} \overset{i.i.d.}{\sim} Unif((0,1])$ is the threshold.
		
		\item For each $uv \in E$, $w_{uv} \in [0,1]$ is the weight on $uv$; it can be thought as the influence of $u$ on $v$.
	\end{itemize}
	
	\begin{block}{Remark}
		We may assume $\sum_{u \in N^{-}(v)} w_{uv} \le 1$; otherwise, we can renormalise the weight of each arc by dividing by $\sum_{u \in N^{-}(v)} w_{uv}$.
	\end{block}
\end{frame}

\begin{frame}{Rule of Propagation (LTM)}
	The rule to propagate in LTM is deterministic. Assume we are at time $t \in \bN$, and we have $A^{t-1}$.
	\begin{itemize}
		\item Any $v \in V \sm A_t$, if $\sum_{u \in N^-(v) \cap A_{t-1}} w_{uv} \ge \theta_v$, then $v$ becomes active at time $t$, so $v \in A_t$.
		
		\item The process stops at time $k+1$, where $k \in \bN$ is such that $A_{k+1} = A_k$.
	\end{itemize}
\end{frame}

\begin{frame}{Example of LTM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex] (p) at  (0,0) {\footnotesize{P, \textcolor{red}{0.93}}};
			\node[vertex] (q) at  (2.5,0) {\footnotesize{Q,\textcolor{red}{0.14}}};
			\node[vertex] (r) at  (0, 2.5) {\footnotesize{R,\textcolor{red}{0.72}}};
			\node[vertex] (u) at  (-2.5, -2.5) {\footnotesize{U,\textcolor{red}{0.98}}};
			\node[vertex] (v) at (-2.5, 1) {\footnotesize{V,\textcolor{red}{0.5}}};
			\node[vertex] (w) at (0,-2.5) {\footnotesize{W,\textcolor{red}{0.5}}};
			% Edges
			\draw[edge] (p) to node[above] {\textcolor{green}{\small 0.95}} (q);
			\draw[edge] (p) to node[right] {{\textcolor{green}{\small 0.59}}} (r);
			\draw[edge] (p) to node[below right] {{\textcolor{green}{\small 0.63}}} (u);
			\draw[edge] (q) to node[above right] {{\textcolor{green}{\small 0.05}}} (r);
			\draw[edge] (r) to node[above left] {{\textcolor{green}{\small 0.03}}} (u);
			\draw[edge] (q) to node[below right] {{\textcolor{green}{\small 0.25}}} (w);
			\draw[edge] (w) to node[below] {{\textcolor{green}{\small 0.34}}} (u);
			\draw[edge] (u) to node[left] {{\textcolor{green}{\small 0.17}}} (v);
		\end{tikzpicture}\\
		The green numbers above represents the weight on each arc, and the red number in the nodes represents the threshold of each node.
	\end{center}
\end{frame}

\begin{frame}{Example of LTM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex, fill=blue!25] (p) at  (0,0) {\footnotesize{P, \textcolor{red}{0.93}}};
			\node[vertex] (q) at  (2.5,0) {\footnotesize{Q,\textcolor{red}{0.14}}};
			\node[vertex] (r) at  (0, 2.5) {\footnotesize{R,\textcolor{red}{0.72}}};
			\node[vertex] (u) at  (-2.5, -2.5) {\footnotesize{U,\textcolor{red}{0.98}}};
			\node[vertex] (v) at (-2.5, 1) {\footnotesize{V,\textcolor{red}{0.5}}};
			\node[vertex] (w) at (0,-2.5) {\footnotesize{W,\textcolor{red}{0.5}}};
			% Edges
			\draw[edge] (p) to node[above] {\textcolor{green}{\small 0.95}} (q);
			\draw[edge] (p) to node[right] {{\textcolor{green}{\small 0.59}}} (r);
			\draw[edge] (p) to node[below right] {{\textcolor{green}{\small 0.63}}} (u);
			\draw[edge] (q) to node[above right] {{\textcolor{green}{\small 0.05}}} (r);
			\draw[edge] (r) to node[above left] {{\textcolor{green}{\small 0.03}}} (u);
			\draw[edge] (q) to node[below right] {{\textcolor{green}{\small 0.25}}} (w);
			\draw[edge] (w) to node[below] {{\textcolor{green}{\small 0.34}}} (u);
			\draw[edge] (u) to node[left] {{\textcolor{green}{\small 0.17}}} (v);
		\end{tikzpicture}\\
		Again we choose $A_0 = \set{P}$.
	\end{center}
\end{frame}

\begin{frame}{Example of LTM}
	\begin{center}
		\begin{tikzpicture}[scale = 0.8]
			\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
			\tikzset{edge/.style = {->,> = latex}}
			% Vertices
			\node[vertex, fill=blue!25] (p) at  (0,0) {\footnotesize{P, \textcolor{red}{0.93}}};
			\node[vertex, fill=blue!25] (q) at  (2.5,0) {\footnotesize{Q,\textcolor{red}{0.14}}};
			\node[vertex] (r) at  (0, 2.5) {\footnotesize{R,\textcolor{red}{0.72}}};
			\node[vertex] (u) at  (-2.5, -2.5) {\footnotesize{U,\textcolor{red}{0.98}}};
			\node[vertex] (v) at (-2.5, 1) {\footnotesize{V,\textcolor{red}{0.5}}};
			\node[vertex] (w) at (0,-2.5) {\footnotesize{W,\textcolor{red}{0.5}}};
			% Edges
			\draw[edge] (p) to node[above] {\textcolor{green}{\small 0.95}} (q);
			\draw[edge] (p) to node[right] {{\textcolor{green}{\small 0.59}}} (r);
			\draw[edge] (p) to node[below right] {{\textcolor{green}{\small 0.63}}} (u);
			\draw[edge] (q) to node[above right] {{\textcolor{green}{\small 0.05}}} (r);
			\draw[edge] (r) to node[above left] {{\textcolor{green}{\small 0.03}}} (u);
			\draw[edge] (q) to node[below right] {{\textcolor{green}{\small 0.25}}} (w);
			\draw[edge] (w) to node[below] {{\textcolor{green}{\small 0.34}}} (u);
			\draw[edge] (u) to node[left] {{\textcolor{green}{\small 0.17}}} (v);
		\end{tikzpicture}\\
		$Q$ is activated. Influence of in-neighbours of $R$ and $W$ does not exceed threshold, process ends since no new nodes is being activated.
	\end{center}
\end{frame}

\section{Algorithms}
\begin{frame}{Submodularity}
	Before we continue, we need to know the definition of submodularity.	
	\begin{block}{Definition}
		There are two A functional $f: X \to \bR$ is called \emph{submodular} if it satisfies $f(S \cup \set{x}) - f(S) \ge f(T \cup \set{x}) - f(T)$, for all $S \subseteq T \subseteq X$ and for all $x \in X$.
	\end{block}
	There are other definitions of submodularity, but this definition is most suitable to us; it is called the ``diminishing return" property.
\end{frame}

\begin{frame}{Greedy Hill-Climbing Algorithm}
	\begin{block}{Greedy Approximation Algorithm}
		\begin{algorithmic}[1]
			\STATE Initialise with $A = \emptyset$
			\FOR{$i=1$ \TO $k$}
			\STATE Find $v_i := \argmax_{v \in V} [\sigma(A \cup \set{v}) - \sigma(A)]$
			\STATE Set $A \leftarrow A \cup \set{v_i}$
			\ENDFOR
		\end{algorithmic}
	\end{block}
\end{frame}

\begin{frame}{Approximation for Submodular Functions}
	\begin{block}{Theorem (Nemhauser, Wolsey, Fisher - 1978)}
		Let $X$ be a set, and $f: 2^X \to \bR$ be a non-decreasing, submodular function. Given the following maximisation problem
		\[
			\max_{S \subseteq X} f(S)
		\]
		Then greedy algorithm yields a $(1 - \frac{1}{e})$-approximation solution.
	\end{block}
\end{frame}

\section{$\sigma(\cdot)$}
\begin{frame}{Properties of Influence Function, $\sigma(\cdot)$}
	\begin{block}{Theorem}
		(Kempe, Kleinberg, Tardos - 2003) For any arbitrary instance of Independent Cascade Model, the resulting influence function $\sigma(\cdot)$ is non-decreasing and submodular.
	\end{block}
	
	\begin{block}{Observation}
		\begin{enumerate}
			\item $v \in \ph(A)$ is active if and only if there is a $u \in A$ such that there exists a live-edge $uv$-path. Note that it is possible that $v \in A$, in this case we consider it to be a path of length 0.
			
			\item If $v \in R(u,X)$, then the live-edge $uv$-path is unique, $X \subseteq V$.
		\end{enumerate}
	\end{block}
\end{frame}

\begin{frame}{Proof (Sketch)}
		Let $X \subseteq V$ be arbitrary. Denote by $\sigma_X(A)$ the total number of nodes  (in $X$) activated given $A = A_0$.
		\begin{enumerate}
			\item Since $A$ and $X$ are fixed, $\sigma_X(A)$ is deterministic.
			
			\item By observation, $\sigma_X(A)$ is the total number of nodes reachable by nodes in $A$, i.e. $\sigma_X(A) = |\bigcup_{u \in A} R(u,X)|$.
			
			\item Let $S \subseteq T \subseteq V$, $\bigcup_{u \in S} R(u,X) \subseteq \bigcup_{u \in T} R(u,X)$
			
			\item $\sigma_X(\cdot)$ is submodular; $\sigma_X(S \cup \set{v}) - \sigma_X(S)$ is the number of active nodes in $R(v,X) \sm \bigcup_{u \in S} R(u,X)$
			
			\item $\sigma(A) = \sum_{A \subseteq X \subseteq V} P[\ph(A) = X] \cdot \sigma_X(A) = E[|\ph(A)|]$ is submodular as linear combinations of submodular functions.
		\end{enumerate}
\end{frame}

\begin{frame}{Computation \& Estimation of $\sigma(\cdot)$}
	In general, it is not clear how to evaluate $\sigma(\cdot)$ exactly; it is equivalent to the set cover problem, which is itself NP-complete.\\[0.25 cm]
	
	However, we can estimate $\sigma(\cdot)$; we can simulate the process multiple times and take compute the expected outcome distribution of the set of final active vertices.
\end{frame}

\begin{frame}{($1 - \frac{1}{e} - \ee$)-Approximation}	
	\begin{block}{Theorem (Kempe, Tardos, Klein - 2005)}
		Let $A^* := \argmax_{\substack{A \subseteq V\\ |A| = k}} \sigma(A)$.
		\begin{enumerate}
			\item If $v_i^*$ is optimally chosen for each iteration, the greedy algorithm yield a $(1 - \frac{1}{e})$-approximation.
			
			\item Let $\ee' > 0$, if $v_i$ is chosen to be $1 - \ee'$ approximation of $v_i^*$, the greedy algorithm yields $(1-\frac{1}{e} - \ee)$-approximation, where $\ee = \text{poly}(\ee')$.
		\end{enumerate}
	\end{block}
\end{frame}

\begin{frame}{Computing $\sigma(\cdot)$ of LTM}
	\begin{block}{Theorem (Kempe, Tardos, Klein - 2003)}
		The linear threshold model and independent cascade model are equivalent.
	\end{block}
	
	\begin{block}{Corollary}
		Computation of $\sigma(\cdot)$ of linear threshold model is also NP-complete.
	\end{block}
\end{frame}

\section{Generalisations}
\begin{frame}{Generalisation}
	
	\textbf{General Threshold Model} For each $v \in V$, we have $\theta_v$, and a functional $f : V \times 2^V \to [0,1]$, $v$ is then activated if $f(v,S) \ge \theta_v$; here, $S := N^{-}(v) \cap A_{t-1}$, the active neighbours of $v$ at time $t-1$.\\[0.25 cm]
	
	\textbf{General Cascade Model} Similar to independent cascade model, except the probability on the arc $uv \in E$ is now $p_v(u,S)$, where $\set{u}$ and $S$ are disjoint subsets of neighbours of $v$.\\
[0.25 cm]
	
	\textbf{Decreasing Cascade Model} We require the probability $p_v(u,\cdot)$ to be non-decreasing, i.e. if $S \subseteq T$, then $p_u(v,S) \ge p_u(v,T)$, where $S,T \subseteq N^{-}(v)$ are neighbours that have attempted to activate $v$.
\end{frame}

\section{References}
\begin{frame}[allowframebreaks]
	\frametitle{References}
	\footnotesize{
		\begin{thebibliography}{100} % Beamer does not support BibTeX so references must be inserted manually as below
			\bibitem[Kempe, Kleinberg, Tardos - 2003]{Kempe 2003} Kempe D., Kleinberg J., Tardos É. (2003)
			\newblock Maximizing the spread of influence through a social network
			\newblock \emph{Proceedings of the ninth ACM SIGKDD international conference on Knowledge discovery and data mining} Pages 137-146
			
			\bibitem[Kempe, Kleinberg, Tardos - 2005]{Kempe 2005} Kempe D., Kleinberg J., Tardos É. (2005)
			\newblock Influential Nodes in a Diffusion Model for Social Networks
			\newblock \emph{Automata, Languages and Programming} pg 1127-1138.
			
			\bibitem[Nemhauser, Wolsey, Fisher - 1978][Nemhauser 19978] Nemhauser, G.L., Wolsey, L.A. \& Fisher
			\newblock An analysis of approximations for maximizing submodular set functions
			\newblock \emph{M.L. Mathematical Programming (1978)} 14: 265.
		\end{thebibliography}
	}
\end{frame}

\end{document}
