\documentclass[11pt]{article}

\usepackage{hedgehog}

\newcommand{\handout}[4]{
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { { Summary } \hfill #1 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #4  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { {\em #2 \hfill #3} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newcommand{\lecture}[4]{\handout{#2}{#3}{Prepared By: #4}{#1}}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{thm}{Theorem}[section]
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem{defn}[thm]{Definition}
\newtheorem*{eg}{Example}
\newtheorem*{ex}{Exercise}
% \newtheorem{conj}[thm]{Conjecture}[section]

\theoremstyle{remark}
\newtheorem*{rmk}{Remark}
\newtheorem*{nota}{Notation}
\newtheorem*{note}{Note}
\newtheorem*{conven}{Convention}
\newtheorem*{conj}{Conjecture}

\begin{document}

\lecture{Probabilistic Method in Combinatorics}{Autumn 2018}{All the best! \smiley}{GuoXian Yau}

\section{Bachman-Landau Notations/Asymptotic Notations}
Let $f(n)$ and $g(n)$ be arbitrary functions.\\
\begin{tabularx}{\textwidth}{l m{2 in} X}
	\rowcolor{purple!25} Notation & Description & Formal Definition\\
	$f(n) = o(g(n))$ & $f$ is asymptotically dominated by $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| < C g(n)$ \\
	\rowcolor{purple!25} $f(n) = O(g(n))$ & $|f|$ is asymptotically bounded above by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| \le C g(n)$\\
	$f(n) = \Theta(g(n))$ & $f$ is asymptotically bounded above and below by $g$ & $\exists C_1,C_2 > 0$, $\exists n_0 > 0$ s.t. $\forall n > n_0$, $C_1 g(n) \le f(n) \le C_2 g(n)$\\
	\rowcolor{purple!25} $f(n)  \sim g(n)$ & $f$ is on the order of $g$ & $\forall \ee > 0, \exists n_0 : \forall n > n_0, |\frac{f(n)}{g(n)} - 1| < \ee$\\
	$f(n) = \Omega(g(n))$ & $f$ is asymptotically lower bounded by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $f(n) \ge Cg(n)$\\
	\rowcolor{purple!25} $f(n) = \omega(g(n))$ & $f$ asymptotically dominates $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| > C |g(n)|$
\end{tabularx}
One can show the following relation using $\ee$-$\dd$ proofs.
\begin{multicols}{2}
	\begin{itemize}
		\item $f(n) = o(g(n)) \iff \lim_{n} \frac{f(n)}{g(n)} = 0$
		
		\item $f(n) = O(g(n)) \iff \limsup_{n} \frac{|f(n)|}{g(n)} < \infty$
		
		\item $f(n) = \Theta(g(n)) \iff f(n) = O(g(n)) \wedge f(n) = \Omega(g(n))$
		
		\item $f(n) \sim g(n) \iff \lim_{n} \frac{f(n)}{g(n)} = 1$
		
		\item $f(n) = \Omega(g(n)) \iff \liminf_{n} \frac{f(n)}{g(n)} > 0$
		
		\item $f(n) = \omega(g(n)) \iff \lim_{n} \left| \frac{f(n)}{g(n)} \right| = \infty$
	\end{itemize}
\end{multicols}

\section{Simple Analytical Bounds}
Most commonly seen inequalities.
\begin{itemize}\item $\sum_{k = 0}^{n} {n \choose k} = 2^n$, ${n \choose k} \le 2^n$
	
	\item $\frac{(n-k+1)^k}{k!} \le {n \choose k} \le \frac{n^k}{k!}$
	
	\item (Sterling's Inequality) $(\frac{n}{k})^k \le {n \choose k} \le (\frac{en}{k})^k$
	
	\item (Sterling's Approximation) $k! \sim  \sqrt{2 \pi k} (\frac{k}{e})^k$
	
	\item (Harmonic Number) For $n \in \bN$, let $H_n := \sum_{i=1}^{n} i^{-1}$. Then $\ln n \le H_n \le \ln n + 1$
	
	\item For any $x \in \bR$, $(1 + x) \le \sum_{n \in \bN_0} \frac{x^n}{n!} = e^x$
	
	\item For any $x \in \bR_{\ge 0}$, if $C$ is large enough, then $e^x \le 1 + Cx$
	
	\item (Ratios of Binomial Coefficients) If $k \le n \le m$, then
	\[
		\left( \frac{n-k+1}{m-k+1} \right)^k \le \frac{{n \choose k}}{{m \choose k}} = \prod_{i=0}^{k-1} \frac{n-i}{m-i} \le \left( \frac{n}{m} \right)^k
	\]
\end{itemize}

\section{Concentration Inequalitites}
\begin{thm}
	(Union Bound) Let $X_1,\cdots,X_n$ be random variables. Then $\bP(\bigcup_{k=1}^{n} X_k) \le \sum_{k=1}^{n} \bP(X_k)$.
\end{thm}

\begin{thm}
	(Markov Inequality) Let $X$ be a non-negative random variable. Then
	\[
		\bP(X \ge t) \le \frac{\bE[X]}{t} \equiv \bP(X \ge s \bE[X]) \le \frac{1}{s}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\bE[X]}$.
\end{thm}

\begin{thm}
	(Chebychev Inequality) Let $X$ be a random variable. Then
	\[
		\bP( |X - \bE[X]| \ge t) \le \frac{\var[X]}{t^2} \equiv \bP(X \ge s \sqrt{\var[X]}) \le \frac{1}{s^2}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\var[X]}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then for every $\dd > 0$, $\bP(X \ge (1+\dd) \mu) \le \exp(- \frac{\exp(\dd)}{(1+\dd)^{(1+\dd)}})^{\mu}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds, simplified version) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then
	\begin{itemize}
		\item $\bP(X \ge (1+\dd)\mu) \le \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \le (1-\dd)\mu) \le \exp(-\frac{\mu \dd^2}{2})$ for all $0 < \dd \le 1$.
		
		\item $\bP(|X - \mu| \ge \dd \mu) \le 2 \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \ge t) \le 2^{-t}$ for all $t \ge 2 e \mu$.
	\end{itemize}
\end{thm}

\begin{coro}
	(Chernoff's Bounds, Alon \& Spencer version) For independent Bernoulli random variables $X_i \overset{i.i.d.}{\sim} Ber(p)$, and $X = \sum_{i=1}^{n} X_i$, we have the following bounds.
	\begin{enumerate}
		\item If we set
		\[
			X_i := \begin{cases}
				1 & p = \frac{1}{2}\\
				-1 & 1 - p = \frac{1}{2}
			\end{cases}
		\]
		Then $\bP(|X| \ge a) \le 2 \exp(-\frac{a^2}{2n})$.
		
		\item If we have
		\[
			X_i := \begin{cases}
				1 & p = \frac{1}{2}\\
				0 & 1 - p = \frac{1}{2}
			\end{cases}
		\]
		Then we have $\bP(|X - \frac{n}{2}| \ge a) \le 2 \exp(-\frac{na^2}{6})$.
		
		\item If we have
		\[
			X_i := \begin{cases}
				1 & \text{With probability $p$}\\
				0 & \text{With probability $1-p$}
			\end{cases}
		\]
		Then we have $\bP(|X - np| \ge \dd np) \le 2 \exp(-\frac{\dd^2 np}{3})$ for $0 < \dd < 1$.
	\end{enumerate}
\end{coro}

\begin{thm}
	(Azuma-Hoeffding) Let $(\Omega,\bP)$ be product of $N$ discrete probability space, call them $(\Omega_1,\bP_1), \ldots, (\Omega_N,\bP_N)$, and let $X : \Omega \to \bR$ be a random variable. Suppose $c_i$'s are the Lipscitz constants associated to the space $(\Omega_i,\bP_i)$. Then for all $t \ge 0$, $\bP(X \ge \bE[X] + t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; $\bP(X \le \bE[X] - t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; succinctly, $\bP( |X - \bE[X]| \ge t) \le 2 \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$.
\end{thm}

\section{Conjunctive Normal Form/Disjunctive Normal Form}
Let $X = x_1,\cdots,x_n$ be the literals, and $C_1,\cdots,C_m$ be the clauses, with $\set{y_{i,1},\cdots,y_{i,l(i)}} \subseteq X$. Let us denote by $f$ the formula, then
\begin{itemize}
	\item (Conjunctive Normal Form, CNF) $f = C_1 \wedge \ldots \wedge C_m$
	
	\item (Disjunctive Normal Form, DNF) $f = C_1 \vee \ldots \vee C_m$
\end{itemize}
Let $l(i) := |C_i|$. The 3SAT problem is the decision problem with $l(i) = 3$ for all $i = 1,\ldots,m$; it is NP-complete. In general, a $k$-SAT formula is when $l(i) = k$ for all $i = 1,\ldots,m$.

\section{Basics}
\begin{thm}
	(Erd\"{o}s) If $A$ is a set of positive integers, then there exists $B \subseteq A$ such that $|B| > \frac{n}{3}$ is sum free set.
\end{thm}

\begin{thm}
	(Bollob\'{a}s)\index{Bollobas Theorem@ Bollob\'{a}s Theorem} Given $\set{(A_i,B_i)}_{i=1}^{m}$, with the following properties:
	\begin{itemize}
		\item $|A_i| = a$, $i = 1,\ldots,m$
		
		\item $|B_i| = b$, $i = 1,\ldots,m$
		
		\item $A_i \cap B_i = \emptyset$, and $A_i \cap B_j \ne \emptyset$ if $i \ne j$.
	\end{itemize}
	Then $m \le {a + b \choose a}$.
\end{thm}

\subsection{Hamiltonian Path}
Suppose $P(n)$ denote the number of maximal Hamiltonian path for a tournament $T(n)$ on $n$ vertices. Alon's theorem says $P(n) \le n^{\frac{3}{2}} \frac{n!}{2^{n-1}}$.

\begin{defn}
	Given $A \in \bR^{n \times n}$, then $\per(A) = \sum_{\sigma \in S_n} \prod_{i=1}^{n} a_{i \sigma(i)}$.
\end{defn}

\begin{thm}
	(Br\'{e}gman Theorem) $\per(A) \le \prod_{i=1}^{n} (r_i!)^{\frac{1}{r_i}} \approx \prod_{i=1}^{n} \frac{r_i}{e}$ where $r_i := \sum_{j=1}^{n} a_{ij}$.
\end{thm}

\section{Alteration Methods}
Alteration method is useful when we choose a graph/set uniformly at random and then modify it to get the structure that we want. E.g. independence set, dominating set, recolouring of graphs, etc.

\subsection{Uniform Hypergraphs}
\begin{defn}
	We define $m(n)$ is the minimum number of edge required in a $n$-uniform hypergraph to be non-two colourable.
\end{defn}

\begin{thm}
	Given $n \in \bN$ and an $n$-uniform hypergraph $H = (V,E)$, clearly $2^{n-1} \le m(n) \le C n^2 2^n$ for some $C > 0$. A tighter lower bound would be $m(n) \ge 2^{n} \sqrt{\frac{n}{\log n}}$.
\end{thm}

\section{Dependent Random Choice}
\begin{lemma}
	Let $G = (V,E)$, $|V| = n$, $m = a+b$, $a$, $r$ are some integers, and $d = \frac{2|E|}{n}$ is the average degree. If there exists some integer $t$ such that $\frac{d^t}{n^{t-1}} - {n \choose r}(\frac{m}{n})^{t} \ge a$, then $G$ contains a subset $U$ with $|U| = a$ such that any $r$ vertices in $U$ have at least $m$ common neighbours.
\end{lemma}

\begin{lemma}
	Given a bipartite graph $H$ with partition $V(H) = A \sqcup B$, $|A| = a$, $|B| = b$, with the property that all vertices $v \in B$, $d(v) \le r$ for some $r \in \bN$. Suppose $G$ is a graph with $|U| = a$ is a subset of $G$ such that any $r$ vertices in $U$, we have at least $a+b$ common neighbours. Then $G$ has a copy of $H$.
\end{lemma}

\section{Methods of Moments}
Usually pairs with Markov and Chebyshev's inequality.

\begin{thm}
	Let $(X_n)_n$ be a sequence of random variables.
	\begin{itemize}
		\item If $X_n$'s are non-negative, then $\bE[X] \in o(1) \implies \bP(X_n = 0) = 1 - o(1)$.
		
		\item If $\bE[X_n] \ne 0$ and $\var[X_n] = o(\bE[X_n])$, then $\bP(X_n = 0) = o(1)$.
	\end{itemize}
\end{thm}

Useful: If $X$ is non-negative, then $\bP(X = 0) \le \min \set{\frac{\var[X]}{\bE^2[X]}, \frac{\var[X]}{\bE[X^2]}}$; use $t = \frac{\bE[X]}{\sqrt{\var[X]}}$ for first element, and use Cauchy Schwarz in the form of $(\sum_k k \bP(X = k))^2 \le (\sum_k \bP(X = k)) (\sum_k k^2 \bP(X = k))$. If we are two colouring, and counting the differences, then consider $X \in \set{\pm 1}$ instead for the random variable.

\subsection{Sum Free Sets \& Prime Divisor}
\begin{defn}
	Let $A = \set{a_i}_{i=1}^{k} \subseteq k$. We say $A$ is sum free if $\sum_{\substack{i \in S \\ S \subseteq A}} a_i \ne 0$
\end{defn}

\begin{thm}
	Let $A \subseteq [n]$ be a sum free set. Then $\log_2 n \le k \le \log_2 n + \log_2 \log_2 n + C_0(n)$.
\end{thm}

\subsection{Random Graphs}
\begin{defn}
	The class $\cG(n,p)$ is the class of graphs on $n$ vertices and each edge exists with probability $p \in [0,1]$.
\end{defn}

\begin{defn}
	A \emph{(weak) threshold} for containing $K_4$ is a number such that
	\begin{itemize}
		\item For all $p \gg p_0$, (e.g. $p > C p_0$), $f(p) \ge 1 - \ee$
		
		\item For all $p \ll p_0$, (e.g. $p < \frac{1}{C} p_0$), we have$f(p) < \ee$
	\end{itemize}
	where $C$ is a very large constant.
\end{defn}
\begin{rmk}
	Usually, first moment method is used to prove the first statement and second moment method is used to prove the second statement.
\end{rmk}

\section{Concentration Bounds}
Mainly about Chernoff's bounds. For Azuma Hoeffding see Martingale.

\subsection{Tournaments}
\begin{defn}
	Let $T(n)$ be a tournament on $n$ vertices. If $i$ beats $j$ (i.e. $i < j$), we define the arc (directed edge) to be $i \to j$. Let $\sigma \in S_n$ be a random permutation. An upset is an arc $ij$ such that $\sigma(i) > \sigma(j)$.
\end{defn}

\begin{thm}
	Given $T(n)$ a tournament on $n$ vertices, the number of upset is at least $\frac{1}{2} {n \choose 2} - c n^{\frac{3}{2}} \sqrt{\log n}$.
\end{thm}

\section{Lov\'{a}sz Local Lemma}
\begin{thm}
	(Lov\'{a}sz Local Lemma) Let $A_1,\ldots,A_m$ be the ``bad"  events (i.e. events in which we do not want to happen) in some probability space $(\Omega, \bP)$, such that
	\begin{itemize}
		\item $\bP(A_i) \le p < 1$
		
		\item Every $A_i$ is independent of all but $d$ events $A_j$
		
		\item $e p(d+1) \le 1$; we are using natural log, i.e. $e = 2.718281828 \ldots$ here.
	\end{itemize}
	In this case, if $e p (d+1) < 1$, then $\bP(\bigcap_{i=1}^{m} \bar{A}_i) > 0$; this is a very strong result, which we will prove next time.
\end{thm}

\subsection{2-Colourable Hypergraphs}
\begin{thm}
	Given $n \in \bN$ and an $n$-uniform hypergraph $H = (V,E)$, a tight lower bound would be $m(n) \ge 2^{n} \sqrt{\frac{n}{\log n}}$.
\end{thm}

\begin{thm}
 	If $H$ is $k$-uniform hypergraph, and each hyperedge intersects at most $d$ other edges, and $e(d+1) \le 2^{k-1}$, then $H$ is 2 colourable.
\end{thm}

\subsection{Directed Cycles}
\begin{thm}
	Assume $k \in \bN$ is arbitrary. Let $D := D(V,E)$ be a directed graph with maximum in-degree $\Delta^{-}$ and minimum out degree $\dd^{+}$ such that
	\[
		e ( (\dd^{+} + 1) \Delta^{-} + 1) (1 - \frac{1}{k})^{\dd^{+}} \le 1
	\]
	Then we can find a cycle of length $\equiv 0 \mod k$.
\end{thm}

\section{Martingale}
\begin{defn}
	A function $f$ is \emph{$C_f$-Lipschitz}\index{Lipschitz Functions} if $|f(x) - f(x')| \le C_f$ where $\norm{x - x'} = 1$, i.e. $x$ and $x'$ differs only in one coordinate. More generally, we have $|f(x) - f(x')| \le C_f \norm{x - x'}$ for some normed space $x,x' \in (\bR^n,\norm{\cdot})$.
\end{defn}
\begin{rmk}
	Always good to keep in mind if we use edge exposure, then $\Omega_e = \set{0,1}$ in the obvious way and there are $m$ of them; if vertex exposure is used, then take an ordering of the vertices, and $\Omega_{i}$ are the degrees of $v_i$, and there are $n-1$ of them.
\end{rmk}

\begin{eg}
	Here are a examples on graphs. Let $G$ be a graph.
	\begin{multicols}{2}
		\begin{itemize}
			\item Chromatic number $\chi(G)$ is 1-Lipschitz
			
			\item Independence number $\alpha(G)$ is 1-Lipschitz
			
			\item Maximal Matching $\nu(G)$ is 1-Lipschitz
			
			\item Edge connectivity $\kappa'(G)$ is 1-Lipschitz
		\end{itemize}
	\end{multicols}
\end{eg}

\begin{thm}
	(Azuma-Hoeffding) Given the above settings, we have
	\[
		\bP[|f - \bE[f]| \ge \lambda) \le 2 \exp(-\frac{\lambda^2}{2 c^2 n})
	\]
	where $c := C_f$. Remark: There is a more general case call Martingale.
\end{thm}

\subsection{Chromatic Number of Random Graph}
\begin{thm}
	Let $G \in \cG(n,\frac{1}{2})$. Then $\bP(|\chi(G) - \bE[\chi(G)]| \ge a) \le 2 \exp(-\frac{a^2}{2(n-1)})$.
\end{thm}
\begin{rmk}
	This is done via vertex exposure.
\end{rmk}

\begin{thm}
	Let $p = n^{-\alpha}$, $\alpha > \frac{5}{6}$. Let $G \in G(n,p)$. Then there exists $u := u(n,p)$ such that with probability $\ge 1 - o(1)$, we have $u \le \chi(G) \le u + 3$.
\end{thm}

\begin{thm}
	Now let $G \sim G(n,\frac{1}{2})$. We have $\chi(G) \sim \frac{n}{2 \log_2 n}$ almost everywhere.
\end{thm}

\begin{thm}
	Let $f(k) = {n \choose k} 2^{-{k \choose 2}}$, and $k_0 \in \bZ$ such that $f(k_0 - 1) > 1 > f(k_0)$. We will choose $k = k_0 - 4$, i.e. $k \sim 2 \log_2 n$, and $f(k) > n^{3 + o(1)}$. Then $G \sim G(n,\frac{1}{2})$, we have $\bP(\omega(G) < k) \le \exp(-n^{2 + o(1)})$.
\end{thm}

\section{Correlation Inequality}
\begin{defn}
	Property $P$ of graphs is monotone increasing/decreasing if $P$ is preserved under addition or subtraction of edges.
\end{defn}

\begin{eg}
	Below are some examples of increasing and decreasing properties.
	\begin{description}
		\item[Increasing] Having a Hamiltonian cycle, containing a fixed graph $H$, being connected, having a matching of size $k$, having a perfect matching.
		
		\item[Decreasing] Containing independence set of size $k$, being $k$ colourable, planarity of the graph.
	\end{description}
\end{eg}

\begin{defn}
	For any event $A \subseteq \set{0,1}^N$, we say that the event $A$ is \emph{increasing}\index{Events! Increasing Events} if for any $x \in A$, if $y \in \Omega$ is such that whenever $\sum_i x_i \le \sum_i y_i$, then $y \in A$. Similarly, $A$ is \emph{decreasing}\index{Events! Decreasing Events} if $x \in A$ and $y \le x$, then $y \in A$.
\end{defn}
\begin{rmk}
	We will abuse the notation and write $x \le y \equiv \sum_i x_i \le \sum_i y_i$.
\end{rmk}

\begin{thm}
	Let $A,B \subseteq \set{0,1}^N$ to be monotonic increasing sets of vectors. Then 
	\[
		\bP(A \cap B) \ge \bP(A) \bP(B)
	\]
	Analogously, if they are both monotonic decreasing, the above holds. If precisely one is monotonic increasing and another is monotonic decreasing, then $\bP(A \cap B) \le \bP(A) \bP(B)$.
\end{thm}

\end{document}