##############################
# P values for flips example from last time
##############################

# Simulated Null distribution of number of changes in random 0 - 1 sequence of 
# length 20
mat <- matrix(rbinom(n = 1000000, size = 1, prob = 0.5), ncol = 20)
dim(mat)

n.flips <- function(x) {
  no <- 0
  for (i in 1:(length(x) - 1)) {
    no <- no + (x[i] != x[i + 1])
  }
  return(no)
}

res <- apply(mat, 1, n.flips)
str(res)
hist(res, freq=F)

# empirical distribution function of the number of flips
# approximates the theoretical distribution function 
# (Glivenko-Cantelli theorem https://en.wikipedia.org/wiki/Glivenko%E2%80%93Cantelli_theorem)
plot(ecdf(res),xlim=c(0,20))

# theoretical distribution function is Binom(19,0.5)
# plot are almost identical
points(0:19, pbinom(q=0:19,size=19,prob=0.5),pch=19,xlim=c(0,20),col=2)
lines(c(0,rep(1:19,each=2),19), rep(pbinom(q=0:19,size=19,prob=0.5),each=2),xlim=c(0,20),col=2)

rep(0:20, each=2)

# Hence, the empirical distribution function is a good approximation of the real distribution of the number of flips 
# if we assume that the null hypothesis H_0 is true. (=20 independent Bernoulli samples with p=0.5)

# if we assume that the alternative to H_0 is "samples flip with probability p>0.5" (s_1 Ber(0.5), s_{i+1}=(1-s_i)*t_i+s_i*t_i and t_i is Ber(p)
# then a useful definition of a p-value is

pval <- function(f){
	sum(res>=f)/length(res)
}

# where f is an observed number of flips in the sequence of 20 observed 0-1 variables.
# Here we used the empirical simulated distribution. This is useful if we do not know the theoretical distribution.

# We reject at 5% level if the p-value pval of the observed number of flips is smaller than or equal to 0.05

pval(14)

# 14 would be rejected
# Interpretation, 14 or more flips are an extreme observation which happens in less than 5% of all cases if H_0 is true, hence we reject the assumption that H_0 is true.

##############################
# model diagnostics
##############################

x=runif(100)
y=1+x+0.2*rnorm(100)
fit<-lm(y~x)
summary(fit)
plot(fit) # 4 plots for model diagnostics


####################
# Plot1: Residuals vs fitted (Tukey-Anscombe plot)
####################
# Are there obvious trends, misspecifications in the model, non-constant variance,...?
plot( fit$fitted, fit$residuals)
# see last time


####################
# Plot 2: qq-plot
####################
# Does the distribution of the residuals look like a normal distribution?
# It compares theoretical quantiles with empirical quantiles of normal distribution

ltSamples<-rcauchy(1000) #long tailed samples
mean(ltSamples) # nice fact: mean is again cauchy distributied, no strong law of large numbers
plot(ltSamples)
hist(ltSamples)

qqnorm(ltSamples)

#reproduce by hand
normalQuantiles<-qnorm((1:1000-0.5)/1000)
sampleQuantiles<- ltSamples[order(ltSamples)]
plot(normalQuantiles,sampleQuantiles)
title("qqplot by hand")

stSamples<-runif(1000,min=-1,max=2) #short tailed samples
plot(stSamples)
hist(stSamples)

qqnorm(stSamples) # has S shape

nSamples<-rnorm(1000) # normal samples
plot(nSamples)
hist(nSamples)
qqnorm(nSamples) #should lie more or less on a straight line (random)
# but at the borders it can deviate because there are few observations

####################
# Plot 3: Scale-Location
####################
# Theory says that the residuals are not exactly normally distributed even if the samples come from the linear model. (noise != residuals)
# => standardize residuals
plot(fit,which=3)

####################
# Plot 4: Residuals vs Leverage
####################
# Leverage: Depending on the position of an observation in the predictor space, the regression plane is more sensitive to the changes in the corresponding y-value, (measure of sensitivity)
# If Leverage AND standardized residual of a sample are large then it might have influenced the regression plane substantially.
plot(fit,which=5)


##############################
# useful R hints
##############################

# extract coefficients and standard errors

s<-summary(fit)
coef<-s$coefficients
str(coef)
colnames(coef)
rownames(coef)

beta_x<-coef["x","Estimate"]
se_x<-coef["x","Std. Error"]

