\documentclass[11pt]{article}

\usepackage{hedgehog}

\newcommand{\handout}[4]{
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { { Summary } \hfill #1 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #4  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { {\em #2 \hfill #3} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newcommand{\lecture}[4]{\handout{#2}{#3}{Prepared By: #4}{#1}}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{thm}{Theorem}[section]
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem{defn}[thm]{Definition}
\newtheorem*{eg}{Example}
\newtheorem*{ex}{Exercise}
% \newtheorem{conj}[thm]{Conjecture}[section]

\theoremstyle{remark}
\newtheorem*{rmk}{Remark}
\newtheorem*{nota}{Notation}
\newtheorem*{note}{Note}
\newtheorem*{conven}{Convention}
\newtheorem*{conj}{Conjecture}

\begin{document}

\lecture{Probabilistic Method in Combinatorics}{Autumn 2018}{All the best! \smiley}{GuoXian Yau}

\section{Bachman-Landau Notations/Asymptotic Notations}
Let $f(n)$ and $g(n)$ be arbitrary functions.\\
\begin{tabularx}{\textwidth}{l m{2 in} X}
	\rowcolor{purple!25} Notation & Description & Formal Definition\\
	$f(n) = o(g(n))$ & $f$ is asymptotically dominated by $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| < C g(n)$ \\
	\rowcolor{purple!25} $f(n) = O(g(n))$ & $|f|$ is asymptotically bounded above by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| \le C g(n)$\\
	$f(n) = \Theta(g(n))$ & $f$ is asymptotically bounded above and below by $g$ & $\exists C_1,C_2 > 0$, $\exists n_0 > 0$ s.t. $\forall n > n_0$, $C_1 g(n) \le f(n) \le C_2 g(n)$\\
	\rowcolor{purple!25} $f(n)  \sim g(n)$ & $f$ is on the order of $g$ & $\forall \ee > 0, \exists n_0 : \forall n > n_0, |\frac{f(n)}{g(n)} - 1| < \ee$\\
	$f(n) = \Omega(g(n))$ & $f$ is asymptotically lower bounded by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $f(n) \ge Cg(n)$\\
	\rowcolor{purple!25} $f(n) = \omega(g(n))$ & $f$ asymptotically dominates $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| > C |g(n)|$
\end{tabularx}
One can show the following relation using $\ee$-$\dd$ proofs.
\begin{multicols}{2}
	\begin{itemize}
		\item $f(n) = o(g(n)) \iff \lim_{n} \frac{f(n)}{g(n)} = 0$
		
		\item $f(n) = O(g(n)) \iff \limsup_{n} \frac{|f(n)|}{g(n)} < \infty$
		
		\item $f(n) = \Theta(g(n)) \iff f(n) = O(g(n)) \wedge f(n) = \Omega(g(n))$
		
		\item $f(n) \sim g(n) \iff \lim_{n} \frac{f(n)}{g(n)} = 1$
		
		\item $f(n) = \Omega(g(n)) \iff \liminf_{n} \frac{f(n)}{g(n)} > 0$
		
		\item $f(n) = \omega(g(n)) \iff \lim_{n} \left| \frac{f(n)}{g(n)} \right| = \infty$
	\end{itemize}
\end{multicols}

\section{Simple Analytical Bounds}
Most commonly seen inequalities.
\begin{itemize}\item $\sum_{k = 0}^{n} {n \choose k} = 2^n$, ${n \choose k} \le 2^n$
	
	\item $\frac{(n-k+1)^k}{k!} \le {n \choose k} \le \frac{n^k}{k!}$
	
	\item (Sterling's Inequality) $(\frac{n}{k})^k \le {n \choose k} \le (\frac{en}{k})^k$
	
	\item (Sterling's Approximation) $k! \sim  \sqrt{2 \pi k} (\frac{k}{e})^k$
	
	\item (Harmonic Number) For $n \in \bN$, let $H_n := \sum_{i=1}^{n} i^{-1}$. Then $\ln n \le H_n \le \ln n + 1$
	
	\item For any $x \in \bR$, $(1 + x) \le \sum_{n \in \bN_0} \frac{x^n}{n!} = e^x$
	
	\item For any $x \in \bR_{\ge 0}$, if $C$ is large enough, then $e^x \le 1 + Cx$
	
	\item (Ratios of Binomial Coefficients) If $k \le n \le m$, then
	\[
		\left( \frac{n-k+1}{m-k+1} \right)^k \le \frac{{n \choose k}}{{m \choose k}} = \prod_{i=0}^{k-1} \frac{n-i}{m-i} \le \left( \frac{n}{m} \right)^k
	\]
\end{itemize}

\section{Concentration Inequalitites}
\begin{thm}
	(Union Bound) Let $X_1,\cdots,X_n$ be random variables. Then $\bP(\bigcup_{k=1}^{n} X_k) \le \sum_{k=1}^{n} \bP(X_k)$.
\end{thm}

\begin{thm}
	(Markov Inequality) Let $X$ be a non-negative random variable. Then
	\[
		\bP(X \ge t) \le \frac{\bE[X]}{t} \equiv \bP(X \ge s \bE[X]) \le \frac{1}{s}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\bE[X]}$.
\end{thm}

\begin{thm}
	(Chebychev Inequality) Let $X$ be a random variable. Then
	\[
		\bP( |X - \bE[X]| \ge t) \le \frac{\var[X]}{t^2} \equiv \bP(X \ge s \sqrt{\var[X]}) \le \frac{1}{s^2}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\var[X]}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then for every $\dd > 0$, $\bP(X \ge (1+\dd) \mu) \le \exp(- \frac{\exp(\dd)}{(1+\dd)^{(1+\dd)}})^{\mu}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds, simplified version) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then
	\begin{itemize}
		\item $\bP(X \ge (1+\dd)\mu) \le \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \le (1-\dd)\mu) \le \exp(-\frac{\mu \dd^2}{2})$ for all $0 < \dd \le 1$.
		
		\item $\bP(|X - \mu| \ge \dd \mu) \le 2 \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \ge t) \le 2^{-t}$ for all $t \ge 2 e \mu$.
	\end{itemize}
\end{thm}

\begin{coro}
	(Chernoff's Bounds, Alon \& Spencer version) For independent Bernoulli random variables $X_i \overset{i.i.d.}{\sim} Ber(p)$, and $X = \sum_{i=1}^{n} X_i$, we have the following bounds.
	\begin{enumerate}
		\item If we set
		\[
			X_i := \begin{cases}
				1 & p = \frac{1}{2}\\
				-1 & 1 - p = \frac{1}{2}
			\end{cases}
		\]
		Then $\bP(|X| \ge a) \le 2 \exp(-\frac{a^2}{2n})$.
		
		\item If we have
		\[
			X_i := \begin{cases}
				1 & p = \frac{1}{2}\\
				0 & 1 - p = \frac{1}{2}
			\end{cases}
		\]
		Then we have $\bP(|X - \frac{n}{2}| \ge a) \le 2 \exp(-\frac{na^2}{6})$.
		
		\item If we have
		\[
			X_i := \begin{cases}
				1 & \text{With probability $p$}\\
				0 & \text{With probability $1-p$}
			\end{cases}
		\]
		Then we have $\bP(|X - np| \ge \dd np) \le 2 \exp(-\frac{\dd^2 np}{3})$ for $0 < \dd < 1$.
	\end{enumerate}
\end{coro}

\begin{thm}
	(Azuma-Hoeffding) Let $(\Omega,\bP)$ be product of $N$ discrete probability space, call them $(\Omega_1,\bP_1), \ldots, (\Omega_N,\bP_N)$, and let $X : \Omega \to \bR$ be a random variable. Suppose $c_i$'s are the Lipscitz constants associated to the space $(\Omega_i,\bP_i)$. Then for all $t \ge 0$, $\bP(X \ge \bE[X] + t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; $\bP(X \le \bE[X] - t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; succinctly, $\bP( |X - \bE[X]| \ge t) \le 2 \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$.
\end{thm}

\section{Conjunctive Normal Form/Disjunctive Normal Form}
Let $X = x_1,\cdots,x_n$ be the literals, and $C_1,\cdots,C_m$ be the clauses, with $\set{y_{i,1},\cdots,y_{i,l(i)}} \subseteq X$. Let us denote by $f$ the formula, then
\begin{itemize}
	\item (Conjunctive Normal Form, CNF) $f = C_1 \wedge \ldots \wedge C_m$
	
	\item (Disjunctive Normal Form, DNF) $f = C_1 \vee \ldots \vee C_m$
\end{itemize}
Let $l(i) := |C_i|$. The 3SAT problem is the decision problem with $l(i) = 3$ for all $i = 1,\ldots,m$; it is NP-complete. In general, a $k$-SAT formula is when $l(i) = k$ for all $i = 1,\ldots,m$.

\section{Main Ideas}
\subsection{Minimum Set Cover}
Given elements $[n]$ and subsets $S_1,\ldots,S_m \subseteq [n]$, with costs $c(S_i)$, we want to find minimum number of sets that cover all $n$ elements. Use greedy algorithm with respect to price-per-item at each stage. The approximation factor is $H_n$, i.e. $H_n := \sum_{k=1}^{n} \frac{1}{k}$.

\subsection{Knapsack}
Given $n$ items, with values $v_i$, size $s_i$, and a budget $B$, we want to maximize the total value of items we put in the knapsack while maintaining the total size to be less than budget. Use dynamic programming to solve this exactly, but the input is exponential. To get a FPTAS, scale down the profit by dividing by $k := \max \set{1,\floor{\frac{\ee p_{\max}}{n}}}$, and solve with exact algorithm. The approximation factor is $(1+\ee)$. FPTAS knapsack runs in $O(\frac{n^2 p_{\max}}{k})$ time. Runs in $O(\frac{1}{\ee} n^{\frac{h+1}{\ee}})$ time.

\subsection{Bin Packing}
Given items of sizes, and identical bins of total size 1. We want to pack all items using least bins possible. First discard smaller sizes items, and round up the rest of the items to each category. Use another round down instances with its exact solution for all but last category of items. Open one bin for each item for the last bin, and then put in the rest of the small items. Approximation factor is $(1+\ee)$. Similar for bin covering, but we first round down the instance and then round up the instance instead, and put all remaining small items in one bin if possible.

\subsection{Minimum Makespan Scheduling/Load Balancing}
We have $n$ jobs, and $m$ identical machines. Each job has a processing time $p_i$, and want to minimize makespan (time at which all jobs on all machines are done). Graham's algorithm is greedy at least loaded machine at each given time. This is a 2-approximation. Modified Graham is $\frac{4}{3}$-approximation. For instance $I$ with a better $(1+\ee)$ PTAS, first ignore small jobs, and reduce this to bin packing problem with bin sizes $t$, where we guess $t \in [|\opt(I)|, 2 |\opt(I)|]$, and make guesses $t$ with binary search for minimum $t$ such that the minimum bin used is $m$, and we remove items of sizes $< \ee t$ and proceed as bin packing. First fit runs in $O(mn)$ time and 

\subsection{FPRAS}
For DNF counting with $m$ clauses and $n$ literals, we sample the 1's and count the number of top-most 1's, in a $m \times f(I)$ table, where $f(I)$ is the number of satisfied assignments. Each entry is 1 if the assignment satisfies the clause and 0 otherwise. Runtime is $O(\frac{mn^2(m+n)}{\ee^2})$\\

For (proper) graph colouring counting, we sample a (not necessary proper) random colouring, and estimate the number of proper colouring. Runtime is $O(mk(n \log \frac{n}{\ee} + m))$ for $|V| = n, |E| = m$, with $k$ colouring.

\subsection{Rounding Integer Programming}
Relax the ILP to LP, solve it and get optimal solution. Using optimal solution, interpret them as probability of each variable being 1 or 0. Minimizing congestion in multi-commodity model is done precisely this way.

\subsection{Probabilistic Tree Embedding}
The idea is as follows: choose a random enumeration of the vertices $\set{v_1,\ldots,v_n}$, and diameter $D = \diam(G)$. We carve the balls by putting into a set the vertices that are radius $\frac{D}{2^{i+2}}$ at level $i$, in the predefined order. This will partition the vertex set; do this repeatedly until each ball contains one vertex. This algorithm is ConstructT in the scribe notes. To get an actual tree, use ContractT, which will contract an edge incident to an auxiliary vertex until no auxiliary vertex left.

\subsection{Counting Majority Elements}
Set up a list of guesses and counter; if observe new element, put it into the list of guesses and increase counter; if list is full, and new element is observe, reduce all counter by 1, and replace (arbitrarily) the guess whose counter reached 0.

\subsection{Streaming Algorithms}
The idea is to estimate the parameter of interest in an unbiased way without remembering all input. We only need to make a decision at the end of the stream. Two examples are given for this, namely $k^{th}$ moment estimator, and graph streaming algorithms for finding cuts.\\

For general $k^{th}$ moment estimation for a stream of $m$ elements, set $X = m(r^k - (r-1)^k)$; here, we pick $i \in [m]$ randomly, and $r$ is the number of latter elements (inclusive this one) in the stream that are the same.\\

For graph sketching, we use ComputeSketches to get a series of cuts, say $XOR_1,\ldots,XOR_n$, and StreamingMaximalForest simulates Boruvska's algorithm by joining connected components using $XOR_i$'s. For each vertex, $O(\log n)$ memory is used to remember the ID, and $O(\log^3 n)$ is number of copies per vertex. So in total $O(n \log^4 n)$ is used.

\subsection{Preserving Distance}
We use $(\alpha,\beta)$-spanners to preserve distance. For $\alpha$-multiplicative spanners, enumerate the edges $E$, and if $e = uv$ and $\dist_T(u,v)$ is currently larger than $2k$, then add this to the edge set of the tree. Otherwise ignore it. The number of edges in the end will be $O(n^{1 + \frac{1}{k}})$, where $k$ is largest such that $g(G) > 2k$.\\

For $\beta$-additive spanner, we know that if $\beta = 2$, then we require $\tilde{O}(n^{\frac{3}{2}})$ edges. The construction is to add all edges incident to light weight vertices; choose a subset $S \subseteq V$ (independently with some $p$), and for each $v \in S$, build a BFS tree rooted at $v$ and add them into the edge collection.\\

Sacrificing for a larger $\beta$, we can get by using $\tilde{O}(n^{\frac{7}{5}})$ edges. Define light weight vertices to be the ones with $\deg_G(v) \le n^{\frac{2}{5}}$, and the rest as heavy vertices. As before, add all edges incident to light vertices. Pick a random subset $S \subseteq V$ (independently with some probability $p$), and build BFS tree for each $v \in  S$. Finally, pick another subset $T \subseteq V$, and if there is a heavy vertex $w$ adjacent to some $t \in T$, add this to the set.

\subsection{Preserving Cuts}
One can sparsify the graphs while preserving minimum cuts too. Given a graph $G$, we want to come up with a weighted graph $H$ such that the minimum cut of $H$ is within $\ee$ error of minimum cut of $G$. To do this, sample the edge from $E$ with probability $p$ independently for each edge, and set the weight $w(e) = \frac{1}{p}$ for the ones being sampled. More generally, for non-uniform sampling suppose $k_e$ is the strength of $e$, then set $p_e := \frac{k_e}{q}$ for some $q = \frac{c \log n}{\ee^2}$ and weight $w(e) = \frac{1}{p_e}$. Minimum cut size of $H$ is $\mu(H) \ge \frac{c \log n}{\ee^2}$ for uniform sampling, and $\bE[|E'|] \le O(\frac{n \log n}{\ee^2})$.

\subsection{Online Algorithms}
Given an online (possibly randomized) algorithm $\cA$, and $\opt$ is the optimal offline algorithm, $\cA$ is $\alpha$ competitive if $c_{\cA}(\sigma) \le \alpha c_{\opt}(\sigma)$ for any input $\sigma$ and a cost function $c(\cdot)$.\\

For ski-rental problem, rent for the first $B$ days, and buy the ski on $B+1$ day; this yield 2-competitive algorithm.\\

For the linear search problem, we use the move-to-front (MTF) algorithm. The cost will be $\Phi_t$ which is defined by the number of different $(i,j)$ pairs in $\opt$ and our algorithm. We can think of $\Phi$ as the number of swaps required between $\opt$ stack and $MTF$ stack. MTF is 2-competitive.\\

For paging problem, we have $k$ slots for the cache and infinite size for the slow memory. We want to minimize cache misses, namely pages that are not in the cache upon request. Any conservative online algorithm (like FIFO) is $k$-competitive. Using random marking algorithm, that is to unmark all marked pages and evict an arbitrary marked page is $O(\log k)$-competitive (more precisely, it is $O(H_k)$ where $H_k$ is the harmonic constant. Optimal offline strategy is obviously to evict the page needed latest down the line and make request so that there is no cache misses.\\

One should keep in mind Yao's minimax principle, namely given an algorithm space $\cA$ and input space $X$, we have $\max_{a \in \cA} \bE_p [ c(a,x) ] \le \min_{x \in X} \bE_q[c(a,x)]$ where $p = \bP(A = a)$ and $q = \bP(X = x)$.\\

In the $k$-server problem, we have a sequence of input $\sigma$, and we want to minimize the distance travelled by the servers. Double coverage is to move two nearest servers to the request at the same time. Double coverage is $k$-competitive.

\end{document}