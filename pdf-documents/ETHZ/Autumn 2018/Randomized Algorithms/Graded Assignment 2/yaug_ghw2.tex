\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Randomized Algorithms - Graded Homework 2}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Randomized Algorithms - Graded Homework 2}
\author{Guo Xian Yau}
\date{\today}

\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{prop}{Proposition}

\begin{document}

\maketitle

\begin{center}
	\textit{\centering Collaborator(s): Konstantinos Andrikopoukos, Pontus Westermark, Simon Weber}
\end{center}

\paragraph{Problem 1} Let $A \in \set{0,+1,-1}^{n \times n}$ be a matrix, $c \in \bZ^n$, and we are given the fractional solution $y \in [0,1]^{n}$ such that $A y = c$. Let us define $\hat{x} \in \set{0,1}^{n}$ by the following
\[
	\hat{x}_i = \begin{cases}
		1 & \text{With probability $y_i$}\\
		0 & \text{With probability $1 - y_i$}
	\end{cases}
\]
where $\hat{x}_i$ and $y_i$ are the $i^{th}$ coordinate of the vector $\hat{x}$ and $y$ respectively. Now fix an arbitrary $i \in [n]$, and let $a_{i}$ denote by $a_i = (a_{ij})_{j \in [n]}$, i.e. the $i^{th}$ row of matrix $A$.\\

By construction, we know that $\bE[\hat{x}_i] = y_i$ for all $i \in [n]$. So we can compute
\[
	\bE [a_i^T \hat{x}] \overset{(LE)}{=} \sum_{j=1}^{n} a_{ij} \bE[\hat{x}_j] = \sum_{j=1}^{n} a_{ij} y_j = c_i
\]
where $(LE)$ is by linearity of expectation. Let $Z_i := a_i^T \hat{x}$. If we consider $\Omega_i := Ber(y_i)$, then $\hat{x}$ has distribution $\Omega := \prod_{i=1}^{n} \Omega_i$, and therefore $Z_i : \Omega \to \bR$. We can apply Azuma's inequality to $Z_i$; notice that the effect of each $\hat{x}_j$ is at most 1, since $a_{ij} \hat{x}_j \in \set{0,\pm 1}$. By Azuma,
\begin{align*}
	\bP(|Z_i - \bE[Z_i]| \ge t) \le \exp ( -  \frac{t^2}{2 \sum_{j=1}^{n} 1^2}) = \exp(- \frac{t^2}{2n})
\end{align*}
Choosing $t = \sqrt{3 n \log n}$, we get
\[
	\bP(|Z_i - \bE[Z_i]| > t) \le \bP(|Z_i - \bE[Z_i]| \ge t) \le \exp( - \frac{3n\log n}{2n}) = \exp ( - \frac{3}{2} \log n) = O(n^{-\frac{3}{2}})
\]
Hence if $n \to \infty$, $\bP(|Z_i - \bE[Z_i]| > t) \to 0$; in other words, if $n$ is large enough, the probability that $|Z_i - \bE[Z_i]| = |\sum_{j=1}^{n} a_{ij} \hat{x}_j - c_i| > \sqrt{3n \log n}$ is very small.\\

Note that since we have chosen $i \in [n]$ arbitrarily, this probability holds for any $a_i^T \hat{x} - c_i$. In order to prove $\norm{Ax - c}_{\infty} < \sqrt{3 n \log n}$, we need every $i \in [n]$ satisfy the probability that when $n$ is large enough, probability that $|a_i^T \hat{x} - c_i| > \sqrt{3 n \log n}$ is small. Applying union bound at $(UB)$, we get for any $\hat{x} \in \Omega$,
\begin{gather*}
	\bP(\norm{A\hat{x} - c}_{\infty} \le \sqrt{3 n \log n})\\
	= 1 - \bP(\exists i \in [n] : |a_i^T \hat{x} - c_i| > \sqrt{3n \log n})\\
	\ge 1 - \sum_{i=1}^{n} \bP(|a_i^T \hat{x} - c_i| > \sqrt{3n \log n})\\
	= 1 - n \cdot O(n^{-\frac{3}{2}}) = 1 - O(n^{-\frac{1}{2}}) \xrightarrow[]{n \to \infty} 1 - 0 = 1
\end{gather*}
Since any probability is at most 1, thus probability $\hat{x}$ is an approximate solution to the equation $Ax = c$ such that $\norm{A\hat{x} - c}_{\infty} \le \sqrt{3n \log n}$ is asymptotically 1, i.e. when $n$ is large enough, the probability that $\norm{A \hat{x} - c}_{\infty} \le \sqrt{3n \log n}$ is 1. \qed

\newpage

\paragraph{Problem 2} Let $G = (V,E) \in G_{n,\frac{1}{2}}$ be a random graph. Suppose we also have $s \gg (\log n)^3$ and $a = \sqrt{\log s} = \sqrt{3 \log \log n}$. Our goal is to prove every subset of $V$ of size $s$ has an independent set of size $a$.\\

Let us define, for $S \subseteq [n]$, $|S| = s$ and $A \subseteq S$, $|A| = a$, the following indicator variable
\[
	X_{S,A} = \begin{cases}
		1 & \text{$A$ is an independent set}\\
		0 & \text{Otherwise}
	\end{cases}
	, ~ ~ ~ ~ ~ X_S = \sum_{\substack{A \subseteq S\\|A| = a}} X_{S,A} 
\]
$\bE[X_{S,A}] = 2^{- {a \choose 2}}$ since $S$ and $A$ are fixed; in particular, $\lambda := \bE[X_S] = {s \choose a} 2^{-{a \choose 2}}$.\\

We would like to apply Janson to $X_S$; for this, we need to also compute $\bE[X_{S,A} X_{S,A'}]$. We have
\begin{gather*}
	\Delta := \bE[X_{S,A} X_{S,A'}] = \bP(X_{S,A} = X_{S,A'} = 1)\\
	= \sum_{m = 2}^{a-1} {s \choose m} {s - m \choose a - m} {s - a \choose a - m} 2^{- 2 {a \choose 2} + {m \choose 2}}\\
	\le 2^{-{a \choose 2}} \sum_{m = 2}^{a-1} \Theta(s^m) \Theta(s^{a - m}) \Theta(s^{a-m})\\
	= 2^{-{a \choose 2}} \sum_{m = 2}^{a-1} \Theta(s^{2a - m}) = 2^{-{a \choose 2}} \Theta(s^{2a - 2})
\end{gather*}
Now consider $\frac{\lambda^2}{\lambda + \Delta}$, we have the following long chain of computations
\setcounter{equation}{0}
\begin{gather}
	\frac{\lambda^2}{\lambda + \Delta} = \frac{{s \choose a}^2 2^{-2{a \choose 2}}}{{s \choose a} 2^{-{a \choose 2}} + \sum_{m = 2}^{a-1} {s \choose m} {s - m \choose a - m} {s - a \choose a - m} 2^{- 2 {a \choose 2} + {m \choose 2}}}\\
	\ge \frac{{s \choose a}^2 2^{- 2 {a \choose 2}}}{2^{-{a \choose 2}} {s \choose a} + 2^{-{a \choose 2}} \Theta(s^{2a - 2})}\\
	= \frac{{s \choose a}^2 2^{-{a \choose 2}}}{\Theta(s^a) + \Theta(s^{2a - 2})}\\
	= \frac{{s \choose a}^2 2^{-{a \choose 2}}}{2 \Theta(s^{2a - 2})} \ge \frac{(\frac{s}{a})^{2a} 2^{-{a \choose 2} - 1}}{\Theta(s^{2a - 2})}\\
	= \Theta(s^2) \cdot \Theta(s^{-{a \choose 2} \frac{\ln 2}{\ln s}}) \cdot \Theta(s^{-2a \frac{\ln a}{\ln s}})\\
	= \Theta (s^{2 - \frac{\ln 2}{2} - 2a \cdot \frac{\ln a}{\ln(\exp(a^2))}})\\
	= \Theta (s^{2 - \frac{\ln 2}{2} - 2 \frac{\ln a}{a}}) \\
	= \Theta(s^{2 - \frac{\ln 2}{2} + o(1)}) > \Theta(s^{\frac{4}{3}})
\end{gather}
We now justify each line. Line (1) is by definition; from (1) to (2), we use the fact that $\Delta \le 2^{-{a \choose 2}} \Theta(s^{2a - 2}) \implies (\lambda + \Delta)^{-1} \ge (\lambda + 2^{-{a \choose 2}} \Theta(s^{2a - 2}))^{-1}$; from (2) to (3), we cancel out the factor $2^{-{a \choose 2}}$ from both the numerator and denominator; from (3) to (4), clearly $\Theta(s^a) \le \Theta(s^{2a - 2})$, thus  we have
\[
	\Theta(s^a) + \Theta(s^{2a - 2}) \le 2 \Theta(s^{2a - 2}) \implies (\Theta(s^a) + \Theta(s^{2a - 2}))^{-1} \ge (2 \Theta(s^{2a - 2}))^{-1}
\]
the inequality in line (4) is due to Sterling's inequality i.e. ${s \choose a} \ge (\frac{s}{a})^a$, also note that we have moved the 2 at the denominator to the numerator; from (4) to (5), we need to justify each term, first we have $\frac{s^{2a}}{\Theta(s^{2a - 2})} = \Theta(s^2)$, $2^{-{a \choose 2} - 1} = \Theta(s^{-{a \choose 2} \frac{\ln 2}{\ln s}})$ since
\[
	2^{- {a \choose 2} - 1} = \frac{1}{2} s^{-{a \choose 2} \log_s 2} = \Theta(s^{- {a \choose 2} \frac{\ln 2}{\ln s}})
\]
and similarly, using change of base we also have $a^{-2a} = \Theta(s^{-2a \frac{\ln a}{\ln s}})$; from (5) to (6), note that ${a \choose 2} = \frac{a(a-1)}{2} = \frac{1}{2} \Theta(a^2) = \frac{1}{2} \Theta(\log s)$, so $-{a \choose 2} \frac{\ln 2}{\ln s} = -\frac{\ln 2}{2}$, and we also have $a = \sqrt{\log s} $ implying $s = \exp(a^2)$; from (7) to (8) is due to $\ln a = o(a)$, thus $\frac{\ln a}{a} = o(1)$; finally, in (8), clearly $2 > \frac{4}{3}$, hence the inequality hold.\\

Now by Janson's inequality, for any $S \subseteq [n]$, we have 
\[
	\bP(\text{$S$ does not have independence set of size $a$}) = \exp( - \frac{\lambda^2}{2(\lambda + \Delta)}) < \exp(- \frac{1}{2} \Theta(s^\frac{4}{3}))
\]
So by union bound, we have
\begin{gather*}
	\bP(\text{Some subset of size $s$ has independent set of size $a$})\\
	= 1 - \sum_{S \subseteq [n]} \bP(\text{$S$ does not have independence set of size $a$})\\
	\overset{(UB)}{\ge} 1 - {n \choose s} \bP(\text{$S$ does not have independence set of size $a$})\\
	> 1 - \Theta(n^s) \exp(-\frac{1}{2} \Theta(s^{\frac{4}{3}}))\\
	= 1 - \exp(\Theta(s \ln n) - \Theta(s^{\frac{4}{3}}))\\
	= 1 - \exp( \Theta(s (\underbrace{\ln n - s^{\frac{1}{3}}}_{\ll 0}) ) \gg 1 - \ee
\end{gather*}
for some arbitrary small $\ee > 0$. Notice that $s >> \log^3 n \implies \log n - s^{\frac{1}{3}} \ll 0$. Thus with high probability that every set of size $s$ has an independent set of size $a$. \qed

\pagebreak

\paragraph{Problem 3} Let $0 < p_i < 1$, $i \in [n]$ be probability that face $i$ show up in any independent throws. Suppose we throw $m$ times, independently for each throw, and $D_j$ be the outcome of throw $j \in [m]$. Let $\tilde{p}_i = \frac{1}{m} \sum_{j=1}^{m} X_{ij}$, where $X_{ij} = \i1(D_j = i)$.\\

Let $(\Omega_j,\bP_j)$ be the probability space that for the $j^{th}$ throw. So $\bP_j (D = i) = p_i$ for any $i \in [n]$ and any $j \in [m]$, where $D$ is the outcome of the $j^{th}$ throw. Then we can define $(\Omega,\bP) = \prod_{j=1}^{m} (\Omega_j,\bP_j)$ to be the outcome of all $m$ throws. Choose $D = (D_j)_j \in \Omega$ randomly. Let $0 < \dd, \ee < 1$.
\begin{enumerate}[(a)]
	\item Let $m = \ceil{\frac{6}{\dd^2} \log \frac{2 n}{\ee}}$. Notice that $\tilde{p}_{i}$ is a random variable for all $i \in [n]$. Fixed an arbitrary $i \in [n]$, then we have
	\[
		\bE[\tilde{p}_i] \overset{(L.E.)}{=} \frac{1}{m} \sum_{j=1}^{m} \bE[X_{ij}] = \frac{1}{m} \sum_{j=1}^{m} \bP(X_{ij} = 1) = p_i \cdot (\frac{1}{m} \cdot m) = p_i
	\]
	Moreover, if we consider $c_j$ to be the maximum effect of the $j^{th}$ throw on $\tilde{p}_i$, then we know that $c_j = \frac{1}{m}$; if $D_j = i$, then we add $\frac{1}{m}$ to $\tilde{p}_i$, otherwise the effect is 0. Thus by Azuma's inequality $(A)$, we have
	\[
		\bP(|\tilde{p}_i - p_i| > t) \le \bP(|\tilde{p}_i - p_i| \ge t) \overset{(A)}{\le} 2 \exp ( - \frac{t^2}{2 \sum_j c_j^2}) = 2 \exp( - \frac{t^2 m}{2})
	\]
	Now choose $t = \dd$, then we have
	\[
		\bP(|\tilde{p}_i - p_i| > \dd) \le 2 \exp( - \frac{\dd^2 m}{2}) \le 2 \exp( - 3 \log \frac{2n}{\ee}) = \frac{\ee^3}{4n^3}
	\]
	Finally, we take union bound over all $\tilde{p}_i$, and get 
	\begin{gather*}
		\bP(\forall i \in [n] : |\tilde{p}_i - p_i| \le \dd) = 1 - \bP(\exists i \in [n] : |\tilde{p}_i - p_i| > \dd)\\
		\ge 1 - \sum_{i=1}^{n} \bP(|\tilde{p}_i - p_i| > \dd) \ge 1 - n \cdot \frac{\ee^3}{4n^3} = 1 - (\frac{\ee}{2n})^2 \ee \ge 1 - \ee
	\end{gather*}
	As we desired in the problem.
	
	\item We will assume $m \ge C := C(\dd,\ee)$, and choose $C$ later. Similar setup as above, we have $X_{ij} = \i1(D_j = i)$. Notice that $\sum_{i=1}^{n} X_{ij} \equiv 1$, so each $\set{X_{ij}}_{i=1}^{n}$ are negatively correlated, using the lemma proven in class. Let notice that since each throw is independent, so $\set{X_{ij}}_{i}$'s and $\set{X_{ij'}}_{i}$'s are also mutually independent. Thus $\set{X_{ij}}_{i,j}$ are also negatively correlated by another lemma proven in class.\\
	
	We will use the negative association later. For now, since $\dd \in (0,1)$, we can break the interval into $(0,\dd) \cup [\dd,1)$. We claim that there are at most $\frac{1}{\dd}$ many $p_i$'s are such that $p_i \in [\dd,1)$; suppose otherwise, then we have $\sum_{i=1}^{n} p_i > \frac{1}{\dd} \cdot \dd = 1$ which is a contradiction that $\sum p_i = 1$. Thus we conclude there are $\le \frac{1}{\dd}$ many of $p_i$'s are such that $p_i \in [\dd,1)$ Using similar argument to part (a), we will get that $\bP(|\tilde{p}_i - p_i| > \dd) \le \frac{\ee^3}{4 n^3}$. Therefore by union bound $(UB)$, we have
	\begin{gather*}
		\bP(\exists i \in [n] : |\tilde{p}_i - p_i| > \dd) \overset{(UB)}{\le} \frac{1}{\dd} \bP(|\tilde{p}_i - p_i| > \dd)\\
		\le \frac{1}{\dd} \cdot 2 \frac{\ee^3}{4n^3} \overset{(*)}{\le} \frac{\ee^3}{4} \cdot \frac{\dd^3}{\dd} = \frac{\ee^3 \dd^2}{4}\\
		\le \frac{1}{\dd} \cdot 2 \exp(-\frac{\dd^2 m}{2}) \overset{(*)}{\le} \frac{2}{\dd} \exp (-\frac{\dd C}{2})
	\end{gather*}
	Note that $m \ge C \implies -m \le -C$, and $\exp(\cdot)$ is a strictly increasing function, hence $(*)$ is valid. If we now set $\frac{1}{\dd} \cdot [\frac{2}{\dd} \exp (-\frac{\dd C}{2})] \le \frac{\ee}{2}$ and solve for $C$, we have $C \ge - \frac{2}{\dd} \log \frac{\ee \dd^2}{4}$. It remains to find $C$ for the case when $p_i \in (0,\dd)$.\\
	
	Define $X_i = \sum_{j=1}^{m} X_{ij}$, then $X_i$ counts the number of times $i$ shows up in $m$ throws. Then $\tilde{p}_i = m X_i$ for all $i \in [n]$. Moreover, $\bE[X_i] = m \bE[\tilde{p}_i] = m p_i$, so we have
	\[
		\bP(|\tilde{p}_i - p_i| \le \dd) = \bP(|m \tilde{p}_i - m p_i| \le m \dd) = \bP(|X_i - \bE[X_i]| \le m \dd)
	\]
	But notice that $p_i \in (0,\dd)$ implies that 
	\[
		\bP(X_i - \bE[X_i] \le - m \dd) = \bP(X_i \le m p_i - m \dd) = 0
	\]
	as $X_i \ge 0$ but $m p_i - m \dd < 0$. Therefore the probability 
	\[
		\bP(|X_i - \bE[X_i]| \ge m \dd) = \bP(X_i - \bE[X_i] \ge m \dd) + 0 = \bP(X_i \ge \bE[X_i] + m \dd)
	\]
	Now since $p_i \in (0,\dd)$, we may assume that $p_i \in [\frac{\dd}{2^{k+1}},\frac{\dd}{2^k})$ for some $k \in \bN_0$. Therefore we have $2^k \cdot p_i < 2^k \cdot \frac{\dd}{2^k} = \dd$. Thus we have
	\[
		\bE[X_i] + m \dd > m p_i + 2^k m p_i = (1 + 2^k) mp_i = (1+2^k) \bE[X_i]
	\]
	This implies that we can rewrite the previous probability as
	\[
		\bP(X_i \ge \bE[X_i] + m \dd) = \bP(X_i > (1+\gamma)\bE[X_i]) \le \bP(X_i \ge (1+\gamma)\bE[X_i]) \overset{(C)}{\le} \left( \frac{\exp(\gamma)}{(1+\gamma)^{1+\gamma}} \right)^{\bE[X_i]}
	\]
	where $\gamma = 2^k$. Note that we apply Chernoff bound at $(C)$ and get
	\begin{align*}
		\bP(X_i > (1+\gamma)\bE[X_i]) \le & \left( \frac{\exp(2^k)}{(1 + 2^k)^{1 + 2^k}} \right)^{m p_i} = \left( \frac{e}{(1 + 2^k) \cdot (1+2^k)^{\frac{1}{2^k}}} \right)^{2^k p_i m}\\
		= &  \left( \frac{e}{(1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{2^{k+1} \cdot p_i}{2} \cdot m} \overset{(*)}{\le} \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd m}{2}}
	\end{align*}
	Observe that $f(x) = \frac{e^x}{(1 + x)^{1+x}} \le 1$ for $x \in \bZ_{\ge 0}$; when $x = 0$, we have $f(0) = 1$; when $x = 1$, $f(x) = \frac{e}{4} < 1$; in general, for $x > 1$, we have $f(x) = (\frac{e}{1+x})^x \cdot \frac{1}{1+x}$, and since $\frac{e}{1+x} < 1$, so $f(x) < 1$. In particular, if $x \in [0,1]$, and that $y < z$, then $x^z < x^y$; in other words, since $p_i \in [\frac{\dd}{2^{k+1}},\frac{\dd}{2^k})$, therefore $p^i \cdot 2^{k+1} \ge \dd$, so $(*)$ holds. Also keep in mind that eventually we want to choose $m \ge C$, therefore we also have
	\[
		\left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd m}{2}} \le \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd C}{2}}
	\]
	Let $I_k := [\frac{\dd}{2^{k+1}},\frac{\dd}{2^{k}})$. Now for $p_i \in (0,\dd)$, we get that
	\begin{gather*}
		\bP(\exists i \in [n] : p_i \in (0,\dd), |\tilde{p}_i - p_i| > \dd)\\
		\overset{(UB)}{\le} \sum_{k=0}^{\infty} \frac{2^{k+1}}{\dd} \bP(p_i \in I_k, |\tilde{p}_i - p_i| > \dd)\\
		= \sum_{k=0}^{\infty} \frac{2^{k+1}}{\dd} \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd m}{2}}
	\end{gather*}
	Again, $(UB)$ is by union bound. We want to make $\bP(\exists i \in [n] : p_i \in (0,\dd), |\tilde{p}_i - p_i| > \dd) \le \frac{\ee}{2}$ as well, so let $C$ be such that $\sum_{k=0}^{\infty} \frac{2^{k+1}}{\dd} \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd C}{2}} \le \frac{\ee}{2}$; in particular, we will set $C$ such that $\frac{2^{k+1}}{\dd} \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd C}{2}} \le \frac{\ee}{2} \cdot \frac{1}{2^{k+1}}$. Now notice that the infinite sum $\sum_{k=0}^{\infty} \frac{1}{2^{k+1}} = 1$. For each $k \in \bN_0$, we can get the following estimate
	\setcounter{equation}{0}
	\begin{gather}
		\frac{2^{k+1}}{\dd} \left( \frac{e}{ (1+2^k)^{1 + \frac{1}{2^k}}} \right)^{\frac{\dd C}{2}} \le \frac{\ee}{2} \cdot \frac{1}{2^{k+1}}\\
		\equiv \frac{\dd C}{2} \log  \frac{e}{(1+2^k)^{1 + 2^{-k}}} \le \log \frac{\ee \dd}{2^{2k+3}}\\
		\equiv \frac{\dd C}{2} \ge \frac{\log \frac{\ee \dd}{2^{2k+3}}}{\log  \frac{e}{(1+2^k)^{1 + 2^{-k}}}}\\
		\equiv C \ge \frac{2 \log \frac{\ee \dd}{2^{2k+3}}}{\dd \log  \frac{e}{(1+2^k)^{1 + 2^{-k}}}} = \frac{2}{\dd} \cdot \frac{\log (\ee \dd) - (2k+3) \log 2}{\log e - (1+2^{-k}) \log (1+2^k)}
	\end{gather}
	Note that from line (2) to (3), we reversed the inequality because $\frac{e}{(1+2^k)^{1 + 2^{-k}}} < 1$ as we have shown before. Consider 
	\[
		\frac{\log (\ee \dd) - (2k+3) \log 2}{\log e - (1+2^{-k}) \log (1+2^k)}
	\]
	This ratio achieves its maximum when $k = 0$, therefore $C \ge \frac{2[\log(\ee \dd) - 3 \log 2]}{\dd[\log e - 2 \log 2]}$. Finally, if we choose $C$ with
	\[
		C \ge \max \set{- \frac{2}{\dd} \log \frac{\ee \dd^2}{4}, \frac{2[\log_2 (\ee \dd) - 3]}{\dd[\log_2 e - 2]}}
	\]
	then we will get the following inequalities.
	\setcounter{equation}{0}
	\begin{gather}
		\bP(\forall i \in [n]: |\tilde{p}_i - p_i| \le \dd)\\
		= 1 - \bP(\exists i \in [n] : |\tilde{p}_i - p_i| > \dd)\\
		\ge 1 - \sum_{i=1}^{n} \bP(|\tilde{p}_i - p_i| > \dd)\\
		= 1 - \sum_{k=0}^{\infty} \frac{2^{k+1}}{\dd} \bP(p_i \in I_k, |\tilde{p}_i - p_i| > \dd) - \frac{1}{\dd} \bP(p_i \in [\dd,1), |\tilde{p}_i - p_i| > \dd)\\
		\ge 1 - \frac{\ee}{2} - \frac{\ee}{2} = 1 - \ee
	\end{gather}
	Which is as we desired. We will now justify each line above. From (1) to (2) is by the fact that probability of $A$ happens is  $1 - \bP(A^C)$, where $A^C$ is the complement of event $A$; from (2) to (3), we use union bound; from (3) to (4), we separate the $p_i$'s into corresponding  intervals $I_k$'s and $[\dd,1)$; finally from (4) to (5) is by the choice of $C$, so that when $m > C$, the probabilities are $\frac{\ee}{2}$ respectively. This concludes the proof. \qed
\end{enumerate}

\pagebreak

\paragraph{Problem 4} Let $G = (V,E)$ be a graph, $|V| = n$, and $K := \chi(G)$. First, observe that for any $G_1,G_2$ graphs on the same vertex set, let $c_1,c_2$ be colouring of $G_1,G_2$ with $\chi(G_1),\chi(G_2)$ colours. Then the assignment $c(v) = (c_1(v),c_2(v))$ for all $v \in V(G_1)$ is a valid colouring. Therefore $\chi(G_1 \cup G_2) \le \chi(G_1) \chi(G_2)$. By induction if there are $G_1,\cdots,G_k$ graphs on the same vertex, then $\chi(\prod_{i=1}^{k} G_i) \le \prod_{i=1}^{k} \chi(G_i)$.\\

Consider the family $G_{n,\frac{1}{k}}$, we will construct $k$ subgraphs of $G$ that is in this family. Let $H_1,\cdots,H_k$ be graphs on $V$, and for each $e \in E$, we set $\bP(e \in E(H_i)) = \frac{1}{k}$ for all $i \in [k]$. Clearly $H_i$'s are subgraphs of $G$, in fact, $H_i$'s partition the edge set $E$. Moreover, since probability of $e$ exists in $H_i$ is $\frac{1}{k}$ for all $e \in E$, therefore $H_i \in G_{n,\frac{1}{k}}$ for all $i = 1,\cdots,k$.\\

Now for $e \in E$, and $i \in [k]$, let us define the following variable:
\[
	X_{e,i} = \begin{cases}
		1 & e \in E(H_i)\\
		0 & e \notin E(H_i)
	\end{cases}
\]
By construction, $\sum_{i=1}^{k} X_{e,i} \equiv 1$. So using a lemma in class, since $X_{e,i}$'s are indicator variables, so $X_{e,i}$'s are negatively associated when $e$ is fixed. Recall we also have another result proven in class that any $e,f \in E$ such that $e \ne f$, if $\set{X_{e,i}}_i$ and $\set{X_{f,i}}_{i}$ are mutually independent; knowing that $e$ is in $H_i$ does not affect the probability that $f$ is in $H_i$ and vice versa. Then  $\set{X_{e,i},X_{f,i}}_{i=1}^{k}$ is again negatively associated. Hence $\set{X_{e,i}}_{e \in E, i \in [k]}$ is negatively associated.\\

We now claim that for any $e_0 \notin E(H_i)$, we have
\[
	\chi(H_i) \le \chi(H_i \cup \set{e_0}) \le \chi(H_i) + 1
\]
Since $e_0 \notin E(H_i)$, so take a proper colouring of $H_i$. If the colouring on the vertices of $e_0$ are different, then the chromatic number remains the same; otherwise, we will need to add at most 1 colour to make it into a proper colouring, this explains the upper and lower bound. In particular, $\chi(\cdot)$ is a non-decreasing function w.r.t. to $E$.\\

By definition of negative association, we have $\bE[\chi(H_i) \cdot \chi(H_j)] \le \bE[\chi(H_i)] \cdot \bE[\chi(H_j)]$ for any $i,j \in [n]$. This means that we have the following inequality.
\[
	\chi(G) = \bE[\chi(G)] = \bE[\chi(\bigcup_{i=1}^{k} H_i)] \le \bE[\prod_{i=1}^{k} \chi(H_i)] \le \prod_{i=1}^{k} \bE[\chi(H_i)]
\]
Note that $\chi(G) = \bE[\chi(G)]$ because it is a constant. Finally, notice that each copy of $H_i$ is a random graph from $G_{n,\frac{1}{k}}$, thus they have the same expectation i.e. if $H \in G_{n,\frac{1}{k}}$, then $\bE[\chi(H_i)] = \bE[\chi(H)]$, and we get $\chi(G) \le (\bE[\chi(H)])^{k}$, thus proving the inequality as required.

\end{document}