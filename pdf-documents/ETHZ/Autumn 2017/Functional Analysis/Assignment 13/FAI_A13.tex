\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 13}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 13}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\paragraph*{Problem 13.1} (Definitions of Resolvent Set) Let $(X,\norm{\cdot}_X)$ be a complex Banach space, and $A : D_A \subseteq X \to X$ be a linear operator. Let $T_{\lambda} := \lambda I - A$. Consider the following two sets:
\begin{itemize}
	\item $\rho(A) = \set{\lambda \in \bC: \text{$T_{\lambda} : D_A \to X$ is bijective, and $\exists T_{\lambda}^{-1} \in L(X)$}}$
	
	\item $\tilde{\rho}(A) = \set{\tilde{\lambda} \in \bC: \text{$T_{\tilde{\lambda}} : D_A \to X$ is injection, $\bar{\im (T_{\tilde{\lambda}})} = X$, and $\exists T_{\lambda}^{-1} \in L(X)$}}$
\end{itemize}
Prove that the two sets above are equivalent if $\Gamma(A)$ is closed.\\

First, notice that $\Gamma(A)$ is closed if and only if $\Gamma(T_{\lambda})$ is closed for $\lambda \in \bC$; let $(x_n)_n \in X^{\bN}$ be a sequence such that $x_n \to x$, where $x \in X$. Since $\Gamma(A)$ is closed, so $\lim Ax_n = Ax$. Note that the identity map $I$ is continuous and hence $\Gamma(I)$ is also closed. Therefore,
\[
	\lim_n T_{\lambda} x_n = \lim_n (\lambda I - A) (x_n) = \lim_n \lambda x_n - \lim_n A x_n = \lambda x - Ax = T_{\lambda} x
\]
and $\Gamma(T_{\lambda})$ is indeed closed. The converse is analogous to this.\\

Let $\tilde{\lambda} \in \tilde{\rho}(A)$, then $T_{\tilde{\lambda}}^{-1}$ exists, and $T_{\tilde{\lambda}}$ is an injection. We want to prove that $T_{\tilde{\lambda}}$ is a bijection; if so, then $\tilde{\lambda} \in \rho(A)$, and $\tilde{\rho}(A) \subseteq \rho(A)$. We claim that $T_{\tilde{\lambda}}^{-1}$ is an injection. Let $x \in \ker(T_{\tilde{\lambda}}^{-1}) \subseteq X$, so we have the following implication
\[
	T_{\tilde{\lambda}}^{-1} (x) = 0 \implies x = T_{\tilde{\lambda}} \circ T_{\tilde{\lambda}}^{-1} (x)) = T_{\tilde{\lambda}} (0) = 0
\]
by linearity. Thus $\ker(T_{\tilde{\lambda}})$ is trivial, and $T_{\tilde{\lambda}}^{-1}$ is injective. By Cantor-Schr\"{o}der-Bernstein, $T_{\tilde{\lambda}}$ is a bijection and $\tilde{\lambda} \in \rho(A)$.\\

Conversely, suppose $\lambda \in \rho(A)$, then $T_{\lambda}$ is a bijection. Let $x \in X$ be arbitrary, and let $(x_n)_n \in X^{\bN}$ be a sequence such that $x_n \to x$. Since $\Gamma(T_{\lambda})$ is closed, so $\lim T_{\lambda} (x_n) = T_{\lambda} (x) \in \im(T_{\lambda})$. In particular, it is surjective, i.e. $\im(T_{\lambda}) = X$. So $\bar{\im(T{\lambda})} = \bar{X} = X$ is dense, and $\lambda \in \tilde{\rho}(A)$. This proves that $\rho(A) \subseteq \tilde{\rho}(A)$, and we are done. \qed

\paragraph*{Problem 13.2} (Unitary Operators) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space. Let $T \in L(\cH)$. Prove the following:
\begin{enumerate}
	\item $T$ is unitary if and only if $T$ is a isometric bijection.
	
	\item If $T$ is unitary, then its spectrum lies on the unit circle, i.e. $\sigma(T) \subseteq S^1 = \set{z \in \bC: |z| = 1}$.
\end{enumerate}
First, we will prove part 1. By definition, $T$ is unitary if $T^* = T^{-1}$. So $T^{-1}$ exists. Moreover,
\begin{align*}
	T^* = T^{-1} \iff & T \circ T^{*} = T^{*} \circ T = \id_{\cH} \iff T^* \circ T (x) = x\\
	\iff & \inp{T^* T x}{x}_{\cH} = \inp{x}{x}_{\cH} = \norm{x}_{\cH}^2\\
	\iff & \norm{Tx}_{\cH}^2 = \inp{Tx}{Tx}_{\cH} = \inp{T^* \circ T (x)}{x}_{\cH} = \norm{x}_{\cH}^2
\end{align*}
Thus $T$ is unitary if and only if $T$ is an isometric bijection. As a corollary, $T^*$ is also an isometric bijection.\\

Now to prove the second part, if $T$ is unitary, by argument above $T$ is a isometric bijection. Let $\lambda \in \sigma(T)$, then $(\lambda I - T)$ is not invertible. In particular, let $x \in \cH$ be a non-zero element, then consider the equation $(\lambda I - T)x = 0$, we have
\[
	0 = (\lambda I - T) x = \lambda x - Tx \iff Tx = \lambda x \iff x = \lambda T^{-1} x = \lambda T^* x
\]
Therefore by definition of unitary, we have
\[
	\inp{Tx}{Tx}_{\cH} = \norm{Tx}_{\cH}^2 = \norm{x}_{\cH}^2 = \norm{\lambda T^* x}_{\cH}^2 = |\lambda|^2 \norm{T^* x}^2 = |\lambda|^2 \norm{x}_{\cH}^2
\]
This means that $|\lambda|^2 \norm{x}_{\cH}^2 = \norm{x}_{\cH}^2 \implies |\lambda| = 1$, as desired. \qed

\paragraph*{Problem 13.3} (Integral Operators Revisited) Let $\Omega \subseteq \bR^m$ be a bounded subset, $k \in L^2(\Omega \times \Omega)$ s.t. $k(x,y) = k(y,x)$ a.e. on $\Omega \times \Omega$, and $K : L^2(\Omega) \to L^2(\Omega)$, $f \mapsto \int_{\Omega} k(x,y) f(y) ~ dy$. We have seen that $K$ is a compact operator from previous assignment. Denote $A := I - k$, where $I$ is the identity operator, and $(\cH,\inp{\cdot}{\cdot}_{\cH}) = (L^2(\Omega;\bR),\inp{\cdot}{\cdot}_{L^2(\Omega;\bR)})$.\\

By close range theorem, $\ker(A) = \im(A^*)^{\perp}$ and $\im(A) = {}^{\perp} \ker(A^*)$. In particular, if $A$ is self-adjoint, i.e. $A = A^*$ and $D_A = D_{A^*}$, then $\ker(A) = \im(A^*)^{\perp} = \im(A)$. Thus if $A$ is injective, we get $\im(A)^{\perp} = \ker(A) = \{0\}$, and indeed injectivity of $A$ is equivalent to surjectivity of $A$.\\

It remains to show that $A$ is self adjoint. First notice that $A$ is self adjoint if and only if $K$ is self adjoint;
\[
	A = A^* \iff (I - K) = (I - K)^* \iff I - K = I^* - K^* = I - K^* \iff K = K^*
\]
So it suffices to show that $K$ is self-adjoint. Let $f,g \in \cH$ be chosen arbitrarily; without loss of generality, assume that $\norm{f}_{\cH} = \norm{g}_{\cH} = 1$. We want to show that
\[
	\inp{Kf}{g} = \inp{K^* f}{g}_{\cH}  \equiv \inp{Kf}{g}_{\cH} = \inp{f}{Kg}_{\cH}
\]
So we have the following equality:
\begin{align*}
	\inp{Kf}{g}_{\cH} = & \int_{\Omega} (Kf)(x) g(x) ~ dx = \inp{\Omega \times \Omega} k(x,y) f(y) g(x) ~ dy ~ dx\\
	\overset{*}{=} & \int_{\Omega} \left( \int_{\Omega} k(y,x) g(x) ~ dx \right) f(y) ~ dy\\
	= & \int_{\Omega} (Kg)(y) f(y) ~ dy = \inp{Kg}{f}_{\cH} \overset{\circ}{=} \inp{f}{Kg}_{\cH}
\end{align*}
Where (*) is due to Fubini's theorem; $\int_{\Omega} k(x,y) f(y) g(x) ~ dy$ is integrable clearly. Also note that ($\circ$) is true because we are in a real Hilbert space. Thus $K$ is self-adjoint as claimed. \qed

\paragraph*{Problem 13.4} (Resolvent and Spectral Distance) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space. Let $A,B \in L(\cH)$ be self-adjoint operators.
\begin{enumerate}[(a)]
	\item Let $\lambda \in \rho(A)$. We want to show that the resolvent $R_{\lambda} := (\lambda I - A)$ is a normal operator, i.e. $R_{\lambda} R_{\lambda}^* = R_{\lambda}^* R_{\lambda}$, where $I$ is the identity operator.We have
	\begin{align*}
		R_{\lambda} R_{\lambda}^* = & (\lambda I - A)^{-1} \circ [(\lambda I - A)^{-1}]^* = (\lambda I - A)^{-1} \circ [(\lambda I - A)^*]^{-1}\\
		= & (\lambda I - A)^{-1} \circ [\bar{\lambda} I^* - A^*]^{-1} = (\lambda I - A)^{-1} \circ [\bar{\lambda} I - A]^{-1}\\
		= & \left[|\lambda|^2 I - (\lambda + \bar{\lambda}) A + A^2 \right]^{-1} = \left[ \bar{\lambda} \cdot \lambda I^2 - (\bar{\lambda} + \lambda) A + A^2 \right]^{-1}\\
		= & (\bar{\lambda} I - A)^{-1} \circ (\lambda I - A)^{-1} = R_{\lambda}^* R_{\lambda}
	\end{align*}
	where $\bar{\lambda}$ is the complex conjugate of $\lambda$. So the resolvent is indeed a normal operator. \qed
	
	\item \textcolor{red}{Come back to this}
\end{enumerate}

\paragraph*{Problem 13.5} (Heisenberg's Uncertainty Principle) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a complex Hilbert space. Let $A: D_A \subseteq \cH \to \cH$, $B: D_B \subseteq \cH \to \cH$ be symmetric linear operators, with $D_A,D_B \subseteq \cH$ being dense subspaces. The commutator $[A,B]: D_{[A,B]} \subseteq \cH \to \cH$ defined by $[A,B] = AB - BA$, where $D_{[A,B]} = D_A \cap D_B$.
\begin{enumerate}[(a)]
	\item Let $x \in D_{[A,B]}$ be arbitrary. Expanding the inner product, we get
	\begin{align*}
		|\inp{x}{[A,B]x}_{\cH}| = & | \inp{x}{AB x}_{\cH} - \inp{x}{BA x}_{\cH}| = |\inp{A^* x}{Bx}_{\cH} - \inp{B^* x}{Ax}_{\cH}|\\
		\overset{\circ}{=} & |\inp{A x}{Bx}_{\cH} - \inp{B x}{Ax}_{\cH}| \overset{\triangle-ineq}{\le} |\inp{Ax}{Bx}_{\cH}| + |\inp{Bx}{Ax}_{\cH}|\\
		\overset{C.S.}{\le} & \norm{Ax}_{\cH} \norm{Bx}_{\cH} + \norm{Bx}_{\cH} \norm{Ax}_{\cH} = 2 \norm{Ax}_{\cH} \norm{Bx}_{\cH}
	\end{align*}
	Note that ($\circ$) is true because $A$ and $B$ are symmetric. Therefore the inequality holds.
	
	\item Define the standard deviation of a symmetric operator $A$ to be
	\[
		\varsigma(A,x) := \sqrt{\inp{x}{A(Ax)}_{\cH} - \inp{x}{Ax}^2}, ~ ~ ~ x \in D_A, ~ \norm{x}_{\cH} = 1
	\]
	Now using the fact that $A$ is a symmetric operator, notice the following facts:
	\begin{itemize}
		\item $\inp{x}{A(Ax)}_{\cH} = \inp{Ax}{Ax}_{\cH} = \norm{Ax}_{\cH}^2$
		
		\item $\inp{x}{Ax}_{\cH}^2 = \inp{x}{Ax}_{\cH} \cdot \inp{Ax}{x}_{\cH} = \inp{x}{Ax}_{\cH} \cdot \bar{\inp{x}{Ax}_{\cH}} = |\inp{x}{Ax}_{\cH}|^2$
	\end{itemize}
	So we can rewrite $\varsigma(A,x) = \left( \norm{Ax}_{\cH}^2 - |\inp{x}{Ax}_{\cH}|^2 \right)^{\frac{1}{2}}$. It suffices to find two symmetric operators $\tilde{A}$, $\tilde{B}$ such that $\varsigma(A,x)^2 = \norm{\tilde{A}x}_{\cH}^2$, $\varsigma(B,x)^2 = \norm{\tilde{B}x}_{\cH}^2$, such that $[A,B] = [\tilde{A},\tilde{B}]$. If we can find such operators $\tilde{A},\tilde{B}$, then we get that
	\[
		2 \varsigma(A,x) \varsigma(B,x) = 2 \norm{\tilde{A}x}_{\cH}^2 \norm{\tilde{B}x}_{\cH}^2 \ge |\inp{x}{[\tilde{A},\tilde{B}]x}_{\cH}| = |\inp{x}{[A,B]x}_{\cH}|
	\]
	by part (a), and we are done. So it remains to find the operators $\tilde{A},\tilde{B}$.\\
	
	We claim that $\tilde{A} := A - k_A I$, where $k_A := \inp{x}{Ax}_{\cH}$ satisfies $\varsigma(A,x)^2 = \norm{\tilde{A}x}_{\cH}^2$.
	\[
		\norm{\tilde{A}x}_{\cH}^2 = \norm{Ax}_{\cH}^2 - k_A \underbrace{\inp{x}{Ax}_{\cH}}_{= k_A} - k_A \underbrace{\inp{Ax}{x}_{\cH}}_{k_A} + k_A^2 \underbrace{\norm{x}_{\cH}^2}_{= 1} = \varsigma(A,x)^2
	\]
	We can define $\tilde{B}$ analogously and such that $k_B := \inp{x}{Bx}_{\cH}$. Moreover, the commutator of the operators yields
	\begin{align*}
		[\tilde{A},\tilde{B}] = & [A - k_A I, B - k_B I] = [A,B - k_B I] - [k_A I,B - k_B I]\\
		= & [A,B] - k_B [A,I] - k_A [I,B] - k_A k_B [I,I]\\
		= & [A,B] - 0 - 0 + 0 = [A,B]
	\end{align*}
	Note that $[I,T] = 0$ for any operator $T$ since the identity operator $I$ commutes with any operator $T$. Therefore we are done.
	
	\item Since $D_B = \cH$, so $D_{B^2} = \cH$; by induction $D_{B^n} = \cH$ for all $n \in \bN$. Therefore we have $D_{[A,B^n]} = D_A \cap D_{B^n} = D_A$. We will first look at $n = 2$, we get
	\begin{align*}
		[A,B^2] = & (AB)B - B (BA) = (AB) B - (BA) B + (BA) B - B (BA)\\
		= & [A,B]B + B[A,B] = B(i \id_{D_{[A,B]}}) + (i \id_{D_{[A,B]}}) B\\
		= & i B \id_{D_{[A,B]}} + (i \id_{D_{[A,B]}} B = 2i B
	\end{align*}
	For $n \ge 3$, we have $[A,B^{n-1}] = (n-1) i B^{n-2}$ similar idea, we get
	\begin{align*}
		[A,B^n] = & AB^n - B^n A = (AB) B^{n-1} - B^{n-1} (BA) + \sum_{k=1}^{n} [B^k A B^{n-k} - B^k A B^{n-k}]\\
		= & \sum_{k=0}^{n-1} [B^k(AB - BA) B^{n-k}] = n i B^{n-1}
	\end{align*}
	Where we take $B^0 = I = \id$.\\
	
	Now suppose $A$ has finite operator norm, then
	\begin{align*}
		2 \norm{A}_{L(D_A,\cH)} \norm{B^n}_{L(\cH)} \ge & 2 \norm{Ax}_{\cH} \norm{Bx}_{\cH} \ge |\inp{x}{[A,B^n]x}_{\cH}| = |\inp{x}{niB^{n-1} x}_{\cH}|\\
		= & |n| \cdot |i| \cdot |\inp{x}{B^{n-1}x}_{\cH}| \ge n \norm{B}_{L(\cH)}^{n-1}
	\end{align*}
	Now since $\norm{B}_{L(\cH)}^{n} \ge \norm{B^n}_{L(\cH)}$, so $\norm{A}_{L(D_A,\cH)} \norm{B}_{L(\cH)} \ge \frac{n}{2}$. So $\norm{A}_{L(D_A,\cH)} \norm{B}_{\L(\cH)}$ is unbounded, contradicting $\norm{A}_{L(D_A,\cH)}$ and $\norm{B}_{L(\cH)}$ are both finite.
	
	\item Let $(\cH,\inp{\cdot}{\cdot}_{\cH}) = (L^2([0,1];\bC),\inp{\cdot}{\cdot}_{L^2([0,1];\bC)})$, and define the following subspaces
	\begin{itemize}
		\item $D_P := C_0^1([0,1];\bC) = \set{f \in L^2([0,1];\bC): f \in C^1([0,1];\bC), f(0) = f(1) = 0}$
		
		\item $D_Q := L^2([0,1];\bC) = \cH$
	\end{itemize}
	Now define the two operators $P$ and $Q$ as follows:
	\begin{itemize}
		\item $P : D_P \to \cH$, $f \mapsto i f'$
		
		\item $Q : \cH \to \cH$, $f(s) \mapsto s f(s)$.
	\end{itemize}
	Then $Pf \in \cH$, since 
	\begin{align*}
		\int_{0}^{1} |Pf|^2 = & \int_0^1 |e^{i \frac{\pi}{2}}|^2 |f'(s)|^2 ~ ds = |(e^{i \frac{\pi}{2}})^2| \cdot \int_0^1 |f'(s) \cdot f'(s)| ~ ds\\
		\overset{*}{\le} & \norm{f'}_{\cH}^2 \cdot \norm{f'}_{\cH}^2
	\end{align*}
	where (*) is due to H\"{o}lder with H\"{o}lder's conjugate being $p = q = 2$. Thus $P$ is well defined. $Q$ is also well defined since $\int_{0}^{1} s^2 (f(s))^2 ~ ds$ is integrable from last week's assignment.\\
	
	Now $[P,Q]$ is also well defined since $Qf \in D_P$ for all $f \in D_{[P,Q]}$; note that $f(0) = f(1) = 0$, so $Qf(0) = Qf(1) = 0$ trivially. Moreover, $Qf$ is continuous because multiplication is continuous, and $(Qf)' = f(s) + sf'(s)$ is also continuous as multiplication and addition are both continuous. Thus $[P,Q]$ is well defined.\\
	
	It is easy to see that $P,Q$ are symmetric; using integration by parts, we can see that for any $f,g \in D_P$, indeed $\inp{f}{Pg}_{\cH} = \inp{Pf}{g}_{\cH}$. To show that $Q$ is symmetric, observe that $|f(s) \bar{sg(s)}| = |\bar{f(s) \bar{sg(s)}}|$, and that $|s| = \bar{s}|$, and hence $Q$ is symmetric.\\
	
	Finally, we have
	\[
		([P,Q] f) (s) = [(PQ - QP)(f)](s) = if(s) + s(P(f)) - s(P(f)) = if(s)
	\]
	So by part (b), we get
	\[
		2 \varsigma(A,x) \varsigma(B,x) \ge |\inp{f}{[P,Q]f}_{\cH}| = |i \norm{f}_{\cH}^2| = 1
	\]
	As we desired.
\end{enumerate}
This concludes the proof of Heisenberg's Uncertainty Principle. Quod Erat Demonstrandum.

\end{document}