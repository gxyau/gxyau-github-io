\documentclass[11pt]{article}

\usepackage{hedgehog}

\newcommand{\handout}[4]{
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { { Summary } \hfill #1 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #4  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { {\em #2 \hfill #3} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newcommand{\lecture}[4]{\handout{#2}{#3}{Prepared By: #4}{#1}}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{thm}{Theorem}[section]
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem{defn}[thm]{Definition}
\newtheorem*{eg}{Example}
\newtheorem*{ex}{Exercise}
% \newtheorem{conj}[thm]{Conjecture}[section]

\theoremstyle{remark}
\newtheorem*{rmk}{Remark}
\newtheorem*{nota}{Notation}
\newtheorem*{note}{Note}
\newtheorem*{conven}{Convention}
\newtheorem*{conj}{Conjecture}

\begin{document}

\lecture{Randomized Algorithms \& Probabilistic Method}{Autumn 2018}{All the best! \smiley}{GuoXian Yau}

\section{Bachman-Landau Notations/Asymptotic Notations}
Let $f(n)$ and $g(n)$ be arbitrary functions.\\
\begin{tabularx}{\textwidth}{l m{2 in} X}
	\rowcolor{purple!25} Notation & Description & Formal Definition\\
	$f(n) = o(g(n))$ & $f$ is asymptotically dominated by $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| < C g(n)$ \\
	\rowcolor{purple!25} $f(n) = O(g(n))$ & $|f|$ is asymptotically bounded above by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| \le C g(n)$\\
	$f(n) = \Theta(g(n))$ & $f$ is asymptotically bounded above and below by $g$ & $\exists C_1,C_2 > 0$, $\exists n_0 > 0$ s.t. $\forall n > n_0$, $C_1 g(n) \le f(n) \le C_2 g(n)$\\
	\rowcolor{purple!25} $f(n)  \sim g(n)$ & $f$ is on the order of $g$ & $\forall \ee > 0, \exists n_0 : \forall n > n_0, |\frac{f(n)}{g(n)} - 1| < \ee$\\
	$f(n) = \Omega(g(n))$ & $f$ is asymptotically lower bounded by $g$ & $\exists C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $f(n) \ge Cg(n)$\\
	\rowcolor{purple!25} $f(n) = \omega(g(n))$ & $f$ asymptotically dominates $g$ & $\forall C > 0$, $\exists n_0$ s.t. $\forall n > n_0$, $|f(n)| > C |g(n)|$
\end{tabularx}
One can show the following relation using $\ee$-$\dd$ proofs.
\begin{multicols}{2}
	\begin{itemize}
		\item $f(n) = o(g(n)) \iff \lim_{n} \frac{f(n)}{g(n)} = 0$
		
		\item $f(n) = O(g(n)) \iff \limsup_{n} \frac{|f(n)|}{g(n)} < \infty$
		
		\item $f(n) = \Theta(g(n)) \iff f(n) = O(g(n)) \wedge f(n) = \Omega(g(n))$
		
		\item $f(n) \sim g(n) \iff \lim_{n} \frac{f(n)}{g(n)} = 1$
		
		\item $f(n) = \Omega(g(n)) \iff \liminf_{n} \frac{f(n)}{g(n)} > 0$
		
		\item $f(n) = \omega(g(n)) \iff \lim_{n} \left| \frac{f(n)}{g(n)} \right| = \infty$
	\end{itemize}
\end{multicols}

\section{Simple Analytical Bounds}
Most commonly seen inequalities.
\begin{itemize}\item $\sum_{k = 0}^{n} {n \choose k} = 2^n$, ${n \choose k} \le 2^n$
	
	\item $\frac{(n-k+1)^k}{k!} \le {n \choose k} \le \frac{n^k}{k!}$
	
	\item (Sterling's Inequality) $(\frac{n}{k})^k \le {n \choose k} \le (\frac{en}{k})^k$
	
	\item (Sterling's Approximation) $k! \sim  \sqrt{2 \pi k} (\frac{k}{e})^k$
	
	\item (Harmonic Number) For $n \in \bN$, let $H_n := \sum_{i=1}^{n} i^{-1}$. Then $\ln n \le H_n \le \ln n + 1$
	
	\item For any $x \in \bR$, $(1 + x) \le \sum_{n \in \bN_0} \frac{x^n}{n!} = e^x$
	
	\item For any $x \in \bR_{\ge 0}$, if $C$ is large enough, then $e^x \le 1 + Cx$
	
	\item (Ratios of Binomial Coefficients) If $k \le n \le m$, then
	\[
		\left( \frac{n-k+1}{m-k+1} \right)^k \le \frac{{n \choose k}}{{m \choose k}} = \prod_{i=0}^{k-1} \frac{n-i}{m-i} \le \left( \frac{n}{m} \right)^k
	\]
	
	\item (Jensen's Inequality)
\end{itemize}

\section{Concentration Inequalitites}
\begin{thm}
	(Union Bound) Let $X_1,\cdots,X_n$ be random variables. Then $\bP(\bigcup_{k=1}^{n} X_k) \le \sum_{k=1}^{n} \bP(X_k)$.
\end{thm}

\begin{thm}
	(Markov Inequality) Let $X$ be a non-negative random variable. Then
	\[
		\bP(X \ge t) \le \frac{\bE[X]}{t} \equiv \bP(X \ge s \bE[X]) \le \frac{1}{s}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\bE[X]}$.
\end{thm}

\begin{thm}
	(Chebychev Inequality) Let $X$ be a random variable. Then
	\[
		\bP( |X - \bE[X]| \ge t) \le \frac{\var[X]}{t^2} \equiv \bP(X \ge s \sqrt{\var[X]}) \le \frac{1}{s^2}
	\]
	The above equivalence can be seen by setting $s = \frac{t}{\var[X]}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then for every $\dd > 0$, $\bP(X \ge (1+\dd) \mu) \le \exp(- \frac{\exp(\dd)}{(1+\dd)^{(1+\dd)}})^{\mu}$.
\end{thm}

\begin{thm}
	(Chernoff Bounds, simplified version) Let $X_1,\ldots,X_n$ be Bernoulli R.V. with $\bP(X_i = 1) = p_i$. Suppose $X = \sum_{i} X_i$, and $\mu = \bE[X]$, then
	\begin{itemize}
		\item $\bP(X \ge (1+\dd)\mu) \le \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \le (1-\dd)\mu) \le \exp(-\frac{\mu \dd^2}{2})$ for all $0 < \dd \le 1$.
		
		\item $\bP(|X - \mu| \ge \dd \mu) \le 2 \exp(-\frac{\mu \dd^2}{3})$ for all $0 < \dd \le 1$.
		
		\item $\bP(X \ge t) \le 2^{-t}$ for all $t \ge 2 e \mu$.
	\end{itemize}
\end{thm}

\begin{coro}
	For independent Bernoulli random variables $X_i \overset{i.i.d.}{\sim} Ber(p)$, and $X = \sum_{i=1}^{n} X_i$, we have the following bounds.
\begin{enumerate}
	\item If we set
	\[
		X_i := \begin{cases}
			1 & p = \frac{1}{2}\\
			-1 & 1 - p = \frac{1}{2}
		\end{cases}
	\]
	Then $\bP(|X| \ge a) \le 2 \exp(-\frac{a^2}{2n})$.
	
	\item If we have
	\[
		X_i := \begin{cases}
			1 & p = \frac{1}{2}\\
			0 & 1 - p = \frac{1}{2}
		\end{cases}
	\]
	Then we have $\bP(|X - \frac{n}{2}| \ge a) \le 2 \exp(-\frac{na^2}{6})$.
	
	\item If we have
	\[
		X_i := \begin{cases}
			1 & \text{With probability $p$}\\
			0 & \text{With probability $1-p$}
		\end{cases}
	\]
	Then we have $\bP(|X - np| \ge \dd np) \le 2 \exp(-\frac{\dd^2 np}{3})$ for $0 < \dd < 1$.
\end{enumerate}
\end{coro}

\begin{thm}
	(Azuma-Hoeffding) Let $(\Omega,\bP)$ be product of $N$ discrete probability space, call them $(\Omega_1,\bP_1), \ldots, (\Omega_N,\bP_N)$, and let $X : \Omega \to \bR$ be a random variable. Suppose $c_i$'s are the Lipscitz constants associated to the space $(\Omega_i,\bP_i)$. Then for all $t \ge 0$, $\bP(X \ge \bE[X] + t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; $\bP(X \le \bE[X] - t) \le \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$; succinctly, $\bP( |X - \bE[X]| \ge t) \le 2 \exp(- \frac{t^2}{2\sum_{i=1}^{N} c_i^2})$.
\end{thm}

\begin{thm}
	(Janson's Inequality) Let $(\Omega,\bP) = \prod_{i=1}^{N} (\Omega_i,\bP_i)$, $X_1,\ldots,X_m$ be indicator variables, $I_1,\ldots,l_m$ be set of coordinates such that for all $i \in [m]$, $X_i$ only depends on $I_i$. If $X := \sum_{i=1}^{m} X_i$, $\lambda = \bE[X]$, and $\Delta := \sum_{\substack{i \ne j\\ I_i \cap I_j \ne \emptyset}} \bE[X_i X_j]$, then
	\[
		\bP(X \le \bE[X] - t) \le \exp(-\frac{t^2}{2 (\lambda + \Delta)})
	\]
	In particular, if $t = \bE[X]$, then $\bP(X = 0) \le \exp ( - \frac{\lambda^2}{\lambda + \Delta}) \le \exp( -\frac{1}{4} \min \set{\lambda, \frac{\lambda^2}{\Delta}})$.
\end{thm}
\begin{rmk}
	Let $X : \Omega \to \bR$ be a random variable, where $\Omega = \prod_{i=1}^{N} \Omega_i$. Let $X_i : \Omega_i \to \bR$ be the effect of $\Omega_i$ on random variable $X$. Then $X_i$ is $c_i$-Lipschitz if $|X(\omega) - X(\omega')| \le c_i$.
\end{rmk}

\begin{thm}
	(Talagrand Inequality, Version 1) Let $(\Omega, \bP) = \prod_{i=1}^{N} (\Omega_i,\bP_i)$ be a product space consist of discrete probability spaces. Let $X: \Omega \to \bR$, and $c_i$'s are component-wise Lipschitz constant. Let $\psi: \bR \to \bR$ be such that for all $\omega \in \Omega$, and all $r \in \bR$, whenever $X(\omega) \ge r$, then there exists an index set $J \subseteq [N]$ with the property that for all $\omega' \in \Omega$ with $\omega_i' = \omega_i$, $i \in J$, then $X(\omega') \ge r$ and $\sum_{i \in J} c_i^2 \le \psi(r)$. If $m \in \bR$ is the median of $X$, then $\bP(|X - m| \ge t) \le 4 \exp(- \frac{t^2}{4 \psi(m + t)})$.
\end{thm}

\begin{thm}
	(Talagrand Inequality, Version 2) Suppose $X,(\Omega,\bP),\psi$ as above, and in addition, $c_i = 1$ for all $i$ and $\psi(r) = r$ for every $r \in \bN$, then $\bP(|X - \bE[X]| \ge t) = 4 \exp(-\Omega(\frac{t^2}{\bE[X] + t}))$.
\end{thm}

\section{Methods of Moments}
Let $(X_n)_n$ be a sequence of random variables.
\begin{description}
	\item[$1^{st}$ Moment Method] If $X_i$'s takes on non-negative integer values, then $\bE[X_n] = o(1)$ implies $\bP(X_n = 0) = 1 - o(1)$.
	
	\item[$2^{nd}$ Moment Method] If $\bE[X_n] \ne 0$ for large $n$, and $\var[X_n] = o(\bE^2[X])$, then $\bP(X_n = 0) = o(1)$.
\end{description}
If $p_0(n)$ is a threshold for probability $p$, then first moment method proves that $\bP(X = 0) = 1$ if $p \ll p_0(n)$ and second moment method proves $\bP(X = 0) = 0$ if $p \gg p_0(n)$.

\section{Negative Correlation}
\begin{defn}
	If $X,Y$ are random variables, then $X,Y$ are negatively correlated if and only if $\bE[XY] \le \bE[X] \cdot \bE[Y]$.
\end{defn}

\begin{lemma}
	Let $X_1,\ldots,X_n$ be Bernoulli random variables. If $X_i$'s are pairwise negatively correlated, i.e. $\bE[X_i X_j] \le \bE[X_i] \cdot \bE[X_j]$ for all $i \ne j$, and $X = \sum_i X_i$, then $\var[X] \le \bE[X]$.
\end{lemma}

\begin{defn}
	Random variables $X_1,\ldots,X_n$ are negatively associated if and only if every disjoint pairs $I,J \subseteq [n]$, and $f,g$ either both monotonic increasing or monotonic decreasing,
	\[
		\bE[f(X_i, i \in I) g(X_j, j \in J)] \le \bE[f(X_i, i \in I)] \bE[g(X_j, j \in J)]
	\]
	Note that $f,g$ does not have to be strictly monotonic.
\end{defn}

\begin{lemma}
	If $X = (X_1,\ldots,X_n)$ and $Y = (Y_1,\ldots,Y_m)$ are both negatively associated and mutually independent, then $(X_1,\ldots,X_n,Y_1,\ldots,Y_m)$ are negatively associated as well.
\end{lemma}

\section{Markov Chains}
To define a Markov chain, one has to specify:
\begin{itemize}
	\item State space $S$
	
	\item The sequence of random variables $(X_t)_t$
	
	\item The transition probability at time $t$, $p_{ij}(t) := \bP(X_{t+1} = j | X_{t} = i)$ for all $i,j \in S$; state at time $t+1$ only depends on state at time $t$
	
	\item To prove homogeneity, one has to prove that $p_{ij}$ is independent of $t$
\end{itemize}
Terminology for hitting times and coupling:
\begin{itemize}
	\item $T_{ij}$ is the minimum number of steps to transition from $i$ to $j$
	
	\item $h_{ij} = \bE[T_{ij}]$ is the expected hitting time
	
	\item $f_{ij} = \bP(T_{ij} < \infty)$ is the visiting probability
	
	\item $p_{ij}^{(k)}$ is the probability to transition from $i$ to $j$ in precisely $k$ steps
\end{itemize}

\begin{defn}
	A Markov chain $(X_t)_t$ is in its stationary distribution $\pi$ if $\pi P = \pi$ with the transition matrix $P$.
\end{defn}

\begin{defn}
	Periodicity of state $j$ is $\gcd \set{n \in \bN : p_{jj}^{(n)} > 0}$. It is aperiodic if $\gcd = 1$.
\end{defn}

In particular, let $(X_t)_t$ be a Markov chain with transition matrix $P$. Then $(X_t)$ is said to be:
\begin{itemize}
	\item Stationary, if the distribution $X_t = \pi$, implies that $X_{t+1} = X_t P = \pi P = \pi$
	
	\item Irreducible, if for all $i,j \in [n]$ and there exists $k \in \bN$, $p_{ij}^{(k)} > 0$.
	
	\item Aperiodic, if periodicity of all states $j$ of $(X_t)_t$ is 1.
	
	\item Ergodic, if it is aperiodic and irreducible.
	
	\item If $(X_t)_t$ is ergodic, then it is reversible if $\pi_x p_{xy} = \pi_y p_{yx}$.
\end{itemize}

To measure the converging time, we use
\begin{itemize}
	\item $\tau(\ee) := \min \set{t \in \bN :  |(q_0 P^t)_y - \pi_y| < \ee, ~ \text{for all initial distribution $q_0$ and all states $y \in S$}}$
	
	\item $\tau_{TV}(\ee) := \set{t \in \bN : \frac{1}{2} \sum_{y \in S} |(q_0 P^t)_y - \pi_y| < \ee, \text{for all initial distribution $q_0$}}$
\end{itemize}

For the Gambler's ruin, the running time is $O(n^2)$. If we have ergodic Markov chain, then the expected number of steps to visit state $j$ starting from state $i$ is $h_{ij}$. To set up a coupling, take a shuffling method, and preserving the order/method. Use the shuffling on an arbitrary initial distribution, and move the same card in the stationary distribution in the same way.

\begin{lemma}
	(10.16) Let $(M_t)_t$ be a finite Markov chain, state space $S$, and $Z_t = (X_t,Y_t)$ is a coupling, $X_t$ comes from arbitrary initial distribution, and $Y_t$ from the stationary distribution. If $\ee > 0$ is such that $\bP(X_{t_0} \ne Y_{t_0} | X_0 = x, Y_0 = y) \le \ee$, $\forall x,y \in S$, then $\tau_{TV}(\ee) \le t_0$.
\end{lemma}

\begin{lemma}
	(10.17) Let $X_t, Y_t$ as in above. If $X_t = Y_t \implies X_{t+1} = Y_{t+1}$, and $t_0$ is such that	$\bP(X_{t_0} \ne Y_{t_0} | X_0 = x, Y_0 = y) \le \frac{1}{2}$ is satisfied for all $x,y \in S$. Then $\tau_{TV}(\ee) \le \log_2 \frac{1}{\ee} \cdot t_0$, $\forall \ee > 0$.
\end{lemma}

\section{Conjunctive Normal Form/Disjunctive Normal Form}
Let $X = x_1,\cdots,x_n$ be the literals, and $C_1,\cdots,C_m$ be the clauses, with $\set{y_{i,1},\cdots,y_{i,l(i)}} \subseteq X$. Let us denote by $f$ the formula, then
\begin{itemize}
	\item (Conjunctive Normal Form, CNF) $f = C_1 \wedge \ldots \wedge C_m$
	
	\item (Disjunctive Normal Form, DNF) $f = C_1 \vee \ldots \vee C_m$
\end{itemize}
Let $l(i) := |C_i|$. The 3SAT problem is the decision problem with $l(i) = 3$ for all $i = 1,\ldots,m$; it is NP-complete. In general, a $k$-SAT formula is when $l(i) = k$ for all $i = 1,\ldots,m$.

\section{Coupon Collector}
There are $n$ cards, $X$ is the number of purchase until we collect $n$ card; $X_i$ denote the number of purchase to collect the $i^{th}$ card. Then $\bE[X] = n H_n = \Theta(n \log n)$ by linearity of expectation. If we let $X_i$ denote the number of trials until we get card $i$ upon getting $i-1$ distinct cards, we have $X_i \sim Geo(\frac{n-i+1}{n})$. By setting $p_i := \frac{n-i+1}{n}$, we get $\bE[X_i] = \frac{1}{p_i}$ and $\var[X_i] = \frac{q_i}{p_i^2}$.

\section{Hitting Times}
Let $(X_t)_t$ be a ergodic, time-homogeneous Markov chain with state space $S$, $|S| = [0,\infty)$, and let $T := \argmin_{t \in \bN} \set{X_{t} = k}$ for some $k \in \bN$. Since $(X_t)_t$ is homogeneous, and suppose $T_c$ is the first time we have $X_t \ge c$, $c \le k$, then $\bE[T|X_0 = n] = \bE[ T_c | X_0 = n] + \bE[T | X_0 = c]$. The expected hitting times and visit probabilities can be found using formula below.
\begin{itemize}
	\item $h_{ij} = 1 + \sum_{k \ne j} p_{ik} h_{kj}$ for all $i,j \in S$
	
	\item $f_{ij} = p_{ij} + \sum_{k \ne j} p_{ik} f_{kj}$ for all $i,j \in S$.
\end{itemize}

\section{Monte Carlo/Las Vegas Algorithm}
Las Vegas algorithm is just a special case of Monte Carlo algorithm. For Monte Carlo method, suppose $X$ is the random variable of interest. By using one of the concentration inequalities, we can get $\bP( X \ge (1 \pm \ee) \bE[X])$ with high probability. Then by repeating $\log \frac{1}{\dd}$ times and output the median, we get an algorithm that with $1 - \dd$ probability that we will make an error of $\ee$ both ways.

%\section{Alteration Methods}

\end{document}