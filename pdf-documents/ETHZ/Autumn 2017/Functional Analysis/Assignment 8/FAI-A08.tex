\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 8}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 8}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\begin{lemma}
	Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a real Hilbert space, and $K \subseteq \cH$ is a closed, convex set. Then for each $x_0 \in \cH$, there exists unique $y_0 \in K$ such that the following are equivalent:
	\begin{enumerate}
		\item   $d(x_0,K) = \inf_{y \in K} \norm{x_0 - y}_{\cH} = \norm{x_0 - y_0}_{\cH}$.
		
		\item For all $y \in K$, $\inp{x_0 - y_0}{y - y_0} \le 0$.
	\end{enumerate}
\end{lemma}
\begin{proof}
	We will prove uniqueness of $y_0 \in K$ later. Suppose 1 is true. Then $y_0 \in K$ the unique minimizer of $d(x_0,K)$. Now for any $y \in K$, since $K$ is convex, we can find $w \in K$ such that $y = (1-t)y_0 + tw$ for some $t \in [0,1]$. By definition of infimum, we have
	\begin{align*}
		\norm{x_0 - y_0}_{\cH} \le & \norm{x_0 - y}_{\cH} = \norm{x_0 - [(1-t)y_0 + tw]}_{\cH}\\
		= & \norm{(x_0 - y_0) - t(y_0 - w)}_{\cH}
	\end{align*}
	Therefore $\norm{x_0 - y_0}_{\cH}^2 \le \norm{(x_0 - y_0) - t(y_0 - w)}_{\cH}^2$; by using the definition of inner product, we get the following chain of inequalities
	\begin{align*}
		\inp{x_0 - y_0}{x_0 - y_0}_{\cH} \le & \inp{(x_0 - y_0) - t(y_0 - w)}{(x_0 - y_0) - t(y_0 - w)}\\
		= & \norm{x_0 - y_0}_{\cH}^2 + t^2 \norm{y_0 - w}_{\cH}^2 - 2t\inp{x_0 - y_0}{y_0 - w})
	\end{align*}
	So we get $\inp{x_0 - y_0}{y_0 - w}_{\cH} \le \frac{t}{2} \norm{y_0 - w}_{\cH}^2$. So as $t \to 0$, we get the result as desired.\\
	
	Conversely suppose 2 holds. Then $\inp{x_0 - y_0}{x_0 - y}_{\cH} \le 0$ for all $y \in K$. In particular,
	\[
		0 \le \norm{x_0 - y_0}_{\cH} = \inp{x_0 - y_0}{x_0 - y_0} \le 0
	\]
	Thus $\norm{x_0 - y_0}_{\cH} = 0$. Since $\norm{\cdot}_{\cH}$ is non-negative, therefore 
	\[
		d(x_0,K) = \inf_{y \in K} \norm{x_0 -y}_{\cH} \ge 0 = \norm{x_0 - y_0}_{\cH}
	\]
	On the other hand, $d(x_0,K) \le \norm{x_0 - y_0}_{\cH}$ by definition of infimum and the fact that $y_0 \in K$. Thus 1 holds.\\
	
	To prove uniqueness, suppose we have two minimizer, say $y_0 \in K$ and $y_0' \in K$. Then we get
	\begin{itemize}
		\item $\inp{x_0 - y_0}{y_0' - y_0}_{\cH} \le 0$
		
		\item $\inp{x_0 - y_0'}{y_0 - y_0'}_{\cH} \le 0$.
	\end{itemize}
	Adding both equations, we get
	\[
		0 \le \norm{y_0' - y_0}_{\cH} = \inp{x_0 - y_0}{y_0' - y_0}_{\cH} - \inp{x_0 - y_0'}{y_0' - y_0}_{\cH} \le 0
	\]
	So $\norm{y_0' - y_0}_{\cH} = 0$ and $y_0' = y_0$. In fact, by problem 7.7, $y_0 = P x_0$, where $P$ is the projection onto $K$.
\end{proof}

\paragraph*{Problem 8.1} (Lions-Stampacchia) Let ($\cH,\inp{\cdot}{\cdot}_{\cH})$ be a Hilbert space, $K \subseteq \cH$ be a non-empty convex subset. Suppose $f: \cH \to \bR$ is a continuous, linear functional, and $a : \cH \times \cH \to \bR$ is a bilinear map satisfying
\begin{enumerate}
	\item $a(x,y) = a(y,x)$ for all $x,y \in \cH$
	
	\item There exists $\Lambda > 0$ such that $a(x,y) \le \Lambda \norm{x}_{\cH} \norm{y}_{\cH}$ for all $x,y \in \cH$, i.e. $a(\cdot,\cdot)$ is continuous.
	
	\item There exists $\lambda > 0$, such that $a(x,x) \ge \lambda \norm{x}_{\cH}$ for all $x \in \cH$, i.e. $a(\cdot,\cdot)$ is coercive.
\end{enumerate}
Since $f$ is continuous linear functional, by Riesz representation theorem, there exists $x_f \in \cH$ such that $f(x) = \inp{x_f}{x}_{\cH}$ for all $x \in \cH$. Since $a(\cdot,\cdot)$ is a continuous, coercive, bilinear form, so by Lax-Milgram theorem, there exists an invertible $A \in L(\cH)$ such that
\[
	a(x,y) = \inp{Ax}{y}_{\cH} = \inp{x}{Ay}_{\cH} = a(y,x)
\]
Then we can write $J(x) = a(x,x) - 2f(x) = \inp{Ax - 2Ax_0}{x}_{\cH} = a(x_0 - x,x_0 - x) - a(x_0,x_0)$, where we choose $x_0 \in \cH$ such that $Ax_0 = x_f$. Note that $x_0$ is unique by invertibility of $A$.\\

Consider $Y := (\cH,	a(\cdot,\cdot))$, we claim that $Y$ is also a Hilbert space. Notice that by properties of $a(\cdot,\cdot)$, we get $\lambda \norm{x}_{\cH}^2 \le a(x,x) \le \Lambda \norm{x}_{\cH}^2$ for all $x \in \cH$, so the norm induced by $a(\cdot,\cdot)$ is equivalent to the norm induced by $\norm{\cdot}_{\cH}$, thus $Y$ is also complete. Set $d := a(x_0,x_0)$, where $x_0 \in \cH$. Then by problem 7.7(b), there exists a unique $y_0 \in K$ that minimizes $a(x - x_0,x - x_0) = d$, i.e. $y_0$ minimizes $J(x)$. Thus $J(y_0) \le J(y)$ for all $y \in \cH$.\\

The second inequality is equivalent to the statement that $f(y-y_0) - a(y_0,y-y_0) \le 0$; that means we need to prove
\begin{gather*}
	\inp{x_f - Ay_0}{y-y_0}_{\cH} = f(y-y_0) - a(y_0,y-y_0) \le 0\\
	\equiv \inp{r \cdot x_f - r \cdot Ay_0 + y_0 - y_0}{y-y_0}_{\cH}  \le 0
\end{gather*}
for all $y \in K$ and for some $r \in \bR_{+}$. We will determine this $r$ later. Let $P_K$ be the natural projection of $\cH$ onto $K$, if we can prove that $P_K (rx_f - rA y_0 + y_0) = y_0$, then by lemma 1, we are done. Define $T: \cH \to \cH$ be defined by $v \mapsto P_K(r x_f - r Av + v)$; note that $T$ is not linear, that $P_K$ is linear, and $\norm{P_K(x-y)}_{\cH} \le \norm{x-y}_{\cH}$ for all $x,y \in \cH$. For any $x,y \in \cH$, we have
\begin{align}
	\setcounter{equation}{0}
	\norm{T(x) - T(y)}_{\cH}^2 \le & \norm{(x-y) - rA (x-y)}_{\cH}^2\\
	= & \norm{x-y}_{\cH}^2 - 2r \inp{x-y}{A(x-y)} + r^2 \norm{A(x-y)}^2\\
	\le & \norm{x-y}_{\cH}^2 - 2r\lambda \norm{x-y}_{\cH}^2 + r^2 \underbrace{\norm{A}_{L(\cH)}}_{=: s} \norm{x-y}_{\cH}^2\\
	= & \norm{x-y}_{\cH}^2 ( 1 - 2r \lambda + r^2 s)
\end{align}
Note that from (2) to (3), we use the fact that $\norm{Ax}_{\cH} \le \norm{A}_{L(\cH)} \norm{x}_{\cH}$. We want to choose $r$ such that $1 - 2r \lambda + r^2 s$, i.e. $0 < r < \frac{2 \lambda}{s}$, say $r := \frac{2 \lambda}{s + 1}$. Then $T$ is a strict contraction on a Hilbert space $\cH$. By Banach fix point theorem, there exists a unique $y_0 \in \cH$ such that $y_0 = Ty_0$. Now

\paragraph*{Problem 8.2} (Duality of Sequence Spaces) Recall the sequence spaces
\begin{itemize}
	\item $c_0 := \set{(x_j)_k \in \ell^{\infty} \big| \lim_{k} x_k = 0}$
	
	\item $c := \set{(x_j)_k \in \ell^{\infty} \big| \text{$\lim_{k} x_k$ exits}}$
\end{itemize}
From previous assignment, $(c,\norm{\cdot}_{\infty})$ is a complete norm space since every Cauchy sequence converges; the pointwise distance of two sequences are closer and closer and hence their supremum norm tends to 0. Then $c_0$ is also Banach as a closed subspace of $c$.\\

Let $(X,\norm{\cdot}_{X}) = (c_0,\norm{\cdot}_{\ell^{\infty}}$, and denote the dual space of $X$ by $(X^*,\norm{\cdot}_{X^*})$.

\paragraph*{Problem 8.3} (Projection to Convex Sets) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a Hilbert space, $K \subseteq \cH$ be a closed, convex subset, $P: \cH \to K$ be the projection operator.
\begin{enumerate}[(a)]
	\item For any $x_1,x_2 \in \cH$, $Px_1,Px_2 \in K$. Applying lemma 1, we get
	\begin{itemize}
		\item $\inp{x_1 - Px_1}{Px_2 - Px_1}_{\cH} \le 0$
		
		\item $\inp{x_2 - Px_2}{Px_1 - Px_2}_{\cH} \le 0$
	\end{itemize}
	So adding both the inequalities yield
	\[
		\inp{x_1 - x_2 - P(x_1 - x_2)}{P(x_2 - x_1)} \le 0 \iff - \inp{x_1 - x_2}{P(x_1 - x_2)} + \norm{P(x_1 - x_2)}_{\cH}^2 \le 0
	\]
	So by Cauchy-Schwarz, we get
	\begin{align*}
		\norm{P(x_2 - x_1)}_{\cH}^2 \le & \inp{x_1 - x_2}{P(x_1 - x_2)} \le \norm{x_1 - x_2}_{\cH} \cdot \norm{P(x_1 - x_2)}_{\cH}
	\end{align*}
	and we are done. \qed
	
	\item Let us denote $K' := \bigcap_{x \in \cH} \set{y \in \cH| \inp{x - Px}{y-Px}_{\cH} \le 0}$, so we want to prove $K = K'$. Let $y \in K$, by lemma 1, for any $x \in \cH$, $\inp{x - Px}{y-Px}_{\cH} \le 0$, so $y \in K'$. Conversely, suppose $y' \in K'$ is chosen arbitrarily. Then $\inp{x - Px}{y'-Px}_{\cH} \le 0$ for all $x \in \cH$. In particular, choose $x = y'$, then $0 \le \norm{y' - Py'} \le 0$, thus $y' = Py'$, and hence $y' \in K$. Thus $K' = K$. \qed
\end{enumerate}

\paragraph*{Problem 8.4} (Strict Convexity) Let $(X,\norm{\cdot}_{X})$ be a normed space. Abundance lemma states that for each $x \in X$, there exists $x^* \in X^*$ such that $\norm{x^*}_{X^*}^2 = x^*(x) = \norm{x}_{X}^2$.
\begin{enumerate}[(a)]
	\item Suppose $X^*$ is strictly convex. Fix and arbitrary $x \in X$, and suppose $x_1^*,x_2^* \in X^*$ satisfy the condition 
	\[
		\norm{x_i^*}_{X^*}^2 = x_i^*(x) = \norm{x}_{X}^2, ~ ~ ~i = 1,2
	\]
	Without loss of generality, assume $\norm{x_i^*}_{X^*} = 1$ for $i = 1,2$; otherwise, we can define $y_i^* := \frac{x_i^*}{\norm{x_i^*}_{X^*}}$ for $i = 1,2$ and use $y_i^*$'s instead. Since $X^*$ is strictly convex, in particular, convex, so $x^* := \frac{x_1^* + x_2^*}{2} \in X^*$, and we get
	\[
		x^*(x) = \frac{1}{2} (x_1^*(x) + x_2^*(x)) = \frac{1}{2} (\norm{x_1^*}_{X^*}^2 + \norm{x_2^*}_{X^*}^2) = \frac{1}{2}(1+1) = 1
	\]
	By the fact that $X^*$ is strictly convex, we must have $x^* = x_1^* = x_2^*$, and hence $x^*$ is the unique element $x^* \in X^*$ such that satisfies the condition. \qed
	
	\item $X = \bR^2$, $x = \begin{pmatrix} 1 \\ 1 \end{pmatrix}$. Define $l_1, l_2 : X \to \bR$ by $l_i(x_1,x_2) \mapsto x_i$, $i = 1,2$. Then $l_i \in X^*$ clearly. But $l_1(x) = \norm{x}^2 = l_2(x)$, thus $x^*$ is not unique.
\end{enumerate}

\paragraph*{Problem 8.5} (Functional of Span of a Sequence) Let $(X,\norm{\cdot}_{X})$ be a normed space and $x := (x_n)_n \in X^{\bN}$ be a sequence in $X$, $\alpha := (\alpha_n)_{n} \in \bR^{\bN}$ be a sequence in $\bR$, we want to show that the following are equivalent.
\begin{enumerate}
	\item There exists $l \in X^*$ such that $l(x_k) = \alpha_k$ for all $k \in \bN$.
	
	\item There exists $\gamma > 0$ such that for every real sequence $(\beta_k)_k \in \bR^{\bN}$ and all $n \in \bN$, we have $|\sum_{k=1}^{n} \beta_k \alpha_k| \le \gamma \norm{\sum_{k=1}^{n} \beta_k x_k}_{X}$.
\end{enumerate}
First suppose 1 is true. Then we have
\[
	\left| \sum_{k=1}^{n} \beta_k \alpha_k \right| = \left| l \left( \sum_{k=1}^{n} \beta_k x_k \right) \right| \le \norm{l}_{X^*} \cdot \norm{\sum_{k=1}^{n} \beta_k x_k}_{X}
\]
By defining $\gamma := \norm{l}_{X^*}$ then we are done.\\

Conversely suppose 2 holds. Let $U := \Span \set{x_n}_{n \in \bN}$ be a subspace of $X$, and define $\tilde{l} : U \to \bR$ by $l(x_k) = \alpha_k$ for all $k \in \bN$, and extend it linearly to all of $U$. Also, define $p:U \to \bR$ by $p(u) = \gamma \cdot \norm{u}_{X}$, and since $\norm{\cdot}_{X}$ is linear, in particular, sublinear, then $p$ is also sublinear.\\

Now for any arbitrary $x \in U$, if $x$ is a finite sum of $x_k$'s, say $x_1,\cdots,x_{n_0}$, then by hypothesis,
\[
	|\tilde{l}(u)| = \left| \sum_{k=1}^{n_0} \beta_k l(x_k) \right| \le \gamma \cdot \norm{\sum_{k=1}^{n_0} \beta_k x_k}_{X} = p(u)
\]
Otherwise, if $u = \sum_{k\in \bN} \beta_k x_k = \lim_n \sum_{k=1}^{n} x_k$, we get
\begin{align*}
	|\tilde{l}(u)| = & \left| \sum_{k=1}^{\infty} \beta_k l(x_k) \right| = \lim_{n \to \infty} \left| \sum_{k=1}^{n} \beta_k l(x_k) \right|\\
	\le & \lim_{n \to \infty} \gamma \cdot \norm{\sum_{k=1}^{n} \beta_k x_k}_{X} = \gamma \cdot \norm{\lim_{n \to \infty} \sum_{k=1}^{n} \beta_k x_k}_{X}\\
	= & \gamma \cdot \norm{u}_{X} = p(u)
\end{align*}
Therefore $\tilde{l}(u)$ is bounded by a sublinear functional $p$. By Hahn-Banach theorem, there exists $l \in X^*$ such that $\restr{l}{U} = \tilde{l}$, and that $\norm{l}_{X^*} = \norm{\tilde{l}}_{L(U,\bR)}$. In particular, $l(x_k) = \tilde{l}(x_k) = \alpha_k$, and we are done. \qed

\end{document}