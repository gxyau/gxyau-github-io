function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

var bnDisp = 0;
var bPostsPerPage = ($(window).height() - 260) / 60;
bPostsPerPage = Math.max(bPostsPerPage, 5);
function displayMore(arr, n, url_field, title_field, date_field, tags_field, doWriteTags, doAnimate)
{
    $("#post-list li:last").remove();

    for (var i = 0; i < n; i++, bnDisp++)
    {
        if (!arr[bnDisp])
            return;

        if (arr[bnDisp][date_field] != "")
            var pdate_str = arr[bnDisp][date_field].toDateString();
        else
            var pdate_str = "";

        $("#post-list").append(
            '<li style="display:none"><a href="' + arr[bnDisp][url_field] + '">' +
            '<h3 class="post-title">' + arr[bnDisp][title_field] + '</h3>' + 
            '<time class="post-date">' + pdate_str + '</time>' + 
            '<span class="post-tags">' + (doWriteTags ? "Tags: " : "") + arr[bnDisp][tags_field].join(", ") + '</span>' + 
            '</a></li>');
        if (doAnimate)
            $("#post-list li:last").slideDown();
        else
            $("#post-list li:last").show();
    }

    $("#post-list").append('<li><a href="javascript:;" id="post-more" style="display:block;width:100%;text-align:center">More</a></li>');
    $("#post-more").click(function(){
        displayMore(arr, n, url_field, title_field, date_field, tags_field, doWriteTags, true);
        return false;
    });
}