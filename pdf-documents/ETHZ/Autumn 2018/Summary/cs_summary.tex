 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% writeLaTeX Example: A quick guide to LaTeX
%
% Source: Dave Richeson (divisbyzero.com), Dickinson College
% 
% A one-size-fits-all LaTeX cheat sheet. Kept to two pages, so it 
% can be printed (double-sided) on one piece of paper
% 
% Feel free to distribute this example, but please keep the referral
% to divisbyzero.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use writeLaTeX: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[9pt,landscape]{article}
\usepackage{hedgehog}


\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.25in,left=.25in,right=.25in,bottom=.25in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}
\pagestyle{empty}
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother
\setcounter{secnumdepth}{1}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}
% -----------------------------------------------------------------------

\title{Summary of Graph Theory}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem*{thm}{Theorem}
\newtheorem*{lemma}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{coro}{Corollary}

\theoremstyle{definition}% Definition style
\newtheorem*{defn}{Definition}

\begin{document}

\raggedright
\footnotesize

\begin{center}
     \Large{\textbf{Computational Statistics}} \\
\end{center}

\begin{scriptsize}
\begin{multicols}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Regression}
\subsection{Linear Regression}
For simple linear regression, we have
\[
	y = \beta_1 x + \beta_0 \ee
\]
where $\beta_1$ is the gradient and $\beta_0$ is the intercept. For multiple linear regression, we have
\[
	Y_i = x_i^T \beta + \ee_i ~ ~ ~ \forall i \in \set{1,\ldots,n}
\]
where each $i \in \set{1,\ldots,n}$ is an observation. In matrix form, we have
\[
	Y = X \beta + \ee
\]
where $Y, \ee \in \bR^{n \times 1}$, $X \in \bR^{n \times p}$, $\beta \in \bR^{p \times 1}$.

\subsection{Least Squares Model}
Given linear model $Y = X \beta + \ee$, the least squares estimator, $\hat{\beta}$, of $\beta$ is as follows:
\[
	\hat{\beta} = \min_{b \in \bR^p} \norm{Y - X b}_2^2
\]
where $\norm{\cdot}_2$ is the 2-norm (Euclidean norm); $\hat{\beta}$ is given by
\[
	\hat{\beta} = (X^T X)^{-1} X^T Y
\]
Projection matrix is $X (X^T X)^{-1} X^T$. Mean and standard error of $\hat{\beta}$ is as follows:
\begin{itemize}
	\item $\bE(\hat{\beta}) = \beta$
	
	\item $S.E.(\hat{\beta}) = \sqrt{\sigma^2 (X^T X)^{-1}}$
\end{itemize}
In single variable, we have $\hat{\beta}_j = \frac{1}{\sum_{i=1}^{n}X_{ij}^2} \sum \limits_{i=1}^{n} X_{ij} y_i$. The estimate for $\sigma^2$ is $\hat{\sigma}^2 = \frac{RSS}{n-p}$.

\begin{defn}
	The t-statistics is $t(\hat{\beta}) = \frac{\hat{\beta} - \beta_0}{S.E.(\hat{\beta})} \sim t_{n-p}(1-\frac{\alpha}{2})$, where $\hat{\beta}$ is the estimated variable $\beta$, and  $\beta_0$ is the guess of $\beta$ (normally this is what we want to test), i.e. population mean.
\end{defn}

\begin{defn}
	Let $t_0 := t(\hat{\beta})$ be the t-statistic of $\hat{\beta}$. Given $\alpha \in [0,100]$, the p-value of $\hat{\beta}$ is $2 \cdot \bP(|t| \ge t_0)$ for two tail test, and $\bP(t \ge t_0)$ for one tail test.
\end{defn}

\begin{defn}
	Standard error is $\hat{S.E.}(\hat{\beta}_j) = \sqrt{\frac{\var(\hat{\beta}_j)}{n}}$. The $(1 - \alpha)$-confidence interval for $\hat{\beta}_j$ at $(1-\alpha) \%$ is $\hat{\beta}_j \pm t_{n-p}(1 - \frac{\alpha}{2}) \cdot \hat{S.E.}(\hat{\beta}_j)$. The $(1-\alpha)$-confidence interval for $\beta_j$ (prediction interval) is $\hat{\beta}_j \pm t_{n-p}(1 - \frac{\alpha}{2}) \cdot \hat{S.E.}(\hat{\beta}_j) \cdot \sqrt{1 + \frac{1}{n}}$. Both are centred around $\bE[\beta_j]$.
\end{defn}

\begin{defn}
	Let $y = X \hat{\beta}$ be a submodel $y = X \tilde{\beta}$ with $p_0$ and $p_1$ predictors, i.e. $X \hat{\beta}$ is null hypothesis and $X \tilde{\beta}$ is the alternative hypothesis. The F-statistics is given by 
	\[
		F = \frac{\norm{X \hat{\beta} - X \tilde{\beta}}^2/(p_1 - p_0)}{\norm{y - X \beta}^2/(n-p_1)}
	\]
	The degrees of freedom are $p_0,p_1$ respectively. Use $anova(\cdot)$ in R to compute by hand.
\end{defn}

\begin{defn}
	Mean square error is defined to be
	\[
		MSE = \frac{1}{n} \sum_{i=1}^{n} |y_i - f(x_i)|^2 = \frac{1}{n} \norm{y - X^T \beta}_2^2
	\]
	where ${(x_i,y_i)}_{i=1}^{n}$ are observations. The residual standard error of previously said data is
	\[
		RSE = \sqrt{\frac{\sum_{i=1}^{n} (y_i - f(x_i))^2}{n-df}}
	\]
	where $df$ is the degrees of freedom. Let $fit = lm(y \sim x_1)$, then residuals is given by $fit.res$ in $R$. Test MSE would be MSE using the model we built and the test data.
\end{defn}

\begin{lemma}
	$\text{MSE} = \text{Bias}^2 + \var$, i.e.
	\[
		\bE[(y - \hat{f}(x))^2] = (\bE[y - \hat{f}(x)])^2 + \var(\hat{f}(x))
	\]
	This is the bias-variance trade off formula.
\end{lemma}

We define $RSS = \sum_{i=1}^{n} |y_i - f(x_i)|^2$ and $TSS = \sum_{i=1}^{n} |y_i - \bar{y}|^2$.

\begin{defn}
	The $R^2$-statistics, given by $R^2 = 1 - \frac{RSS}{TSS}$, is the proportion of variability explained by the model. The adjusted $R^2$ is $R_{adj}^2 = 1 - (1-R^2)\frac{n-1}{n-p-1}$. Want to be maximized.
\end{defn}

\begin{defn}
	The variance inflation factor (VIF) is $\hat{\var}[\hat{\beta}_j] = \frac{\hat{\sigma}^2}{(n-1) \hat{\var}[X_j]} \cdot \frac{1}{1 - R_j^2}$, the latter term is the VIF; $R_j^2$ is multiple $R^2$ from the regression of $X_j$ onto other predictors.
\end{defn}

\begin{defn}
	Standard residuals is $\frac{e_i}{\hat{\sigma} p_{ii}}$; $p_{ij}$ is $ij$ entry of projection matrix; outliers are data with large standard residuals.
\end{defn}

\begin{defn}
	Cook's distance $D_i = \frac{1}{p} \cdot \frac{e_i^2}{\hat{\sigma}^2 (1-p_{ii})} \cdot \frac{p_{ii}}{1-p_{ii}}$ measure how influential the point is. Outliers and leverage points are influential.
\end{defn}

\subsection{Projection Matrix}
Let $P = X(X^T X)^{-1} X^T$. Then fitted values is $\hat{y} = X \hat{\beta} = Py$. Below are some properties of $P$.
\begin{itemize}
	\item $\hat{y}_i$ is linear combination of $y_1, \ldots, y_n$, with weights $p_{ij}$.
	
	\item $P^T = P$, $P^T P = P$, $P^2 = P$.
	
	\item $p_{jj} \in [\frac{1}{n},1]$.
	
	\item $p_{jj}$ summarizes contribution of $y_j$ to $\hat{y}_1,\ldots,\hat{y}_n$.
	
	\item $\frac{1}{n} \sum_{j} p_{jj} = \frac{p}{n}$
	
	\item $p_{jj}$ is large if $p_{jj} > 2 \frac{p}{n}$.
	
	\item $p_{jj}$ is large if unusual data point.
	
	\item $p_{jj}$ is leverage value.
\end{itemize}

\section{KNN \& CV}
\begin{defn}
	$K$-nearest neighbour $\hat{f}(x_0) = \frac{1}{k} \sum_{i=1}^{K} y_i$, where $y_i$'s are the $K$ nearest training data point to $x_0$.
\end{defn}

\begin{defn}
	The $K$-fold cross validation error is
	\[
		CV ~ error = \frac{1}{K} \sum_{j=1}^{K} \rho(y^{-I_j}, \hat{m}(X^{-I_j}))
	\]
	where $y^{-i}$ denotes all observation except for $i$, $\bigsqcup_{j=1}^{K} I_j = [n]$, $y^{-I_j}$ denotes all observations except for the one indexed by elements of $I_j$.
\end{defn}

\subsection{Double CV}
The idea is to split the dataset into training, tuning, and testing sets; train and tune on the training tuning set, and then testing using the last set.

\section{Bootstrapping}
Suppose $\set{(x_i,y_i)}_{i=1}^{n}$ are observations, which we denote by $Z_i = (x_i,y_i)$. We also denote by $\hat{P}_n$ is the empirical distribution of $\set{Z_i}_{i=1}^{n}$. The procedure to produce bootstrap sample is as follows:
\begin{enumerate}
	\item Generate $\set{Z_i^*}_{i=1}^{n} \overset{i.i.d.}{\sim} \hat{P}_n$ (sampling with replacement)
	
	\item Compute $\hat{\theta}_n^{*} = g(Z_1^*,\ldots,Z_n^*)$
	
	\item Repeat $B$ times to get $\set{\hat{\theta}_n^{*j}}_{j=1}^{B}$
\end{enumerate}

We can then use bootstrap estimators in 3 to get the bootstrap distribution, i.e.
\begin{itemize}
	\item (B. Mean) $\bE^{*}(\hat{\theta}_n^*) = \frac{1}{B} \sum_{i=1}^{B} \hat{\theta}_n^{*i}$
	
	\item (B. Var) $\var^*(\hat{\theta}_n^*) = \frac{1}{B-1} \sum_{i=1}^{B} \left( \hat{\theta}_n^{*i} - \bE^{*}(\hat{\theta}_n^*) \right)^2$
	
	\item (B. Bias) $\bias^*(\hat{\theta}_n^*) = \frac{1}{B} \sum_{i=1}^{B} (\hat{\theta}_n^{*i} - \bE^{*}(\hat{\theta}_n^*))$
	
	\item (B. Q4) $\alpha$-quantile of $\hat{\theta}_n^*$ distribution is approximately empirical  $\alpha$-quantile for $\set{\hat{\theta}_n^{*i}}_{i=1}^{B}$.
\end{itemize}
We write the bootstrap distribution of $\hat{\theta}_n^*$ as $P^*$.

\begin{defn}
	A bootstrap distribution $\hat{\theta}_n^*$ is consistent for $\hat{\theta}_n$ if
	\
	\[
		\bP(\sqrt{n} | \hat{\theta}_n - \theta | \le x) - \bP^*(\sqrt{n} | \hat{\theta}_n - \hat{\theta}_n^* | \le x) \overset{P}{\to} 0
	\]
	where $\bP^*$ comes from the bootstrap distribution $P^*$.
\end{defn}

Consistency of bootstrap usually implies consistency of variance and bias of bootstrap i.e.
\begin{multicols}{2}
	\begin{itemize}
		\item $\frac{\bE(\hat{\theta}_n) - \theta}{\bE(\hat{\theta}_n^*) - \hat{\theta}_n} \xrightarrow[]{P} 1$
		
		\item $\frac{\var(\hat{\theta}_n)}{\var(\hat{\theta}_n^*)} \xrightarrow[]{P} 1$
	\end{itemize}
\end{multicols}
Minima of $\var(\alpha X + (1-\alpha) Y)$ is attained at $\alpha = \frac{\sigma_y^2 - \sigma_{xy}}{\sigma_x^2 + \sigma_y^2 - 2 \sigma_{xy}}$

\subsection{Bootstrap CI}
Let $X_1,\ldots,X_n$, and $\theta$ is the parameter of interest, we can estimate $\theta$ with $\hat{\theta}_n  = g(X_1,\ldots,X_n)$. Suppose $\hat{\theta}_n^*$ is consistent for $\hat{\theta}_n$, where $\hat{\theta}_n^*$ is the bootstrap estimate. We are interested in 4 types of confidence interval at level $\alpha$.
\begin{itemize}
	\item Quantile: $[q_{\hat{\theta}_n^*}(\frac{\alpha}{2}),q_{\hat{\theta}_n^*}(1 - \frac{\alpha}{2})]$
	
	\item Normal Approximation: $[\hat{\theta}_n \pm q_Z(1-\frac{\alpha}{2}) \hat{sd}(\hat{\theta}_n)]$
	
	\item Reversed Quantile: $[\hat{\theta}_n - q_{\hat{\theta}_n^* - \hat{\theta}_n}(1 - \frac{\alpha}{2}), \hat{\theta}_n - q_{\hat{\theta}_n^* - \hat{\theta}_n}(\frac{\alpha}{2})]$
	
	\item Bootstrap T: Student T with Bootstrap Samples. Mean is from first layer of bootstrap and s.d. is from second layer.
\end{itemize}
where $Z \sim N(0,1)$ a	nd $\hat{sd}(\hat{\theta}_n) = (\frac{1}{R-1} \sum_{i=1}^{R} (\hat{\theta}^{*i} - \bar{\hat{\theta}^*})^2)^{\frac{1}{2}}$

\section{Hypothesis Testing}
There are parametric and non-parametric tests:
\begin{itemize}
	\item (Parametric) Assume normal distribution if variance known
	
	\item (Parametric) Assume $t$-distribution if variance unknown
	
	\item (Non-Parametric) Wilcoxon rank sum test
	
	\item (Non-Parametric) Permutation test
\end{itemize}

\paragraph{Wilcoxon Rank Sum Test} Two sample $X_i \sim F_1$, $Y_j \sim F_2$; $H_0: F_1 = F_2$, $H_1 : F_1 \ne F_2$; $Z_l$'s permutation of $\set{X_i,Y_j}_{i,j}$; $\rank(Z_l)$; test statistics $\sum_{x \sim F_1} \rank(x)$; permute $X_i$'s, $Y_j$'s again.

\paragraph{Wilcoxon Signed Rank Test} Paired sample $\set{(X_i, Y_i)}_{i=1}^{n}$; $H_0 : D_i = 0$, $H_1 : D_i \ne 0$; $D_i := X_i - Y_i$; $\rank(i) = \rank(|D_i|)$; $\sgn(i)$ random; statistics $\sum_{i=1}^{n} \sgn(i) \cdot \rank(i) * \i1(D_i > 0)$.

\paragraph{Permutation Test} To get the distribution of $U$ under $H_0$, permute $Y$'s in both groups in all possible ways. For each group assignment compute sum of ranks within group. $H_0$ says the values would've been the same regardless.
\begin{itemize}
	\item Small sample sizes: Compute all permutations
	
	\item Large sample sizes: Simulate large number of simulations.
\end{itemize}
Permutation test assumes the labels are distribution indifferent.

\subsection{FDR \& FWER}
\begin{tabularx}{\columnwidth}{X X X X}
	& $H_0$ True & $H_0$ False & Total\\
	Not Reject & Conf., $U$ & II (-ve), $T$ & $m - R$\\
	Reject & I (+ve), $V$ & Power, $S$ & $R$	\\
	Total & $m_0$ & $m - m_0$ & $m$
\end{tabularx}\\
$R$ is observed rejection; $Q = \frac{V}{R}$ is proportion of false discovery; false discovery rate (FDR) is $\bE(Q)$; family wise error rate (FWER) is $\bP(V \ge 1) \ge \text{FDR}$. Bonferroni correction: conduct $H_0$ at level $\frac{\alpha}{m}$ to control the FWER at level $\alpha$. For $m$ independent hypothesis test at level $\alpha$, we have $\alpha \le FWER \le m \alpha$.\\

\paragraph{Westfall-Young procedure} controls FWER. Repeat many times: permute treatment assignment; perform test for column $x_j$; store $p_j$ p-value of $x_j$; store $\min_j p_j$. Let $\dd$ be empirical $\alpha$-quantile distr. of $\min p_j$; reject any $H_0$ where test has p-value $\le \dd$.

\section{Ridge \& LASSO}
\paragraph{Information Criteria}
Let $p$ be number of estimated parameters, $\hat{L}$ be max value of likelihood function of model, $n$ observations.
\begin{description}
	\item[AIC] $2p - 2 \ln (\hat{L})$; minimize.
	
	\item[BIC] $\ln(n) p - 2 \ln(\hat{L})$; minimize.
	
	\item[Mallow's Cp] $\frac{RSS_p}{S^2} - n + 2p$; minimize.
\end{description}
where $S^2$ is MSE on $k$ parameters, and we choose $p < k$ parameters.

\paragraph{Ridge regression} use $\ell^2$ penalty, shrinks, min. coeffs.
\[
	\min \frac{1}{n} \sum_{i=1}^{n} (y_i - x_i^T \beta)^2 + \lambda \sum_{j=1}^{p} |\beta_j|^2 \equiv \min MSE(\beta) + \lambda \norm{\beta}_{2}^2
\]
\paragraph{LASSO} uses the $\ell^1$-norm penalty, shrinks, model selection
\[
	\min \frac{1}{n} \sum_{i=1}^{n} (y_i - x_i^T \beta)^2 + \lambda \sum_{j=1}^{p} |\beta_j| \equiv \min MSE(\beta) + \lambda \norm{\beta}_{1}
\]
In both cases $\lambda$ is chosen by cross validation; minimal test MSE. LASSO for selection/sparse; ridge for prediction/dense.

\paragraph{Flexibility/Variability} Ridge is more flexible/variable than LASSO (more precisely, $\norm{\cdot}_2$ more flexible than $\norm{\cdot}_1$ more flexible than $\norm{\cdot}_0$); so LASSO set variables to 0 more often than ridge. Also degree of polynomial higher implies higher variability, meaning it performs better when there are more data.

\section{Beyond Linearity}
\paragraph{Spline} Spline of order $n$ is piecewise polynomial function fo degree $n-1$; $r$ knots coincides means first $n - r$ derivatives of splines are continuous across knots. Splines with $k$ knots of degree $d$; $(k+1) \cdot (d+1)$ parameters for degree $d$ polynomial; $(k+1)$ intervals; $d \cdot k$ continuity constraints; degrees of freedom: $(d+1)(k+1) - dk = k + (d+1)$. Choice of basis for spline is $(x - \xi)_{+}^3$, $\xi$ is the knot whose $3^{rd}$ derivative is not continuous. Cubic spline: 
\[
	y_i = \beta_0 + \beta_1 x_i + \beta_2 x_i^2 + \gamma_1 h(x_i,\xi) + \ldots + \gamma_k h(x_i,\xi) + \ee_i
\]
Where $h(x_i,\xi) = (x_i - \xi)_+^3$. Natural spline assumes additional constraint; linear outside the boundary. DF: $k + 4 - 2 \cdot 2 = k$. Smoothing spline: Let
\[
	\cG := \set{g: [a,b] \to \bR : g'' ~ \text{exists} \wedge \int_{a}^{b} (g''(x))^2 ~ dx < \infty}
\]
Then $\hat{g} := \argmin_{g \in \cG} \set{\sum_{i=1}^{n} (y_i - g(x_i))^2 + \lambda \int_{a}^{b} (g''(x))^2 ~ dx}$. $\hat{g}$ is natural cubic spline with $n$ knots at data points. When $\lambda \to 0$, smoothing spline becomes interpolating spline; $\lambda \to \infty$, estimate becomes least square estimate.

\subsection{Local Regression}
At $x = x_0$, we do
\begin{enumerate}
	\item Gather $s = \frac{k}{n}$ of training points closest to $x_0$.
	
	\item Assign weight $w_{i0} = w(x_i,x_0)$ to each $x_i$ in neighbourhood.
	
	\item Fit weighted least squares minimizing $\sum_{i=1}^{n} w_{i0} (y_i - \beta_0 - \beta_1 x_i)^2$.
	
	\item Fitted value is $\hat{f}(x_0) = \hat{\beta}_0 + \hat{\beta}_1 x_0$.
\end{enumerate}

\subsection{GAM \& Backfitting}
Generalised additive models: $y_i = \beta_0 + \sum_{j=1}^{p} f_j (x_{ij}) + \ee_i$; $\ee_i \sim N(0,\sigma^2)$; non-linearity makes GAM generalisable; constraint: $\frac{1}{n}\sum_{j=1}^{p} f_j (x_{ij}) = 0$. GAM does not allow interactions. Backfitting procedure is as follows:
\begin{enumerate}
	\item Initialise est. mean $\hat{\mu} = \frac{\sum_{i=1}^{n} y_i}{n}$, predictors $\hat{g}_j = 0$, for all $j \in 1,\ldots,p$.
	
	\item For $j$, $\hat{g}_j = S_j(y - \hat{\mu} \i1 - \sum_{k \ne j} \hat{g}_k)$; note $y - \hat{\mu} \i1$ is center. Normalise $\hat{g}_j \leftarrow \hat{g}_j - \frac{\sum_{i=1}^{n} \hat{g}_j (x_{ij})}{n}$.
\end{enumerate}
Where $\hat{g}_j = (\hat{g}_j(x_{1j}), \ldots, \hat{g}_j (x_{nj}))^T$. Repeat 2 until convergence; convergence measure $\max_j \frac{\norm{\hat{g}_{j,new} - \norm{\hat{g}_{j,old}}^2}}{\norm{\hat{g}_{j,old}}^2} \le \text{tolerance}$.\\

\paragraph{Basis} The basis for GAM is $\i1(R_j)$, where $R_j$'s partition the input space for $x$'s, and $\i1$ denote the indicator function. Thus $\i1(R_i)$ and $\i1(R_j)$ are orthogonal if $i \ne j$.

\subsection{CART \& RF}
Let $\set{R_i}_{i=1}^{M}$ be a partition of $\bR^p$. For CART,
\[
	g_{tree}(x) = \sum_{r=1}^{M} \beta_r\i1_{x \in R_r}
\]
If partitions are given, then $\hat{\beta}_j := \frac{\sum_{i=1}^{n} y_i \i1_{x_i \in R_j}}{\sum_{i=1}^{n} \i1_{x_i \in R_j}}$. To find the partitions, choose a dimension to split it; refine the partitions by doing this repeatedly. When tree is big enough (i.e. enough branches), stop. Prune the tree by using $\min_{T \in \cT_0} \set{err(T) + \alpha |T|}$.\\

\paragraph{Bagging} Key point is to decrease variance without increasing bias by aggregating estimators. Random forest is more independent than bagging so better performance. Out of bag error on $x_i$ uses only trees that doesn't have $x_i$ in them. Bagging uses bootstap samples, build model and aggregate predictors.
\begin{enumerate}
	\item Generate bootstrap samples $\set{(x_i^*,y_i^*)}_{i=1}^{n}$	and compute $\hat{g}^*(\cdot)$.
	
	\item Repeat $B$ times to get $\hat{g}_1^*, \cdot \hat{g}_B^*$
	
	\item Aggregate by $\hat{g}_{bag} = \sum_i \hat{g}_i^*$; class prob: $\hat{P}_k^{bag}(x) = \frac{1}{B} \sum_i \hat{P}_k^{*i}(x)$.
\end{enumerate}


\end{multicols}

\section*{R Cheatsheet \& Packages}
\begin{multicols}{2}
\begin{tabularx}{\columnwidth}{X X}
	anova(model1,model2) & F-test on the models\\
	abline(a,b,v,h) & a slope, b intersect, v vertical, h horizontal\\
	boot(data,statistic) & statistic is a function\\
	boot.ci(boot.out, conf) & boot.out is boot object\\
	boxplot(x,y) & Boxplot\\
	bs(x,knots,data) & Basis for spline\\
	coef(obj,i) & Coefficient of the model with $i$ predictors\\
	cut(x,breaks,labels) & Cutting a vector into different groups\\
	gam.s(x,y,df,data) & Smoothing spline basis for GAM\\
	glmnet(x,y,alpha,lambda) & alpha = 0 Ridge, alpha = 1 LASSO, lambda is tuning parameter\\
	hist(x,breaks,prob) & Histogram\\
	ifelse(if,then,else) & if then else\\
	kknn(formula, train, test, k) & KNN, train \& test are matrix or df\\
	lines(x,y) & Draw lines on graphs\\
	lm($y \sim \cdot$) & Linear models\\
	lo(x,span) & Basis for using LOOES\\
	model.matrix(form, data) & Create $x$ matrix for Ridge and LASSO\\
	ns(x,knots,data) & Basis for natural spline\\
	order(vec) &f\ Order of each element\\
	pairs(formula, data) & Pairs plot
\end{tabularx}

\begin{tabularx}{\columnwidth}{X X}
	par(mfrow) & Multiple plots diff. graphs\\
	par(new) & Multiple plots same graph\\
	poly(x,n) & Polynomial degrees $n$ in $x$\\
	randomForest(formula,mtry, ntree`) & Random Forest\\
	rank(x) & Computes the rank of the samples\\
	regsubsets(formula,data,nvmax) & Finding best subset of predictors\\
	replicate(n, expr) & Replicate expr n times\\
	s(x,df,data) & Basis for smoothing spline\\
	sample(x,size,replace) & Sampling from $n$ numbers\\
	sort(vec) & Sorting the vector\\
	var(vec) & Variance of the vector\\
	which(expression) & Which element fulfils the expression\\
	wilcox.test(x,y, alternative=``less") & Wilcoxon Rank test\\
	\hline
	library(kknn) & K nearest neighbours\\
	library(ISLR) & Package for ISLR datasets and stuffs\\
	library(leaps) & For regsubsets, etc\\
	library(spline) & Package for splines, bs() for spline and ns() for natural spline\\
	library(gam) & General additive models\\
	library(rpart) & Classification and Regression Trees
\end{tabularx}

\end{multicols}

\end{scriptsize}

\end{document}
