\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 4}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 4}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\paragraph*{Problem 4.1} We will first prove several lemmas for this problem.\\

\begin{lemma}
	Let $(X,\norm{\cdot})$ be a normed space. Then any finite subspace $S \subseteq X$ is closed.
\end{lemma}
\begin{proof}
	Let $(s_n)_n$ be a converging sequence in $S$, say $s = \lim_n s_n$, where $s \in X$. Since every converging sequence is Cauchy, $(s_n)_n$ is Cauchy in $X$; hence it is also Cauchy in $S$. Using the fact that finite dimensional normed space are Banach spaces, $s \in S$, and hence $S$ is a closed subspace of $X$.
\end{proof}

\begin{lemma}
	Every proper subspace of a normed space $(X,\norm{\cdot}_X)$ has empty interior.
\end{lemma}
\begin{proof}
	We will prove the contrapositive of this lemma. The contrapositive states that if there exists a subspace $Y \subseteq X$, such that $\mathring{Y} \ne \emptyset$, then $Y = X$.\\
	
	So let $Y \subseteq X$ be a subspace such that $\mathring{Y} \ne \emptyset$. Then there exists $x_0 \in Y$ and $r > 0$ such that $B(x_0,r) \subseteq Y$. If we can prove that $X \subseteq Y$, then we are done. So let $z \in X$ be arbitrary, and consider $y := x_0 + \frac{z}{2\norm{z}} \cdot r$, it is easy to see that $\norm{y - x_0} < r$, hence $y \in Y$. Rewrite $z = \frac{2\norm{z}}{r} (y - x_0)$; since $Y$ is a subspace, so it is closed under scalar multiplication and translation. Hence $z \in Y$, i.e. $X \subseteq Y$. Therefore $X = Y$.
\end{proof}

\begin{enumerate}[(a)]
	\item Let $(X,\norm{\cdot})$ be a complete normed space which has a countable basis, say $\{e_n\}_n$ is the set of basis. Then by defining $A_n := \Span \{e_1,\cdots,e_n\}$, we can write $X = \bigcup_{n \in \bN}$. Since $A_n$'s are finite dimensional, so by lemma 1, $\bar{A}_n = A_n$. Moreover, $A_n \subsetneq X$ since $X$ is infinite dimensional, so $\mathring{A}_n = \mathring{\bar{A}}_n = \emptyset$, i.e. each $A_n$'s are nowhere dense set. By Baire category theorem, $X$ is a countable union of nowhere dense set, hence it has empty interior, contradicting that $X$ is complete. \qed
	
	\item Consider $X = (c_{c},\norm{\cdot}_{\infty})$. It is a subspace of $(c_0,\norm{\cdot}_{\infty})$, hence it is a normed space with norm inherited from $c_0$. Moreover, $B = \{e_n\}_n$, where $e_n$ contains $1$ in the $n^{th}$ position and 0 everywhere else, is an algebraic basis for $X$. Hence $X$ is a normed space with countably infinite algebraic basis. By part (a), $X$ is incomplete.
\end{enumerate}

\paragraph*{Problem 4.2} Let $X = (\ell_1,\norm{\cdot}_1)$, where we write $\norm{\cdot}_1 = \norm{\cdot}_{\ell^1}$ for simplicity. Define the following subspaces.
\begin{align*}
	U = & \set{(x_n)_n \in \ell^1 \big|\forall n \in \bN, ~ x_{2n} = 0}\\
	V = & \set{(x_n)_n \in \ell^1 \big|\forall n \in \bN, ~ x_{2n-1} = nx_{2n}}
\end{align*}
We will first show that $U,V$ are closed in $\ell^1$ under $\norm{\cdot}_1$.\\

Let $(u_n)_n$ be an arbitrary sequence in $U$, where $u_n = (u_{nk})_{k \in \bN}$ is a sequence in $\bR$, such that $\lim_n u_n = u_{\infty}$ where $u \in \ell^1$; we want to show that $u \in U$. Consider the sequence $(u_{nm})_{n \in \bN}$, we claim that $\lim_n u_{nm} = u_{\infty,m}$. Since $\lim_{n} u_n = u_{\infty}$, hence for all $\ee > 0$, there exists $N(\ee) > 0$ such that 
\[
	\norm{u_n - u_{\infty}} = \sum_{m \in \bN} |u_{nm} - u_{\infty,m}| < \ee
\]
whenever $n > N(\ee)$. Therefore $|u_{nm} - u_{\infty,m}| \le \norm{u_n - u_{\infty}} < \ee$ whenever $n > N(\ee)$, and indeed $\lim_{n} u_{nm} = u_{\infty,m}$. Since $\bR$ is complete so $u_{\infty,m} \in \bR$ for all $m \in \bN$. Now we need to show that $\sum_{m \in \bN} |u_{\infty,m}| < \infty$.\\

Now for $\ee > 0$, choose $N'(\ee) > 0$ such that $\norm{u_n - u_{\infty}} < \ee$ whenever $n > N'(\ee)$. So let $n_0 > N'(\ee)$ be arbitrarily chosen, and
\[
	\sum_{m \in \bN} |u_{\infty,m}| \le \sum_{m \in \bN} |u_{\infty,m} - u_{n_0 m}| + \sum_{m \in \bN} | u_{n_0 m}| = \norm{u_{\infty} - u_{n_0}} + \norm{u_{n_0}} = \norm{u_{n_0}} + \ee < \infty
\]
and indeed $u_{\infty} \in \ell^1$. Proving $V$ is a closed subspace is analogous to the above construction.\\

Now we will show that $U \oplus V$ is not closed in $\ell^1$. Recall that
\[
	U \oplus V := \{u + v  \in \ell^1 : u \in U, v \in V\}
\]
We want to show that $U \oplus V$ is not closed in $X$. Consider $c_c$, we will first show that $c_c \subseteq U \oplus V$.\\

Let $x = (x_n)_n \in c_c$ be arbitrary. There are two cases
\begin{itemize}
	\item The last non-zero entry has index of the form $2n - 1$
	
	\item The last non-zero entry has index of the form $2n$
\end{itemize}
where $n \in \bN$. If it is the first case, then simply write
\begin{align*}
	u = & (x_1 - x_2, 0, x_3 - 2 x_4, 0 ,\cdots, x_{2n-3} - (n-1) x_{2n-2}, 0, x_{2n-1} - n x_{2n}, 0, 0,\cdots) \in U\\
	v = & (x_2,x_2, 2x_4, x_4, \cdots, (n) x_{2n}, x_{2n},0, \cdots) \in V
\end{align*}
Note that $u,v$ are in $U,V$ respectively because the sum of a finite non-zero terms is finite. On the second case, we also have
\begin{align*}
	u = & (x_1 - x_2, 0, x_3 - 2 x_4, 0 ,\cdots, x_{2n-3} - (n-1) x_{2n-2}, 0, x_{2n-1}, 0, 0,\cdots) \in U\\
	v = & (x_2,x_2, 2x_4, x_4, \cdots, (n-1) x_{2n-2}, x_{2n-2},0, \cdots) \in V
\end{align*}
Therefore $c_c \subseteq U \oplus V$. Now let us construct a sequence of elements in $\ell^1$, i.e. $(w_n)_n$ such that $w_n \in \ell^1$.\\

Let us define $w_n$ for each $n \in \bN$ by
\[
	w_n = (1,\frac{1}{2},\cdots,\frac{1}{n},0,\cdots)
\]
Then each $w_n \in c_c \subseteq U \oplus V$. However, $\lim_n w_n = (\frac{1}{n})_{n \in \bN}$ is the Harmonic sequence, and it is not even in $\ell^1$! Hence $U \oplus V$ is not closed in $\ell^1$. \qed

\paragraph*{Problem 4.3} Let $(X,\norm{\cdot})$ be a normed vector space. The following statements are equivalent.
\begin{enumerate}
	\item $(X,\norm{\cdot})$ is a Banach space.
	
	\item Every sequence with $\sum_{n \in \bN} \norm{x_n} < \infty$ implies the limit $\lim_{N \to \infty} \sum_{n=1}^{N} x_n$ exists.
\end{enumerate}
\begin{proof}
	(1 $\implies$ 2) Suppose $(x_n)_n$ is a sequence in $(X,\norm{\cdot})$ such that $\sum_{n \in \bN} \norm{x_n} < \infty$. Define a sequence $(y_k)_{k \in \bN}$ such that $y_k := \sum_{n=1}^{k} x_n$. We claim that $(y_k)_k$ is Cauchy. Let $k,l \in \bN$ be arbitrary such that $k < l$. Then $y_k - y_l = \sum_{n=k+1}^{l} x_n$, thus
	\[
		\norm{y_k - y_l} \le \sum_{n=k+1}^{l} \norm{x_n} \le \sum_{n \in \bN} \norm{x_n} < \infty
	\]
	So for each $\ee > 0$, we can choose $N \in \bN$ large enough so that $\norm{y_k - y_l} < \ee$, and $(y_k)_k$ is indeed a Cauchy sequence. Since $(X,\norm{\cdot})$ is Banach, hence $\exists y \in X$ such that $\lim_k y_k = y$, i.e. $\lim_k \sum_{n=1}^{k} x_n$ exists.\\
	
	(2 $\implies$ 1) Let $(x_n)_n$ be a Cauchy sequence in $(X,\norm{\cdot})$. Find $N_1 \in \bN$ such that $\norm{x_{k} - x_{l}} < \frac{1}{2}$ whenever $k,l \ge N_1$. Define $y_1 = x_{N_1}$. For $n \ge 2$, we find $N_n \ge N_{n-1}$ such that $\norm{x_k - x_k} < \frac{1}{2^{n+1}}$ whenever $k,l \ge N_n$, and define $y_n := x_{N_n} - x_{N_{n-1}}$. Then we have $\norm{y_n} < \frac{1}{2^{(n-1)+1}} = \frac{1}{2^n}$, and
	\[
		\sum_{n \in \bN} \norm{y_n} = \norm{x_1} + \sum_{n \ge 2} \norm{x_{N_n} - x_{N_{n-1}}} < \frac{1}{2} + \sum_{n=2}^{\infty} \frac{1}{2^n} = 1 < \infty
	\]
	By hypothesis, there exists $y \in X$ such that $y = \lim_n \sum_{k=1}^{n} y_k$. By definition of $y_n$'s, we have $\sum_{k=1}^{n} y_k = x_{N_n}$, thus we get $\lim_{n \to \infty} X_{N_n} = y$.\\
	
	Let $\ee > 0$. Now let $K_1,K_2 \in \bN$ such that $\norm{x_{k} - x_l} < \frac{\ee}{2}$ whenever $k,l > K_1$, and $\norm{x_{m} - y} < \frac{\ee}{2}$ whenever $m > K_2$. We know that $K_1,K_2$ exists by definition of Cauchy sequence and definition of $y$ being the limit of the subsequence $(x_{N_n})_n$. Define $K := \max\set{K_1,K_2}$. Then for any $n > K$, we get
	\[
		\norm{x_n - y} \le \norm{x_n - x_{N_K}} + \norm{x_{N_K} - y} < \frac{\ee}{2} + \frac{\ee}{2} = \ee
	\]
	and hence the Cauchy sequence converge to $y \in X$. Therefore $X$ is complete and indeed a Banach space.
\end{proof}

\paragraph*{Problem 4.4} Let $(X,\norm{\cdot})$ be an infinite dimensional normed vector space, and let $Z \subseteq X$ be a bounded subset with compact boundary. $\sup_{x \in Z} \norm{x} \le C \in \bR$\\

Suppose $\mathring{Z} \ne \emptyset$, then we can pick $x_0 \in \mathring{Z}$ such that there exists $U_{x_0} \in \tau_X$ such that $x_0 \in U_{x_0} \subseteq Z$. Without loss of generality, we can assume $U_{x_0} = B(x_0,1)$. Define a map
\[
	T: \di Z \to \bar{U}_{x_0}, ~ ~ ~ z \mapsto x_0 + \frac{r}{\norm{z - x_0}} \cdot (z - x_0)
\]
Then $T$ is continuous because it is basically scaling and translation of the vector $z \in \di Z$. Moreover, it is surjective; for every $x \in S_0$, there is a vector $z \in \di Z$ such that $T z = x$. We can always find such $z$ because $Z$ is bounded. Therefore $T(\di Z) = S_0$, and it is compact as continuous image of compact set. This contradicts that the unit sphere (and by scaling and translation, all spheres) of infinite dimensional normed space is not compact. Thus we are done. \qed

\paragraph*{Problem 4.5} Let $X = C^0([-1,1],\bR)$, equipped with the norm $\norm{\cdot}_X := \norm{\cdot}_{C^0([-1,1],\bR)}$. Define 
\[
	\ph :X \to \bR, ~ ~ ~ f \mapsto \int_0^1 f(t) dt - \int_{-1}^{0} f(t) dt
\]
Recall that $\norm{f}_{X} := \sup_{x \in [-1,1]} |f(x)|$, and $\norm{\ph}_{L(X,\bR)} := \sup_{\norm{f}_{X} \le 1} \norm{\ph(f)}_X$. Let us denote $\norm{\cdot}_{L(X,\bR)}$ by $\norm{\cdot}_{op}$ for simplicity of notation
\begin{enumerate}[(a)]
	\item We have
	\begin{align*}
		\norm{\ph}_{op} = & \sup_{ \norm{f}_{X} \le 1} \norm{\ph(f)}_X = \sup_{ \norm{f}_{X} \le 1} \norm{\int_0^1 f(t) dt - \int_{-1}^0 f(t) dt}_X\\
		\le & \sup_{ \norm{f}_{X} \le 1}  \left( \norm{\int_{0}^{1} f(t) dt}_X + \norm{\int_{-1}^{0} f(t) dt}_X \right)\\
		\le & \sup_{\norm{f}_X \le 1} \left( \int_0^1 1 ~ dt + \int_{-1}^0 1 ~ dt \right) = \sup_{\norm{f}_X \le 1} (1 + 1) = 2
	\end{align*}
	and we are done.
	
	\item Did not figure out this question. \qedsad
	
	\item Did not figure out this question either. \qedsad
\end{enumerate}

\paragraph*{Problem 4.6} Consider the space $(\ell^{\infty},\norm{\cdot})$, where $\norm{\cdot}$ is the supremum norm. Let $T: c_c \to c_c$ be a map defined by $(x_n)_n \mapsto (nx_n)_n$.
\begin{enumerate}[(a)]
	\item By continuity criteria, $T$ is continuous if and only if
	\[
		\sup_{\norm{x}_{\ell^{\infty}} \le 1} \norm{T x}_{\ell^{\infty}} < \infty
	\]
	i.e. the unit ball is bounded. Let us consider $e_n \in c_c$, where the $n^{th}$ entry of the sequence is 1, whereas the rest are 0. Then $\norm{e_n} = 1$ for all $n \in \bN$. Moreover, $\norm{T e_n} = n$, so as $n \to \infty$, $\norm{T e_n} \to \infty$. Therefore $\sup_{\norm{x} \le 1} \norm{T x}$ is unbounded and $T$ is not continuous.
	
	\item Let $x = (x_n)_{n \in \bN} \in c_c$ be arbitrary. Define the map
	\[
		T_m : c_c \to c_c, ~ ~ ~ (x_n)_n \mapsto (x_1,\cdots,x_m,0,\cdots)
	\]
	i.e. the $m^{th}$ cut-off function of $x$. Since $x \in c_c$, we may assume $m_0 \in \bN$ is the unique smallest integer such that $x_{n} = 0$ for all $n > m_0$. Then for each $\ee > 0$, $\norm{T_m x - T x} = 0$ for all $m > m_0$. Thus $T_m \to T$ as $m \to \infty$. Moreover, $T_m$'s are continuous on $x$, and since $X$ is a normed space, it is continuous everywhere on $X$. Hence we are done.
\end{enumerate}


\end{document}