\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Probabilistic Method in Combinatorics}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Probabilistic Method in Combinatorics \\ Problem Set 6}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Problem 6.1}  Let $X \in \bZ_{\ge 0}$ be a random variable. We want to prove
\begin{enumerate}[(i)]
	\item $\bP(X = 0) \le \frac{\var[X]}{\bE^2[X]}$
	
	\item $\bP(X = 0) \le \frac{\var[X]}{\bE[X^2]}$
\end{enumerate}
The first item is directly from Chebychev inequality. Consider the following:
\[
	\bP(|X - \bE[X]| \ge t) = \bP(\text{$X - \bE[X] \le - t$ or $X - \bE[X] \ge t$}) = \underbrace{\bP(X \le \bE[X] - t)}_{(*)} + \underbrace{\bP(X \ge \bE[X] + t)}_{\ge 0}
\]
If we choose $t = \bE[X]$ and apply to $(*)$, we get that
\[
	\bP(X = 0) \le \bP(X \le 0) + \bP(X \ge 2 \bE[X]) \le \frac{\var[X]}{\bE^2[X]}
\]
as we desired. For second item, note that $\bP(X = 0) = 1 - \bP(X \ge 1)$, and so we can rewrite the inequality as the following:
\[
	\bP(X = 0) \le \frac{\var[X]}{\bE[X^2]} = 1 - \frac{\bE^2[X]}{\bE[X^2]} \iff \bP(X \ge 1) \ge \frac{\bE^2[X]}{\bE[X^2]}
\]
Notice that $\bE[X^2]$ is not a random variable, so it suffices to show that $\bE[X^2] \bP(X \ge 1) \ge \bE^2[X]$. Recall that
\begin{itemize}
	\item $\bP(X \ge 1) = \sum_{i=1}^{\infty} \bP(X = i)$
	
	\item $\bE[X] = \sum_{i=1}^{\infty} i \bP(X = i)$
	
	\item $\bE[X^2] = \sum_{i=1}^{\infty} i^2 \bP(X = i)$
\end{itemize}
Notice that $\bE[X]$ and $\bE[X^2]$ does not have the term $\bP(X = 0)$ because $X = 0$ in both cases. Therefore we have
\[
	\bE^2[X] = \left( \sum_{k=1}^{\infty} k \bP(X = k) \right)^2 \overset{(CS)}{\le} \left( \sum_{k=1}^{\infty} \bP(X = k) \right) \left( \sum_{k=1}^{\infty} k^2 \bP(X = k) \right)
\]
where $(CS)$ is by applying Cauchy Schwarz inequality to the vectors $v_1 = ((\bP(X = k)^{\frac{1}{2}})_{k=1}^{\infty}$ and $v_2 = (k(\bP(X = k)^{\frac{1}{2}})_{k=1}^{\infty}$, which are well-defined vectors in the infinite dimensional vector space.

\paragraph{Problem 6.2} Let $G = (V,E)$ be a graph with $|V| = n$ and $|E| = m$. Let $k \in \bN$. We want to prove that
\begin{enumerate}[(a)]
	\item There are at least $k^n (1 - \frac{m}{k})$ proper $k$-vertex colouring of $G$.
	
	\item There are at most $k^n \cdot \frac{k-1}{m}$ proper $k$-vertex colouring of $G$.
	
	\item Upper bound in (b) can be improved to $k^n \cdot \frac{k-1}{k+m-1}$.
\end{enumerate}
Let $c:V \to [k]$ be a (not necessarily proper) colouring of $V$ by assigning a colour from $[k]$ uniformly at random, i.e. $\bP(c(v) = i) = \frac{1}{k}$ for all $v \in V$ and all $i \in [k]$. Let $\cC$ be the collection of all colouring (including non-proper vertex colouring) of $G$, since each vertex is assigned a colour uniformly at random, $|\cC| = k^n$. Then we know that for each $uv \in E$, we have
\[
	\bP(c(u) = c(v)) = \frac{k \cdot 1}{k^2} = \frac{1}{k}
\]
Let us define $X \in \bZ{\ge 0}$ be the number of proper colouring of $G$, and for each $c \in \cC$,
\[
	X_c = \begin{cases}
		1 & \text{$c$ is a proper colouring of $G$}\\
		0 & \text{$c$ is not a proper colouring of $G$}
	\end{cases}
\]
Clearly $X = \sum_{c \in \cC} X_c$. Therefore we have
\begin{align*}
	\bE[X] = & \sum_{c \in \cC} \bE[X_c] = \sum_{c \in C} \bP(X_c = 1)\\
	= & \sum_{c \in \cC} \bP(c(u) \ne c(v) ~ \forall uv \in E)\\
	= & \sum_{c \in \cC} (1 - \bP(\exists uv \in E: c(u) = c(v)))\\
	\overset{(UB)}{\ge} & \sum_{c \in \cC} \left( 1 - \sum_{e \in E} \bP(c(u) = c(v)) \right)\\
	= & \sum_{c \in C} (1 - \frac{m}{k}) = k^n (1-\frac{m}{k})
\end{align*}
where $(UB)$ is due to union bound. Since $\bE[X] = \sum_{c \in C} (1 - \frac{m}{k}) = k^n (1-\frac{m}{k})$. Therefore there are at least $k^n (1-\frac{m}{k})$ proper $k$-vertex colouring of $G$, we have proven (a).\\

Now notice that for any fixed $e = uv \in E$, we know that $\bP(c(u) = c(v)) = \frac{1}{k}$, therefore $\bP(c(u) \ne c(v)) = \frac{k-1}{k}$. Now fix a colouring $c \in \cC$; observe that $c$ is a proper $k$-vertex colouring if and only if any $uv \in E$, we have $c(u) \ne c(v)$.\\

We know that each vertex is coloured independently from the other vertices, so if we choose an edge $e = uv \in E$ at random, the probability that $e$ is such that $c(u) \ne c(v)$ is as follows
\[
	\bP(e = uv, c(u) \ne c(v) = i) = \bP(c(v) \ne c(v) = i) \cdot \bP(\text{$e = uv$ is chosen}) = \frac{k-1}{k} \cdot \frac{1}{m} = \frac{k-1}{mk}
\]
for some $i \in [k]$. Therefore the probability that $c$ is a proper colouring is
\begin{align*}
	\bP(X_c = 1) = & \bP(c(u) \ne c(v), ~ \forall uv \in E) \le \sum_{i=1}^{k} \bP(c(u) \ne c(v) = i, e = uv)
\end{align*}
\textcolor{orange}{This is weird, keep getting bigger bounds}

\paragraph{Problem 6.3} Let $\set{(x_i,y_i)}_{i=1}^{n} \subseteq \bZ^2$ be a collection of two dimensional vectors, with the following property
\[
	\max \set{|x_i|,|y_i|} \le \frac{1}{100\sqrt{n}} 2^{\frac{n}{2}} ~ ~ ~ ~ ~ \forall i \in [n]
\]
We want to find two disjoint subsets $I,J \subseteq [n]$ such that $\sum_{i \in I} v_i = \sum_{j \in J} v_j$, where $v_l = (x_l,y_l)$ for all $l \in [n]$. In other words, we want $\sum_{i \in I} x_i = \sum_{j \in J} x_j$ and $\sum_{i \in I} y_i = \sum_{j \in J} y_j$ for some disjoint subsets $I,J \subseteq [n]$.\\

Without loss of generality, we may assume $v_i$'s are unique for otherwise if $v_i,v_j$ are identical copy, we may just choose $I = \set{i}$ and $J = \set{j}$ and the problem is trivially solved. We will first construct index sets $I',J'$ (allowing $I'$ and $J'$ having non-trivial intersection) such that $\sum_{i \in I'}v_i = \sum_{j \in J'} v_j$. First, for each $k \in [n]$, set $\bP(k \in I') = \bP(k \in J') = \frac{1}{2}$, i.e. each index $k \in [n]$ are chosen independently, with equal chance of being in $I'$ or not in $I'$ (resp. in $J'$ or not in $J'$). Let us write $\chi(k \in L)$ to be the indicator function that $k \in L$ for all $k \in [n]$, where $L \in \set{I',J'}$ is some index set.\\

Then $\chi(k \in L)$ are random variables, and therefore we can compute its expectation. By symmetry, we may assume that $L = I'$. For arbitrary $k \in [n]$,
\[
	\bE[\chi(k \in I')] = \frac{1}{2} \cdot 0 + \frac{1}{2} \cdot 1 = \frac{1}{2}
\]
Now since $v_i = (x_i,y_i)$, and we want to prove that $\sum_{i \in I'} v_i = \sum_{j \in J'} v_j$, it suffices to show that $\sum_{i \in I'} x_i = \sum_{j \in J'} x_j$; by symmetry we may conclude that $\sum_{i \in I'} y_i = \sum_{j \in J'} y_j$.

\paragraph{Problem 6.4} Let $H = (A \sqcup B,F)$ be a bipartite graph, and $\max_{v \in B} \deg(v) \le r$, and for simplicity, we write $a = |A|, b = |B|$. Let $G = (V,E)$ be a graph on $n = |V|$ vertices, with $|E| \ge c n^{2 - \frac{1}{r}}$ edges, where $c := c(H)$ is to be decided later. Then the average degree 
\[
	\bar{d} := \bar{d}(G) \ge \frac{2 c n^{2 - \frac{1}{r}}}{n} = 2 c r^{1- \frac{1}{r}} \implies \frac{\bar{d}^r}{n^{r-1}} \ge (2c)^r \tag{1}
\]
On the other hand, we also know that ${n \choose r} \le (\frac{en}{r})^r$, let $m = a+b$,
\[
	- {n \choose r} \cdot \left( \frac{m}{n} \right)^{r} \ge - \left( \frac{en}{r} \right)^r \cdot \left( \frac{m}{n} \right)^{r} = - \left( \frac{e(a+b)}{r} \right)^r \tag{2}
\]
Combining (1) and (2), we get that
\[
	\frac{\bar{d}^r}{n^{r-1}} - {n \choose r} \cdot \left( \frac{m}{n} \right)^{r} \ge (2c)^r - \left( \frac{e(a+b)}{r} \right)^r
\]
Choose $c$ such that $(2c)^r - \left( \frac{e(a+b)}{r} \right)^r \ge a$, then by a theorem proven in class, we get a subset $U$ of at least $a$ vertices, and each $r$-subset of $U$ has at least $m = a+b$ common neighbours. We will now show that $H$ embeds into $G$ via $U$.\\

Let $f: A \sqcup B \to V$ by an arbitrary injection $f:A \to U$. From above, we know that $U$ has at least $a$ elements, so there is an injection from $f : A \to U$. For simplicity, let us order elements in $B$ by $v_1,\cdots,v_b$. We will now embed the rest of $B$ into $G$, in this order. First, we will embed $v_1$. Clearly $f(N_H(v_1)) \subseteq U$ is non-empty, so we can arbitrarily pick a vertex $w_1 \in f(N_H(v_1))$ such that $v_1 \sim_{G} w_1$. Easy.\\

Assuming we have embedded $i-1$ vertices, and now we want to embed $v_i$ into $G$. Since max degree of $B$ is at most $r$, so $|N_H(v_i)| \le r$, therefore, $f(N_H(v_i)) \subseteq U$ is such that $|f(N_H(v_i))| \le r$. Now since every $r$-subset of $U$ has at least $a+b$ common neighbours, therefore $f(N_H(v_i))$ has at least $a+b$ common neighbours in $G$; this claim follows directly from the fact that intersection of fewer sets yield a larger set overall. Since we have embedded less than $a+b$ vertices, there is a vertex $w_i$ that is not among previously embedded vertices. This means that we can set $f(v_i) = w_i \in G$. Do this for $v_1,\cdots,v_b$ and we are done. Finally, $f$ is an embedding since we have (literally) constructed the embedding $f: H \to G$ by assigning vertices (one vertex at a time) of $G$ to vertices of $H$. \qed


\end{document}