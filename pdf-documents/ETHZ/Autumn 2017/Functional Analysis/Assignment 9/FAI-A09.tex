\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 9}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 9}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\paragraph*{Problem 9.1} (Minkowski Functional) Let $(X,\norm{\cdot}$ be a normed space, and $Q \subseteq X$ be a non-empty, open, convex subset of $X$ that contains the origin. The Minkowski functional is defined as $p_Q:X \to \bR$, $x \mapsto \inf \set{\lambda > 0: \frac{1}{\lambda} x \in Q}$. We want to find $\Upsilon \subseteq X^*$ s.t. 
\[
	Q = \bigcap_{f \in \Upsilon} \set{x \in X | f(x) < 1}
\]
Without loss of generality, we may assume $Q = B(0,1)$, the unit ball centred at origin; otherwise, we can scale the vectors in $Q$ into the unit ball and work with that instead.\\

First, we claim that $p_Q(\cdot) = \norm{\cdot}$. First note that for all $x \in X$, and $\ee > 0$, $\frac{x}{\norm{x} + \ee} \in Q$, thus as a lower bound, $\lambda := p_Q(x) \le \norm{x} + \ee \to \norm{x}$ as $\ee \to 0$. Thus $\lambda \le \norm{x}$. Suppose $\lambda < \norm{x}$, then $\frac{1}{\lambda} > \frac{1}{\norm{x}}$ for all $x \in X$. But this means that $\norm{\frac{x}{\lambda}} > \norm{\frac{x}{\norm{x}}} = 1$ for all $x \in X$, and this is absurd since $x \in Q$ are such that $\norm{x} < 1$. Thus $\lambda = \norm{x}$. Now we can define $\Upsilon = \set{p_Q} \subseteq X^*$. Clearly
\[
	Q = B(0,1) = \set{x \in X: p_Q(x) = \norm{x} < 1}
\]
and we are done. \qed

\paragraph{Problem 9.2} (Extremal Points) Recall that for any vector space $V$ over the field $\bK$, a point $u \in V$ is called an extreme point if for all $v_1,v_2$, and all $t \in [0,1]$, $u = t v_1 + (1-t) v_2$ means that $u = v_1 = v_2 \in V$.

\paragraph{Problem 9.3} (Extremal Subsets) Recall that for any vector space $V$, $K \subseteq V$ be a convex set, a subset $M$ is call an extremal subset of $K$ if for all $x_1,x_2 \in K$ and ll $\lambda \in (0,1)$ such that $\lambda x_ 1 + (1-\lambda) x_2 \in M$ means that $x_1,x_2 \in M$. Suppose $X$ is a vector space, and $K \subseteq X$ is a convex subset with $|K| > 1$.
\begin{enumerate}[(a)]
	\item Let $M \subseteq K$ be an extremal subset of $K$, $x,y \in K \subseteq M$ be arbitrary, and let $t \in [0,1]$. Consider $z := tx + (1-t)y$, if $z \in M$, by definition of extremal subset, $x,y \in M$, which contradicts the choice of $x,y$. Thus $z \notin M$. By definition of convexity, $z \in K$, thus $z \in K \setminus M$ and $K \setminus M$ is convex. \qed
	
	\item Consider $X = R$, $K = [0,1]$, and $N = [0,1)$. Clearly $N$ and $K \setminus N$ are convex ($K \setminus N = \{1\}$ is an extremal point, which is extremal subset of $K$ by definition), however, $N$ is not extremal; since $\frac{1}{2} = \frac{1}{2} \cdot 0 + \frac{1}{2} \cdot 1$ and $1 \notin N$. More generally, any proper subset $[0,x)$ and $[x,1]$ of $K$ will serve as a counterexample, where $x \in [0,1]$.
	
	\item Let $M = \{y\}$, where $y \in K$. By (a), if $M$ is extremal, then $K \setminus M$ is convex. Conversely suppose that $K \setminus M$ is convex. Let $t \in [0,1]$, and $x_1,x_2 \in K \setminus M$. Then by definition of convexity, $tx_1 + (1-t) x_2 \in K \setminus M$. Suppose there exists $t \in [0,1]$ and $x_1,x_2 \in K \setminus M = \set{y}$ such that $y = tx_1 + (1-t)x_2$, by convexity of $K \setminus M$, we must have $y \in K \setminus M$; this is a contradiction since $M = \set{y}$. Thus no such $x_1,x_2$ exists, and we may conclude that if $y = tx_1 + (1-t) x_2$ for some $t \in [0,1]$, then it must be $y = x_1 = x_2$ and $y$ is indeed an extremal point.
\end{enumerate}

\paragraph*{Problem 9.4} (Weak Sequential Continuity of Linear Operators) Let $(X,\norm{\cdot}_X)$ and $(Y,\norm{\cdot}_Y)$ be normed spaces, and $T:X \to Y$ be a linear operator. Prove the following are equivalent.
\begin{enumerate}
	\item $T$ is continuous.
	
	\item If every sequence $(x_n)_n \in X^{\bN}$ such that $x_n \xrightarrow[n \to \infty]{w} x$ in $X$ , then the sequence $(Tx_n)_n$ is such that $T x_n \xrightarrow[n \to \infty]{w} Tx$ in $Y$.
\end{enumerate}
First, notice the following observation. Let $l \in X^*$ be arbitrary, we can induce a linear functional $\tilde{l} : Y \to \bR$ such that the following diagram commutes.
\[
	\begin{tikzcd}
		X \ar{rr}{T} \ar{dr}[swap]{l}& & Y \ar{dl}[swap]{\tilde{l}}\\
		& \bR &
	\end{tikzcd}
\]
by defining $\tilde{l} :\im(T) \to \bR$, $y \mapsto l \circ T^{-1} (y)$. Then it is easy to see that $l$ is continuous if and only if $T$ and $\tilde{l}$ are continuous.\\

Now suppose $T$ is continuous, then $l$ is continuous if and only if $\tilde{l}$ is continuous. Let $(x_n)_n \in X^{\bN}$ be a sequence that converges weakly to $x$ in $X$. Then $l(x_n) \to l(x)$ for all $l \in X^*$. Thus there exists $\tilde{l}:Y \to \bR$ such that $l = \tilde{l} \circ T$. In particular, we have
\[
	l(x_n) \to l(x) ~ ~ ~ \forall l \in X^* \iff \tilde{l} \circ T (x_n) \to \tilde{l} \circ T(x) ~ ~ ~\forall \tilde{l} \in Y^*
\]
Therefore $T(x_n) \xrightarrow[n \to \infty]{w} T(x)$ in $Y$.\\

Conversely suppose every weakly converging sequence $(x_n)_n$ to $x$ in $X$ is weakly converging in $Y$ under transformation $T$, i.e.
\[
	x_n \xrightarrow[n \to \infty]{w} x \implies Tx_n \xrightarrow[n \to \infty]{w} Tx
\]
Let $l \in X^*$ be arbitrary, then by Satz 4.6.1, $l$ is bounded and hence continuous. Likewise, the induced functional $\tilde{l}:Y \to \bR$ is also continuous. Thus it follows that $T$ must be continuous.\\

It remains to show that $l$ is continuous if and only if both $T$ and $\tilde{l}$ is continuous. Suppose $l$ is continuous, then
\[
	\sup_{\norm{x}_{X} = 1} |l(x)| = C < \infty
\]
Then we get
\[
	\sup_{\norm{x}_X = 1} |l(x)| = \sup_{\norm{x}_X = 1} |\tilde{l} \circ T(x)| = \sup_{\norm{x}_X = 1} |l(x)| \norm{Tx}_{Y} |\tilde{l}( \frac{T(x)}{\norm{T(x)}_Y})| = \norm{T}_{L(X,Y)} \cdot \norm{\tilde{l}}_{Y^*}
\]
Therefore $T$ and $\tilde{l}$ are bounded, hence continuous. The other case is similar. \qed

\paragraph{Problem 9.5} (Weak Convergence in Finite Dimension) It suffices to check that every norm-open set is also weak-open since the weak topology on $X$ is the coarsest topology on $X$. Since $X$ is finite dimensional, so we may assume that $E := \set{e_i}_{i=1}^{n}$ is a basis of $X$, with $\norm{e_i} = 1$ for all $i = 1,\cdots,n$. Let $x \in X$ be arbitrary, we can write $x = \sum_{i=1}^{n} x_i e_i$. Denote the functionals $f_i \in X^*$ such that $f_i(x) = x_i$; note that each $f_i$ is bounded by $\norm{x}$, and hence $f_i$'s are continuous.\\

Let $U = B(x_0,r)$ for some $x_0 \in X$ and some $r > 0$ be arbitrary. We claim that for each $x \in U$, there exists $V_x \in 
\tau_W$ such that $x \in V_x \subseteq U$. Since $f_i$ are continuous, we can find $y \in X$ such that $|f_i(y - x)| < \ee$ for all $i = 1,\cdots,n$; such $y$ exists because $\dim X = n < \infty$. Now set 
\[
	V_x = \set{y \in X: |f_i(y-x)| < \ee ~ ~ ~ \forall i = 1,\cdots,n}
\]
Then for each $y \in V_x$, we have $\norm{y - x_0} \le \sum_{i=1}^{n} |f(x - x_0)| < n \ee$; by choosing $\ee = \frac{r}{n}$, we have that $y \in B(x_0,r)$, and hence $V_x \subseteq U$. Thus $U \in \tau_W$ as union of open sets. \qed

\paragraph{Problem 9.6} (Weak Convergence in Hilbert Spaces) Let $(\cH,\inp{\cdot}{\cdot}_{\cH})$ be a real, infinite dimensional Hilbert space, and let $(x_n)_{n \in \bN}$ be a sequence in $\cH$.
\begin{enumerate}[(a)]
	\item Let $x_n \xrightarrow[]{w} x$ in $\cH$, and that $\norm{x_n}_{\cH} \to \norm{x}_{\cH}$. By definition, we have $\inp{x_n}{x} \to \inp{x}{x}$ as $n \to \infty$. Then
	\[
		\norm{x_n - x}_{\cH} = \inp{x_n - x}{x_n - x}_{\cH} = \norm{x_n}_{\cH}^2 - 2 \inp{x_n}{x}_{\cH} + \norm{x}_{\cH}^2 \to 0
	\]
	and we are done. \qed
\end{enumerate}

\end{document}