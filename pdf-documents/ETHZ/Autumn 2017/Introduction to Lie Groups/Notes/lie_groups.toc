\contentsline {part}{I\hspace {1em}Topological Groups \& Haar Measure}{1}{part.1}
\contentsline {section}{\numberline {1}Topological Groups}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Definition \& Example}{1}{subsection.1.1}
\contentsline {section}{\numberline {2}Compactness or Local Compactness of Examples}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Facts about Topological Groups}{8}{subsection.2.1}
\contentsline {section}{\numberline {3}Haar Measure \& Homogeneous Spaces}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Existence of Haar Measure}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Uniqueness of Haar Measure}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Homogeneous Spaces}{18}{subsection.3.3}
\contentsline {part}{II\hspace {1em}Lie Groups \& Lie Algebra}{27}{part.2}
\contentsline {section}{\numberline {4}Lie Groups}{27}{section.4}
\contentsline {subsection}{\numberline {4.1}General Facts about Lie Groups and Lie Algebras}{30}{subsection.4.1}
\contentsline {section}{\numberline {5}Lie Algebra}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}Invariant Vector Fields: The Lie Algebra of a Lie Group}{33}{subsection.5.1}
\contentsline {section}{\numberline {6}Exponential Map}{46}{section.6}
\contentsline {subsection}{\numberline {6.1}Properties of the Exponential Map}{46}{subsection.6.1}
\contentsline {section}{\numberline {7}The Adjoint Representations}{57}{section.7}
\contentsline {part}{III\hspace {1em}Structure Theory}{59}{part.3}
\contentsline {section}{\numberline {8}Stability}{59}{section.8}
\contentsline {subsection}{\numberline {8.1}Application of Lie's Theorem}{63}{subsection.8.1}
\contentsline {section}{\numberline {9}Nilpotency}{65}{section.9}
\contentsline {subsection}{\numberline {9.1}Semi-Simplicity}{70}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Compact Groups}{72}{subsection.9.2}
