\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis II - Problem Set 9}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis II - Problem Set 9}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

\pagestyle{fancy}

\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\section{Survival Kit}
\paragraph{Problem 9.1 - Elliptic Equations in Non-Divergence Form} Let
\[
	Lu = \sum_{i,j = 1}^{n} a_{ij} \frac{\di^2 u}{\di x_i \di x_j}
\]
be an operator in the non-divergence form.
\begin{enumerate}[(a)]
	\item Let $\Omega \subseteq \bR^n$ be open and bounded. Let $f \in L^2(\Omega)$, and $a_{ij} \in C^2(\bar{\Omega})$, and $c \in C^0(\bar{\Omega})$ satisfy
	\[
		\forall x \in \Omega: ~ ~ ~ c(x) \ge \frac{1}{2} \sum_{i,j = 1}^{n} \frac{\di^2 a_{ij}}{\di x_i \di x_j} (x)
	\]
	where we assume $a_{ij}$ are measurable and are uniformly elliptic. Prove that $\exists ! u \in H^1_0(\Omega)$ such that for all $\ph \in H^1_0(\Omega)$,
	\[
		\sum_{i,j = 1}^{n} \int_{\Omega}  \left( a_{ij} \frac{\di u}{\di x_j} \frac{\di \ph}{\di x_i} + \frac{\di a_{ij}}{\di x_i} \frac{\di u}{\di x_j} \ph  \right) ~ dx + \int_{\Omega} cu\ph ~ dx = \int_{\Omega} f \ph ~ dx \tag{*} \label{Equation}
	\]
	
	\item Under the same assumption as (a), prove that a classical solution $u \in C^2(\Omega) \cap H^1_0(\Omega)$ of $-Lu + cu = f$ \eqref{Equation}.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item Let us define the following bilinear form
		\[
			B(u,\ph) = \sum_{i,j = 1}^{n} \int_{\Omega} \left( a_{ij} \frac{\di u}{\di x_j} \frac{\di \ph}{\di x_i} + \frac{\di a_{ij}}{\di x_i} \frac{\di u}{\di x_j} \ph \right) ~ dx + \int_{\Omega} C u \ph ~ dx
		\]
		We want to use Lax-Milgram, then we will be done. We need to prove that it is bounded, and coercive.\\

To prove that $B$ is bounded, we have
\begin{gather*}
	|B(u,\ph)| \le \int_{\Omega} |a_{ij}| |\nabla u| |\nabla \ph| ~ dx + \int_{\Omega} \left| \frac{\di a_{ij}}{\di x_i} \right| |\nabla u| \ph ~ dx + \int_{\Omega} |C| ~ |u| \cdot |\ph| ~ dx\\
	\le \norm{a_{ij}}_{C^0(\bar{\Omega})} \underbrace{\norm{\nabla u}_{L^2}}_{(*)} \norm{\nabla \ph}_{L^2} + \norm{a_{ij}}_{C^1} \norm{\nabla u}_{L^2} \norm{\ph}_{L^2} + \norm{C}_{C^0} \norm{u}_{L^2} \norm{\ph}_{L^2}\\
	\overset{\text{Poincar\'{e}}}{\le} C \norm{\nabla u}_{L^2}\ \norm{\nabla \ph}_{L^2}
\end{gather*}
Note that (*) is equivalent to $\norm{\nabla u}_{H^1_0}$ by Poincar\'{e} inequality. To prove that $B$ is coercive, we first notice that
\[
	\int_{\Omega} a_{ij} \frac{\di u}{\di x_j} \frac{\di u}{\di x_i} \overset{(\square)}{\ge} \lambda \norm{\nabla u}^2_{L^2}
\]
where we use uniform ellipticity and $\xi = \nabla u$ at ($\square$). Then we have
\begin{gather*}
	\int_{\Omega} \frac{\di a_{ij}}{\di x_i} \frac{\di u}{\di x_i} u ~ dx + \int_{\Omega} C u^2 = \frac{1}{2} \int_{\Omega} \frac{\di a_{ij}}{\di x_i} \frac{\di u^2}{\di x_i} ~ dx + \int_{\Omega} C u^2\\
	\overset{Parts}{=} - \frac{1}{2} \int_{\Omega} \frac{\di^2 a_{ij}}{\di x_i \di x_j} u^2 + \int_{\Omega} C u^2\\
	\implies C(x) \ge \frac{1}{2} \frac{\di^2 a_{ij}}{\di x_i \di x_j}
\end{gather*}
This proves that $|B(u,u)| \ge \lambda \norm{\nabla u}_{L^2}^2$.\\

	\item We want to prove $- \frac{\di^2 u}{\di x_i \di x_j} a_{ij} + Cu = f$. Let $\ph \in H^1_0$, then
\begin{gather*}
	- \int_{\Omega} \frac{\di^2 u}{\di x_i \di x_j} a_{ij} \ph + \int_{\Omega} Cu \ph = \int_{\Omega} f \ph\\
	\implies \int_{\Omega} \frac{\di u}{\di x_j} \left( \frac{\di a_{ij}}{\di x_i} \ph + a_{ij} \frac{\di \ph}{\di x_i} \right) + \int_{\Omega} C u \ph = \int_{\Omega} f \ph
\end{gather*}
and we are done.
	\end{enumerate}
\end{proof}

\paragraph{Problem 9.2 - Reflection Lemma Towards Boundary Regularity} Define
\begin{itemize}
	\item $R_{+}^{n} := \set{x \in \bR^n: x_n > 0}$
	
	\item $R_{-}^{n} := \set{x \in \bR^n: x_n < 0}$
\end{itemize}
Let $u \in H^1_0(\bR_{+}^{n}	)$ with $\supp(u) \Subset \bR^n$ be a weak solution to $-\Delta u = f$ in $\bR_{+}^{n}$. Define $\bar{u},\bar{f} : \bR^n \to \bR$ by the following
\begin{itemize}
	\item $\bar{u} (x',x_n) = \begin{cases} u(x',x_n) & x_n > 0 \\ -u(x',-x_n) & x_n < 0\end{cases}$
	
	\item $\bar{f} (x',x_n) = \begin{cases} f(x',x_n) & x_n > 0 \\ -f(x',-x_n) & x_n < 0\end{cases}$
\end{itemize}
Show that $\bar{u} \in H^1_0(\bR^n)$ has compact support and that $\bar{u}$ is a weak solution of $-\Delta \bar{u} = \bar{f}$ in $\bR^n$.
\begin{proof}
	Since $\supp(u) \Subset \bR^n$, so $u$ has compact support. Now note that
	\begin{gather*}
		\supp(\bar{u}) = \set{x \in \bR^n : \bar{u}(x) \ne 0}\\
		= \set{x \in \bR^n \big| \substack{u(x',x_n) \ne 0, x_n > 0\\ -u(x',-x_n) \ne 0, x_n < 0}}\\
		= \set{x \in \bR_{+}^{n} \big| u(x) \ne 0} \sqcup \set{x \in \bR_{-}^{n} \big| -u(x',-x_n) \ne 0}\\
		\implies |\supp(\bar{u})| = |\supp(u)| + \left| \set{x \in \bR_{-}^{n} \big| -u(x',-x_n) \ne 0} \right|\\
		\overset{(*)}{=} |\supp(u)| + \left| \set{x \in \bR_{+}^{n} \big| -u(x',x_n) \ne 0}\right| = |\supp(u)| + |\supp(-u)|
	\end{gather*}
	Where (*) is due to $x_n < 0 \iff -x_n > 0$, so we can replace $x \in \bR_{-}^{n}$ with  $x \in bR_{+}^{n}$. Now note that $|\supp(u)| = |\supp(-u)|$, so $|\supp(\bar{u})| = 2|\supp(u)| < \infty$, hence $\bar{u}$ has compact support.\\
	
	Now since $u$ is a weak solution to the equation $-\Delta u = f$ in $\bR_{+}^{n}$, so for all $\ph \in C_c^{\infty}(\bR_{+}^{n})$, we have
	\[
		- \int_{\bR_{+}^{n}} \Delta u \cdot \ph ~ dx = \int_{\bR_{+}^{n}} f \ph
	\]
	Now let $\psi(x) := \ph(x',x_n) - \ph(x',-x_n)$, since $\ph \in C_{c}^{\infty}(\bR_{+}^{n})$, so $\psi \in C^{\infty} \cap H^1_0(\bR_{+}^{n})$. Now we have
	\begin{align*}
		- \int_{\bR^n} \Delta \bar{u} \psi ~ dx = & -  \int_{\bR_{+}^{n}} \Delta u(x) (\ph(x',x_n) - \ph(x',-x_n)) ~ dx\\
		& - \int_{\bR_{-}^{n}} \Delta [- u(x',-x_n)] (\ph(x',x_n) - \ph(x',-x_n)) ~ dx\\
		= & - \int_{\bR_{+}^{n}} \Delta u \ph ~ dx + \int_{\bR_{+}^{n}} \Delta u (x) \ph(x',-x_n) ~ dx\\
		& + \int_{\bR_{-}^{n}} \Delta u(x',-x_n) \ph(x) ~ dx - \int_{\bR_{-}^{n}} \Delta u(x',-x_n) \ph(x',-x_n) ~ dx\\
		= & \int_{\bR_{+}^{n}} f \ph ~ dx  - \int_{\bR_{+}^{n}} f(x) \ph(x',-x_n) ~ dx - \int_{\bR_{+}^{n}} f (x) \ph(x',-x_n) ~ dx\\
		& + \int_{\bR_{+}^{n}} f (x) \ph(x) ~ dx\\
		= & \int_{\bR_{+}^{n}} f \psi ~ dx - \int_{\bR_{+}^{n}} f(x) \ph(x',-x_n) ~ dx + \int_{\bR_{+}^{n}} f \ph ~ dx\\
		= & \int_{\bR_{+}^{n}} f \psi ~ dx - \int_{\bR_{-}^{n}} f(x',-x_n) \ph(x) ~ dx + \int_{\bR_{-}^{n}} f(x',-x_n) \ph(x',-x_n) ~ dx\\
		= & \int_{\bR_{+}^{n}} f \psi ~ dx - \int_{\bR_{-}^{n}} f(x',-x_n) \psi(x) ~ dx = \int_{\bR} \bar{f} \psi ~ dx
	\end{align*}
	Since $\ph$ is chosen arbitrarily, this proves that $\bar{u}$ is a weak solution to the equation $-\Delta \bar{u} = \bar{f}$ in $\bR^n$ as desired from the question.
\end{proof}

\paragraph{Problem 9.3 - Horizontal Derivatives} Given $u \in H^2(\bR_{+}^{n}) \cap H^1_0(\bR_{+}^{n})$. Prove that for all $i \in \set{1,\cdots,n-1}$, $\frac{\di u}{\di x_i} \in H^1_0(\bR_{+}^{n})$.

\section{Project on Bilaplacian}
\paragraph{Problem 9.4 - Properties of the Bilaplacian}
Let $\Omega \subset \bR^n$ be open bounded with smooth boundary, and
\[
	\Xi = \set{u \in H^4(\Omega) \cap H^1_0(\Omega): \Delta u \in H^1_0(\Omega)}
\]
\begin{enumerate}[(a)]
	\item Prove that the operator $\Delta^2 : \Xi \to L^2(\Omega)$, $u \mapsto \Delta (\Delta u)$ is bijective.
	
	\item Let $f \in L^2(\Omega)$ with $u \in \Xi$ s.t. $\Delta^2 u = f$. Prove that for all $\ph \in \Xi$, $\int_{\Omega} u \Delta^2 \ph ~ dx = \int_{\Omega} f \ph ~ dx$.
	
	\item Assume $u,f \in \Xi$ satisfy properties of (b). Prove that $u \in \Xi$.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item we want to prove that $\Delta^2 : \Xi \to L62(\Omega)$ is bijective. To prove injectivity, we want to show that $\ker(\delta^2) = \set{0}$. Indeed, let $u \in \Xi$ such that $\Delta^2 u = 0$. Since $v := \Delta^2 u \in H^2 \cap H^1_0$, therefore again implies $\Delta v = 0 \implies v = 0$; in particular, $\Delta u = 0 \implies u = 0$. To show that it is surjective, let $f \in L^2(\Omega)$, and we want to find $u \in \Xi$ such that $\Delta^2 u = f$. Let $w \in H^2 \cap H^1_0(\Omega)$ be such that $\Delta w = f$; since $w \in H^2(\Omega) \subseteq L^(\Omega)$, so we can again find $u \in H^2 \cap H^1_0(\Omega)$ such that $\Delta u = w$. But this means that $\Delta u \in H^2 \cap H^1_0 \implies u \in H^4 \cap H^1_0$, and $\Delta^2 u = f$. So $\Delta^2$ is bijective.
		
		\item $f \in L^2, u \in \Xi$, $\Delta^2 u = f$. Let $\ph \in \Xi$, then 
\begin{gather}
	\setcounter{equation}{0}
	\int_{\Omega} u \Delta^2 \ph = - \int_{\Omega} \nabla u \cdot \nabla (\Delta \ph)\\
	= \int_{\Omega} \Delta u \cdot \Delta \ph\\
	= \int_{\Omega} \Delta^2 u \ph = \int_{\Omega} f \ph
\end{gather}
Note that (1) is by definition; (2) is by the fact that $\nabla \ph \in H^1_0$; (3) is by the fact that $\Delta u \in H^1_0$ and by definition.
		
		\item $u,f \in L^2$. For all $\psi \in C_c^{\infty}(\Omega)$, we can find $\ph \in H^1_0(\Omega)$ such that $\Delta \ph = \psi$. Then $\int)_{\Omega} \Delta \psi u = \int_{\Omega} \Delta^2 \ph u = \int_{\Omega} f \ph$, hence proving the claim.

	\end{enumerate}
\end{proof}

\paragraph{Problem 9.5 - Weak Solution of the Bilaplace Equation}
Let $\Omega \subset \bR^n$ be open bounded with smooth boundary.
\begin{enumerate}[(a)]
	\item Prove that $\inp{u}{v} := \int_{\Omega} \Delta u \cdot \Delta v ~ dx$ defines a scalar product on $H^2(\Omega) \cap H^1_0(\Omega)$, and is equivalent to the standard scalar product on $H^2(\Omega)$.
	
	\item Show that $(H^2 \cap H^1_0(\Omega),\inp{\cdot}{\cdot})$ is a Hilbert space.
	
	\item Prove that given $f \in L^2(\Omega)$, there exists a unique $u \in H^2(\Omega) \cap H^1_0(\Omega)$ satisfying
	\[
		\inp{u}{v} = \int_{\Omega} f v ~ dx ~ ~ ~ \forall v \in H^2 \cap H^1_0(\Omega)
	\]
	Show that $u \in \Xi$, where $\Xi$ is from problem 9.4.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item We will first prove that $\inp{u}{v}$ defines an inner product on $H^2 \cap H^1_0(\Omega)$. Since the domain $\Omega \subseteq \bR^n$, so $\inp{u}{v} = \int_{\Omega} u v ~ dx = \int_{\Omega} v u ~ dx = \inp{v}{u}$, i.e. $\inp{\cdot}{\cdot}$ is symmetric. Now let $\alpha \in \bR$, $u_1,u_2, v \in H^2 \cap H^1_0(\Omega)$. Then
		\begin{align*}
			\inp{\alpha u_1 + u_2}{v} =  &\int_{\Omega} \Delta (\alpha u_1 + u_2) \cdot \Delta v ~ dx = \alpha \int_{\Omega} \Delta u_1 \cdot \Delta v ~ dx + \int_{\Omega} \Delta u_2 \Delta v ~ dx\\
			= & \alpha \inp{u_1}{v} + \inp{u_2}{v}
		\end{align*}
		By symmetry, $\inp{\cdot}{\cdot}$ is bilinear. Finally, let $u \in H^2 \cap H^1_0 (\Omega)$ be arbitrary. Then
		\[
			\inp{u}{u} = \int_{\Omega} \underbrace{(\Delta u)^2}_{\ge 0} ~ dx \ge 0
		\]
		Now suppose $\inp{u}{u} = 0$, so
		\[
			\int_{\Omega} (\Delta u)^2 ~ dx = 0 \iff (\Delta u)^2 \overset{a.e.}{\equiv} 0 \iff \Delta u \overset{a.e.}{\equiv} 0
		\]
		Since $u \in H^2 \cap H^1_0 \subseteq H^1_0$, so $\nabla u \equiv c$ on $\Omega$; in fact, $\nabla u \equiv 0$ on $\Omega$. Hence this implies $u \equiv 0$ almost everywhere.\\
		
		To prove that $\inp{\cdot}{\cdot}$ is equivalent to the usual inner product on $H^2(\Omega)$, it suffices to show that the two induced norms are equivalent. Indeed, we have let $\norm{\cdot}_1$ denotes the norm induced by $\inp{\cdot}{\cdot}$. Then
		\begin{align*}
			\norm{u}_1^2 = & \int_{\Omega} |\Delta u|^2 ~ dx = \norm{\Delta u}_{L^2} \le \norm{u}_{H^2}^2\\
			\norm{u}_{H^2}^2 = & \norm{u}_{L^2} + \norm{\nabla u}_{L^2} + \norm{\Delta u}_{L^2} \le C \norm{\Delta u}_{1}^{2}
		\end{align*}
		Where the second inequality above is by applying Poincar\'{e} inequality twice. Thus $\norm{\cdot}_1$ and $\norm{\cdot}_{H^2}$ are equivalent norms.
		
		\item Let $\cH := H^2 \cap H^1_0(\Omega)$. We have proved that $(\cH,\inp{\cdot}{\cdot})$ is a inner product space. Thus we only need to prove that it is complete. Let $(u_k)_k$  be a sequence in $\cH$. Since $H^1_0$ is a closed subspace of $H^1$, thus it contains all its limit points; in particular there exists $u \in H^1_0$ such that $u_k \xrightarrow[]{H^1_0} u$. Moreover, Laplace operator $\Delta : H^2(\Omega) \to L^2(\Omega)$ is continuous, so
		\[
			\lim_k \Delta (u_k) = \Delta (\lim_k u_k) = \Delta u
		\]
		Thus $\Delta u$ exists, and $u \in H^2(\Omega)$. So $u \in \cH$, and $\cH$ is indeed complete. \qedhere
	\end{enumerate}
\end{proof}

\end{document}