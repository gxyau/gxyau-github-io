\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Probabilistic Method in Combinatorics}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Probabilistic Method in Combinatorics \\ Problem Set 2}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Problem 2.1} Let $G = (V,E)$ be a bipartite graph, $V = A \sqcup B$, $|V| = n$. Suppose $S(v)$ is a list of colours associated to $v \in V$, such that $|S(v)| > \log_2 n$ for all $v \in V$. Now let $S := \bigcup_{v \in V} S(v)$, i.e. the union of all colours associated to each vertex.\\

We partition $S$ into $S(A)$ and $S(B)$ as following: for each $c \in S$ set
\[
	\bP(c \in S(A)) = \bP(c \in S(B)) = \frac{1}{2}
\]
independently. In other words, the colour $S(A),S(B)$ is a bipartition of the colours in $S$, with expectation of half the colours is in $S(A)$, and the other half in $S(B)$.\\

If we can prove that there exists a partition such that $S(v) \cap S(A) \ne \emptyset$ for each $v \in A$ and $S(v) \cap S(B) \ne \emptyset$ for each $v \in B$, then we can colour each vertex with the colours in $S(v) \cap S(A)$ or $S(v) \cap S(B)$. Assuming such partition is possible, then the colouring scheme is indeed a proper colouring of $V$. Note that for a colouring to be improper, we require $e = uv$ with $u \in A$, $v \in B$ such that $c(u) = c(v)$; but under our partition scheme of $S$, this is not possible since this would require colour $c$ to be in both $S(A)$ and in $S(B)$ simultaneously.\\

It remains to show that the partition $S = S(A) \sqcup S(B)$ with aforementioned property exists. For each $v \in A$ (resp. $v \in B$), define the following indicator variable
\[
	X_v = \begin{cases}
		1 & S(v) \cap S(A) = \emptyset ~ ~ ~ \text{(resp. $S(v) \cap S(B) = \emptyset$)}\\
		0 & S(v) \cap S(A) \ne \emptyset ~ ~ ~ \text{(resp. $S(v) \cap S(B) \ne \emptyset$)}
	\end{cases}
\]
Then $\bP(X_v = 1) = (\frac{1}{2})^{|S(v)|} < 2^{- \log_2 n} = \frac{1}{n}$. Therefore
\begin{gather*}
	\bP(\text{There exists a proper colouring}) = \bP \left( \sum_{v \in V} X_v \right) = 1 - \bP \left( \sum_{v \in V} X_v > 0 \right)\\
	\overset{(*)}{\ge} 1 - \sum_{v \in V} \bP(X_v = 1) > 1 - \sum_{v \in V} \frac{1}{n} = 0
\end{gather*}
Where $(*)$ is by union bound. This concludes the proof.

\paragraph{Problem 2.2} Let $\cA$ and $[n]$ be as described in the problem statement. Suppose $|\cA| = L$, and write $\cA = \set{A_i}_{i=1}^{L}$. For each $A_i \in \cA$, we define $B_i := [n] \sm A_i$, i.e. $B_i$ is the complement of $A_i$ in $[n]$. Thus we have $A_i \cap B_i = \emptyset$ for all $i \in [L]$, and $A_i \cap B_j \ne \emptyset$ for all $i \ne j$. Let $k_i := |A_i|$ for all $A_i \in \cA$, and without loss of generality, we may assume $k_i \le \frac{n}{2}$ since we can define $\cA' := \cA \sm \set{A_i} \cup \set{B_i}$ and work with $\cA'$ for each $k_i > \frac{n}{2}$.\\

Define $E_i$ to be the event that order of all elements of $A_i$ is before order of elements in $B_i$. As discussed in class, since $A_i \cap B_j \ne \emptyset \ne A_j \cap B_i$ for all $i \ne j$, we claim that $E_i \cap E_j = \empty$. Suppose $E_i \cap E_j \ne \emptyset$, then there is an ordering of $[n]$ such that all elements of $A_i$ is before $B_i$, and all elements of $A_j$ is before $B_j$.\\

Without loss of generality, assume $k_i > k_j$. Let us denote by $a_{(l)}^{i}$ be the $l^{th}$ element in $A_i$ in increasing order. Then $a_{(k_i)}^{i}$ is the largest element in $A_i$. Define $b_{(l)}^{i}$ analogously. Since $E_i \cap E_j \ne \emptyset$, therefore there is an ordering of $[n]$ such that $a_{(k_i)}^{i} \le b_{(1)}^{i}$, and $a_{(k_j)}^{j} \le b_{(1)}^{j}$. We know that $A_j \cap B_i \ne \emptyset$, so this shows that $a_{(k_j)}^{j} \ge b_{(1)}^{i}$. But this means that $b_{(1)}^{j} > b_{(1)}^{i} \ge a_{(k_j)}^{j} > a_{(k_i)}^{i}$, i.e. all elements of $A_i$ is before elements of $B_j$, contradicting $A_i \cap B_j \ne \emptyset$.\\

Finally, we know that $\sum_{i=1}^{L} \bP(E_i) \le 1$ since $\bigcup_{i=1}^{L} E_i$ is a subspace of a probability space. So
\[
	1 \ge \sum_{i=1}^{L} \bP(E_i) = \sum_{i=1}^{L} {n \choose k_i}^{-1} \ge \sum_{i=1}^{L} {n \choose \floor{\frac{n}{2}}}^{-1} = \frac{L}{{n \choose \floor{\frac{n}{2}}}}
\]
Finally, observe that since $k_i \le \frac{n}{2}$, so ${n \choose k_i} \le {n \choose \floor{\frac{n}{2}}} \implies {n \choose k_i}^{-1} \ge {n \choose \floor{\frac{n}{2}}}^{-1}$. This proves our statement.

\paragraph{Problem 2.3} This is similar to Bollob\'{a}s' theorem, with $(A_i \cap B_j) \cup (A_j \cap B_i) \ne \emptyset$ for all $i \ne j$ instead of $A_i \cap B_j \ne \emptyset \ne A_j \cap B_i$. For simplicity, write $n = k+l$, and let us still define $E_i$ to be the event that all elements of $A_i$ are before $B_i$. Since $A_i \cap B_j) \cup (A_j \cap B_i) \ne \emptyset$, then either one of the following is true
\begin{itemize}
	\item $A_i \cap B_j \ne \emptyset$, and $A_j \cap B_i \ne \emptyset$;
	
	\item $A_i \cap B_j = \emptyset$, and $A_j \cap B_i \ne \emptyset$;
	
	\item $A_i \cap B_j \ne \emptyset$, and $A_j \cap B_i = \emptyset$;
\end{itemize}
The first case is precisely the condition in Bollob\'{a}s' theorem, so $E_i \cap E_j = \emptyset$, and we have $\bP(E_i) = {n \choose k}^{-1} \ge {n \choose k}^{-2} = {n \choose k}^{-1} \cdot {n \choose l}^{-1}$. Second and third case are analogous, so we will only focus on second case.\\

In the second case, $\bP(E_j) = {n \choose k}^{-1} \ge {n \choose k}^{-1} \cdot {n \choose l}^{-1}$. Since $A_j \cap B)i \ne \emptyset$, so we only care about all elements of $A_j$ is before $B_j$. $\bP(E_i)$ is trickier. We claim that $\bP(E_i) = {n \choose k}^{-1} {n \choose l}^{-1}$. To see this, notice that since $A_i \cap B_j = \emptyset$, thus we must have elements of $A_i$ has order smaller than elements in $B_i$ and in $B_j$. Thus
\[
	\bP(E_i) = \underbrace{\frac{k! l!}{n!}}_{\text{$A_i$ before $B_i$}} \cdot \underbrace{\frac{k! l!}{n!}}_{\text{$A_i$ before $B_j$}} = {n \choose k}^{-1} \cdot {n \choose l}^{-1}
\]
Finally, $\bigcup_{i=1}^{h} E_i$ is again a subspace of a probability space, so
\[
	1 \ge \sum_{i=1}^{h} \bP(E_i) \ge \sum_{i=1}^{h} {n \choose k}^{-1} {n \choose l}^{-1} = \frac{h}{{n \choose k}{n \choose l}} \overset{(*)}{\ge} \frac{h}{	(\frac{n}{k})^k (\frac{n}{l})^{l}} = h \cdot \left( \frac{(k+l)^{k+l}}{k^k l^l} \right)^{-1}
\]
as desired. Note that $(\frac{n}{k})^{k} \le {n \choose k} \implies (\frac{n}{k})^{-k} \ge {n \choose k}^{-1}$ justifies the inequality in $(*)$. This concludes the proof.

\paragraph{Problem 2.4}

\end{document}