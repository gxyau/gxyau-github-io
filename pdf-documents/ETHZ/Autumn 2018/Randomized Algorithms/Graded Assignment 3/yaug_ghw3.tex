\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Randomized Algorithms - Graded Homework 3}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Randomized Algorithms - Graded Homework 3}
\author{Guo Xian Yau}
\date{\today}

\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{prop}{Proposition}

% New column type for tabular X
\newcolumntype{b}{X}
\newcolumntype{s}{>{\hsize=.5\hsize}X}

\begin{document}

\maketitle

\begin{center}
	\textit{\centering Collaborator(s): Konstantinos Andrikopoukos, Pontus Westermark, Simon Weber}
\end{center}

\paragraph{Problem 1} Let $n \in \bN$ be arbitrary, $G = (V,E)$ be a graph on $n$ vertices, $p \gg \frac{\log n}{n}$, $m := |E|$. Suppose $H$ is a subgraph of $G$ such that  every edge is chosen independently as an edge of $H$, and we include only those vertices of $G$ if it is connected to at least one chosen edge. Let $X$ be the number of vertices in the graph $H$.\\

Pick an arbitrary ordering of the edges, say $E = \set{e_i}_{i=1}^{m}$, we will include the edges in this order, each with probability $p$ independently of other edges. Consider $\Omega = \set{0,1}^{m}$, $\Omega_e = \set{0,1}$, and $\omega_e \in \Omega_e$ indicates whether or not $e$ is chosen.\\

Then $X(\omega)$, the number of vertices in $H$, is just the number of vertices that are not isolated; moreover, the effect of each coordinate on $X$ is at most 2 since for each edge added, the maximum number of vertices that are not already exposed to previously added edges is 2. Hence $c_e = 2$ for all $e \in E$. Finally, we define $\psi(r) = \left( \max_{i=1,\ldots,m} c_i^2 \right) \cdot r = 4r$ for all $r \in \bR$; note that $\max_{i=1,\ldots,m} c_i^2$ exists because this is a finite set.\\

For an arbitrary $\omega \in \Omega$, suppose $X(\omega) \ge r$, let $J(\omega) = \set{i \in [m] : 1 \le i \le i_r(\omega)}$ where $i_r(\omega)$ is the first coordinate such that the number of vertices exceeds $r$ for the first time using $\omega$. Notice that the first edge that was added increase the number of vertices by 2, so in the Wurst-K\"{a}se-Szenario where we only increaser the number of non-isolated vertices by 1 for every edge we add, we still have $|J(\omega)| = r-1 < r$, hence $|J(\omega)| \le r$. Then if $\omega' \in \Omega$ is such that for all $i \in J := J(\omega)$, $\omega_i' = \omega_i$, clearly $X(\omega') \ge r$, and $\sum_{i \in J} c_i^2 \le 4 |J| \le 4r = \psi(r)$. We are now in a position to apply Talagrand inequality.\\

Assuming $m_0$ is the median of $X(\omega)$ for all $\omega \in \Omega$. Then Talagrand says
\[
	\bP(|X - m_0| \ge t) \le 4 \exp(-\frac{t^2}{16(m_0 + t)})
\]
Now using similar analysis as in the notes, we can prove that $|\bE[X] - m_0| = O(\sqrt{\bE[X]})$; instead of $|\bE[X] - m_0| \le 8 \sqrt{\pi \bE[X]} + 32$, we have
\[
	|\bE[X] - m_0| \le 2 \sqrt{32 \pi m_0} + C_0 \le 16\sqrt{\pi} \cdot \sqrt{\bE[X]} + C_0 = C_1 \sqrt{\bE[X]} + C_0 = O(\sqrt{\bE[X]})
\]
Rearranging the inequalities above, we will arrive at the following inequalities:
\[
	\bE[X] - O(\sqrt{\bE[X]}) \le m_0 \le \bE[X] + O(\sqrt{\bE[X]})
\]
Using this at $(*)$ and triangle inequality at $(\triangle)$, we get the following for arbitrary $t \in \bR_{\ge 0}$.
\begin{gather*}
	\bP(|X - \bE[X]| \ge t) \overset{(\triangle)}{\le} \bP(|X - m_0| \ge t - (|m_0 - \bE[X]|))\\
	\le 4 \exp( - \frac{( t - O(\sqrt{\bE[X]}) )^2}{16 (m_0 + t - O(\sqrt{\bE[X]}))}) \le 4 \exp( - \frac{t^2}{16 (m_0 + t - O(\sqrt{\bE[X]}))})\\
	\overset{(*)}{=} 4 \exp( - \frac{t^2}{16(\bE[X] - O(\sqrt{\bE[X]}) + t)} ) = 4 \exp( - \Omega(\frac{t^2}{\bE[X] + t}) )
\end{gather*}
Now using $t = \ee \bE[X]$, we get
\[
	\bP(|X - \bE[X]| \ge \ee \bE[X]) \le 4 \exp( - \Omega(\frac{\ee^2}{1 + \ee} \bE[X]) ) = 4 \exp( - \Omega(\bE[X]))
\]
Let $C$ such that $\bP(|X - \bE[X]| \ge \ee \bE[X]) \le 4 \exp( - C \bE[X]) = C' \exp(-\bE[X])$ using the definition of $\Omega(\bE[X])$. We now claim that $\bE[X] = \Omega(\log n)$. Indeed, notice that since $p \in [0,1]$, implies that $(1-p) \in [0,1]$, and consequently $(1-p)^l \le (1-p)$ for any $l \in \bN$. Now for any $v \in V$,
\[
	\bP(\text{$v$ is not isolated}) = 1 - (1-p)^{\deg_G(v)} > 1 - (1 - p) = p
\]
Note that the strict inequality is due to connectedness of $G$ and the fact that $(1-p) > (1-p)^l$ for any $l \ge 2$. Therefore the expected number of vertex in $H$ is
\[
	\bE[\text{\# of vertices in $H$}] = \sum_{v \in V} \bP(\text{$v$ is not isolated}) > np = n \cdot \frac{\log n}{n} = \log n
\]
Combining this and the result before, we get
\[
	\bP(|X - \bE[X]| \ge \ee \bE[X]) \le C' \exp(-\bE[X]) < C' \exp(- \log n) = o(\frac{1}{n})
\]
as required in the problem. \qed

\newpage

\paragraph{Problem 2} Let $n \in \bN$, and $k := n^{\frac{3}{5}}$ be such that $k$ is an integer. Let $c : [n] \to [k]$ be a colouring such that $\bP(c(i) = j) = \frac{1}{k}$ for all $j \in [k]$ and independently for all $i \in [n]$; in other words, choose a colouring of $[n]$ with $k$ colours uniformly at random.\\

Fix arbitrary $h,i,j \in [n]$. Since we colour each element $h,i,j$ independently, therefore for an arbitrary colour $i_0 \in [k]$,
\[
	\bP(c(h) = c(i) = c(j) = i_0) = \bP(c(h) = i_0) \cdot \bP(c(i) = i_0) \cdot \bP(c(j) = i_0) = \frac{1}{k^3}
\]
Let $X^{(i)}$ be the number of AP's that is coloured $i$ for some fixed arbitrary $i \in [k]$, and for any $h,i,j \in [n]$ an AP, we define
\[
	X_{x,y,z}^{(i)} = \begin{cases}
		1 & \text{The AP $x,y,z$ is monochoratic with colour $i$}\\
		0 & \text{Otherwise}
	\end{cases}
\]
Then clearly $X^{(i)} = \sum_{\text{$(x,y,z)$ an AP}} X_{x,y,z}^{(i)}$. We will now count the number of AP triplets in the set $[n]$. For simplicity of notation, assume $a_l := \set{x_l,y_l,z_l}$ be the $l^{th}$ AP triplet with $x_l < y_l < z_l$. Let us define the probability space $(\Omega_i,\bP_i)$ to be $\Omega_i = {0,\ldots,n^2}$; we will prove later that there are $O(n^2)$ AP's in total, hence we can use $0,\ldots,n^2]$ as the probability space, and $\bP_i(x) = \bP(X^{(i)} = x)$ for $x \in \Omega_i$. Then we can define $(\Omega,\bP) = \prod_{i=1}^{k} (\Omega_i,\bP(i))$.\\

First, we will count the number of middle element, namely if $x < y < z$, we want to count the number of $y$'s; we cannot choose $y = 1$ or $y = n$, since $y$ is the middle element. Next, notice that once we fixed $y$, there are $\min \set{y, n-y}$ choices in total to choose $x$ and $z$; consider $y < n - y$, then as soon as we pick $x$, say $x$, then $z$ is fixed since we need $z - y = y - x$. For $n - y < y$ case, it is similar, except we pick $z$ instead of $x$. So we have
\[
	\text{\# of AP} = 2 \cdot \sum_{y=2}^{\frac{n}{2}} \sum_{x = 1}^{y-1} 1 \in \Theta(n^2)
\]
For simplicity of notation we will denote by $L$ the number of AP triplets in $[n]$, so $L \in \Theta(n^2)$, and we will denote $X_l^{(i)}$ by the $l^{th}$ AP triplet. Then the expectation $\bE[X^{(i)}]$ is
\[
	\lambda_i := \bE[X^{(i)}] = \sum_{l = 1}^{L} \bE[X_l^{(i)}] = \sum_{l=1}^{L} \bP(X_l^{(i)} = 1) = O(n^2) \cdot \frac{1}{k^3} = O(n^2 \cdot n^{-\frac{9}{5}}) = O(n^{\frac{1}{5}})
\]
Now for $l,l' \in [L]$, with $l \ne l'$, suppose that the $l^{th}$ AP triplet and the $l'^{th}$ AP triplet intersect non-trivially. Then either one or two of the three numbers are the same. We will treat both cases separately. First assume they share 2 numbers, then
\[
	\bE[X_{l}^{(i)} X_{l'}^{(i)}] = \bP(X_l^{(i)} X_{l'}^{(i)}) = \frac{1}{k^4} = \Theta(n^{-\frac{12}{5}})
\]
where $(I)$ comes from choosing two out of the triplet $x < y < z$ and $(II)$ comes from choosing two out of the triplet $x' < y' < z'$. Also note that first inequality is strict, and since $l \ne l'$, the case where $x' = x$ and $z' = z$ should not have been accounted for. So $\bE[X_{l}^{(i)} X_{l'}^{(i)}] \in \Theta(n^{-\frac{12}{5}})$ if they share precisely 2 out of the three numbers. On the other hand, if they only share one number, then we can also easily see that $\bE[X_{l}^{(i)} X_{l'}^{(i)}] \in \Theta(k^{-5}) = \Theta(n^{-3})$. We shall now count the number of $X_l^{(i)}$ and $X_{l'}^{(i)}$ in each cases.\\

In the case where each AP's has two out of three numbers are the same, since there are ${3 \choose 2} = 3$ ways to choose two numbers out of three numbers in the first AP, and 3 ways to choose for the second AP, the total number of pairs of AP's that intersect in two numbers is at most $9 n^2 = O(n^2)$; there are $\Theta(n^2)$ AP's in total, and each of them has at most $9$ other AP's that will intersect in precisely 2 numbers. For AP's that shares exactly one number, since upon fixing one number, we have $\lessapprox \frac{n}{2}$ choices for the second number, and the third one is somewhat fixed. Since there are $\Theta(n^2)$ choices for the first AP and $O(n)$ choices for the second AP, hence number of pairs of AP's that share one common number is $O(n^3)$. This means that
\[
	\Delta_i = \sum_{\substack{l \ne l' \\ a_l \cap a_{l'} \ne \emptyset}} = O(n^3) \cdot \frac{1}{k^5} + O(n^2) \cdot \frac{1}{k^4} = O(1)
\]
Hence $\min \set{\lambda_i, \frac{\lambda_i^2}{\Delta_i}} = O(n^{\frac{1}{5}})$. Using Janson's concentration inequality, we have
\[
	\bP(X^{(i)} = 0) \le \exp(-\frac{1}{4}O(n^{\frac{1}{5}})) = C_0 \exp(- n^{\frac{1}{5}})
\]
Now using union bound $(UB)$, we get
\[
	\bP(\exists i \in [k] : X^{(i)} = 0) \overset{(UB)}{\le} \sum_{i=1}^{k} \bP(X^{(i)} = 0) \le k \cdot O(n^{-\frac{1}{5}}) = n^{\frac{3}{5}} \cdot C_0 \exp(n^{-\frac{1}{5}}) \xrightarrow[]{n \to \infty} 0
\]
Therefore the probability that each $X^{(i)}$ has an AP triplet which is monochromatic coloured $i$ is $\bP(\forall i \in [k], X^{(i)} = 1) = 1 - \bP(\exists i \in [k]:X^{(i)} = 0) \to 1$ is increasingly approximates 1 provided $n$ is large. \qed


\newpage

\paragraph{Problem 3} Suppose we have $n$ cards and a mixing strategy as proposed in the problem.
\begin{enumerate}[(a)]
	\item Let $T_{ij}$ be the first time of state $(i,j)$ reaching the form $(x,x)$. For $i,j \in [n]$, define 
	\[
		d(i,j) = \begin{cases}
			|i - j| & i,j < n\\
			\max \set{n - i - 1, 0} & i < n, j = n\\
			\max \set{n - j - 1, 0} & i = n, j < n
		\end{cases}
	\]
	and $f(i,j) = n^2 - (d(i,j))^2 - (n - d(i,j))^2 = 2d(i,j)(n - d(i,j))$. we want to define a Markov chain for with initial state $(i,j)$. Note that $d(i,j)$ is a random variable, and we will abuse the notation $d(X_t) = d(i,j)$ where $X_t = (i,j)$. Before we continue, we should analyse $d(X_t)$ first; by considering three cases for both $i$ and $j$, we will get the following table\\
	\begin{tabularx}{\textwidth}{X X X X}
	\rowcolor{green!25} & $j < n-1$ & $j = n-1$ & $j = n$\\
	$i < n-1$ & $d(i,j) = |i-j|$ & $d(i,j) = n - 1 - i$ & $d(i,j) = n - 1 - i$\\
	\rowcolor{green!25} $i = n-1$ & $d(i,j) = n - 1 - j$ & $d(i,j) = 0$ & $d(i,j) = 0$ \\
	$i = n$ & $d(i,j) = n - 1 - j$ & $d(i,j) = 0$ & $d(i,j) = 0$
	\end{tabularx}\\
	
	Using this table and the relation $f(i,j) = 2d(i,j) (n - d(i,j))$ from above, we can get the following table of $f(i,j)$'s.\\
	
	\begin{tabularx}{\textwidth}{s b b b}
	\rowcolor{green!25} & $j < n-1$ & $j = n-1$ & $j = n$\\
	$i < n-1$ & $2|i-j|(n-|i-j|)$ & $2(i+1)(n - (i+1))$ & $2(i+1)(n - (i+1))$ \\
	\rowcolor{green!25} $i = n-1$ & $2(j+1)(n - (j+1))$ & $0$ & $0$ \\
	$i = n$ & $2(j+1)(n - (j+1))$ & $0$ & $0$
	\end{tabularx}\\
	
	Note that we can focus on the case where at least one of $i,j$ is in $\set{m-1,n}$, for otherwise they are deterministic. Without loss of generality, we will assume $i \in \set{n-1,n}$. We will now define the Markov chain $(M_t)_t$ as follows:
	\begin{itemize}
		\item The state space $S := \set{d(i,j) : i,j \in [n]} = \set{0,\ldots,n-2}$
		
		\item For $s,s' \in S$, we define
		\[
			P_{s,s'} = \begin{cases}
				\frac{1}{2} & s' = s-1 \vee s' = n - s - 1\\
				0 & \text{Otherwise}
			\end{cases}
		\]
		We also set the initial state $P_{0,0} = 1$, $P_{1,0} = P_{1,n-2} = \frac{1}{2}$
	\end{itemize}
	We can now define $f(s) = n^2 - s^2 - (n-s)^2 = 2s(n-s)$. Now, we have
	\begin{itemize}
		\item $\bE[f(X_t)|X_t = s] = f(s)$
		
		\item $\bE[f(X_{t+1}) | X_t = s] = \frac{1}{2} f(s-1) + \frac{1}{2} f(n - s - 1)$
	\end{itemize}
	So we have
	\[
		\bE[f(X_{t+1}) - f(X_t)|X_t = s] = \frac{1}{2} (f(s-1) + f(n - s - 1)) - f(s)
	\]
	Moreover, if $C \in \set{0,1}$, whenever $X_t \le C$, we have
	\begin{gather*}
		\frac{1}{2} (f(s-1) + f(n - s - 1)) = \frac{1}{2}(n^2 - (s-1)^2 - (n - s + 1)^2 + n^2 - (n - s - 1)^2 - (s + 1)^2)\\
		= n^2 -  (s^2 + 1) -  ( (n-s)^2 + 1) = n^2 - s^2 - 1 - (n^2 - 2ns + s^2 + 1)\\
		= 2ns - 2s^2 - 2 = 2s(n-s) - 2 = f(s) - 2
	\end{gather*}
	Implying that $\bE[f(X_{t+1}) - f(X_t)|X_t = s] = -2$. So whenever $s > C$, we get
	\begin{gather*}
		-2 = \bE[f(X_{t+1}) - f(X_t)|X_t = s] = \sum_{s \in S} \bP(X_{t+1} = n | X_t = x) f(n) - f(x)\\
		\implies -2 \ge \sum_{n > C} \bP(X_{t+1} = n | X_t = x) f(n) - f(x)\\
		\implies \sum_{n > C} \bP(X_{t+1} = n | X_t = x) f(n) \le f(x) - 2
	\end{gather*}
	For all $x > C$,$n \in S$. By theorem 9.11, we have $\bE[T_C] \le \frac{f(n)}{2} \le \frac{2n^2}{2} = n^2$. Finally, notice that there are $O(n)$ states of $X_t$ such that $d(i,j) = s$. Hence there are $O(n)$ pairs of $(i,j)$ such that $T_{ij}$ can be simplified to $T_C$. Thus $\bE[T_{ij}] = O(n) \bE[T_C] = O(n^3)$. \qed
	
	\item Let $b(n)$ be a function satisfying $b(n) \xrightarrow[]{n \to \infty} \infty$. By Markov's inequality and result from (a), we have
	\[
		\bP(T_{ij} > t) \le \bP(T_{ij} \ge t) \le \frac{\bE[T_{ij}]}{t} \le \frac{c n^3}{t}
	\]
	Should we choose $t = cn^3$, then $\bP(T_{ij} > cn^3) \le 1$, not very helpful; instead, if we choose $t = k \cdot cn^3$ for some $k \in \bN$, we can see that $\bP(T_{ij} > kcn^3) \le \frac{1}{k}$. Notice that ss long as the Markov chain is not in the state of the form $(x,x)$, we can do this over and over again. In particular, we have $\bP(T_{ij} \ge ecn^3) \le \bP(T_{ij} \ge ecn^3) \le \frac{1}{e}$, so if we define $M := \floor{\frac{b(n)}{ce}}$, we get
	\begin{gather*}
		\bP(T_{ij} > b(n) n^3) \le \bP(T_{ij} \ge \frac{b(n)}{ec} \cdot ecn^3)\\
		\le \bP(T_{ij} \ge M \cdot ecn^3) \le (\frac{1}{e})^M = e^{-M} = \exp(- \floor{\frac{b(n)}{ce}})
	\end{gather*}
	From here, notice that for some $C' \in \bR_{\ge 1}$, we get $M = \frac{b(n)}{ce} - \log C$, and
	\[
		\frac{b(n)}{ce} \le M + 1 \implies - \frac{b(n)}{ce} + 1 \ge - M
	\]
	This means that $\bP(T_{ij} > b(n) n^3) \le \exp(-M) \le \exp(-\alpha b(n) + 1) = C \exp(-\alpha b(n))$ where $C = \frac{1}{e}$ and $\alpha = \frac{1}{ce}$. \qed
	
	\item Consider $(Z_t)_t = (X_t,Y_t)_t$ of a Markov chain $(M_t)_t$ with state space $S$, $(X_t)_t$ is some arbitrary distribution, of the Markov chain, and $(Y_t)_t$ is in the stationary distribution. To make $(Z_t)_t$ into a coupling, for each $t \in \bN$ we will do the following:
	\begin{enumerate}[1.]
		\item Sample one of the last two cards of $X_t$ uniformly at random
		
		\item For the last two cards in $X_t$, if there is a matching pair of cards in $X_t$ and $Y_t$ (could potentially be two pairs), then if the card chosen for $X_t$ is in a pair, we move the matching pair to the top of both decks respectively; if a non-matching-pair card is chosen, we move the non-matching cards to the top of respective decks, leaving the matching pair at position $n$ at each deck.
		
		\item If there is no match for the bottom two cards, say $n-1$ is chosen from $X_t$, then we move the $(n-1)^{th}$ card of $Y_t$ to the top of the deck as well
	\end{enumerate}		
	To elaborate on the matching pair from above, say card $i_0$ is at the $n-1$ position in $X_t$ and $n$ position in $Y_t$ then they form a matching pair; If the last two cards of $X_t$ and $Y_t$ are any combination of $i_0,i_1$, then card $i_0$ (resp. $i_1$) in $X_t$ forms a matching pair with card $i_0$ (resp. $i_1$) in $Y_t$; if no such pairs exists then there are no matching pair. Then let $x,x'$ be two configurations from the distribution of $(X_t)_t$ and $y,y'$ be two configurations from the stationary distribution. We will write $x_i$ to denote the $i^{th}$ card in the configuration $x$; analogously define $x_i',y_i,y_i'$ for $x',y,y'$ respectively. Then clearly fixing $Y_t = y$ and $t \in \bN_0$, we have
	\[
		p_{xx'} = \bP(X_{t+1} = x' | Z_t = (x,y)) = \begin{cases}
			\frac{1}{2} & x' \in \set{(x_{n-1},x_1,\ldots,x_{n-1},x_n), (x_{n},x_1,\ldots,x_{n-1})}\\
			0 & \text{Otherwise}
		\end{cases}
	\]
	Analogously, fixing $x$ and $t \in \bN_0$, we have $p_{yy'} = \bP(Y_{t+1} = y'|Z_t = (x,y))$. Thus it is indeed a coupling.\\
	
	If we want to apply Theorem 10.15 in the notes, we need to prove several conditions conditions.
	\begin{itemize}
		\item The Markov chain $(M_t)_t$ is ergodic
		
		\item $\bP(X_{t_0} \ne Y_{t_0} | X_0 = x, Y_0 = y) \le \frac{1}{2}$ for some $t_0 \in \bN$; in other words we want to find $t_0 \in \bN$ such that the condition holds for all $(x,y) \in S \times S$.
		
		\item $X_t = Y_t \implies X_{t+1} = Y_{t+1}$
	\end{itemize}
	Proving the third condition is straightforward; let $x = X_t = Y_t = y$. Suppose $X_{t+1} = x'$ and $Y_{t+1} = y'$, then clearly $x_{i}' := x_{i-1} = y_{i-1} = y_i'$; so we can assume $i \in \set{n-1,n}$. By the coupling method defined above, once two cards are matched by either placing them first in the deck or last in the deck, they will continue to be in sync forever and ever until the end of time. Thus if, for instance, $n-1$ is chosen, then $x_1' = x_{n-1} = y_{n-1} = y_1'$, and clearly the last pair stays in sync. Hence $X_{t+1} = x' = y' = Y_{t+1}$.\\
	
	We now want to prove that $(M_t)_t$ is ergodic using the shuffling method defined in the problem. Let $x \in S$ be arbitrary, then $p^{(n-1)}_{xx} > 0$, since it may be the case that we keep getting $n-1$ in the process, and similarly $p^{(n)}_{xx} > 0$ if we keep choosing $n^{th}$ card in the process. Since $\gcd(n-1,n) = 1$, thus $x$ is aperiodic, and consequently $(M_t)_t$ is an aperiodic Markov chain. Now we want to prove that $(M_t)_t$ is irreducible; this is clear by the method of shuffling. Thus $(M_t)_t$ is ergodic.\\ 
	
	Now we will show the second condition holds. Consider $Z_0 = (x,y) \in S \times S$ be arbitrary. Suppose a card has initial position $i$ in $X_0$ and initial position $j$ in $Y_0$. By part (b), we know that the following holds
	\[
		\bP(T_{ij} > b(n) n^3) \le C \exp(-\alpha b(n))
	\]
	Consider $Z_0 = (x,y)$, and for card $l$, let $i_l$ be the position of card $l$ in  $x$ and $j_l$ be the position of card $l$ in $y$. Then by union bound $(UB)$, we have
	\begin{gather*}
		\bP(X_{t_0} \ne Y_{t_0} | Z_0 = (x,y)) = \bP(\exists l \in [n] : T_{i_l j_l} > t_0)\\
		\overset{(UB)}{\le} \sum_{l=1}^{n} \bP(T_{i_l j_l} > t_0) \le n \cdot \bP(\min_{l = 1,\ldots,n} T_{i_l j_l} > t_0) \overset{(*)}{=} n \bP(T_{i_1 j_1} > t_0)
	\end{gather*}
	Up to renaming, we may assume without loss of generality that $\min_{l = 1,\ldots,n} T_{i_l j_l} = T_{i_1 j_1}$ at $(*)$. Now if we choose $t_0 := t_0(n) \ge b(n) n^3$, then we have
	\[
		\bP(X_{t_0} \ne Y_{t_0} | Z_0 = (x,y)) \le n \bP(T_{i_1 j_1} > t_0) \le n \bP(T_{i_1 j_1} > b(n) n^3) \le n \cdot C e^{-\alpha b(n)}
	\]
	Setting the last term $n C e^{-\alpha b(n)} \le \frac{1}{2}$ and solve for $b(n)$, we see that $b(n) \ge \frac{1}{\alpha} (\log 2C + \log n)$; indeed this $b(n)$ satisfies $b(n) > 0$ and $b(n) \to \infty$ as $n \to \infty$ as required in problem 3(b). Now choose $t_0(n) \ge b(n) n^3$, say $t_0(n) = 2 b(n) n^3$, then we have
	\[
		\bP(X_{t_0} \ne Y_{t_0} | Z_0 = (x,y)) \le n \bP(T_{i_1 j_1} > t_0) \le n \cdot C e^{-\alpha b(n)} \le \frac{1}{2}
	\]
	thus satisfying the hypothesis of Theorem 10.15. This means that $\tau_{TV}(\epsilon) \le \log_2 \frac{1}{\ee} \cdot t_0$ for all $\ee > 0$. It remains to show that $t_0 = O(n^3 \log n)$, which concludes the proof. Indeed,
	\[
		t_0(n) = 2 b(n) n^3 = 2 \cdot \frac{1}{\alpha} (\log 2C + \log n) \cdot n^3 = \frac{2}{\alpha} n^3 \log n + \frac{2 \log 2C}{\alpha} n^3
	\]
	Thus $t_0(n) \in O(n^3 \log n)$ as we desired. This concludes the proof.\qed
\end{enumerate}

\end{document}