\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis II - Problem Set 1}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis II - Problem Set 1}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

\pagestyle{fancy}

\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\section{Survival Kit}
\paragraph*{Problem 1.1 - Dirichlet Energy}
Let $\Omega \subseteq \bR^n$ be open, connected, and regular. Let $u \in C^2(\bar{\Omega})$ with $\restr{u}{\di \Omega} \equiv 0$. Prove the following statements
\begin{enumerate}[(a)]
	\item   $\int_{\Omega} |\nabla u|^2 ~ dx \le \left( \int_{\Omega} u^2 ~ dx \right)^{\frac{1}{2}} \cdot \left( \int_{\Omega} (\Delta u)^2 ~ dx \right)^{\frac{1}{2}}$
	
	\item If $u$ satisfies $\restr{\Delta u}{\Omega} \equiv 0$, then $u \equiv 0$.
\end{enumerate}
\begin{proof}
	Let $u \in C^2(\bar{\Omega})$ be as described.
	\begin{enumerate}[(a)]
		\item Using integration by parts and Cauchy-Schwarz, we have
		\[
			\left| \int_{\Omega} |\nabla u|^2 ~ dx \right|^2 = \left| - \int_{\Omega} u \cdot \Delta u ~ dx \right|^2 \le \int_{\Omega} u^2 ~ dx \cdot \int_{\Omega} (\Delta u)^2 ~ dx
		\]
		and we are done.
		
		\item By part (a), we have
		\[
			0 \le \int_{\Omega} |\nabla u|^2 ~ dx \le \left( \int_{\Omega} u^2 ~ dx \right)^{\frac{1}{2}} \cdot \underbrace{\left( \int_{\Omega} (\Delta u)^2 ~ dx \right)^{\frac{1}{2}}}_{=0} = 0
		\]
		Since $\restr{\Delta u}{\Omega} \equiv 0$. Thus it must be that $|\nabla u|^2 = 0$, i.e. $\nabla u = 0$. From here we conclude that $u \equiv c$ for some constant $c \in \bR$. Now since $\Omega$ is regular and $\restr{u}{\di \Omega} \equiv 0$, therefore we conclude $u \equiv c = 0$ on $\Omega$. \qedhere
	\end{enumerate}
\end{proof}

\paragraph*{Problem 1.2 - $p$-Energy} Let $\Omega \subseteq \bR^n$ be open, connected, regular, $2 \le p < \infty$, and $g \in C^2(\di \Omega)$. The $p$-energy is defined to be $E_p(u) = \int_{\Omega} |\nabla u|^p ~ dx$ with domain $\frA = \set{u \in C^2(\bar{\Omega}) : \restr{u}{\di \Omega} = g}$
\begin{enumerate}[(a)]
	\item Determine whether there is at most one function $u \in \frA$ satisfy $E_p(u) = \inf_{v \in \frA} E_p(v)$.
	
	\item Derive the partial differential equation satisfied by minimisers $u$ of $E_p$ from part (a).
	
	\item Prove that for any $u \in C^2(\bar{\Omega})$ satisfying $\restr{u}{\di \Omega} \equiv 0$, the inequality
	\[
		\int_{\Omega} |\nabla u|^p ~ dx \le C_{p,n} \left( \int_{\Omega} |u|^p ~ dx \right)^{\frac{1}{2}} \left( \int_{\Omega} |D^2 u|^p ~ dx \right)^{\frac{1}{2}}
	\]
	hold, where $p$ is the exponent and $n$ is the dimension.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item \textcolor{red}{(The solution for (a) is incorrect, please ignore.)} Note that $|\cdot|$ is strictly convex, so $|\cdot|^{p}$ is also strictly convex. Let $\lambda := \inf \set{E_p(u):u \in \frA}$, and let $u_1,u_2 \in C^2(\bar{\Omega})$ be distinct such that $E_p(u_1) = \lambda = E_p(u_2)$. Consider $\frac{u_1 + u_2}{2}$, we have
		\begin{align*}
			E_p \left( \frac{u_1 + u_2}{2} \right) = & \int_{\Omega} \left| \nabla \frac{(u_1 + u_2)}{2} \right|^p ~ dx = \frac{1}{2^p} \int_{\Omega} \left| \nabla u_1 + \nabla u_2 \right|^p ~ dx\\
			< & \frac{1}{2^p} \left( \int_{\Omega} |\nabla u_1|^p ~ dx + \int_{\Omega} |\nabla u_2|^p ~ dx\right)\\
			= & 2^{-p} \left( E_p(u_1) + E_p(u_2) \right) = 2^{-p+1} \lambda < \lambda
		\end{align*}
		Since $u_1,u_2 \in C^2(\bar{\Omega})$, so $\frac{u_1 + u_2}{2} \in C^2(\bar{\Omega})$, and this contradicts $\lambda$ as a lower bound. Thus $u_1 = u_2$, and there is at most one function that satisfies $E_p(u) = \inf_{v \in \frA} E_p(v)$.
		
		\item Let $u_0$ be the minimizer of $E_p(\cdot)$, and consider $E_p(u_0 + t v)$, where $v \in \frA$. Thus
		\begin{align*}
			0 = &  \eval{\frac{d}{dt}}_{t = 0}E_p(u_0 + tv) = \eval{\frac{d}{dt}}_{t = 0} \int_{\Omega} |\nabla (u_0 + tv)|^p ~ dx\\
			= & \eval{\frac{d}{dt}}_{t = 0} \int_{\Omega} |\nabla u_0 + t \nabla v|^p ~ dx\\
			= & \eval{\frac{d}{dt}}_{t = 0} \int_{\Omega} \sum_{k=0}^{p} {p \choose k} (\nabla u_0)^k (t \nabla v)^{p-k} ~ dx\\
			= & \eval{\frac{d}{dt}}_{t = 0} \sum_{k=0}^{p} {p \choose k} t^{p-k} \int_{\Omega} (\nabla u_0)^k (\nabla v)^{p-k} ~ dx\\
			\overset{(*)}{=} & {p \choose p-1} \int_{\Omega} (\nabla u_0)^{p-1} \cdot (\nabla v) ~ dx\\
			= & {p \choose p-1} \left[ - \int_{\Omega} v \cdot \nabla((\nabla u_0)^{p-1}) ~ dx \right]
		\end{align*}
		Where we use integration by parts at (*). Therefore $\int_{\Omega} v \cdot \nabla((\nabla u_0)^{p-1}) ~ dx = 0$, and this happens if and only if
		\[
			\nabla((\nabla u_0)^{p-1}) = (p-1) [(\nabla \cdot \nabla) u_0]^{p-2} = 0 \equiv (\Delta u_0)^{p-2} = 0
		\]
		and we have the partial differential equation as desired. \qed
		
		\item ??? \qedhere
	\end{enumerate}
\end{proof}

\paragraph*{Problem 1.3 - Laplace's Equation} Let $a,b,c,d \in \bR$, $a < b$, $c < d$, $\Omega := (a,b) \times (c,d) \subseteq \bR^2$.
\begin{enumerate}[(a)]
	\item Find all solutions of the form $u(x,y) = v(x) w(y)$ to the Laplace's equation
	\[
		\Delta u = \frac{\di^2 u}{\di x^2} + \frac{\di^2 u}{\di y^2} = 0 ~ ~ ~ ~ ~ ~ \text{on $\Omega$}
	\]
	You may appeal to maximum principle.
	
	\item Prove or disprove: For every $u_0 \in C^2(\di \Omega)$, there exists a solution to the boundary value problem (BVP)
	\[
		\begin{cases}
			\Delta u = 0 & \Omega\\
			u = u_0 & \di \Omega
		\end{cases}
	\]
	of the form $u(x,y) = v(x) w(y)$.
\end{enumerate}
\begin{proof}
	Let $u(x,y) = v(x)w(y)$
	\begin{enumerate}[(a)]
		\item Note that $\frac{\di^2 u(x,y)}{\di x^2} = (\frac{\di^2 v(x)}{\di x^2}) \cdot w(y)$ and $\frac{\di^2 u(x,y)}{\di y^2} = v(x) \cdot (\frac{\di^2 w(y)}{\di y^2})$, so the Laplacian is
		\[
			0 = \Delta u = \frac{\di^2 u}{\di x^2} + \frac{\di^2 u}{\di y^2} = (\frac{\di^2 v(x)}{\di x^2}) \cdot w(y) + v(x) \cdot (\frac{\di^2 w(y)}{\di y^2})
		\]
		Dividing the equation by $v(x) w(y)$, we will get
		\[
			\frac{1}{v} \cdot \frac{\di^2 v}{\di x^2} + \frac{1}{w} \cdot \frac{\di^2 w}{\di y^2} = 0 \tag{*}
		\]
		Since $v$ is independent of $y$ and $w$ is independent of $x$, so it must be that $\frac{d^2 v}{dx^2} = Cv$ and $\frac{d^2 w}{dy^2} = -Cw$. We will now guess the solution.\\		
		
		Since $\frac{d}{dt} \exp(t) = \exp(t)$, so we can guess the solution is of the form of exponentials. If we set $v(x) = e^x$ and $w(y) = - e^y$, then
		\[
			\frac{1}{v} \cdot \frac{\di^2 v}{\di x^2} + \frac{1}{w} \cdot \frac{\di^2 w}{\di y^2} = 1 - 1 = 0 \tag{1}
		\]
		So far so good. We can do better, if we set $v(x) = \exp(kx)$ and $w(y) = -\exp(ky)$, then we will get
		\[
			\frac{1}{v} \cdot \frac{\di^2 v}{\di x^2} + \frac{1}{w} \cdot \frac{\di^2 w}{\di y^2} = k^2 - k^2 = 0 \tag{2}
		\]
		And the first equation is a special case when $k = 1$. Looks pretty good. Since $\frac{\di}{\di x}$ and $\frac{\di}{\di y}$ are linear, so multiplying a solution by a constant yields another solution to the equation. So we have $v(x) = \alpha e^{kx}$ and $w(y) = \beta e^{kx}$, where $\beta = -\alpha$. Moreover, adding two solutions of the PDE yields another solution, so the general solution to the above PDE is $v(x) = \alpha e^{kx} + \beta e^{-kx}$ and $w(y) = -(\alpha e^{kx} + \beta e^{-kx})$.\\
		
		Now note that $\frac{d^2}{dx^2} e^{ikx} = -k^2 e^{ikx}$, thus solution of the form $e^{ikx}$ solves the equation $\frac{d^2 v}{dx^2} + C v = 0$, and in fact if $v = e^{kx}$ and $w = e^{ikx}$, we can see that $u = v \cdot w$ is a solution to (*).\\
		
		Combining with the reasoning above that we can multiply a solution by a scalar and adding two solutions to yield another solution, we have the general solution of $u(x,y) = v(x) w(y)$ with $v = \alpha e^{kx} + \beta e^{-kx}$ and $w(y) = \dd e^{iky} + \gamma e^{-iky}$. So $u(x,y)$ of this form contains all the solution to the PDE (*), where $\alpha,\beta,\dd,\gamma,k \in \bR$ are constants.
		
		\item We already know that if $u_0 \equiv 0$ on $\di \Omega$, then a solution exists; so we only need to consider when $u_0 \not \equiv 0$ on the boundary. Suppose a solution always exists, let us define
		\begin{enumerate}
			\item $p(x) = \exp\left( \frac{1}{b-a} - \frac{1}{x-a} \right)$
			
			\item $q(y) = \exp\left( \frac{1}{c-d} - \frac{1}{y-c} \right)$
		\end{enumerate}
		Consider $u_0$ such that $u_0(a,y) = C = u_0(x,c)$ for some constant $C \in \bR$, and $u_0(b,y) = C + q(y)$ and $u_0(x,d) = C + p(x)$. Then by part (a), $u(x,y) = (\alpha e^{kx} + \beta e^{-kx})(\dd e^{iky} + \gamma e^{-iky})$. Since $u_0(a,y) = C = u_0(x,c)$, so we have that $v(x) = \alpha e^{kx} + \beta e^{-kx}$ and $w(y) = \dd e^{iky} + \gamma e^{-iky}$ are constants, but this contradicts that $u_0(b,y)$ and $u_0(x,d)$ are non-constants. Thus we are done. \qedhere
	\end{enumerate}
\end{proof}

\section{Project on Harmonic Functions}
We will first prove a lemma before proceeding.

\begin{lemma}
	Consider $B_{\bR^n}(0,1) \subseteq \bR^n$, $u \in C^2(\Omega)$, let $\phi(r) := \int_{\di B(0,1)} u(x + rz) ~ d \sigma(z)$, then
	\[
		\frac{d}{dr} \phi(r) = \frac{1}{r^{n-1}} \int_{B_{\bR^n}(x,r)} \Delta u(x) ~ dx
	\]
	where $\Delta u$ denotes the Laplacian of $u$ and $\di B$ denotes the boundary of the unit ball in $\bR^n$.
\end{lemma}
\begin{proof}
	Using Gauss' divergence theorem, we have
	\begin{align*}
		\int_{B_{\bR^n}(x,r)} \Delta u(x) ~ dx = & \int_{\di B(x,r)} \frac{\di u}{\di \nu}(\xi) ~ d\sigma(\xi)\\
		\overset{(*)}{=} & \int_{\di B(0,1)} \frac{\di u}{\di r}(x+rz) ~ d\sigma(x+rz)\\
		= & \int_{\di B(0,1)} \frac{\di u}{\di r}(x+rz) \cdot r^{n-1} ~ d \sigma(z)\\
		= & r^{n-1} \int_{\di B(0,1)} \frac{\di u}{\di r}(x+rz) ~ d\sigma(z)\\
		\overset{(**)}{=} & r^{n-1} \frac{d}{dr} \int u(x+rz) ~ d \sigma(z) = r^{n-1} \frac{d}{dr} \phi(r)
	\end{align*}
	where we use the change of variable $\xi = x + rz$ in (*), and we can interchange the integral and derivative at (**) because they are taken with respect to different variables.  Therefore we have the desired result.
\end{proof}

\paragraph*{Problem 1.4 - Mean-Value Property} Let $(\Omega, \cA, \mu)$ be a measure space, and $Q \in \cA$ with finite non-zero measure. If $f \in L^1(\Omega,\cA,\mu)$, then $\fint f ~ d\mu = \frac{1}{\mu(Q)} \int_Q f ~ d\mu$.
\begin{enumerate}[(a)]
	\item Let $\Omega \subseteq \bR^n$ be open, $u \in C^2(\Omega)$ satisfy the condition that for any $y \in \Omega$ and any $r > 0$, such that $B := B(y,r) \subseteq \Omega$, $u(y) = \fint_{\di B} u ~ d \sigma = \fint_{B} u ~ dx$, prove that $u$ is harmonic i.e. the Laplacian $\Delta u  = 0$.
	
	\item Verify that every harmonic function $u \in C^2(\Omega)$ has the mean value property.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item If $\Delta u \ne 0$, then (WLOG) we can find $x \in \Omega$ such that $\Delta(x) > 0$. Since $u \in C^2(\Omega)$, thus $\Delta u$ is continuous, and we can find $r_0 > 0$ such that $\restr{\Delta u}{B(x,r_0)} > 0$; let $y \in B(x,r_0)$ be arbitrary, and write $y = x + rz$ for some $0 < r < r_0$. By lemma 1, we have
		\[
			0 < \frac{1}{r^{n-1}} \underbrace{\int_{B(x,r_0)} \Delta u(x) ~ dx}_{> 0} = \frac{d}{dr} \int_{\di B(0,1)} u(x+rz) ~ d\sigma(z)
		\]
		So for any $y \in B(x,r_0)$ such that $y = x + rz$, $0 < r < r_0$, we have
		\[
			\int_{\di B(0,1)} u(x + r_0 z) ~ d \sigma(z) > \int_{\di B(0,1)} u(x + r z) ~ d \sigma(z)
		\]
		On the other hand, using mean value property, we have
		\begin{align*}
			u(x) = & \fint_{\di B(x,r_0)} u(\xi) ~ d \sigma(\xi) = \frac{1}{|\di B(x,r_0)|} \lim_{r \to r_0} \int_{\di B(x,r_0)} u(\xi) ~ d \sigma(\xi)\\
			= & \frac{1}{r^{n-1} \cdot |\di B(0,1)|} \lim_{r \to r_0}  \int_{\di B(0,1)} u(x + r z) ~ d \sigma(x + rz)\\
			= & \frac{1}{|\di B(0,1)|} \lim_{r \to r_0} \int_{\di B(0,1)} u(x + r z) ~ d \sigma(z)\\
			< &\frac{1}{|\di B(0,1)|} \int_{\di B(0,1)} u(x + r_0 z) ~ d \sigma(z) = u(x)
		\end{align*}
		which is absurd since we get $u(x) < u(x)$. Thus $\Delta u \le 0$. A similar argument shows that $\Delta u \ge 0$, and so $\Delta u \equiv 0$. \qed
		
		\item If $u$ is harmonic, then lemma 1 yields $\phi'(r) = \frac{1}{r^{n-1}} \int_{B_{\bR^n}(x,r)} \Delta u(x) ~ dx = 0$, so $\phi$ is constant. Moreover, by Lebesgue differentiation theorem, $\phi(r) = \lim_{\rho \downarrow r} \phi(\rho) = u(y)$, so $u$ satisfy the mean value property. \qedhere
	\end{enumerate}
\end{proof}

\paragraph*{Problem 1.5 - Liouville's Theorem} Suppose $u \in C^2(\bR^n)$ is harmonic.
\begin{enumerate}[(a)]
	\item If $u \in L^1(\bR^n)$, prove that $u$ is constant.
	
	\item If $u$ is bounded, prove that $u$ is constant.
\end{enumerate}
\begin{proof}
	\begin{enumerate}[(a)]
		\item Since $u \in L^1(\bR^n)$, so $\int_{\bR^n} |u(x)| ~ dx < \infty$. Because $u$ is harmonic, so mean value property for $u$ holds, and
		\begin{align*}
			|u(x)| = & \frac{1}{|B(x,r)|} \left| \int_{B(x,r)} u(z) ~ dz \right| \le \frac{1}{|B(x,r)|} \int_{B(x,r)} |u(z)| ~ dz\\
			\le & \frac{1}{|B(x,r)|} \cdot \norm{u}_{L^1(\bR^n)} = \frac{\norm{u}_{L^1(\bR^n)}}{|B(x,r)|} \xrightarrow[]{r \to \infty} 0
		\end{align*}
		Since $|u(x)| \ge 0$, so we get that $0 \le |u(x)| \le 0$, and hence we must have $|u(x)| = 0$ for all $x$, thus $u \equiv 0$ for all $x$. \qed
		
		\item Suppose $u$ is bounded, then there exists $C_0 > 0$ such that $|u(x)| < C_0$ for all $x \in \Omega$. Fix arbitrary $x,y \in \bR^n$. Since $u$ is Harmonic, so
		\[
	|u(x) - u(y)| = \frac{1}{|B_r|}\left| \int_{B_r(x)} u - \int_{B_r(y)} u \right| \le \frac{2 C_0}{|B_r|} |B_r(x) \setminus B_r(y)| \le \frac{C_0}{\underbrace{|B_r|}_{\approx r^n}} \cdot \underbrace{|\di B_r|}_{\approx r^{n-1}} \xrightarrow[]{r \to \infty} 0
\]
	Thus $u(x) = u(y)$. Finally since $x,y$ are chosen arbitrarily, $u$ is indeed constant. \qedhere
	\end{enumerate}
\end{proof}

\paragraph*{Problem 1.6 - Harnack's Inequality} Let $\Omega \subseteq \bR^n$ be open, $Q \subseteq \Omega$ be a bounded, connected subset such that $\bar{Q} \subseteq \Omega$. Prove that there exists a constant $C_Q \in \bR$ such that for any non-negative harmonic function $u \in C^2(\Omega)$, $\sup_Q u \le C_Q \inf_Q u$.
\begin{proof}
	Suppose $Q \subseteq \subseteq \Omega$ (i.e. $\bar{Q} \subseteq \Omega$), and let $r := \frac{1}{4} \dist(Q,2\Omega)$. So 
	\begin{align*}
		u(x) = & \frac{1}{|B_{2r}|} \int_{B_{2r}(x)} u(z) ~ dz \ge \frac{1}{|B_{2r}|} \int_{B_r(y)} u(z) ~ dz\\
		= & \frac{|B_r|}{|B_{2r}|} \underbrace{\fint_{B_r(y)} u(z) ~ dz}_{=u(y)} = \frac{r^n}{(2r)^n} u(y) =  \frac{1}{2^n} u(y)
	\end{align*}
	Therefore for any $x,y \in Q$ such that $|x-y| \le r$, we have $u(x) \ge \frac{1}{2^n} u(y)$. Now notice that $\bar{Q}$ is compact, so there exists $x_1,\cdots,x_m$ such that $\bigcup_{i=1}^{m} B_{\frac{r}{2}} (x_i) \supseteq \bar{Q}$ for all $x,y \in Q$; and since $Q$ is connected, it must be that $u(x) \ge \frac{1}{w^{nm}} u(y)$. Taking infimum and supremum in $x$ and $y$ respectively gives us $\inf_Q u \ge \frac{1}{2^{nm}} \sup_Q u \implies \sup_Q u \le C_0 \inf_Q u$, where $C_0 = 2^{nm}$.
\end{proof}


\end{document}