\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Functional Analysis I - Assignment 2}% Header Centre
\rhead{Student ID: 17-937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{\thepage}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-1 in} Functional Analysis I - Assignment 2}
\author{Guo Xian Yau\\ Student ID: 17-937-509}
\date{\today}

%---------------------------- New Environments ------------------------------
\theoremstyle{plain}% Default style
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}

\begin{document}

\maketitle

\begin{lemma}
	For a subset $A \subseteq X$, $A$ is nowhere dense if and only if the complement $X \setminus A$ contains a dense subset.
\end{lemma}
\begin{proof}
	Using the facts that $X \setminus \mathring{A} = \bar{X \setminus A}$ and $X \setminus \bar{A} = (X \setminus A)^{\circ}$, we have
	\begin{align*}
		\mathring{\bar{A}} = \emptyset \iff & X \setminus \mathring{\bar{A}} = X \iff \bar{X \setminus \bar{A}} = X\\
		\iff & \bar{(X \setminus A)^{\circ}} = X\\
		\iff & \text{$(X \setminus A)^{\circ}$ is dense in $X$}\\
		\iff & \text{$(X \setminus A)$ contains an open dense set.}
	\end{align*}
	To justify the last equivalence, $(X \setminus A)^{\circ}$ is dense, then $(X \setminus A)^{\circ}$ is the open dense set of $(X \setminus A)$. Conversely, if $(X \setminus A)$ contains an open dense set, say $U$, then $U \subseteq (X \setminus A)^{\circ}$ as $(X \setminus A)^{\circ}$ is the largest open set contained in $(X \setminus A)$. Thus $(X \setminus A)^{\circ}$ is also dense.
\end{proof}

\begin{coro}
	A corollary to this lemma is that a set $M \subseteq X$ is meagre if and only if its complement contains an open dense subset.
\end{coro}

\paragraph*{Problem 2.1} The set $S := \{(s_n)_{n \in \bN} \in \bR^{\bN}\}$ is the set of all real valued sequences.
\begin{enumerate}[(a)]
	\item Let $d:S \times S \to [0,1)$, $(x,y) \mapsto \sum_{n \in \bN} 2^{-n}	 \frac{|x_n - y_n|}{1+ |x_n - y_n|}$.\\
	
	It is clear that $d(x,y) \ge 0$ as a sum of non-negative numbers. Moreover
	\[
		d(x,y) = 0 \iff |x_n - y_n| = 0 \iff |x_n - y_n| = 0 ~ \forall n \in \bN \iff x_n = y_n ~ \forall n \in \bN \iff x = y
	\]
	Thus $d(x,y) = 0 \iff x = y$. We also know that $d$ is symmetric since $|x_n - y_n| = |y_n - x_n|$, thus $d(x,y) = d(y,x)$.\\
	
	Now let $x,y,z \in S$ be arbitrary. To prove triangle inequality, we need 
	\[
		d(x,z) \le d(x,y) + d(y,z) \equiv \frac{|x_n - z_n|}{1 + |x_n - z_n|} \le \frac{|x_n - y_n|}{1 + |x_n - y_n|} + \frac{|y_n - z_n|}{1 + |y_n - z_n|} ~ \forall n \in \bN
	\]
	since $d(x,y)$ is the sum of the right hand side of the equivalent statement. If we denote $a := |x_n - y_n|$, $b := |y_n - z_n|$, and $c := |x_n - z_n|$. By triangle inequality on real line, we know that $a+b \ge c$. Then the triangle inequality of $S$ is equivalent to the statement
	\begin{align*}
		\frac{a}{1+a} + \frac{b}{1+b} \ge \frac{c}{1+c} \iff & a(1+b)(1+c) + b(1+a)(1+c) \ge c(1+a)(1+b)\\
		\iff & (1+c)(a + b + 2ab) - c(1 + a + b + ab) \ge 0\\
		\iff & (a+b+2ab) + c(ab - 1) \ge 0\\
		\iff & a + b - c + 2ab + abc \ge 0
	\end{align*}
	But we know $a + b \ge c$, thus $a + b - c \ge 0$, and that $a,b,c$ are non-negative real numbers, so $2ab + abc \ge 0$, thus the last expression holds, and triangle inequality is true for $d$. Hence $d$ is a metric on $S$.\qed
	
	\item Let $(x_n)_n \in S^{\bN}$ be a Cauchy sequence; for each $\ee > 0$, we can therefore find $N \in \bN$ such that $d(x_m,x_n) < \ee$ whenever $m,n > N$.\\
	
	Fix an arbitrary $k \in \bN$, and let us define a sequence $(y_j^{(k)})_j \in S^{\bN}$, where $y_j^{(k)} = x_{j,k}$ for $j \in \bN$, i.e. the $k^{th}$ coordinate of each element in the sequence $(x_n)_n$. Then $(y_j)_j \in S$. Let $\ee > 0$, we find $N \in \bN$ such that $d(x_m,x_n) < \frac{\ee}{2^k(1+\ee)}$, and we get
	\[
		d(x_m,x_n) = \sum_{j \in \bN} 2^{-j} \frac{|x_{mj} - x_{nj}|}{1+|x_{mj} - x_{nj}|} < \frac{\ee}{2^k (1+\ee)}
	\]
	In particular, $\frac{|y_m^{(k)} - y_n^{(k)}|}{2^k (1+|y_m^{(k)} - y_n^{(k)}|)} < \frac{\ee}{2^k(1+\ee)}$, that is, $|y_m^{(k)} - y_n^{(k)}| < \ee$. Thus $(y_j^{(k)})_j$ is Cauchy sequence in $\bR^{\bN}$. Since $\bR$ is complete, hence there exists $x*_k \in \bR$ such that $x_k^* = \lim_{j \to \infty} y_j^{(k)}$. Define $x^* := (x_k^*)_{k \in \bN} \in S$. Claim that $\lim_n x_n = x^*$. Let $N, N' \in \bN$ be such that $d(x_m,x_n) < \ee'$ for all $m,n > N$, and $|x_{n,k} - x_k^*| < \ee'$ for all $k\ > N'$. Let us denote by $N_0 := \max \{N,N'\}$.\\
	
	Then for $\ee > 0$ arbitrary, we have
	\[
		d(x_n,x^*) = \sum_{k \in \bN} 2^{-k} \frac{|x_{n,k} - x_k^*|}{1+|x_{n,k} - x_k^*|} < \sum_k 2^{-k} \frac{\ee'}{1+\ee'} = \frac{\ee'}{1+\ee'} \sum_k 2^{-k} = \frac{\ee'}{1+\ee'}
	\]
	Now let $\ee' = \frac{\ee}{1-\ee}$, then  $d(x_n,x^*) < \ee$ whenever $n > N_0$, and hence $(S,d)$ is complete.\qed
	
	\item Let $S_c \subseteq S$ denote the real sequences with compact support. Let $U \subseteq S$ be an open neighbourhood. Without loss of generality, let $U = B(x_0,r)$, the ball centered at $x_0 \in S$ with radius $r > 0$; we can also assume $r \le 1$ since $d(x,y) < 1$ for all $x,y \in S$. Now choose $n_0 := \min \{n \in \bN: 2^{-n} < r\}$. Then define $x_1 \in S_c$ be such that
	\[
		x_{1n} := \begin{cases}
			x_{0n} & n \le n_0\\
			0 & n > n_0
		\end{cases}
	\]
	Then $d(x_0,x_1) = \sum_{n > n_0} 2^{-n} \frac{|x_{0n} - x_{1n}|}{1+|x_{0n} - x_{1n}|}$. Note that $\frac{|x_{0n} - x_{1n}|}{1+|x_{0n} - x_{1n}|} < 1$, hence 
	\[
		d(x_0,x_1) < \sum_{n > n_0} 2^{-n} = 2^{-n_0} < r
	\]
	and $x_1 \in U$. This means that $U \cap S_c \ne \emptyset$, and hence $S_c$ is dense in $S$.\qed
\end{enumerate}

\paragraph{Problem 2.2}
Let $K_1 \subseteq K_2 \subseteq \cdots \subseteq \bR$, $K_n \subseteq K_{n+1}$ for every $n \in \bN$, and $\bigcup_{n \in \bN} \bR^m$. Let $f,g \in C^0(\bR^m)$, define the metric to be
\[
	d(f,g) = \sum_{n \in \bN} 2^{-n} \frac{\norm{f-g}_{C^0(K_n)}}{1+\norm{f-g}_{C^0(K_n)}}
\]
For simplicity of notation, we denote by $\norm{\cdot} := \norm{\cdot}_{C^0(\bR^m)}$, $\norm{\cdot}_n := \norm{\cdot}_{C^0(K_n)}$, and $X := C^0(\bR^m)$.
\begin{enumerate}[(a)]
	\item This is essentially the same as problem 2.1 (a), as $|\cdot|$ can be seen as a norm on $\bR$. \qed
	
	\item This is also very similar to problem 2.1 (b). Let $(f_n)_n$ be Cauchy. Fix $x \in \bR^m$, and define $a_n := f_n(x)$. Then $(a_n)_{n}$ is Cauchy; for all $\ee > 0$, choose $N \in \bN$ such that $d(f_n,f_l) = \sum_j 2^{-j} \frac{\norm{f_n - f_l}}{1+\norm{f_n - f_l}} < \frac{\ee}{2^{k} (1+\ee)}$ whenever $l,n > N$. Then we get
	\[
		|a_n - a_l| < \sup_{x \in X} |f_n(x) - f_l(x)| = \norm{f_n - f_l} < \ee
	\]
	So $(a_n)_n$ is Cauchy. Since $\bR^m$ is complete, so there exists $y \in \bR^m$ such that $\lim_n f_n(x) = y$. Since $x \in \bR^m$ is arbitrary, we now define $f(x) := \lim_n f_n(x)$ for all $x \in \bR^m$. We need to prove two things: $f$ is continuous on $\bR^m$, and that $\lim_n d(f_n,f) = 0$.\\
	
	For $\ee > 0$,  choose $\dd_n > 0$ be such that $|f_n(x) - f_n(y)| < \ee$ for all $|x - y| < \dd_n$, for all pairs $x,y \in \bR^m$. Define $\dd := \inf_{n \in \bN} \{\dd_n\}$. Then we have
	\[
		|f(x) - f(y)| = \lim_n |f_n(x) - f_n(y)| < \lim_n \ee = \ee
	\]
	whenever $|x - y| < \dd$. Thus $f$ is continuous and $f \in C^0(\bR)$.\\
	
	Now we will prove that $(f_n)_n$ converges to $f$ in $(C^0(\bR^m),d)$. First, we claim that $f_n \to f$ in $\norm{\cdot}$. For $\ee > 0$, choose $N_x \in \bN$ such that $|f_n(x) - f(x)| < \ee$ whenever $n > N_x$ for each $x \in X$. Now define $N := \sup_{x \in X} N_x$, then we get $|f_n(x) - f(x)| < \ee$ whenever $n > N$ for all $x \in X$. Thus $\norm{f_n - f} = \sup_{x \in X} |f_n(x) - f(x)| < \ee$ whenever $n > N$, and hence $\norm{f_n - f} \xrightarrow{n \to \infty} 0$. Thus $(f_n)_n$ converge to $f$ in norm. The rest are exactly the same as problem 2.1(b). \qed

	\item Let $X_c := C_c^0(\bR^m)$. Let $U \subseteq X$ be open; without loss of generality, assume $U := B(f_0,r)$ for some $f_0 \in X$ and $1 \ge r > 0$. Let $\Psi_n$ be a sequence of smooth bump functions on $K_n$, and consider the product $f_n := f \cdot \Psi_n$. Then $f_n$ is a compactly supported functions on the compact set $K_n$, and $0$ outside of the compact set. Also, since $f_n$ is a product of two continuous function, $f_n$ is itself continuous.\\
	
	Now choose $n_0 := \min \{n \in \bN: 2^{-n} < r\}$. Now Consider $f_{n_0}$, we have
	\[
		d(f_0,f_{n_0}) = \sum_{n \in \bN} 2^{-n} \frac{\norm{f_0 - f_{n_0}}}{1+\norm{f_n - f_{n_0}}} = \sum_{n > n_0} 2^{-n} \underbrace{\frac{\norm{f_0 - f_{n_0}}}{1+\norm{f_n - f_{n_0}}}}_{\le 1} \le \sum_{n > n_0} 2^{-n} = 2^{-n_0} < r
	\]
	Hence $f_{n_0} \in U$, and $X_c \cap U \ne \emptyset$, and $X_c$ is dense in $X$. \qed
\end{enumerate}

\paragraph*{Problem 2.3} Let $(X,d)$ be a metric space. Show that the following are equivalent.
\begin{enumerate}
	\item Every residual set $\Omega \subseteq X$ is dense in $X$.
	
	\item The interior of every meagre set $A \subseteq X$ is empty.
	
	\item The empty set is the only subset of $X$ which is open and meagre.
	
	\item Countable intersections of open dense sets is dense.
\end{enumerate}
\begin{proof}
	{[1 $\implies$ 2]} Let $A \subseteq X$ be meagre. Therefore $A^C = X \setminus A$ is residual, and by (i), we have
	\[
		\bar{A^C} = X \iff (X \setminus (X \setminus A))^{\circ} = \emptyset \iff \mathring{A} = \emptyset
	\]
	and this implication is done.\\
	
	[2 $\implies$ 3] Let $U \subseteq X$ be open and meagre. Suppose on the contrary that $U \ne \emptyset$, then we can find $x \in U$. By definition of $U$ being open, there exists a neighbourhood $U_x$ of $x$ such that $x \in U_x \subseteq U$. Moreover, $U_x$ is meagre as a subset of meagre set. Hence $\mathring{U}_x = \emptyset$; this is a contradiction since by definition, $x \in \mathring{U}_x$. Thus the only open meagre set is $\emptyset$.\\
	
	[3 $\implies$ 4] Let $U_i \subseteq X$ be open dense set for all $i \in \bN$, and denote $U := \bigcap_{i=1}^{n} U_i$. Then
	\begin{align*}
		\bar{U} = X \iff & (X \setminus U)^{\circ} = \emptyset \iff \left( X \setminus \bigcap_{i=1}^{\infty} U_i \right)^{\circ} = \emptyset\\
		\iff & \left[\bigcup_{i=1}^{\infty} (X \setminus U_i) \right]^{\circ} = \emptyset
	\end{align*}
	Now since $U_i$'s are open dense sets, so $(X \setminus U_i)$ is nowhere dense, and as countable union of nowhere dense set, $U^C = X \setminus U = \bigcup_{i=1}^{\infty} (X \setminus U_i)$ is also nowhere dense. Hence $U$ contains an open dense set by the corollary to the lemma, and therefore $U$ is dense.\\
	
	[4 $\implies$ 1] Let $\Omega$ be a residual set, hence $X \setminus \Omega = \bigcup_{i=1}^{\infty} N_i$ where $N_i$'s are nowhere dense sets. Thus $\Omega = \bigcap_{i=1}^{\infty} (X \setminus N_i)$. By the lemma, $(X \setminus N_i)$ are open dense sets for all $i \in \bN$, hence $\Omega$ is dense as a countable intersection of open dense sets.
\end{proof}

\paragraph*{Problem 2.4} Let $1 \le p < q \le \infty$.
\begin{enumerate}[(a)]
	\item First, notice that for $0 < k < 1$, we have $\left( \sum_n |a_n| \right)^k \le \sum_n (|a_n|)^k$, which we can prove by polynomial inequality and induction. By scaling and translating, we can consider only the unit ball in $\ell^p$ and $\ell^q$.\\
	
	Let $x = (x_n)_{n \in \bN}$, since $1 \le p < q \le \infty$, so $\frac{p}{q} < 1$, and
	\[
		\norm{x}_q = \left( \sum_{i=1}^{\infty} |x_i|^q \right)^{\frac{1}{q}} \le \left( \sum_{i=1}^{\infty} (|x_i|^q)^{\frac{p}{q}}  \right)^{\frac{1}{p}} = \norm{x}_p
	\]
	Now let $x \in \ell_p$. Then $\norm{x}_q \le \norm{x}_p < \infty$, thus $x \in \ell_q$. To show that this is a strict containment; consider $x_n = (\frac{1}{n})^{\frac{1}{p}}$, using the fact that $\norm{x}_q < \infty \iff \sum_{i \in \bN} |x_i|^q < \infty$, we have $\norm{x}_q < 1$ since $\frac{q}{p} < \infty$. On the other hand, $\norm{x}_p$ diverges as $\sum_{i \in \bN} |x_i|^p = \sum_{i=1}^{\infty} \frac{1}{i}$ is the harmonic series. Hence $\ell_p \subsetneq \ell_q$. \qed
	
	\item No idea how to solve this yet, I'm sorry. \frownie
	
	\item By part (a), $\ell_p \subsetneq \ell_q$, and by part (b), $\ell_p$ is meagre in $\ell_q$. Hence $A := \bigcup_{p \in [1,q)} \ell_p$ is meagre as a countable union of meagre set, i.e. $\mathring{A} = \emptyset$. So $A \subsetneq \ell_q$; otherwise, if $A = \ell_q$, then $A$ is dense in $\ell_q$ since every set $X$ is dense in itself.
\end{enumerate}

\end{document}