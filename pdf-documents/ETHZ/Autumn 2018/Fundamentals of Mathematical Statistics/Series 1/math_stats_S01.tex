\documentclass[a4paper, 11pt]{article}

% Packages
\usepackage{hedgehog}
\pagestyle{fancy}

% Headers and Footers
\lhead{Guo Xian Yau}% Header Left
\chead{Fundamental of Mathematical Statistics}% Header Centre
\rhead{Student ID: 17 -937-509}% Header Right
\lfoot{}% Footer Left
\cfoot{}%Footer Centre
\rfoot{}%Footer Right

\title{\vspace{-2 cm} Fundamentals of Mathematical Statistics - Series 1}
\author{Guo Xian Yau}
\date{\today}

\begin{document}

\maketitle

\paragraph{Problem 1.1} Let $X_1,\cdots,X_n \overset{i.i.d.}{\sim} \bP_{\theta}$, $L_X(\theta) = \prod_{i=1}^{n} \bP_{\theta}(X_i)$.
\begin{enumerate}[(a)]
	\item Let $\theta_1$ be the maximizer of $L_X(\theta)$ and $\theta_2$ be the maximizer of $\log L_X(\theta)$. Then clearly the following inequalities hold
	\begin{itemize}
		\item $L_X(\theta_2) \le L_X (\theta_1) \iff \prod_{i=1}^{n} \frac{\bP_{\theta_2}(X_i)}{\bP_{\theta_1}(X_i)} \le 1$
		
		\item $\log(L_X(\theta_1)) \le \log(L_X(\theta_2)) \iff \sum_{i=1}^{n} [\log \bP_{\theta_2}(X_i) - \log \bP_{\theta_1}(X_i)] \ge 0$
	\end{itemize}
	But we also know $\log b - \log a = \log \frac{b}{a}$, so second bullet point yields $\sum \limits_{i=1}^{n} \log \frac{\bP_{\theta_2} (X_i)}{\bP_{\theta_1} (X_i)} \ge 0$. On the other hand, using first bullet point we get 
	\[
		\log (\prod_{i=1}^{n} \frac{\bP_{\theta_2}(X_i)}{\bP_{\theta_1}(X_i)}) \le 0 \iff \sum_{i=1}^{n} \log \frac{\bP_{\theta_2} (X_i)}{\bP_{\theta_1} (X_i)} \le 0
	\]
	Thus we conclude $\sum \limits_{i=1}^{n} \log \frac{\bP_{\theta_2} (X_i)}{\bP_{\theta_1} (X_i)} = 0$; if we can prove $\log \frac{\bP_{\theta_2}(X_i)}{\bP_{\theta_1}(X_i)} \le 0$ (or equivalently $\log \frac{\bP_{\theta_1}(X_i)}{\bP_{\theta_2}(X_i)} \ge 0$), then as a sum of non-negative integers, it must be that $\log \frac{\bP_{\theta_2}(X_i)}{\bP_{\theta_1}(X_i)} = 0$ for all $i = 1,\cdots,n$, and we conclude that indeed $\theta_1 = \theta_2$.\\
	
	It remains to show that $\log \frac{\bP_{\theta_1}(X_i)}{\bP_{\theta_2}(X_i)} \ge 0$ and that $\hat{\theta} := \theta_1$ is indeed the maximizer of $\log L_X(\theta)$. Since $\log : \bR_{\ge 0} \to \bR$ is strictly increasing and that it is a bijection between $\bR_{\ge 0}$ and $\bR$, thus $\hat{\theta}$ is indeed the unique maximizer of $\log L_X(\theta)$. To show that $\log \frac{\bP_{\theta_1}(X_i)}{\bP_{\theta_2}(X_i)} \ge 0$, notice that since $X_i$'s are i.i.d., thus for any $S \subseteq [n]$, $\prod_{i \in S} \frac{\bP_{\theta_1}(X_i)}{\bP_{\theta_2}(X_i)} \ge 1$; in particular, fix any $i \in [n]$, we have indeed $\log \frac{\bP_{\theta_1}(X_i)}{\bP_{\theta_2}(X_i)} \ge 0$. \qed
	
	\item From part (a), it suffices to find the maximizer of the log likelihood function. Since $\bP_{\theta}(X_i) = \theta e^{-\theta x_i}$ for all $x_i \ge 0$, and $0$ for all $x_i < 0$, $i = 1,\cdots,n$, thus 
	\[
		\log L_X(\theta) = \sum_{i=1}^{n} [\log(\theta) - \theta X_i \log e] \implies \frac{\di}{\di \theta} \log L_X(\theta) = \frac{n}{\theta} - \sum_{i=1}^{n} X_i
	\]
	If $\hat{\theta}$ is the maximizer, then $\frac{n}{\hat{\theta}} - \sum_{i=1}^{n} X_i = 0$, hence $\hat{\theta} = \left( \frac{1}{n} \sum_{i=1}^{n} X_i \right)^{-1}$. Note that we implicitly used $\log = \log_{e}$ above.
	
	\item We know that the first moment is 
	\begin{align*}
		\mu = & \bE(X) = \int_{\bR_{\ge 0}} x \cdot \theta e^{-\theta x} ~ dx = \eval{(-x e^{-\theta x})}_{0}^{\infty} - \int_{0}^{\infty} e^{-\theta x} ~ dx\\
		= & 0 - 0  - (-\frac{1}{\theta}) \cdot \eval{(-e^{-\theta x})}_{x = 0}^{\infty} = \frac{1}{\theta}
	\end{align*}
	On the other hand, we know that $\hat{\mu} = \frac{1}{n} \sum_{i=1}^{n} X_i$. Thus $\hat{\theta} = \left( \frac{1}{n} \sum_{i=1}^{n} X_i\right)^{-1}$
\end{enumerate}

\paragraph{Problem 1.2} Let $X_1,\cdots,X_n \overset{i.i.d.}{\sim} Unif([0,\theta])$. Let us denote by $I = [0,\theta]$. Then
\begin{enumerate}[(a)]
	\item First note that $L_X(\theta) = \prod_{i=1}^{n} \frac{1}{\theta} \i1_{x_i \in I} = \frac{1}{\theta^n} \i1_{x_1 \in I} \cdots \i1_{x_n \in I}$, where $x_i$ is the realisation of the random variable $X_i$. Let us denote $x_{(i)}$ be the $i^{th}$ order statistics. Then
	\[
		L_X(\theta) = \frac{1}{\theta^n} \i1_{0 \le x_{(1)}} \i1_{x_{(n)} \le \theta}
	\]
	If $x_{(n)} \le \theta$, then $\hat{\theta} = x_{(n)}$; otherwise $L_X(\theta) = 0$. Thus MLE is $\hat{\theta} = \max \set{x_1,\cdots,x_n}$.
	
	\item The first moment about 0 is $\bE(X) = \frac{\theta}{2}$. The first empirical moment is $\frac{1}{n} \sum_{i=1}^{n} X_i$. Setting the first moment to equal to the empirical moment, we have
	\[
		\frac{\theta}{2} = \frac{n} \sum_{i=1}^{n} X_i \implies \theta = \frac{2}{n} \sum_{i=1}^{n} X_i
	\]
	Thus $\hat{\theta} = \frac{2}{n} \sum_{i=1}^{n} X_i$
\end{enumerate}

\end{document}