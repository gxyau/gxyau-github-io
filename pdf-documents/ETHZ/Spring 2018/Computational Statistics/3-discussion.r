# See book: C. R. Shalizi. Advanced Data Analysis from an Elementary Point of View, Appendix N
########################################
# General facts and hints about functions
########################################
rm(list=ls())

# default values and order of arguments
##############################
f <- function(a,b=2,c){ return(a+b+c)}
f(1,3) 	 #arguments have the same order-> error: c not defined
f(c=3,1) #This is possible
args(f)  #useful to list the arguments of a function


# no forward declaration necessary
##############################
# This means we can use another function in a function definition which is not yet known nor defined
g<-function(a){ return(h(a))}
# However, we cannot evaluate g until we have defined h
g(2)
# We can define it later and do not need to redefine g after h
h<-function(a){ return(a*a)}
g(2)

# return from function
##############################
# for short functions, we do not need the return() statement:
h1<-function(a){a*a}
h1(3)

# if there is no return statement, the last expression is used as the return statement.
h2<-function(a){
	x<-a*a
	y<-x+1
	a
}
h2(4)
# Do not use this feature (bad style, error prone)

# after return statement, the function is left
h3<-function(a){
	x<-a*a
	print(x)
	return(0);
}
h3(3)

h4<-function(a){
	x<-a*a
	return(0);
	print(x) #this is not evaluated
}
h4(3)


# more function return values: lists
##############################

# if you want to return multiple values from a function, you can use lists or vectors
f1<-function(x){return(c(x,x+1,x+2,x^2));}
f1(5)

f2<-function(x){
	l<-list(m=mean(x), s=sd(x))
	return(l)
}
v1<-f1(5)
v2<-f2(v1)

# extract the individual values
v2[["m"]] 
v2[["s"]]


# function as argument
##############################
rm(list=ls())

f<-function(h,a){return(h(a))}
f(3,2) #error 3(2) not defined
# recursion not allowed
f(f,2)

# we need to define another function...
g<-function(x){x^2}
# and can pass it as an argument to f!
f(g,2)



# can use external variables
##############################
f<-function(x){return(x+constant)}
f(3) # we need to define all variables used.

constant<-10
f(3)

# This is useful because we can change the behaviour of a function
# without changing its definition!
constant<- -10
f(3)


# inner variable assignments do not affect outer variables
##############################
tmp<- 0
f<-function(a){
	tmp<-a*a
	return(sin(tmp))
}

f(3)
# after running the function, the outer variable tmp is still unchanged!
tmp

# ... ellipse
##############################
# you can use ... in a function for "other parameters"
f<-function(horizontal,vertical,...){
	plot(horizontal,vertical,...)
}

a<-1:10
b<-sin(a)
f(a,b)
f(a,b,pch=17) 
f(a,b,main="sin")
# => The additional parameters are used for plot!


########################################
# Evaluation on vectors
########################################

# functions which can take vectors
##############################

# write functions such that they can be evaluated on vectors or even matrices

f1<-function(a){
	if (a==0){
		return(5)
	}else{
		return(1)
	}
}

f2<-function(a){
	return((a==0)*4+1)
}

# OK 
f1(4)
f2(4)

# but for vectors not:
a=c(1,0)
str(a)
f1(a) # error, not written for vectors
f2(a) # OK

##############################
# apply 
##############################

M<-matrix(c(0,1,2,3,4,5,6,7,8),nrow=3) # definition by column
M<-matrix(c(0,1,2,3,4,5,6,7,8),nrow=3,byrow=T) # definition by row
# Note: T =TRUE, F=FALSE

apply(M,2,mean) # column-wise mean
apply(M,1,mean) # row-wise mean
apply(M,c(1,2),mean) # original matrix
# The second parameter specifies the marginals the result is based on

# we can use the previous functions
f2(M)

f1(M) # error
apply(M,c(1,2),f1) # even though f2 it is not written for vector or matrix inputs
# DO NOT USE FOR LOOPS FOR THIS!!!

# For vectors use sapply
v<-rbinom(n=10,size=1,p=0.5)
v
sapply(v,f1)



# for lists, use lapply
l<-list(a=c(1,2), b=c(1,0,3), d=c(1,2,3,4))
lapply(l,mean)
lapply(l,f1) #error
lapply(l,f2)



########################################
# How to deal with bugs?
########################################

buggy_function<-function(x){
	return(rnorm(x))
}


f<-function(x){
	return(buggy_function(x))
}

g<-function(x){
	if (x>0) {
		return(x^2);
	}
	else{
		tmp<-f(x)
		return(tmp)
	}
}

g(1) #OK 
g(-1) #error



##############################
# stopifnot

# place stopifnot where you need a condition to be satisfied
# This helps you later to find an error
buggy_function<-function(x){
	stopifnot(x>0) 
	return(rnorm(x))
}
g(-1) #error


##############################
# traceback()

# This is a very useful function, it shows you the nested function calls that caused the problem
traceback()
# The syntax is: 
# n: name at #l
#
# n: "nesting index" , 1=outer function
# name: function name
# l: line number where this function was called in the outer function


##############################
# use print statements

# it often helps to print out some values in functions to understand the bug better
buggy_function<-function(x){
	print("x")
	print(x)
	stopifnot(x>0) 
	return(rnorm(x))
}
g(-1)


##############################
# == vs =

# be careful "=" is assignment whereas "==" is comparison
a<-1
if( (a==2) ){print("a==2")}
if( (a=2) ){print("a=2")}



########################################
# Other guidelines
########################################
# *) Comment your code
# *) Do not repeat code, use functions instead
# *) Break big problems into small subproblems "Nothing is particularly hard if you divide it into small jobs" --Henry Ford


